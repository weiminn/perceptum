﻿using IntegratedInfocommProject.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IntegratedInfocommProject
{
    public partial class Products : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (var db = new ApplicationDbContext())
            {
                var products = db.Products.Where(x=>x.Name == "ss").ToList();

                //DataTable dt = new DataTable();
                //dt.Columns.Add("ID");
                //dt.Columns.Add("Description");
                //foreach(var product in products)
                //{
                //    DataRow dr = dt.NewRow();
                //    dr[0] = product.ProductId;
                //    dr[1] = product.Details;

                //    dt.Rows.Add(dr);
                //}

                ProductsGrid.DataSource = products;// dt;
                ProductsGrid.DataBind();
            }
        }

        protected void ProductsGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = ProductsGrid.Rows[index];
            var productSelected = row.Cells[1].Text;
            this.Session["ProductID"] = productSelected;
            Response.Redirect("ProductDetails.aspx");
        }
    }
}