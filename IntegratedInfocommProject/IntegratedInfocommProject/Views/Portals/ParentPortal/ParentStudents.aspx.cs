﻿using IntegratedInfocommProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IntegratedInfocommProject.Views.Portals.ParentPortal
{
    public partial class ParentStudents : System.Web.UI.Page
    {
        private int ParentID;
        protected void Page_Load(object sender, EventArgs e)
        {
            ParentID = 1;// Convert.ToInt32(Session["ParentID"]);
            using (var db = new ApplicationDbContext())
            {
                var retrievedStudents = db.StudentParents.Where(x => x.ParentId == ParentID).ToList();

                List<object> studentDisplay = new List<object>();
                foreach (var student in retrievedStudents)
                {
                    studentDisplay.Add(db.Students
                        .Select(x => new {
                            x.StudentId,
                            x.UserName,
                            x.Level,
                            x.Telephone
                        })
                        .Single(x => x.StudentId == student.StudentId));
                }

                studentsGrid.DataSource = studentDisplay;
                studentsGrid.DataBind();
            }
        }

        protected void studentsGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = studentsGrid.Rows[index];
            var studentselected = row.Cells[1].Text;
            this.Session["StudentID"] = studentselected;
            Response.Redirect("Classes.aspx");
        }
    }
}