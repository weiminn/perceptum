﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Portals/ParentPortal/Parent.Master" AutoEventWireup="true" CodeBehind="ParentStudents.aspx.cs" Inherits="IntegratedInfocommProject.Views.Portals.ParentPortal.ParentStudents" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Students</h1>

    <asp:GridView ID="studentsGrid" runat="server" OnRowCommand="studentsGrid_RowCommand"
        CssClass="table table-hover table-striped">
        <Columns>
            <asp:ButtonField Text="View" />
        </Columns>
    </asp:GridView>
</asp:Content>
