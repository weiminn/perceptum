﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using IntegratedInfocommProject.Models;


namespace IntegratedInfocommProject.Views.Portals.Teacher
{
    public partial class LeaveApplication : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int EmployeeId = 2;
            Session["EmployeeId"] = EmployeeId;
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            using (var db = new ApplicationDbContext())
            {
                Leave Leave = new Leave
                {
                    EmployeeId = Convert.ToInt32(Session["EmployeeId"]),
                    Reason = Type.Text,
                    Details = Details.Text,
                    DayRequested = DaysRequested.Text,
                    DateApproved = DateTime.Now
                };

                db.LeaveApplication.Add(Leave);
                db.SaveChanges();

                Response.Redirect("../Admin/LeaveApproval.aspx");

            }

        }
    }
}