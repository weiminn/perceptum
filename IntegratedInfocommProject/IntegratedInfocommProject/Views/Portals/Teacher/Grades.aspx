﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Portals/Teacher/Teacher.Master" AutoEventWireup="true" CodeBehind="Grades.aspx.cs" Inherits="IntegratedInfocommProject.Views.Portals.Parent.Grades" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Give a Grade</h1>


    <asp:Label ID="student" runat="server" Text=""></asp:Label>






    <table class="table table-hover">
        <tbody>
            <tr>
                <td>Grade</td>
                <td>
                    <asp:DropDownList runat="server">
                        <asp:ListItem>A</asp:ListItem>
                        <asp:ListItem>B</asp:ListItem>
                        <asp:ListItem>C</asp:ListItem>
                        <asp:ListItem>D</asp:ListItem>
                        <asp:ListItem>E</asp:ListItem>
                        <asp:ListItem>F</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>Month</td>
                <td>
                    <asp:DropDownList runat="server">
                        <asp:ListItem>Jan 2018</asp:ListItem>
                        <asp:ListItem>Feb 2018</asp:ListItem>
                        <asp:ListItem>Mar 2018</asp:ListItem>
                        <asp:ListItem>Apr 2018</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </tbody>
    </table>

    <br />
    <a href="" class="btn btn-info">Give grade</a>


</asp:Content>
