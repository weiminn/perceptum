﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Portals/Teacher/Teacher.Master" AutoEventWireup="true" CodeBehind="Students.aspx.cs" Inherits="IntegratedInfocommProject.Views.Portals.Teacher.Students" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


    <asp:GridView ID="GridView1" runat="server">

        




    </asp:GridView>

    <h1>Students</h1>
    <table class="table table-hover">
        <thead>
            <tr>
            <th>Name</th>
            <th>School</th>
            <th>Level</th>
            <th>Telephone</th>

            </tr>
        </thead>
        <tbody>
            <tr>
            <td><a href="Grades.aspx">John</a></td>
            <td>Victoria School</td>
            <td>Secondary 3</td>
            <td>83729202</td>

            </tr>
            <tr>
            <td><a href="Grades.aspx">Mary</a></td>
            <td>Duman High</td>
            <td>Secondary 3</td>
            <td>93727283</td>

            </tr>
            <tr>
            <td><a href="Grades.aspx">July</a></td>
            <td>Clementi Secondary</td>
            <td>Secondary 3</td>
            <td>84736282</td>

            </tr>
        </tbody>
    </table>
</asp:Content>
