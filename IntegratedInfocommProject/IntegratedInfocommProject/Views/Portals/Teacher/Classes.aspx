﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Portals/Teacher/Teacher.Master" AutoEventWireup="true" CodeBehind="Classes.aspx.cs" Inherits="Perceptum_IIP.View.Portals.Teacher.Classes" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <h1>Classes</h1>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Subject</th>
                <th>Day</th>
                <th>Venue</th>
                <th>Time</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><a href="Students.aspx">Mathematics</a></td>
                <td>Monday</td>
                <td>Bukit Timah</td>
                <td>6 ~ 8 pm</td>
            </tr>
            <tr>
                <td><a href="Students.aspx">Additional Mathematics</a></td>
                <td>Tuesday</td>
                <td>Bukit Timah</td>
                <td>3 ~ 5pm</td>
            </tr>
               <tr>
                <td><a href="Students.aspx">Additional Mathematics</a></td>
                <td>Wednesday</td>
                <td>Bukit Timah</td>
                <td>5 ~ 6 pm</td>
            </tr>
        </tbody>
    </table>
</asp:Content>
