﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Portals/Teacher/Teacher.Master" AutoEventWireup="true" CodeBehind="LeaveApplication.aspx.cs" Inherits="IntegratedInfocommProject.Views.Portals.Teacher.LeaveApplication" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Type" CssClass="col-md-2 control-label">Type of Leave</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Type" CssClass="form-control" />
            </div>
    </div>

     <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Details" CssClass="col-md-2 control-label">Details</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Details" CssClass="form-control" />
            </div>
    </div>

    <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="DaysRequested" CssClass="col-md-2 control-label">Days Requested</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="DaysRequested" CssClass="form-control" />
            </div>
    </div>
    <asp:Button ID="Submit" runat="server" Text="Button" OnClick="Submit_Click" />
 
</asp:Content>
