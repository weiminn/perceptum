﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Portals/Teacher/Teacher.Master" AutoEventWireup="true" CodeBehind="Videos.aspx.cs" Inherits="IntegratedInfocommProject.Views.Portals.Teacher.Videos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <h2> Revision </h2>
    </div>

    <div class="row">
        <div class="col-md-4 col-md-offset-3"> 
            <iframe width="560" height="315" src="https://www.youtube.com/embed/PfgMqMLJ_C8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>
    </div>
    
    <div class="row">
        <h2>Concepts</h2>
    </div>
      
    <div class="row">

        <div class="col-md-4"> 
            <iframe width="350" height="250" src="https://www.youtube.com/embed/4fd0OPz8StU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>
        <div class="col-md-4"> 
            <iframe width="350" height="250" src="https://www.youtube.com/embed/cfvF9SBFp6o" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>
        <div class="col-md-4"> 
            <iframe width="350" height="250" src="https://www.youtube.com/embed/98lNQmSbfLc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>
    </div>
</asp:Content>
