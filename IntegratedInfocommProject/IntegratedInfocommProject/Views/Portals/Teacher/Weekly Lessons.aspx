﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Portals/Teacher/Teacher.Master" AutoEventWireup="true" CodeBehind="Weekly Lessons.aspx.cs" Inherits="IntegratedInfocommProject.Views.Portals.Teacher.Weekly_Lessons" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <div class="row">
      <h2>Area Under A Curve</h2>

                <div class="col-md-4">
                    <img style="width:400px; height:400px;" src="../../../Content/images/Upper%20Secondary%20Maths%20Workshet.JPG" />
                </div>
                <div class="col-md-4">
                    <img style="width:400px; height:400px;"  src="../../../Content/images/Upper%20Secondary%20Maths%20Worksheet%202.JPG" />
                </div>
                <div class="col-md-4">
                    <img style="width:400px; height:400px;"  src="../../../Content/images/Upper%20Secondary%20Maths%20Worksheet%203.JPG" />
                </div>
            </div>
            <div class="row">
            <div class="row">
                <h2>Introduction to Definite Integral</h2>
            </div>
                <div class="col-md-4">
                    <img style="width:400px; height:400px;" src="../../../Content/images/Upper%20Secondary%20Maths%20Worksheet%204.JPG" />
                </div>
                <div class="col-md-4">
                    <img style="width:400px; height:400px;" src="../../../Content/images/Upper%20Secondary%20Maths%205.JPG" />
                </div>
                <div class="col-md-4">
                    <img style="width:400px; height:400px;" src="../../../Content/images/upper%20secondary%20maths%20worksheet%206.JPG" />
                </div>
            </div>
</asp:Content>
