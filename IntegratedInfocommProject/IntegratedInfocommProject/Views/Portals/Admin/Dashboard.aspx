﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Portals/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="IntegratedInfocommProject.Views.Portals.Admin.Dashboard" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row" style="padding: 20px">
        <h2>Dashboard</h2>
    </div>
    <div class="row" style="padding: 20px">
        <div class="col-md-7">
            <div class="row">
                <img width="800" height="400" src="../../../Content/images/sales.jpg" />
            </div>
            <div class="row">
                <img width="800" src="../../../Content/images/Users.jpg" />
            </div>
        </div>

        <div class="col-md-5">
            <div class="row">
                <img width="400" height="100" src="../../../Content/images/c1.jpg" />
            </div>
            <div class="row">
                <img width="400" height="100" src="../../../Content/images/c2.jpg" />
            </div>
            <div class="row">
                <img width="400" height="100" src="../../../Content/images/c3.jpg" />
            </div>
            <div class="row">
                <img width="500" height="600" src="../../../Content/images/todo.jpg" />
            </div>
        </div>
    </div>
    
</asp:Content>
