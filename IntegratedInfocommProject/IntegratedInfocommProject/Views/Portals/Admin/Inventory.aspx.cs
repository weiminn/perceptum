﻿using IntegratedInfocommProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IntegratedInfocommProject.Views.Portals.Admin
{
    public partial class Inventory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (var db = new ApplicationDbContext())
            {
                var retrievedData = db.Products.ToList();
                inventoryGrid.DataSource = retrievedData;
                inventoryGrid.DataBind();
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Edit Inventory.aspx");
        }
    }
}