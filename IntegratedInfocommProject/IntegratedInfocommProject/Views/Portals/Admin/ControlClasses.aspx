﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Portals/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="ControlClasses.aspx.cs" Inherits="IntegratedInfocommProject.Views.Portals.Admin.Classes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">




</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
     <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="ClassDescription" CssClass="col-md-2 control-label">Class Description</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="ClassDescription" CssClass="form-control" />
            </div>
              </div>

     <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Day" CssClass="col-md-2 control-label">Day</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Day" CssClass="form-control" />
            </div>
              </div>


      <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Time" CssClass="col-md-2 control-label">Time</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Time" CssClass="form-control" />
            </div>
              </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Level" CssClass="col-md-2 control-label">Level</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Level" CssClass="form-control"/>
            </div>
        </div>

            <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="SubLevel" CssClass="col-md-2 control-label">Sub Level</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="SubLevel" CssClass="form-control"/>
            </div>
        </div>

            

    <asp:Button ID="submit" runat="server" Text="Submit" OnClick="submit_Click" />







</asp:Content>
