﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Portals/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AddInventory.aspx.cs" Inherits="IntegratedInfocommProject.Views.Portals.Admin.Edit_Inventory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

   

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Name" CssClass="col-md-2 control-label">Name</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Name" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Name"
                    CssClass="text-danger" ErrorMessage="The name field is required." />
            </div>
              </div>
   
     <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Details" CssClass="col-md-2 control-label">Details</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Details" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Details"
                    CssClass="text-danger" ErrorMessage="The details field is required." />
            </div>
              </div>

    <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="ProductCategory" CssClass="col-md-2 control-label">Product Category</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="ProductCategory" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="ProductCategory"
                    CssClass="text-danger" ErrorMessage="The product category field is required." />
            </div>
              </div>

     <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="SalesItem" CssClass="col-md-2 control-label">Sales Item </asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="SalesItem" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="SalesItem"
                    CssClass="text-danger" ErrorMessage="The salesitem field is required." />
            </div>
              </div>
    
             <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="NormalPrice" CssClass="col-md-2 control-label">Normal Price </asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="NormalPrice" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="NormalPrice"
                    CssClass="text-danger" ErrorMessage="The normal price field is required." />
            </div>
              </div> 
    
    <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="PromoPrice" CssClass="col-md-2 control-label">Promotion Price </asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="PromoPrice" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="PromoPrice"
                    CssClass="text-danger" ErrorMessage="The promo price field is required." />
            </div>
              </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="QuantityonHand" CssClass="col-md-2 control-label">Quantity On Hand</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="QuantityOnHand" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="QuantityonHand"
                    CssClass="text-danger" ErrorMessage="The quantity field is required." />
            </div>
              </div>


    <asp:FileUpload ID="FileUploadControl" runat="server"/>
    <asp:Button runat="server" id="UploadButton" text="Upload" onclick="UploadButton_Click" />

    <br />
    <br />

    <asp:Button ID="Sumbit" runat="server" Text="Sumbit" OnClick="Sumbit_Click"/>


    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                Are You Sure?
            </div>
            <div class="modal-footer">
                <button class="btn btn-info">Confirm</button>
                <button class="btn btn-danger">Cancel</button>
            </div>
        </div>
      </div>
    </div>




</asp:Content>
