﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Portals/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="StudentsRegister.aspx.cs" Inherits="IntegratedInfocommProject.Views.Portals.Admin.StudentsRegister" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


    <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="UserName" CssClass="col-md-2 control-label">UserName</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="UserName" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="UserName"
                    CssClass="text-danger" ErrorMessage="The name field is required." />
            </div>
              </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="FirstName" CssClass="col-md-2 control-label">First Name</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="FirstName" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="FirstName"
                    CssClass="text-danger" ErrorMessage="The first name field is required." />
            </div>

              </div>    

    <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="LastName" CssClass="col-md-2 control-label">Last Name</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="LastName" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="LastName"
                    CssClass="text-danger" ErrorMessage="The last name field is required." />
            </div>
              </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-2 control-label">Password</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Password" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Password"
                    CssClass="text-danger" ErrorMessage="The password field is required." />
            </div>
        </div>

            <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Address" CssClass="col-md-2 control-label">Address</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Address" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Address"
                    CssClass="text-danger" ErrorMessage="The address field is required." />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="DOB" CssClass="col-md-2 control-label">Date Of Birth</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="DOB" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="DOB"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The date of birth field is required." />
                </div>
            </div>

 <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Level" CssClass="col-md-2 control-label">Level</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Level" CssClass="form-control" />
                </div>
     </div>

     <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="SubLevel" CssClass="col-md-2 control-label">Sub Level</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="SubLevel" CssClass="form-control" />
                </div>
     </div>


            <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Telephone" CssClass="col-md-2 control-label">Telephone</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Telephone"  CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Telephone"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The telephone field is required." />
                </div>
     </div>     

    <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Email</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Email"  CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The email field is required." />
                </div>
     </div>     

    

     <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtClassID" CssClass="col-md-2 control-label">Class ID</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txtClassID" />
                </div>
     </div>     

    <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Outlet" CssClass="col-md-2 control-label">Outlet</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Outlet"  CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Outlet"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The outlet field is required." />
            </div>
     </div>     

    <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="ParentID" CssClass="col-md-2 control-label">ParentID</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="ParentID"  CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="ParentID"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The ParentID field is required." />
            </div>
     </div>       





        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button ID="CreateUser" runat="server" OnClick="CreateUser_Click1" Text="Register" />
            </div>
        </div>
    



</asp:Content>
