﻿using IntegratedInfocommProject.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IntegratedInfocommProject.Views.Portals.Admin
{
    public partial class EmployeeRegister : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void CreateUser_Click(object sender, EventArgs e)
        {
            var userStore = new UserStore<IdentityUser>();
            var manager = new UserManager<IdentityUser>(userStore);

            // Need to create Identity user object
            var user = new IdentityUser()
            {
                UserName = UserName.Text,
                Email = Email.Text
            };
            IdentityResult result = manager.Create(user, Password.Text);

            if (result.Succeeded)
            {
                using (var db = new ApplicationDbContext())
                {
                    Employee employee = new Employee
                    {
                        UserName = UserName.Text,
                        FirstName = FirstName.Text,
                        LastName = LastName.Text,
                        Password = Password.Text,
                        Email = Email.Text,
                        Position = Position.Text,
                        Address = Address.Text,
                        DOB = Convert.ToDateTime(DOB.Text),
                        Department = Department.Text,
                        Salary = Convert.ToInt32(Salary.Text),
                        Telephone = Convert.ToInt32(Telephone.Text),
                        ApplicationUserId = user.Id
                    };

                    db.Employees.Add(employee);
                    db.SaveChanges();

                    EmployeeClass employeeClass = new EmployeeClass
                    {

                        EmployeeId = employee.EmployeeId,
                        ClassId = Convert.ToInt32(txtClassID.Text)
                    };

                    db.EmployeeClasses.Add(employeeClass);
                    db.SaveChanges();
                }
            }
        }
    }
}