﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Portals/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="MakeUp.aspx.cs" Inherits="Perceptum_IIP.View.Portals.Admin.MakeUp" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <h1>Make Up Classes</h1>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Student</th>
                    <th>Date</th>
                    <th>Venue</th>
                    <th>Time</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>John</td>
                    <td>20 May 2018</td>
                    <td>Bukit Timah</td>
                    <td>6.15 ~ 7 pm</td>
                    <td>
                        <div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            Action
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">Confirm</a>
                                <a class="dropdown-item" href="#">Reject</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Katy</td>
                    <td>22 May 2018</td>
                    <td>261 Bishan St.22</td>
                    <td>3.30 ~ 4 pm</td>
                    <td>
                        <div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            Action
                            </button>
                            <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">Confirm</a>
                            <a class="dropdown-item" href="#">Reject</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Taylor</td>
                    <td>27 May 2018</td>
                    <td>Bukit Timah</td>
                    <td>7.30 ~ 9 pm</td>
                    <td>
                        <div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            Action
                            </button>
                            <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">Confirm</a>
                            <a class="dropdown-item" href="#">Reject</a>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</asp:Content>
