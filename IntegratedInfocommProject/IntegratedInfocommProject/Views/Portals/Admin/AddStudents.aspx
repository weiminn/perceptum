﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Portals/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AddStudents.aspx.cs" Inherits="Perceptum_IIP.View.Portals.Admin.Students" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <table id="StudentsTable" class="auto-style3" style="border-style: none; padding: 2px; margin: 1px; height: 242px; width: 2214px;">

        <tr>
            
            
      <td class="Students" style="margin: 1px; border-style: none; height: 50px; padding: 5px; text-align: center">
          <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="StudentId" DataSourceID="SqlDataSource1" Height="225px" Width="747px">
              <Columns>
                  <asp:BoundField DataField="StudentId" HeaderText="StudentId" InsertVisible="False" ReadOnly="True" SortExpression="StudentId" />
                  <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                  <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
                  <asp:BoundField DataField="BirthDate" HeaderText="BirthDate" SortExpression="BirthDate" />
                  <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level" />
                  <asp:BoundField DataField="Telephone" HeaderText="Telephone" SortExpression="Telephone" />
                  <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                  <asp:BoundField DataField="Outlet" HeaderText="Outlet" SortExpression="Outlet" />
                  <asp:BoundField DataField="ApplicationUserId" HeaderText="ApplicationUserId" SortExpression="ApplicationUserId" />
                  <asp:BoundField DataField="SubLevel" HeaderText="SubLevel" SortExpression="SubLevel" />
                  <asp:BoundField DataField="Password" HeaderText="Password" SortExpression="Password" />
                  <asp:BoundField DataField="School" HeaderText="School" SortExpression="School" />
              </Columns>
          </asp:GridView>
          <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Students]"></asp:SqlDataSource>
        </tr>







    </table>












   <!----- <div class="container">
        <h1>Student Particulars</h1>
        <table class="table table-hover">
            <thead>
                <tr>
                <th>Full Name</th>
                <th>Student Level</th>
                <th>Student ID</th>
                <th>Outlet</th>
                <th>Email</th>      
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">John Lee</a></td>
                    <td><a>Primary 3</a></td>
                    <td><a>S001</a></td>
                    <td><a>Bukit Timah</a></td>
                    <td><a>John@gmail.com</a></td>
                </tr>

                <tr>
                    <td colspan="5">
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row">
                                    <p> Age: 9</p>
                                      </div>
                                <div class="row">


                                    <p> DOB: 15 August</p>
                                                                    </div>
                                  
                            </div>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td><a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Mary Tan</a></td>
                    <td><a>Primary 5</a></td>
                    <td><a>S002</a></td>
                    <td><a>Bukit Timah</a></td>
                    <td><a>Mary@gmail.com</a></td>
                </tr>

                <tr>
                    <td colspan="5">
                        <div id="collapseTwo" class="panel-collapse collapse in">
                            <div class="panel-body">

                                    <div class="row">
                                    <p> Age: 11</p>
                                      </div>
                                <div class="row">


                                    <p> DOB: 14 August</p>
                                                                    </div>
                            </div>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td><a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Sarah Lim</a></td>
                    <td><a>Secondary 4</a></td>
                    <td><a>S003</a></td>
                    <td><a>Bukit Timah</a></td>
                    <td><a>sarah@gmail.com</a></td>
                </tr>

                <tr>
                    <td colspan="5">
                        <div id="collapseThree" class="panel-collapse collapse in">
                            <div class="panel-body">

                                  <div class="row">
                                    <p> Age: 14</p>
                                      </div>
                                <div class="row">


                                    <p> DOB: 20 August</p>
                                                                    </div>


                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div> --->

    </asp:Content>

