﻿using IntegratedInfocommProject.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IntegratedInfocommProject.Views.Portals.Admin
{
    public partial class LeaveApproval : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (var db = new ApplicationDbContext())
            {
                var leaves = db.LeaveApplication.ToList();
                DataTable dt = new DataTable();
                dt.Columns.Add("ID");
                dt.Columns.Add("Employee");
                dt.Columns.Add("Status");

                foreach (var leave in leaves)
                {
                    var theEmployee = db.Employees.Single(x => x.EmployeeId == leave.EmployeeId);
                    DataRow dr = dt.NewRow();
                    dr[0] = leave.LeaveId;
                    dr[1] = theEmployee.EmployeeId;
                    dr[2] = leave.Approval;
                    dt.Rows.Add(dr);
                }

                GridView1.DataSource = dt;
                GridView1.DataBind();
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView1.Rows[index];
            var lId = Convert.ToInt32(row.Cells[2].Text);

            using (var db = new ApplicationDbContext())
            {
                var theLeave = db.LeaveApplication.Single(x => x.LeaveId == lId);
                
                if(e.CommandName == "Approve")
                {
                    theLeave.Approval = "Approved";
                }
                else
                {
                    theLeave.Approval = "Rejected";
                }
                db.SaveChanges();
                Response.Redirect(Request.RawUrl);
            }
        }
    }
}