﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Portals/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="SalesOrders.aspx.cs" Inherits="IntegratedInfocommProject.Views.Portals.Admin.SalesOrders" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


    <div class="container">
        <h1>Sales Orders</h1>
        <table class="table table-hover">
            <thead>
                <tr>
                <th>Product</th>

                <th>OrderID</th>
                <th>Quantity</th>
                <th>Product Category</th>
                <th>Related Id</th>      
                </tr>
            </thead>
            <tbody>
                <tr>

                    <td><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Revision Package</a></td>
                    <td><a>O001</a></td>
                    <td><a>5</a></td>
                    <td><a>Mathematics</a></td>
                    <td><a>P001</a></td>
                </tr>

                <tr>
                    <td colspan="5">
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                Differentiation and Integration
                            </div>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td><a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Assessment Book</a></td>
                    <td><a>O002</a></td>
                    <td><a>6</a></td>
                    <td><a>Science</a></td>
                    <td><a>P002</a></td>
                </tr>

                <tr>
                    <td colspan="5">
                        <div id="collapseTwo" class="panel-collapse collapse in">
                            <div class="panel-body">
                                Ten Year Series Mathematics
                            </div>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td><a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Revision Worksheet</a></td>
                    <td><a>O003</a></td>
                    <td><a>8</a></td>
                    <td><a>Mathematics</a></td>
                    <td><a>S001</a></td>
                </tr>

                 <tr>
                    <td colspan="5">
                        <div id="collapseTwo" class="panel-collapse collapse in">
                            <div class="panel-body">
                                Integer and Values
                            </div>
                        </div>
                    </td>
                </tr>






</asp:Content>
