﻿using IntegratedInfocommProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Perceptum_IIP.View.Portals.Admin
{
    public partial class Teachers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void departments_SelectedIndexChanged(object sender, EventArgs e)
        {
            teachers.Items.Clear();
            using (var db = new ApplicationDbContext())
            {
                var retrievedTeachers = db.Employees.ToList().Where(x => x.Department == departments.SelectedValue);
                foreach (var teacher in retrievedTeachers)
                {
                    teachers.Items.Add(teacher.FirstName);
                }
            }
        }

        protected void teachers_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}