﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Portals/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="EmployeeRegister.aspx.cs" Inherits="IntegratedInfocommProject.Views.Portals.Admin.EmployeeRegister" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="form-horizontal">
        <h4>Create a new account</h4>
        <hr />
        <asp:ValidationSummary runat="server" CssClass="text-danger" />


            <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="UserName" CssClass="col-md-2 control-label">UserName</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="UserName" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="UserName"
                    CssClass="text-danger" ErrorMessage="The name field is required." />
            </div>
              </div>

         <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="FirstName" CssClass="col-md-2 control-label">First Name</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="FirstName" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="FirstName"
                    CssClass="text-danger" ErrorMessage="The first name field is required." />
            </div>
              </div>

        
         <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="LastName" CssClass="col-md-2 control-label">Last Name</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="LastName" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="LastName"
                    CssClass="text-danger" ErrorMessage="The last name field is required." />
            </div>
              </div>


        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-2 control-label">Password</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Password" CssClass="form-control" TextMode="Password" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Password"
                    CssClass="text-danger" ErrorMessage="The password field is required." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Position" CssClass="col-md-2 control-label">Position</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Position" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Position"
                    CssClass="text-danger" ErrorMessage="The position field is required." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Department" CssClass="col-md-2 control-label">Department</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Department" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Department"
                    CssClass="text-danger" ErrorMessage="The department field is required." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="DOB" CssClass="col-md-2 control-label">Date of Birth</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="DOB" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="DOB"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The date of birth field is required." />
            </div>
        </div>

           <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Email</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Email" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The email field is required." />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Address" CssClass="col-md-2 control-label">Address</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Address" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Address"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The address field is required." />
            </div>
        </div>

        
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Telephone" CssClass="col-md-2 control-label">Telephone</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Telephone" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Telephone"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The telephone field is required." />
            </div>
        </div>

            <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Salary" CssClass="col-md-2 control-label">Salary</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Salary" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Salary"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The salary field is required." />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" CssClass="col-md-2 control-label">Classes</asp:Label>
            <div class="col-md-10">
            <asp:RadioButton ID="mathematics" Text="Mathematics" runat="server" />
            <asp:RadioButton ID="science" Text="Science" runat="server" />

            </div>
        </div>

           <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtClassID" CssClass="col-md-2 control-label">Class ID</asp:Label>
            <div class="col-md-10">
            <asp:TextBox ID="txtClassID" runat="server"></asp:TextBox>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button ID="CreateUser" runat="server" OnClick="CreateUser_Click" Text="Register" />
            </div>
        </div>
    </div>



</asp:Content>
