﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Portals/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="LeaveApproval.aspx.cs" Inherits="IntegratedInfocommProject.Views.Portals.Admin.LeaveApproval" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <asp:GridView ID="GridView1" runat="server" OnRowCommand="GridView1_RowCommand"
        CssClass="table table-hover table-striped">
        <Columns>
            <asp:ButtonField Text="Approve" CommandName="Approve" />
            <asp:ButtonField Text="Reject" CommandName="Reject" />
        </Columns>
            
    </asp:GridView>




</asp:Content>
