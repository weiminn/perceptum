﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;
using System.Data;
using System.Configuration;
using IntegratedInfocommProject.Models;

namespace IntegratedInfocommProject.Views.Portals.Admin
{
    public partial class Edit_Inventory : System.Web.UI.Page
    {
        private byte[] bytes;
        protected void Page_Load(object sender, EventArgs e)
        {


        }
        

        protected void Sumbit_Click(object sender, EventArgs e)
        {


            // define connection string and insert query 
            // string CS = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection con = new SqlConnection();
            con.ConnectionString = (@"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=aspnet-IntegratedInfocommProject;Trusted_Connection=Yes;User ID=Perceptum2018;Password=p12345;Integrated Security=True;");
            SqlCommand cmd;
            SqlDataReader rdr;
            string insertQuery = "INSERT INTO Products (Name, Details,ProductCategory,SalesItem, NormalPrice, PromoPrice, QuantityOnHand) " +
                                 "VALUES (@name, @details, @productcategory, @salesitem, @normalprice, @promoprice, @QuantityonHand);";

            // create connection and command for the INSERT
            // using (SqlConnection con = new SqlConnection(con))
            using (SqlCommand cmdInsert = new SqlCommand(insertQuery, con))
            using (var db = new ApplicationDbContext())
            {
                // define parameters and set values
                cmdInsert.Parameters.Add("@name", SqlDbType.NVarChar, 50).Value = Name.Text;
                cmdInsert.Parameters.Add("@details", SqlDbType.NVarChar, 50).Value = Details.Text;
                cmdInsert.Parameters.Add("@productcategory", SqlDbType.NVarChar, 50).Value = ProductCategory.Text;
                cmdInsert.Parameters.Add("@salesitem", SqlDbType.NVarChar, 50).Value = SalesItem.Text;
                cmdInsert.Parameters.Add("@normalprice", SqlDbType.Decimal, 50).Value = NormalPrice.Text;
                cmdInsert.Parameters.Add("@promoprice", SqlDbType.Decimal, 50).Value = PromoPrice.Text;
                cmdInsert.Parameters.Add("@QuantityonHand", SqlDbType.Int, 300).Value = QuantityOnHand.Text;
                // you need to somehow find / determine / pick this value on your form

                // open connection, execute query, close connection
                con.Open();
                cmdInsert.ExecuteNonQuery();
                con.Close();
            }


            using (var db = new ApplicationDbContext())
            {
                Product Product = new Product
                { 
                    Name = Name.Text,
                    Details = Details.Text,
                    ProductCategory = ProductCategory.Text,
                    Salesitem = SalesItem.Text,
                    NormalPrice = Convert.ToDecimal(NormalPrice.Text),
                    PromoPrice = Convert.ToDecimal(PromoPrice.Text),
                    QuantityOnHand = Convert.ToInt32(QuantityOnHand.Text),
                    Image = FileUploadControl.FileBytes
                };

                Session["ProductName"] = (string)Name.Text;


            db.Products.Add(Product);
            db.SaveChanges();
            db.Dispose();
        }
            }
        }
       

}
