﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Portals/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Teachers.aspx.cs" Inherits="Perceptum_IIP.View.Portals.Admin.Teachers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <h1>Teachers</h1>
        <br />

        <div class="row">
            <div class="col-md-3">
                <h3>Select to filter:</h3>
            </div>
            <div class="col-md-3">
                <div class="dropdown">
                    <asp:DropDownList CssClass="dropdown-toggle" AutoPostBack="true" runat="server" ID="departments" OnSelectedIndexChanged="departments_SelectedIndexChanged">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="Science">Science</asp:ListItem>
                        <asp:ListItem Value="Mathematics">Mathematics</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-md-3">
                <div class="dropdown">
                    <asp:DropDownList CssClass="dropdown-toggle" runat="server" ID="teachers" OnSelectedIndexChanged="teachers_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
        </div>

        <div class="row">
            <h2>Classes Assigned</h2>
            <asp:GridView CssClass="table" ID="classGrid" runat="server">
            </asp:GridView>
        </div>
    
        <br />  
        <div class="row">
            <div class="col-md-6">
                <h2>Ms. Caroline Tan</h2>
                <h2>List of Students</h2> 
            </div>
            <div class="col-md-6">
                <h2>Level: Primary 4 </h2>
                <h2>Time: 3pm-5pm</h2>
            </div>
        </div>

        <table class="table table-hover">
            <thead>
                <tr>
                <th>Name</th>

                </tr>
            </thead>
            <tbody>
                <tr>
                <td>John</td>
                </tr>
                <tr>
                <td>Mary</td>
                </tr>
                <tr>
                <td>July</td>
                </tr>
            </tbody>
        </table>    
        <button class="btn btn-success">Add New Student</button>
    </div>
</asp:Content>
