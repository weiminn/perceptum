﻿using IntegratedInfocommProject.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IntegratedInfocommProject.Views.Portals.Admin
{
    public partial class StudentsRegister : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private static readonly string scriptSuccessNewAccount =
          "<script language=\"javascript\">\n" +
          "alert (\"Your account has been succesfully created - Thank You!\");\n" +
          "</script>";

        protected void CreateUser_Click1(object sender, EventArgs e)
        {
            Type csType = this.GetType();

            var userStore = new UserStore<IdentityUser>();
            var manager = new UserManager<IdentityUser>(userStore);

            // Need to create Identity user object
            var user = new IdentityUser()
            {
                UserName = UserName.Text,
                Email = Email.Text
            };
            IdentityResult result = manager.Create(user, Password.Text);

            if (result.Succeeded)
            {
                using (var db = new ApplicationDbContext())
                {
                    Student student = new Student
                    {
                        UserName = UserName.Text,
                        FirstName = FirstName.Text,
                        LastName = LastName.Text,
                        Email = Email.Text,
                        Password = Password.Text,
                        Address = Address.Text,
                        BirthDate = Convert.ToDateTime(DOB.Text),
                        Level = Level.Text,
                        SubLevel= SubLevel.Text,
                        Outlet = Outlet.Text,
                        Telephone = Convert.ToInt32(Telephone.Text),
                        ApplicationUserId = user.Id

                    };


                    db.Students.Add(student);
                    db.SaveChanges();

                    StudentClass studentClass = new StudentClass
                    {

                        StudentId = student.StudentId,
                        ClassId = Convert.ToInt32(txtClassID.Text)
                    };

                    db.StudentClasses.Add(studentClass);
                    db.SaveChanges();

                    StudentParent studentParent = new StudentParent
                    {

                        StudentId = student.StudentId,
                        ParentId = Convert.ToInt32(ParentID.Text)
                    };

                    db.StudentParents.Add(studentParent);
                    db.SaveChanges();
                }



                UserName.Text = "";
                Email.Text = "";
                Address.Text = "";
                DOB.Text = "";
                Level.Text = "";
                SubLevel.Text = "";
                Outlet.Text = "";
                Telephone.Text = "";
                txtClassID.Text = "";


                ClientScript.RegisterStartupScript(csType, "Success", scriptSuccessNewAccount);



            }

        }
    }
}