﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Portals/Parent/Parents.Master" AutoEventWireup="true" CodeBehind="MakeUp.aspx.cs" Inherits="Perceptum_IIP.View.Portals.Parent.MakeUp" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <h2>Make-Up Lesson Booking</h2>
        <br />
        <asp:DropDownList runat="server" id="subjectsDDL" AutoPostBack="true" OnSelectedIndexChanged="subjectsDDL_SelectedIndexChanged">
            <asp:ListItem Value="English">English</asp:ListItem>
            <asp:ListItem Value="Mathematics">Mathematics</asp:ListItem>
            <asp:ListItem Value="Science">Science</asp:ListItem>
        </asp:DropDownList>

        <br /><br />    
        <asp:GridView runat="server" ID="classesGrid" OnRowCommand="classesGrid_RowCommand"
        CssClass="table table-hover table-striped">
            <Columns>
                <asp:ButtonField Text="Book for Next Week" />
            </Columns>
        </asp:GridView>


        <%--<table class="table table-hover">
            <tbody>
                <tr>
                <td>Level</td>
                <td>
                    <asp:DropDownList runat="server">
                        <asp:ListItem>Primary 4</asp:ListItem>
                        <asp:ListItem>Primary 5</asp:ListItem>
                        <asp:ListItem>Primary 6</asp:ListItem>
                    </asp:DropDownList>
                </td>
                </tr>
                <tr>
                <td>Subject</td>
                    <td>
                        <asp:DropDownList runat="server">
                            <asp:ListItem>English</asp:ListItem>
                            <asp:ListItem>Mathematics</asp:ListItem>
                            <asp:ListItem>Science</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                <td>Reason</td>
                <td>
                    <textarea>

                    </textarea>

                </td>
                </tr>
                <tr>
                    <td>Teacher</td>
                    <td>
                        <asp:DropDownList runat="server">
                            <asp:ListItem>Ms Tan</asp:ListItem>
                            <asp:ListItem>Mr Ng</asp:ListItem>
                            <asp:ListItem>Mr Lim</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                <td>Date</td>
                <td>
                    <asp:Calendar runat="server">

                    </asp:Calendar>
                </td>
                </tr>
                <tr>
                <td>Time</td>
                <td>
                    <div class="input-group bootstrap-timepicker timepicker">
                        <input id="timepicker1" type="text" class="form-control input-small">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                    </div>
                </td>
                </tr>
                <tr>
                <td>Venue</td>
                <td>
                    <asp:DropDownList runat="server">
                        <asp:ListItem>Bukit Timah</asp:ListItem>
                        <asp:ListItem>Bishan</asp:ListItem>
                    </asp:DropDownList>

                </td>
                </tr>
            </tbody>
        </table>--%>

        <br />
        <asp:Button ID="Button2" runat="server" Text="Check Availability" />

        <asp:Button ID="Button1" runat="server" Text="Confirm" OnClick="Button1_Click" />

        <script type="text/javascript">
            $('#timepicker1').timepicker();
        </script>

        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="StudentId,ParentId" DataSourceID="SqlDataSource1">
            <Columns>
                <asp:BoundField DataField="StudentId" HeaderText="StudentId" ReadOnly="True" SortExpression="StudentId" />
                <asp:BoundField DataField="ParentId" HeaderText="ParentId" ReadOnly="True" SortExpression="ParentId" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:studentparents connection string %>" SelectCommand="SELECT * FROM [StudentParents]"></asp:SqlDataSource>

</asp:Content>