﻿using IntegratedInfocommProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Perceptum_IIP.View.Portals.Parent
{
    public partial class Students : System.Web.UI.Page
    {
        private int ParentID;
        protected void Page_Load(object sender, EventArgs e)
        {
            ParentID = 4; // Convert.ToInt32(Session["ParentID"]);
            using (var db = new ApplicationDbContext())
            {
                var retrievedStudents = db.StudentParents.Where(x => x.ParentId == ParentID).ToList();

                List<object> studentDisplay = new List<object>();
                foreach(var student in retrievedStudents)
                {
                    studentDisplay.Add(
                        db.Students
                        .Select(x => new {
                            x.StudentId,
                            x.FirstName,
                            x.LastName,
                            x.Level,
                            x.Telephone
                        })
                        .Single(x => x.StudentId == student.StudentId));


                }

                studentsGrid.DataSource = studentDisplay;
                studentsGrid.DataBind();
            }
        }



        protected void studentsGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument); ///value give row number that is clicked
            GridViewRow row = studentsGrid.Rows[index]; // go particular
            var studentselected = row.Cells[1].Text;
            this.Session["StudentID"] = studentselected;
            Response.Redirect("Classes.aspx");
        }
    }
}