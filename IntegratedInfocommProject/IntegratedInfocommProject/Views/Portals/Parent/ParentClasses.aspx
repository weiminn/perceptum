﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Portals/Parent/Parents.Master" AutoEventWireup="true" CodeBehind="ParentClasses.aspx.cs" Inherits="Perceptum_IIP.View.Portals.Parent.Classes" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1><asp:Label ID="stuFirstName" runat="server"></asp:Label>'s Classes</h1>
    <h1><asp:Label ID="stuLastName" runat="server"></asp:Label>'s Classes</h1>

    <asp:GridView ID="classesGrid" runat="server"
        CssClass="table table-hover table-striped" Width="100%" Height="100%" DataSourceID="SqlDataSource1">

    </asp:GridView>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>

    <br />
    <asp:Button runat="server" Text="Book Make Up Lessons" CssClass="btn btn-default" ID="Book" OnClick="Book_Click" />


    <%--<table class="table table-hover">
        <thead>
            <tr>
            <th>Date</th>
            <th>Venue</th>
            <th>Time</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <td>Wednesday</td>
            <td>Bukit Timah</td>
            <td>5 ~ 7 pm</td>
            </tr>
            <tr>
            <td>Friday</td>
            <td>Bishan</td>
            <td>5 ~ 7 pm</td>
            </tr>
            <tr>
            <td>Sunday</td>
            <td>Bukit Timah</td>
            <td>7.30 ~ 9.30 pm</td>
            </tr>
        </tbody>
    </table>
    <br />
    <a href="MakeUp.aspx" class="btn btn-primary">New Make Up Class</a>
    
    <hr />
    <br />
    <h1>Latest Grades</h1>
    <table class="table table-hover">
        <thead>
            <tr>
            <th>Date</th>
            <th>Class</th>
            <th>Grade</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <td>Jan 2018</td>
            <td>English</td>
            <td>A</td>
            </tr>
            <tr>
            <td>Feb 2018</td>
            <td>Mathematics</td>
            <td>B</td>
            </tr>
            <tr>
            <td>Mar 2018</td>
            <td>Science</td>
            <td>C</td>
            </tr>
        </tbody>
    </table
        
    <br />
    <a href="Feedback.aspx" class="btn btn-primary">Give Feedback</a>--%>
</asp:Content>
