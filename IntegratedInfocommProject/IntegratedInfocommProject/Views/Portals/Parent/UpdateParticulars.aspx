﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UpdateParticulars.aspx.cs" Inherits="IntegratedInfocommProject.Views.Portals.Parent.UpdateParticulars" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


      <table id="UpdateAccountTable" class="auto-style1" style="border-style: none; padding: 2px; margin: 1px; text-align: left">
        <tr>
            <td colspan="3" style="border-style: none; padding: 5px; margin: 1px; text-align: center; height: 50px;">Update User Account</td>
        </tr>
        <tr>
            <td class="auto-style2">Data Item</td>
            <td class="auto-style2">Existing Data </td>
            <td class="auto-style2">New Data</td>
        </tr>
        <tr>
            <td>Name</td>
            <td>
                <asp:Label ID="lblName" runat="server"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtName" runat="server" Width="250px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Password</td>
            <td>
                <asp:Label ID="lblPassword" runat="server"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtPassword" runat="server" Width="250px"></asp:TextBox>
            </td>
        </tr>
  
        <tr>
            <td>Email</td>
            <td>
                <asp:Label ID="lblEmail" runat="server"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtEmail" runat="server" Width="250px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>TelePhone</td>
            <td>
                <asp:Label ID="lblTel" runat="server"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtTel" runat="server" Width="250px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: right">
                <asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" Text="Update" />
            </td>
        </tr>
    </table>



</asp:Content>
