﻿using IntegratedInfocommProject.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Perceptum_IIP.View.Portals.Parent
{
    public partial class Classes : System.Web.UI.Page
    {
        private int StudentID;

        protected void Page_Load(object sender, EventArgs e)
        {
            StudentID = Convert.ToInt32(this.Session["StudentID"]);

            using (var db = new ApplicationDbContext())
            {
                var student = db.Students.Single(x => x.StudentId == StudentID);
                stuFirstName.Text = student.FirstName;
                stuLastName.Text = student.LastName;

                var retrievedClasses= db.StudentClasses.Where(x => x.StudentId == StudentID).ToList();

                DataTable dt = new DataTable();

                dt.Columns.Add("Class ID");
                dt.Columns.Add("Description");
                dt.Columns.Add("Level");
                dt.Columns.Add("Day");
                dt.Columns.Add("Time");

                foreach (var classs in retrievedClasses)
                {
                    DataRow dr = dt.NewRow();
                    Class theClass = db.Classes.Single(x => x.ClassId == classs.ClassId);

                    dr[0] = theClass.ClassId;
                    dr[1] = theClass.ClassDescription;
                    dr[2] = theClass.Level;
                    dr[3] = theClass.Day;
                    dr[4] = theClass.Time.ToString("HH:mm");

                    dt.Rows.Add(dr);
                }

                classesGrid.DataSource = dt;
                classesGrid.DataBind();

            }

        }

        protected void Book_Click(object sender, EventArgs e)
        {

            Response.Redirect("MakeUp.aspx");
        }
    }
}