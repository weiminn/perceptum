﻿using IntegratedInfocommProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;

namespace Perceptum_IIP.View.Portals.Parent
{
    public partial class MakeUp : System.Web.UI.Page
    {
        private int studentID;
        protected void Page_Load(object sender, EventArgs e)
        {
            studentID = Convert.ToInt32(this.Session["StudentID"]);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Confirmation Page.aspx");


        }

        protected void subjectsDDL_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (var db = new ApplicationDbContext())
            {
                var studentLevel = db.Students.Single(x => x.StudentId == studentID).Level;
                var classes = db.Classes.Where(x => x.Level == studentLevel && x.ClassDescription == subjectsDDL.SelectedValue).ToList();

                DataTable dt = new DataTable();
                dt.Columns.Add("ID");
                dt.Columns.Add("Description");
                dt.Columns.Add("Level");
                dt.Columns.Add("Day");
                dt.Columns.Add("Time");
                dt.Columns.Add("Available Slots");

                foreach (var classItem in classes){
                    DataRow dr = dt.NewRow();
                    dr[0] = classItem.ClassId;
                    dr[1] = classItem.ClassDescription;
                    dr[2] = classItem.Level;
                    dr[3] = classItem.Day;
                    dr[4] = classItem.Time.ToString("HH:mm");

                    var slots = db.MakeUpClasses.Where(x => x.ClassId == classItem.ClassId).ToList();
                    int occupiedSlots = 0;
                    foreach (var slot in slots)
                    {
                        DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
                        System.Globalization.Calendar cal = dfi.Calendar;

                        var nextWeek = cal.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstDay, DayOfWeek.Monday) + 1;
                        var slotWeek = cal.GetWeekOfYear(slot.dateTime, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
                        if (nextWeek == slotWeek)
                        {
                            occupiedSlots++;
                        }
                    }

                    dr[5] = 5 - occupiedSlots;
                    dt.Rows.Add(dr);
                }

                classesGrid.DataSource = dt;// classes;
                classesGrid.DataBind();
            }
        }

        protected void classesGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = classesGrid.Rows[index];
            var classSelected = Convert.ToInt32(row.Cells[1].Text);

            using (var db = new ApplicationDbContext())
            {
                var theClass = db.Classes.Single(x => x.ClassId == classSelected);
                var bookingDay = GetNextWeekday(theClass.Day);

                var slots = db.MakeUpClasses.Where(x => x.ClassId == theClass.ClassId).ToList();
                int occupiedSlots = 0;
                foreach (var slot in slots)
                {
                    DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
                    System.Globalization.Calendar cal = dfi.Calendar;

                    var nextWeek = cal.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstDay, DayOfWeek.Monday) + 1;
                    var slotWeek = cal.GetWeekOfYear(slot.dateTime, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
                    if (nextWeek == slotWeek)
                    {
                        occupiedSlots++;
                    }
                }

                if (occupiedSlots < 5)
                {
                    MakeUpClass makeUpClass = new MakeUpClass
                    {
                        StudentId = studentID,
                        ClassId = theClass.ClassId,
                        dateTime = bookingDay
                    };

                    db.MakeUpClasses.Add(makeUpClass);
                    db.SaveChanges();
                }

            }

        }

        static DateTime GetNextWeekday(DayOfWeek day)
        {
            DateTime result = DateTime.Now.AddDays(1);
            while (result.DayOfWeek != day)
                result = result.AddDays(1);
            return result;
        }
    }
}