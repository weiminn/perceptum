﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Portals/Parent/Parents.Master" AutoEventWireup="true" CodeBehind="Students.aspx.cs" Inherits="Perceptum_IIP.View.Portals.Parent.Students" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h1>Students</h1>

    <asp:GridView ID="studentsGrid" runat="server" OnRowCommand="studentsGrid_RowCommand" Width="100%" Height="100%"
        CssClass="table table-hover table-striped" AutoGenerateColumns="False">
        <Columns>
            <asp:BoundField DataField="StudentId" HeaderText="StudentId" SortExpression="Level" />

            <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="Name" />
            <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="Name" />

            <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level" />
        </Columns>
    </asp:GridView>


    <br />

</asp:Content>
