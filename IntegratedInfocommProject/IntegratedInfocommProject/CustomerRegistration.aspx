﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CustomerRegistration.aspx.cs" Inherits="IntegratedInfocommProject.CustomerRegistration1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <table id="NewAccountTable" class="auto-style3" style="border-style: none; padding: 2px; margin: 1px; text-align: center">
        <tr>
            <td colspan="2" style="border-style: none; padding: 5px; margin: 1px; text-align: center; vertical-align: middle; height: 50px;">&nbsp;Create New User Account&nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: right; vertical-align: middle">User ID</td>
            <td style="text-align: left; vertical-align: middle">
                <asp:TextBox ID="txtUserId" runat="server" Width="500px"></asp:TextBox>
            </td>
        </tr>

         <tr>
            <td style="text-align: right; vertical-align: middle"> First Name </td>
            <td style="text-align: left; vertical-align: middle">
                <asp:TextBox ID="txtFirstName" runat="server" Width="500px"></asp:TextBox>
            </td>
        </tr>

         <tr>
            <td style="text-align: right; vertical-align: middle">Last Name</td>
            <td style="text-align: left; vertical-align: middle">
                <asp:TextBox ID="txtLastName" runat="server" Width="500px"></asp:TextBox>
            </td>
        </tr>

        <tr>
            <td style="text-align: right; vertical-align: middle" class="auto-style2">Password</td>
            <td style="text-align: left; vertical-align: middle" class="auto-style2">
                <asp:TextBox ID="txtPassword" runat="server" Width="500px"></asp:TextBox>
            </td>
        </tr>

         <tr>
            <td style="text-align: right; vertical-align: middle">Email</td>
            <td style="text-align: left; vertical-align: middle">
                <asp:TextBox ID="txtEmail" runat="server" Width="500px"></asp:TextBox>
            </td>
        </tr>

        <tr>
            <td style="text-align: right; vertical-align: middle">Phone</td>
            <td style="text-align: left; vertical-align: middle">
                <asp:TextBox ID="txtTel" runat="server" Width="500px"></asp:TextBox>
            </td>
        </tr>
       
            <td colspan="2" style="text-align: right; vertical-align: middle">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
