﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IntegratedInfocommProject.Models
{
    public class Student
    {
        [Key, Display(Name = "ID")]
        [ScaffoldColumn(false)]
        public int StudentId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public DateTime BirthDate { get; set; }
        public string School { get; set; }
        public string Level { get; set; }
        public string SubLevel { get; set; }
        public int Telephone { get; set; }
        public string Email { get; set; }
        public string Outlet { get; set; }

        public string ApplicationUserId { get; set; }
        [ForeignKey("ApplicationUserId")]
        public ApplicationUser ApplicationUser { get; set; }
    }

    public class Parent
    {
        [Key, Display(Name = "ID")]
        [ScaffoldColumn(false)]
        public int ParentId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public int Telephone { get; set; }
        public string Email { get; set; }

        public string ApplicationUserId { get; set; }
        [ForeignKey("ApplicationUserId")]
        public ApplicationUser ApplicationUser { get; set; }
    }

    public class Customer
    {
        [Key, Display(Name = "ID")]
        [ScaffoldColumn(false)]
        public int customerId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public int Telephone { get; set; }
        public string Email { get; set; }

        public string ApplicationUserId { get; set; }
        [ForeignKey("ApplicationUserId")]
        public ApplicationUser ApplicationUser { get; set; }
    }

    public class StudentParent
    {
        [Key]
        [Column(Order = 1)]
        public int StudentId { get; set; }
        [ForeignKey("StudentId")]
        public Student Student { get; set; }

        [Key]
        [Column(Order = 2)]
        public int ParentId { get; set; }
        [ForeignKey("ParentId")]
        public Parent Parent { get; set; }

    }

    public class Employee
    {
        [Key]
        [ScaffoldColumn(false)]
        public int EmployeeId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
        public DateTime DOB { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }
        public int Telephone { get; set; }
        public int Salary { get; set; }

        public string ApplicationUserId { get; set; }
        [ForeignKey("ApplicationUserId")]
        public ApplicationUser ApplicationUser { get; set; }

    }

    public class Class
    {
        public int ClassId { get; set; }
        public String ClassDescription { get; set; }
        public String Level { get; set; }
        public String SubLevel { get; set; }
        public DayOfWeek Day { get; set; }
        public DateTime Time { get; set; }
        public int MakeUpSlots { get; set; }
    }

    public class StudentClass
    {
        [Key]
        [Column(Order = 1)]
        public int StudentId { get; set; }
        [ForeignKey("StudentId")]
        public Student Student { get; set; }

        [Key]
        [Column(Order = 2)]
        public int ClassId { get; set; }
        [ForeignKey("ClassId")]
        public Class Class { get; set; }
    }

    public class EmployeeClass
    {
        [Key]
        [Column(Order = 1)]
        public int EmployeeId { get; set; }
        [Key]
        [Column(Order = 2)]
        public int ClassId { get; set; }

        [ForeignKey("EmployeeId")]
        public Employee Employee { get; set; }
        [ForeignKey("ClassId")]
        public Class Class { get; set; }
    }

    public class Grade
    {
        [Key]
        [Column(Order = 1)]
        public int ClassId { get; set; }
        [ForeignKey("ClassId")]
        public Class Class { get; set; }

        [Key]
        [Column(Order = 2)]
        public int StudentId { get; set; }
        [ForeignKey("StudentId")]
        public Student Student { get; set; }

        [Key]
        [Column(Order = 3)]
        public int EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public Employee Employee { get; set; }

        public DateTime Month { get; set; }
        public string Grades { get; set; }
    }

    public class Product
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public string Salesitem { get; set; }
        public string ProductCategory { get; set; }
        public decimal NormalPrice { get; set; }
        public decimal PromoPrice { get; set; }
        public int QuantityOnHand { get; set; }
        public byte[] Image { get; set; }
        public string ProductURL { get; set; }
    }

    public class CartItem
    {
        [Key]
        public int ItemId { get; set; }

        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public Product Product { get; set; }

        public int CustomerId { get; set; }
        [ForeignKey("CustomerId")]
        public Customer Customer { get; set; }
    }

    public class Order
    {
        [Key]
        public int OrderId { get; set; }
        public DateTime DateTime { get; set; }

        public int Quantity { get; set; }
        public int UnitPrice { get; set; }

        public int CartItemId { get; set; }
        [ForeignKey("CartItemId")]
        public CartItem CartItem { get; set; }
    }

    public class Transaction
    {
        [Key]
        public int TransactionId { get; set; }

        public string PaymentMode { get; set; }
        public string DeliveryMode { get; set; }

        public int CustomerId { get; set; }
        [ForeignKey("CustomerId")]
        public Customer Customer { get; set; }

        public int OrderId { get; set; }
        [ForeignKey("OrderId")]
        public Order Order { get; set; }
    }

    public class Feedback
    {
        [Key]
        public int FeedbackId { get; set; }

        public string Details { get; set; }

        public int ParentId { get; set; }
        [ForeignKey("ParentId")]
        public Parent Parent { get; set; }
    }

    public class MakeUpClass
    {
        [Key]
        [Column(Order = 1)]
        public int ClassId { get; set; }
        [ForeignKey("ClassId")]
        public Class Class { get; set; }

        [Key]
        [Column(Order = 2)]
        public int StudentId { get; set; }
        [ForeignKey("StudentId")]
        public Student Student { get; set; }


        [Key]
        [Column(Order = 3)]
        public DateTime dateTime { get; set; }
    }

    public class TuitionFee

    {
        [Key]
        public int FeeId { get; set; }

        public int StudentId { get; set; }
        [ForeignKey("StudentId")]
        public Student Student { get; set; }

        public int ClassId { get; set; }
        [ForeignKey("ClassId")]
        public Class Class { get; set; }

        public int Quantity { get; set; }
        public int TotalFee { get; set; }

    }

    public class Leave
    {
        [Key]
        public int LeaveId { get; set; }
        public int EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public Employee Employee { get; set; }
        public string Reason { get; set; }
        public string Details { get; set; }
        public string DayRequested { get; set; }
        public string Approval { get; set; }
        public DateTime DateApproved { get; set; }

    }


}