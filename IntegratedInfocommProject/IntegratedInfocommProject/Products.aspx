﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Products.aspx.cs" Inherits="IntegratedInfocommProject.Products" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h3>Perceptum Resources</h3>

    <asp:GridView ID="ProductsGrid" runat="server" OnRowCommand="ProductsGrid_RowCommand"
        CssClass="table table-hover table-striped">
        <Columns>
            <asp:ButtonField Text="View" />
            <asp:TemplateField>
                <HeaderTemplate>Image</HeaderTemplate>
                <ItemTemplate>
                    <img src='data:image/jpg;base64,<%# Eval("Image") != System.DBNull.Value ? Convert.ToBase64String((byte[])Eval("Image")) : string.Empty %>' alt="image" height="100" width="200"/>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

    <%--<div class="row">
        <div class="col-md-10">
            <div class="row">
                <h2>General Resources</h2>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <img style="width:300px; height:300px;" src="Content/images/Products%201.JPG" />

                </div>
                <div class="col-md-4">
                    <img style="width:300px; height:300px;"  src="Content/images/product%202.JPG" />
                </div>
                <div class="col-md-4">
                    <img style="width:300px; height:300px;"  src="Content/images/product%203.JPG" />
                </div>
            </div>
            <div class="row">
            <div class="row">
                <h2>Packages</h2>
            </div>
                <div class="col-md-4">
                    <img style="width:300px; height:300px;"  src="Content/images/Primary%205.JPG" />
                </div>
                <div class="col-md-4">
                    <img style="width:300px; height:300px;" src="Content/images/Primary%206.JPG" />
                </div>
                <div class="col-md-4">
                    <img style="width:300px; height:300px;"  src="Content/images/product%204.JPG" />


                </div>
            </div>
            <h2>Weekly Materials</h2>
            <p>You must first log in to view these materials. <a href="http://localhost:54827/Account/Login" />Click to log in</p>
        </div>
        <div class="col-md-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                <h3 class="panel-title">Browse for resources:</h3>
                </div>
                <div class="panel-body">
                                
                <a class="btn btn-primary" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1"">
                    Level

                </a><br />
                    <div class="collapse multi-collapse" id="multiCollapseExample1">
                    <div class="card card-body">
                    <a class="btn btn-secondary" href="#">P1</a><br />
                    <a class="btn btn-secondary" href="#">P2</a><br />
                    <a class="btn btn-secondary" href="#">P3</a><br />
                    </div>
                </div>

                <a class="btn btn-primary" data-toggle="collapse" href="#multiCollapseExample2" role="button" aria-expanded="false" aria-controls="multiCollapseExample2"">
                    Subject

                </a><br />
                    <div class="collapse multi-collapse" id="multiCollapseExample2">
                    <div class="card card-body">
                    <a class="btn btn-secondary" href="#">Engish</a><br />
                    <a class="btn btn-secondary" href="#">Science</a><br />
                    <a class="btn btn-secondary" href="#">Mathematics</a><br />
                    </div>
                </div>

                </div>
            </div>

        </div>
    </div>--%>



</asp:Content>
