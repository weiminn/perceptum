﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="testdatetimepicker.aspx.cs" Inherits="IntegratedInfocommProject.testdatetimepicker" %>


   
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
            <link rel="stylesheet" href="https://resources/demos/style.css" />
            <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
            <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
            <script>
                $(function () {
                    $('#' + '<%= dateTextbox.ClientID %>').datepicker(
                        {
                            changeMonth: true,
                            changeYear: true,
                            inline: true
                        }
                    );
                });
            </script>
     <style>
        label {
            color: white;
        }
   </style>
    

    <div class="container-fluid">
    <div class="container">
      <div class="col-md-6">  
       
        
            <div class="form-group">     
                <i class="fa fa-calendar" style="font-size:36px; padding-top:5%; color:white;"> Employee Schedule</i>
                <asp:Label ID="empIDLabel" runat="server" Text="" Visible="False"></asp:Label>
                 <div style="margin-top:5%;">           
                    <div >
                        <label for="Email" >Select date</label> <br />
                        <asp:TextBox ID="dateTextbox" runat="server"></asp:TextBox>
                    </div>
                
                   <div style="margin-top:2%; color:white;"> 
                       <label> Select Shift</label> <br />
    <asp:RadioButton ID="RadioButton1" runat="server" GroupName="choice" oncheckedchanged="RadioButton1_CheckedChanged" Text="8am-3pm" AutoPostBack="True"/>
    <asp:RadioButton ID="RadioButton2" runat="server" GroupName="choice" Text="3pm-8pm" AutoPostBack="True" oncheckedchanged="RadioButton2_CheckedChanged"/>
    <asp:Label ID="Label1" runat="server" Text="Label" Visible="False"></asp:Label>
                  </div>
               <div style="padding-top:20px;">
                   <a href="EmployeeHomePage.aspx" style="margin-left:10px;" class="btn btn-success">Exit</a>
                   <asp:LinkButton class="btn btn-success" style="float:left;" ID="SchSubmit" runat="server" OnClick="SchSubmit_Click">Submit</asp:LinkButton>
                </div>
               </div>
             </div>
           </div>
         
            <div class="col-md-6">
                <div  style="padding-top:5%" class="col-md-12 form-group">
                   <label for="Email">Additional Notes </label><br />
                    <asp:TextBox ID="notesTextBox" runat="server" rows="10" Columns="70" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>           
        </div>
      </div>





</asp:Content>
