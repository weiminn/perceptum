﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Microsoft.AspNet.Identity;



namespace IntegratedInfocommProject
{
    public partial class ShoppingChart : System.Web.UI.Page
    {
        static readonly string scriptErrorPayment =
"<script language=\"javascript\">\n" +
"alert (\"Please Select a payment method\");\n" +
"</script>";

        static readonly string scriptErrorDelivery =
"<script language=\"javascript\">\n" +
"alert (\"Please Select a delivery method\");\n" +

"</script>";
        string userId;


        protected void Page_Load(object sender, EventArgs e)
        {


        string userId = User.Identity.GetUserId();

            if (userId == null)
            {
                lblShoppingCart.Text = "You have to login first to see your Shopping Cart";
                btnCheckOut.Visible = false;


            }
            else
            {
                // check to see if items have been ordered
                SqlConnection con = new SqlConnection();
                con.ConnectionString = (@"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=aspnet-IntegratedInfocommProject;Trusted_Connection=Yes;User ID=Perceptum2018;Password=p12345;Integrated Security=True;");
                SqlCommand cmd;
                SqlDataReader rdr;
                int intOrderNo = (int)Session["sOrderNo"];
                String strSql = "SELECT ProductId FROM CartItems WHERE iOrderNo = " + intOrderNo;
                cmd = new SqlCommand(strSql, con);
                con.Open();
                rdr = cmd.ExecuteReader();
                Boolean booRows = rdr.HasRows;
                if (booRows)
                {
                    lblShoppingCart.Text = "Your Shopping Cart";

                    btnCheckOut.Visible = true;
                }
                else
                {
                    lblShoppingCart.Text = "Your Shopping Cart is empty";

                    btnCheckOut.Visible = false;
                }
                con.Close();
            }
        }

        protected void btnCheckOut_Click(object sender, EventArgs e)
        {
            Type csType = this.GetType();
            if (Paypal.Checked == false && Cash.Checked == false)
            {
                ClientScript.RegisterStartupScript(csType, "Error", scriptErrorPayment);
                return;
            }
            if (Ninja.Checked == false && Standard.Checked == false && Express.Checked == false)
            {
                ClientScript.RegisterStartupScript(csType, "Error", scriptErrorDelivery);
                return;

            }



            if (Paypal.Checked == true)
            {

                //declare an int to get ordernumber 
                int PaypalOrderNumber = (int)Session["sOrderNo"];

                //declare a string to contain the total amount from PaymentDetails 

            //  decimal PaypalTotalAmount = Convert.ToDecimal(PaymentDetails.Rows[2].Cells[1].Text);

                //declare a string to contain the codes to redirect to Paypal

                string redirect = "";

                //link to paypal                                                             //enter existing email account for paypal
                redirect += "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_xclick&business=" + "@gmail.com";

                //name of buyer
                redirect += "&item_name=" + (string)Session["sFirstName"] + "'s Purchase from Perceptum (Trans ID: "
                    //order number
                    + PaypalOrderNumber.ToString() + ")";

                redirect += "&amount=" + PaypalTotalAmount;

                redirect += "&currency_code=SGD";

                //the order tracking page to return to after a successful transaction

                redirect += "&return" + "OrderTracking.aspx";


               redirect += "&cancel_return"; // + the page to return back after a unsucessful transaction


                // order number of the transaction
                redirect += "&custom" + PaypalOrderNumber.ToString();


                //redirect to Paypal
                Response.Redirect(redirect);

            }

            //Payment Radio Buttons
            string payment = string.Empty;

            if (Paypal.Checked)
            {
                payment = "Paypal";
            }
            else if (Cash.Checked)
            {
                payment = "Cash";
            }


            //Delivery Radio Buttons

            string delivery = string.Empty;

            if (Ninja.Checked)
            {
                delivery = "POP Stations";
            }

            else if (Standard.Checked)
            {
                delivery = "Standard Courier";
            }
            else if (Express.Checked)
            {
                delivery = "Express Courier $3.50";
            }
            //Extracting Values from Grid & Details View

           // string ttlAmtwoGST = PaymentsDetails.Rows[0].Cells[1].Text;
          //  string ttlGSTAmt = PaymentsDetails.Rows[1].Cells[1].Text;
           // string ttlAmt = PaymentsDetails.Rows[2].Cells[1].Text;







            string strStatus = "CheckedOut";
            int intOrderNo = (int)Session["sOrderNo"];
            string strPay = payment.ToString();
            string strDel = delivery.ToString();


            SqlConnection con = new SqlConnection();
            con.ConnectionString = (@"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=aspnet-IntegratedInfocommProject;Trusted_Connection=Yes;User ID=Perceptum2018;Password=p12345;Integrated Security=True;");
            String strSqlUpdate = "UPDATE Transactions SET oPayMode = @Pay,"
            + "oDelMode = @Del, oStatus = @Status WHERE oOrderNo = " + intOrderNo;
            SqlCommand cmd;
            cmd = new SqlCommand(strSqlUpdate, con);
            cmd.Parameters.Add("@Pay", SqlDbType.VarChar).Value = strPay;
            cmd.Parameters.Add("@Del", SqlDbType.VarChar).Value = strDel;
            cmd.Parameters.Add("@Status", SqlDbType.VarChar).Value = strStatus;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();



        }
    }
}
