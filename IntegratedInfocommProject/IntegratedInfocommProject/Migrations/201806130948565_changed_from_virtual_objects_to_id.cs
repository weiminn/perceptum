namespace IntegratedInfocommProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changed_from_virtual_objects_to_id : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CartItems", "Parent_ParentId", "dbo.Parents");
            DropForeignKey("dbo.CartItems", "Product_ProductId", "dbo.Products");
            DropForeignKey("dbo.Feedbacks", "Parent_ParentId", "dbo.Parents");
            DropForeignKey("dbo.Orders", "CartItem_ItemId", "dbo.CartItems");
            DropForeignKey("dbo.Transactions", "Order_OrderId", "dbo.Orders");
            DropForeignKey("dbo.Transactions", "Parent_ParentId", "dbo.Parents");
            DropForeignKey("dbo.TuitionFees", "ClassId_ClassId", "dbo.Classes");
            DropForeignKey("dbo.TuitionFees", "StudentId_StudentId", "dbo.Students");
            DropIndex("dbo.CartItems", new[] { "Parent_ParentId" });
            DropIndex("dbo.CartItems", new[] { "Product_ProductId" });
            DropIndex("dbo.Feedbacks", new[] { "Parent_ParentId" });
            DropIndex("dbo.Orders", new[] { "CartItem_ItemId" });
            DropIndex("dbo.Transactions", new[] { "Order_OrderId" });
            DropIndex("dbo.Transactions", new[] { "Parent_ParentId" });
            DropIndex("dbo.TuitionFees", new[] { "ClassId_ClassId" });
            DropIndex("dbo.TuitionFees", new[] { "StudentId_StudentId" });
            RenameColumn(table: "dbo.CartItems", name: "Parent_ParentId", newName: "ParentId");
            RenameColumn(table: "dbo.CartItems", name: "Product_ProductId", newName: "ProductId");
            RenameColumn(table: "dbo.Parents", name: "ApplicationUser_Id", newName: "ApplicationUserId");
            RenameColumn(table: "dbo.Employees", name: "ApplicationUser_Id", newName: "ApplicationUserId");
            RenameColumn(table: "dbo.Feedbacks", name: "Parent_ParentId", newName: "ParentId");
            RenameColumn(table: "dbo.Students", name: "ApplicationUser_Id", newName: "ApplicationUserId");
            RenameColumn(table: "dbo.Orders", name: "CartItem_ItemId", newName: "CartItemId");
            RenameColumn(table: "dbo.Transactions", name: "Order_OrderId", newName: "OrderId");
            RenameColumn(table: "dbo.Transactions", name: "Parent_ParentId", newName: "ParentId");
            RenameColumn(table: "dbo.TuitionFees", name: "ClassId_ClassId", newName: "ClassId");
            RenameColumn(table: "dbo.TuitionFees", name: "StudentId_StudentId", newName: "StudentId");
            RenameIndex(table: "dbo.Parents", name: "IX_ApplicationUser_Id", newName: "IX_ApplicationUserId");
            RenameIndex(table: "dbo.Employees", name: "IX_ApplicationUser_Id", newName: "IX_ApplicationUserId");
            RenameIndex(table: "dbo.Students", name: "IX_ApplicationUser_Id", newName: "IX_ApplicationUserId");
            AlterColumn("dbo.CartItems", "ParentId", c => c.Int(nullable: false));
            AlterColumn("dbo.CartItems", "ProductId", c => c.Int(nullable: false));
            AlterColumn("dbo.Feedbacks", "ParentId", c => c.Int(nullable: false));
            AlterColumn("dbo.Orders", "CartItemId", c => c.Int(nullable: false));
            AlterColumn("dbo.Transactions", "OrderId", c => c.Int(nullable: false));
            AlterColumn("dbo.Transactions", "ParentId", c => c.Int(nullable: false));
            AlterColumn("dbo.TuitionFees", "ClassId", c => c.Int(nullable: false));
            AlterColumn("dbo.TuitionFees", "StudentId", c => c.Int(nullable: false));
            CreateIndex("dbo.CartItems", "ProductId");
            CreateIndex("dbo.CartItems", "ParentId");
            CreateIndex("dbo.Feedbacks", "ParentId");
            CreateIndex("dbo.Orders", "CartItemId");
            CreateIndex("dbo.Transactions", "ParentId");
            CreateIndex("dbo.Transactions", "OrderId");
            CreateIndex("dbo.TuitionFees", "StudentId");
            CreateIndex("dbo.TuitionFees", "ClassId");
            AddForeignKey("dbo.CartItems", "ParentId", "dbo.Parents", "ParentId");//, cascadeDelete: true
            AddForeignKey("dbo.CartItems", "ProductId", "dbo.Products", "ProductId");//, cascadeDelete: true
            AddForeignKey("dbo.Feedbacks", "ParentId", "dbo.Parents", "ParentId");//, cascadeDelete: true
            AddForeignKey("dbo.Orders", "CartItemId", "dbo.CartItems", "ItemId");//, cascadeDelete: true
            AddForeignKey("dbo.Transactions", "OrderId", "dbo.Orders", "OrderId");//, cascadeDelete: true
            AddForeignKey("dbo.Transactions", "ParentId", "dbo.Parents", "ParentId");//, cascadeDelete: true
            AddForeignKey("dbo.TuitionFees", "ClassId", "dbo.Classes", "ClassId");//, cascadeDelete: true
            AddForeignKey("dbo.TuitionFees", "StudentId", "dbo.Students", "StudentId");//, cascadeDelete: true
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TuitionFees", "StudentId", "dbo.Students");
            DropForeignKey("dbo.TuitionFees", "ClassId", "dbo.Classes");
            DropForeignKey("dbo.Transactions", "ParentId", "dbo.Parents");
            DropForeignKey("dbo.Transactions", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.Orders", "CartItemId", "dbo.CartItems");
            DropForeignKey("dbo.Feedbacks", "ParentId", "dbo.Parents");
            DropForeignKey("dbo.CartItems", "ProductId", "dbo.Products");
            DropForeignKey("dbo.CartItems", "ParentId", "dbo.Parents");
            DropIndex("dbo.TuitionFees", new[] { "ClassId" });
            DropIndex("dbo.TuitionFees", new[] { "StudentId" });
            DropIndex("dbo.Transactions", new[] { "OrderId" });
            DropIndex("dbo.Transactions", new[] { "ParentId" });
            DropIndex("dbo.Orders", new[] { "CartItemId" });
            DropIndex("dbo.Feedbacks", new[] { "ParentId" });
            DropIndex("dbo.CartItems", new[] { "ParentId" });
            DropIndex("dbo.CartItems", new[] { "ProductId" });
            AlterColumn("dbo.TuitionFees", "StudentId", c => c.Int());
            AlterColumn("dbo.TuitionFees", "ClassId", c => c.Int());
            AlterColumn("dbo.Transactions", "ParentId", c => c.Int());
            AlterColumn("dbo.Transactions", "OrderId", c => c.Int());
            AlterColumn("dbo.Orders", "CartItemId", c => c.Int());
            AlterColumn("dbo.Feedbacks", "ParentId", c => c.Int());
            AlterColumn("dbo.CartItems", "ProductId", c => c.Int());
            AlterColumn("dbo.CartItems", "ParentId", c => c.Int());
            RenameIndex(table: "dbo.Students", name: "IX_ApplicationUserId", newName: "IX_ApplicationUser_Id");
            RenameIndex(table: "dbo.Employees", name: "IX_ApplicationUserId", newName: "IX_ApplicationUser_Id");
            RenameIndex(table: "dbo.Parents", name: "IX_ApplicationUserId", newName: "IX_ApplicationUser_Id");
            RenameColumn(table: "dbo.TuitionFees", name: "StudentId", newName: "StudentId_StudentId");
            RenameColumn(table: "dbo.TuitionFees", name: "ClassId", newName: "ClassId_ClassId");
            RenameColumn(table: "dbo.Transactions", name: "ParentId", newName: "Parent_ParentId");
            RenameColumn(table: "dbo.Transactions", name: "OrderId", newName: "Order_OrderId");
            RenameColumn(table: "dbo.Orders", name: "CartItemId", newName: "CartItem_ItemId");
            RenameColumn(table: "dbo.Students", name: "ApplicationUserId", newName: "ApplicationUser_Id");
            RenameColumn(table: "dbo.Feedbacks", name: "ParentId", newName: "Parent_ParentId");
            RenameColumn(table: "dbo.Employees", name: "ApplicationUserId", newName: "ApplicationUser_Id");
            RenameColumn(table: "dbo.Parents", name: "ApplicationUserId", newName: "ApplicationUser_Id");
            RenameColumn(table: "dbo.CartItems", name: "ProductId", newName: "Product_ProductId");
            RenameColumn(table: "dbo.CartItems", name: "ParentId", newName: "Parent_ParentId");
            CreateIndex("dbo.TuitionFees", "StudentId_StudentId");
            CreateIndex("dbo.TuitionFees", "ClassId_ClassId");
            CreateIndex("dbo.Transactions", "Parent_ParentId");
            CreateIndex("dbo.Transactions", "Order_OrderId");
            CreateIndex("dbo.Orders", "CartItem_ItemId");
            CreateIndex("dbo.Feedbacks", "Parent_ParentId");
            CreateIndex("dbo.CartItems", "Product_ProductId");
            CreateIndex("dbo.CartItems", "Parent_ParentId");
            AddForeignKey("dbo.TuitionFees", "StudentId_StudentId", "dbo.Students", "StudentId");
            AddForeignKey("dbo.TuitionFees", "ClassId_ClassId", "dbo.Classes", "ClassId");
            AddForeignKey("dbo.Transactions", "Parent_ParentId", "dbo.Parents", "ParentId");
            AddForeignKey("dbo.Transactions", "Order_OrderId", "dbo.Orders", "OrderId");
            AddForeignKey("dbo.Orders", "CartItem_ItemId", "dbo.CartItems", "ItemId");
            AddForeignKey("dbo.Feedbacks", "Parent_ParentId", "dbo.Parents", "ParentId");
            AddForeignKey("dbo.CartItems", "Product_ProductId", "dbo.Products", "ProductId");
            AddForeignKey("dbo.CartItems", "Parent_ParentId", "dbo.Parents", "ParentId");
        }
    }
}
