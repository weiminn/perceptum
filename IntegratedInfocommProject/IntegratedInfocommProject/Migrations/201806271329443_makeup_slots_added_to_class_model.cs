namespace IntegratedInfocommProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class makeup_slots_added_to_class_model : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Classes", "MakeUpSlots", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Classes", "MakeUpSlots");
        }
    }
}
