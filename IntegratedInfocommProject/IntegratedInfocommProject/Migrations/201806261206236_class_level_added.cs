namespace IntegratedInfocommProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class class_level_added : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Classes", "Level", c => c.String());
            AddColumn("dbo.Employees", "Department", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Employees", "Department");
            DropColumn("dbo.Classes", "Level");
        }
    }
}
