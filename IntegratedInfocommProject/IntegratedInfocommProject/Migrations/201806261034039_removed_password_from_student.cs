namespace IntegratedInfocommProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removed_password_from_student : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Students", "Password");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Students", "Password", c => c.String(nullable: false, maxLength: 40));
        }
    }
}
