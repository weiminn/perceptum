namespace IntegratedInfocommProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_makeupclasses_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MakeUpClasses",
                c => new
                    {
                        ClassId = c.Int(nullable: false),
                        StudentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ClassId, t.StudentId })
                .ForeignKey("dbo.Classes", t => t.ClassId, cascadeDelete: true)
                .ForeignKey("dbo.Students", t => t.StudentId, cascadeDelete: true)
                .Index(t => t.ClassId)
                .Index(t => t.StudentId);
            
            AddColumn("dbo.Classes", "MakeUpSlots", c => c.Int(nullable: false));
            AlterColumn("dbo.Grades", "Month", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MakeUpClasses", "StudentId", "dbo.Students");
            DropForeignKey("dbo.MakeUpClasses", "ClassId", "dbo.Classes");
            DropIndex("dbo.MakeUpClasses", new[] { "StudentId" });
            DropIndex("dbo.MakeUpClasses", new[] { "ClassId" });
            AlterColumn("dbo.Grades", "Month", c => c.String());
            DropColumn("dbo.Classes", "MakeUpSlots");
            DropTable("dbo.MakeUpClasses");
        }
    }
}
