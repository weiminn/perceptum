namespace IntegratedInfocommProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class employeestudentparenttableedited : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Leaves",
                c => new
                    {
                        LeaveId = c.Int(nullable: false, identity: true),
                        EmployeeId = c.Int(nullable: false),
                        Reason = c.String(),
                        Details = c.String(),
                        DayRequested = c.String(),
                        Approval = c.String(),
                        DateApproved = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.LeaveId)
                .ForeignKey("dbo.Employees", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.EmployeeId);
            
            AddColumn("dbo.Parents", "Password", c => c.String());
            AddColumn("dbo.Parents", "Telephone", c => c.Int(nullable: false));
            AddColumn("dbo.Employees", "Password", c => c.String());
            AddColumn("dbo.Students", "Password", c => c.String());
            AddColumn("dbo.Students", "School", c => c.String());
            DropColumn("dbo.Parents", "sTelephone");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Parents", "sTelephone", c => c.Int(nullable: false));
            DropForeignKey("dbo.Leaves", "EmployeeId", "dbo.Employees");
            DropIndex("dbo.Leaves", new[] { "EmployeeId" });
            DropColumn("dbo.Students", "School");
            DropColumn("dbo.Students", "Password");
            DropColumn("dbo.Employees", "Password");
            DropColumn("dbo.Parents", "Telephone");
            DropColumn("dbo.Parents", "Password");
            DropTable("dbo.Leaves");
        }
    }
}
