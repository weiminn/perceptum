namespace IntegratedInfocommProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addedsublevelfieldtostudentstable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Students", "SubLevel", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Students", "SubLevel");
        }
    }
}
