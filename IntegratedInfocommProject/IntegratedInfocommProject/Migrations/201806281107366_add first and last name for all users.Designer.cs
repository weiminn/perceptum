// <auto-generated />
namespace IntegratedInfocommProject.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class addfirstandlastnameforallusers : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addfirstandlastnameforallusers));
        
        string IMigrationMetadata.Id
        {
            get { return "201806281107366_add first and last name for all users"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
