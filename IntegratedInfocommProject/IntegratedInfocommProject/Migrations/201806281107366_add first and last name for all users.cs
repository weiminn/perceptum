namespace IntegratedInfocommProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addfirstandlastnameforallusers : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "UserName", c => c.String());
            AddColumn("dbo.Customers", "FirstName", c => c.String());
            AddColumn("dbo.Customers", "LastName", c => c.String());
            AddColumn("dbo.Employees", "UserName", c => c.String());
            AddColumn("dbo.Employees", "FirstName", c => c.String());
            AddColumn("dbo.Employees", "LastName", c => c.String());
            AddColumn("dbo.Parents", "UserName", c => c.String());
            AddColumn("dbo.Parents", "FirstName", c => c.String());
            AddColumn("dbo.Parents", "LastName", c => c.String());
            AddColumn("dbo.Students", "UserName", c => c.String());
            AddColumn("dbo.Students", "FirstName", c => c.String());
            AddColumn("dbo.Students", "LastName", c => c.String());
            DropColumn("dbo.Customers", "Name");
            DropColumn("dbo.Employees", "Name");
            DropColumn("dbo.Parents", "Name");
            DropColumn("dbo.Students", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Students", "Name", c => c.String());
            AddColumn("dbo.Parents", "Name", c => c.String());
            AddColumn("dbo.Employees", "Name", c => c.String());
            AddColumn("dbo.Customers", "Name", c => c.String());
            DropColumn("dbo.Students", "LastName");
            DropColumn("dbo.Students", "FirstName");
            DropColumn("dbo.Students", "UserName");
            DropColumn("dbo.Parents", "LastName");
            DropColumn("dbo.Parents", "FirstName");
            DropColumn("dbo.Parents", "UserName");
            DropColumn("dbo.Employees", "LastName");
            DropColumn("dbo.Employees", "FirstName");
            DropColumn("dbo.Employees", "UserName");
            DropColumn("dbo.Customers", "LastName");
            DropColumn("dbo.Customers", "FirstName");
            DropColumn("dbo.Customers", "UserName");
        }
    }
}
