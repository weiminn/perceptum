namespace IntegratedInfocommProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedSubLevelfieldforClass : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Classes", "SubLevel", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Classes", "SubLevel");
        }
    }
}
