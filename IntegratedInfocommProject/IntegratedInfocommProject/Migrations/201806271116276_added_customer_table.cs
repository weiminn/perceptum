namespace IntegratedInfocommProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_customer_table : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CartItems", "ParentId", "dbo.Parents");
            DropForeignKey("dbo.Transactions", "ParentId", "dbo.Parents");
            DropIndex("dbo.CartItems", new[] { "ParentId" });
            DropIndex("dbo.Transactions", new[] { "ParentId" });
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        customerId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Password = c.String(),
                        Telephone = c.Int(nullable: false),
                        Email = c.String(),
                        ApplicationUserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.customerId)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserId)
                .Index(t => t.ApplicationUserId);
            
            AddColumn("dbo.CartItems", "CustomerId", c => c.Int(nullable: false));
            AddColumn("dbo.Transactions", "CustomerId", c => c.Int(nullable: false));
            CreateIndex("dbo.CartItems", "CustomerId");
            CreateIndex("dbo.Transactions", "CustomerId");
            AddForeignKey("dbo.CartItems", "CustomerId", "dbo.Customers", "customerId", cascadeDelete: true);
            AddForeignKey("dbo.Transactions", "CustomerId", "dbo.Customers", "customerId", cascadeDelete: true);
            DropColumn("dbo.CartItems", "ParentId");
            DropColumn("dbo.Transactions", "ParentId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Transactions", "ParentId", c => c.Int(nullable: false));
            AddColumn("dbo.CartItems", "ParentId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Transactions", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.CartItems", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.Customers", "ApplicationUserId", "dbo.AspNetUsers");
            DropIndex("dbo.Transactions", new[] { "CustomerId" });
            DropIndex("dbo.Customers", new[] { "ApplicationUserId" });
            DropIndex("dbo.CartItems", new[] { "CustomerId" });
            DropColumn("dbo.Transactions", "CustomerId");
            DropColumn("dbo.CartItems", "CustomerId");
            DropTable("dbo.Customers");
            CreateIndex("dbo.Transactions", "ParentId");
            CreateIndex("dbo.CartItems", "ParentId");
            AddForeignKey("dbo.Transactions", "ParentId", "dbo.Parents", "ParentId", cascadeDelete: true);
            AddForeignKey("dbo.CartItems", "ParentId", "dbo.Parents", "ParentId", cascadeDelete: true);
        }
    }
}
