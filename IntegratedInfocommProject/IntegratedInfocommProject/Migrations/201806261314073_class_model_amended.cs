namespace IntegratedInfocommProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class class_model_amended : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Classes", "Time", c => c.DateTime(nullable: false));
            DropColumn("dbo.Classes", "MakeUpSlots");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Classes", "MakeUpSlots", c => c.Int(nullable: false));
            AlterColumn("dbo.Classes", "Time", c => c.Time(nullable: false, precision: 7));
        }
    }
}
