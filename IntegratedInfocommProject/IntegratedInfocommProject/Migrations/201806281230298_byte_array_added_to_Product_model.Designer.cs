// <auto-generated />
namespace IntegratedInfocommProject.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class byte_array_added_to_Product_model : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(byte_array_added_to_Product_model));
        
        string IMigrationMetadata.Id
        {
            get { return "201806281230298_byte_array_added_to_Product_model"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
