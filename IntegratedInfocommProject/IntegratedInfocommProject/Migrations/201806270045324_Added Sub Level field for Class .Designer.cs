// <auto-generated />
namespace IntegratedInfocommProject.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddedSubLevelfieldforClass : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddedSubLevelfieldforClass));
        
        string IMigrationMetadata.Id
        {
            get { return "201806270045324_Added Sub Level field for Class "; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
