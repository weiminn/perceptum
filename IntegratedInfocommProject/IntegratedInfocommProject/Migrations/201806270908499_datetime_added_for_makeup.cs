namespace IntegratedInfocommProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class datetime_added_for_makeup : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MakeUpClasses", "dateTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MakeUpClasses", "dateTime");
        }
    }
}
