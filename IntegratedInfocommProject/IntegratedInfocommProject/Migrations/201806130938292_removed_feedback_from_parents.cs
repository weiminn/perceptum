namespace IntegratedInfocommProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removed_feedback_from_parents : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Parents", "FeedbackId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Parents", "FeedbackId", c => c.Int(nullable: false));
        }
    }
}
