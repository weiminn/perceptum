namespace IntegratedInfocommProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DatabaseRelationship5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transactions", "Parent_ParentId", c => c.Int());
            CreateIndex("dbo.Transactions", "Parent_ParentId");
            AddForeignKey("dbo.Transactions", "Parent_ParentId", "dbo.Parents", "ParentId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Transactions", "Parent_ParentId", "dbo.Parents");
            DropIndex("dbo.Transactions", new[] { "Parent_ParentId" });
            DropColumn("dbo.Transactions", "Parent_ParentId");
        }
    }
}
