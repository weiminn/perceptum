namespace IntegratedInfocommProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class datetime_as_composite_key_in_makeup : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.MakeUpClasses");
            AddPrimaryKey("dbo.MakeUpClasses", new[] { "ClassId", "StudentId", "dateTime" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.MakeUpClasses");
            AddPrimaryKey("dbo.MakeUpClasses", new[] { "ClassId", "StudentId" });
        }
    }
}
