namespace IntegratedInfocommProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DatabaseRelationship5 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TuitionFees",
                c => new
                    {
                        FeeId = c.Int(nullable: false, identity: true),
                        Quantity = c.Int(nullable: false),
                        TotalFee = c.Int(nullable: false),
                        ClassId_ClassId = c.Int(),
                        StudentId_StudentId = c.Int(),
                    })
                .PrimaryKey(t => t.FeeId)
                .ForeignKey("dbo.Classes", t => t.ClassId_ClassId)
                .ForeignKey("dbo.Students", t => t.StudentId_StudentId)
                .Index(t => t.ClassId_ClassId)
                .Index(t => t.StudentId_StudentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TuitionFees", "StudentId_StudentId", "dbo.Students");
            DropForeignKey("dbo.TuitionFees", "ClassId_ClassId", "dbo.Classes");
            DropIndex("dbo.TuitionFees", new[] { "StudentId_StudentId" });
            DropIndex("dbo.TuitionFees", new[] { "ClassId_ClassId" });
            DropTable("dbo.TuitionFees");
        }
    }
}
