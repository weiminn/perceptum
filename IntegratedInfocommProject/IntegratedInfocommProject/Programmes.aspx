﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Programmes.aspx.cs" Inherits="IntegratedInfocommProject.Programmesaspx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h3>Here are the available programmes:</h3>

    <div class="row">

        <div class="col-md-6">
            <img style="width:500px;" src="Content/images/Primary%20Math.JPG" />
            <h2>Primary Mathematics</h2>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam consectetur ex elit, sed placerat ipsum convallis sit amet. Morbi metus massa, mollis nec auctor sit amet, ullamcorper sit amet ex. Curabitur molestie, odio sed tristique congue, dui tortor iaculis ligula, ac sollicitudin augue enim id libero. Integer quis mollis dui. Sed faucibus vestibulum erat, et dictum arcu dignissim sed. Donec consequat purus mi, at viverra sapien ornare a. Nullam suscipit sapien interdum lectus imperdiet, non commodo lacus rhoncus. Sed efficitur finibus aliquam. Nulla facilisi. Proin ac lorem congue quam pulvinar lobortis.
            </p>

        </div>

        <div class="col-md-6">
           <img style="width:500px;"src="Content/images/Secondary%20Math.JPG" />

            <h2>Secondary Mathematics</h2>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam consectetur ex elit, sed placerat ipsum convallis sit amet. Morbi metus massa, mollis nec auctor sit amet, ullamcorper sit amet ex. Curabitur molestie, odio sed tristique congue, dui tortor iaculis ligula, ac sollicitudin augue enim id libero. Integer quis mollis dui. Sed faucibus vestibulum erat, et dictum arcu dignissim sed. Donec consequat purus mi, at viverra sapien ornare a. Nullam suscipit sapien interdum lectus imperdiet, non commodo lacus rhoncus. Sed efficitur finibus aliquam. Nulla facilisi. Proin ac lorem congue quam pulvinar lobortis.
            </p>
            
        </div>
    </div>

    <div class="row">

        <div class="col-md-6">
            <img style="width:500px;" src="Content/images/Primary%20Science.jpg" />
            <h2>Primary Science</h2>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam consectetur ex elit, sed placerat ipsum convallis sit amet. Morbi metus massa, mollis nec auctor sit amet, ullamcorper sit amet ex. Curabitur molestie, odio sed tristique congue, dui tortor iaculis ligula, ac sollicitudin augue enim id libero. Integer quis mollis dui. Sed faucibus vestibulum erat, et dictum arcu dignissim sed. Donec consequat purus mi, at viverra sapien ornare a. Nullam suscipit sapien interdum lectus imperdiet, non commodo lacus rhoncus. Sed efficitur finibus aliquam. Nulla facilisi. Proin ac lorem congue quam pulvinar lobortis.
            </p>
          
        </div>

        <div class="col-md-6">
        <img style="width:500px;"src="Content/images/Secondary%20Science%20.jpg" />

            <h2>Secondary Science</h2>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam consectetur ex elit, sed placerat ipsum convallis sit amet. Morbi metus massa, mollis nec auctor sit amet, ullamcorper sit amet ex. Curabitur molestie, odio sed tristique congue, dui tortor iaculis ligula, ac sollicitudin augue enim id libero. Integer quis mollis dui. Sed faucibus vestibulum erat, et dictum arcu dignissim sed. Donec consequat purus mi, at viverra sapien ornare a. Nullam suscipit sapien interdum lectus imperdiet, non commodo lacus rhoncus. Sed efficitur finibus aliquam. Nulla facilisi. Proin ac lorem congue quam pulvinar lobortis.
            </p>
           
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title">Program Details</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam consectetur ex elit, sed placerat ipsum convallis sit amet. Morbi metus massa, mollis nec auctor sit amet, ullamcorper sit amet ex. Curabitur molestie, odio sed tristique congue, dui tortor iaculis ligula, ac sollicitudin augue enim id libero. Integer quis mollis dui. Sed faucibus vestibulum erat, et dictum arcu dignissim sed. Donec consequat purus mi, at viverra sapien ornare a. Nullam suscipit sapien interdum lectus imperdiet, non commodo lacus rhoncus. Sed efficitur finibus aliquam. Nulla facilisi. Proin ac lorem congue quam pulvinar lobortis.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>


</div>
    </div>
    </div>
</asp:Content>
