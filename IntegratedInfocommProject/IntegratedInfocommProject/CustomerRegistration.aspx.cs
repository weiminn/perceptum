﻿using IntegratedInfocommProject.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IntegratedInfocommProject
{
    public partial class CustomerRegistration1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            using (var db = new ApplicationDbContext())
            {
                // Default UserStore constructor uses the default connection string named: DefaultConnection

                var userStore = new UserStore<IdentityUser>();
                var manager = new UserManager<IdentityUser>(userStore);

                // Need to create Identity user object
                var user = new IdentityUser() { UserName = txtUserId.Text };
                IdentityResult result = manager.Create(user, txtPassword.Text);

                if (result.Succeeded)
                {
                    Customer newCustomer = new Customer
                    {
                        UserName = txtUserId.Text,
                        FirstName = txtFirstName.Text,
                        LastName = txtLastName.Text,
                        Password = txtPassword.Text,
                        Email = txtEmail.Text,
                        Telephone = Convert.ToInt32(txtTel.Text),
                        ApplicationUserId = user.Id
                    };

                    if (ModelState.IsValid)
                    {
                        db.Customers.Add(newCustomer);
                        db.SaveChanges();
                    }
                }

            }
        }
    }
}