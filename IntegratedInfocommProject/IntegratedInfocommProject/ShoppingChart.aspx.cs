﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using IntegratedInfocommProject.Models;
using Microsoft.AspNet.Identity;

namespace IntegratedInfocommProject
{
    public partial class ShoppingChart : System.Web.UI.Page
    {
        //private string userId = User.Identity.GetUserId(); var currentCustomer = db.Customers.SingleOrDefault(x => x.ApplicationUserId == userId);

        private Customer currentCustomer = new Customer();
        protected void Page_Load(object sender, EventArgs e)
        {
            using (var db = new ApplicationDbContext())
            {
                string userId = User.Identity.GetUserId();
                currentCustomer = db.Customers.SingleOrDefault(x => x.ApplicationUserId == userId);

                var cartItems = db.CartItems.Where(x => x.CustomerId == currentCustomer.customerId).ToList();
                List<Product> cartDisplay = new List<Product>();
                foreach(var item in cartItems)
                {
                    Product theItem = db.Products.SingleOrDefault(x => x.ProductId == item.ProductId);
                    cartDisplay.Add(theItem);
                }

                cartGrid.DataSource = cartDisplay;
                cartGrid.DataBind();
            }
        }

        protected void btnCheckOut_Click(object sender, EventArgs e)
        {

        }
    }
}