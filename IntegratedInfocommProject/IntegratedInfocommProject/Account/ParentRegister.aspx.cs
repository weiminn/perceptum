﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using IntegratedInfocommProject.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web.Providers.Entities;

namespace IntegratedInfocommProject.Account
{
    public partial class Register : Page
    {

        private static readonly string scriptSuccessNewAccount =
             "<script language=\"javascript\">\n" +
             "alert (\"Your account has been succesfully created - Thank You!\");\n" +
             "</script>";
        private static readonly string scriptErrorNewAccount =
            "<script language=\"javascript\">\n" +
            "alert (\"Your account has been succesfully created - Thank You!\");\n" +
            "</script>";

        protected void CreateUser_Click(object sender, EventArgs e)
        {
            Type csType = this.GetType();

            using (var db = new ApplicationDbContext())
            {
                // Default UserStore constructor uses the default connection string named: DefaultConnection

                var userStore = new UserStore<IdentityUser>();
                var manager = new UserManager<IdentityUser>(userStore);

                // Need to create Identity user object
                var user = new IdentityUser() { UserName = UserName.Text };
                IdentityResult result = manager.Create(user, Password.Text);

                if (result.Succeeded)
                {
                    Parent newParent = new Parent
                    {
                        UserName = UserName.Text,
                        FirstName = FirstName.Text,
                        LastName = LastName.Text,

                        Email = Email.Text,
                        Telephone = Convert.ToInt32(Telephone.Text),
                        Password= Password.Text,
                        ApplicationUserId = user.Id
                    };

                    if (ModelState.IsValid)
                    {
                        db.Parents.Add(newParent);
                        db.SaveChanges();

                        ClientScript.RegisterStartupScript(csType, "Success", scriptSuccessNewAccount);

                        var autheticationmanager = HttpContext.Current.GetOwinContext().Authentication;
                        var userIdentity = manager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
                        autheticationmanager.SignIn(new Microsoft.Owin.Security.AuthenticationProperties() { }, userIdentity);
                        Response.Redirect("~/Login");

                    }

                    
               
                }


                else
                {

                    ClientScript.RegisterStartupScript(csType, "Error", scriptErrorNewAccount);
                }

             Session["PName"] = (string)UserName.Text;
             Session["PPassword"] = (string)Password.Text;
             Session["PEmail"] = (string)Email.Text;
             Session["PTelephone"] = (string)Telephone.Text;


            }


            

            

            
            
           

            
        }
    }
}
