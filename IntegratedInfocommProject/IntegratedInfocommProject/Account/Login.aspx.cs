﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Owin;
using IntegratedInfocommProject.Models;

namespace IntegratedInfocommProject.Account
{
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (User.Identity.IsAuthenticated)
                {

                    StatusText.Text = string.Format("Welcome" + User.Identity.GetUserName() + "!");
                    Session["sFlag"] = "T"; //sFlag = "T" means user has logged in 
                    
                    LoginStatus.Visible = true;
                    LogoutButton.Visible = true;
                }

                else
                {
                    LoginForm.Visible = true;
                }
            }

        }

        // insert forget password - sent to email 
        protected void LogIn(object sender, EventArgs e)
        {
            var userStore = new UserStore<IdentityUser>();
            var userManager = new UserManager<IdentityUser>(userStore);
            var user = userManager.Find(UserName.Text, Password.Text);
            Session["useridentity"] = (string)userManager;




       if (user != null)
            {

                var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
                var userIdentity = userManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);

                authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = false }, userIdentity);


                Response.Redirect("~/Default.aspx");
            }
            else
            {
                Session["sFlag"] = "F";
                StatusText.Text = "Invalid username or password.";
                LoginStatus.Visible = true;
            }

        }
        protected void SignOut(object sender, EventArgs e)
        {
            var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
            authenticationManager.SignOut();
            Response.Redirect("~/Login.aspx");
        }
    }
            }
        
