﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="IntegratedInfocommProject.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row" style="width:100%; margin:auto; overflow:hidden;">
            <h3>What Others Say About Us</h3>
           <img src="Content/images/Children-Reading-cropped-v2.jpg" />
        </div>

    <div class="row">
        <div class="col-md-4" >
            <h2>Who We Are</h2>
            <img  style="width:350px" height:"300px" src="Content/images/AboutUs1.jpg" />
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam consectetur ex elit, sed placerat ipsum convallis sit amet. Morbi metus massa, mollis nec auctor sit amet, ullamcorper sit amet ex. Curabitur molestie, odio sed tristique congue, dui tortor iaculis ligula, ac sollicitudin augue enim id libero. Integer quis mollis dui. Sed faucibus vestibulum erat, et dictum arcu dignissim sed. Donec consequat purus mi, at viverra sapien ornare a. Nullam suscipit sapien interdum lectus imperdiet, non commodo lacus rhoncus. Sed efficitur finibus aliquam. Nulla facilisi. Proin ac lorem congue quam pulvinar lobortis.
            </p>
            <p>
                <a class="btn btn-default" href="#">Learn more &raquo;</a>
            </p>
        </div>

        <div class="col-md-4" >
            <h2>Our Mission</h2>
            <img style="width:350px" height:"325px" src="Content/images/AboutUs2.jpg" />

            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam consectetur ex elit, sed placerat ipsum convallis sit amet. Morbi metus massa, mollis nec auctor sit amet, ullamcorper sit amet ex. Curabitur molestie, odio sed tristique congue, dui tortor iaculis ligula, ac sollicitudin augue enim id libero. Integer quis mollis dui. Sed faucibus vestibulum erat, et dictum arcu dignissim sed. Donec consequat purus mi, at viverra sapien ornare a. Nullam suscipit sapien interdum lectus imperdiet, non commodo lacus rhoncus. Sed efficitur finibus aliquam. Nulla facilisi. Proin ac lorem congue quam pulvinar lobortis.            </p>
            <p>
                <a class="btn btn-default" href="#">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>What We Do</h2>
           <img style="width:350px" height:"350px" src="Content/images/AboutUs3.jpg" />

            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam consectetur ex elit, sed placerat ipsum convallis sit amet. Morbi metus massa, mollis nec auctor sit amet, ullamcorper sit amet ex. Curabitur molestie, odio sed tristique congue, dui tortor iaculis ligula, ac sollicitudin augue enim id libero. Integer quis mollis dui. Sed faucibus vestibulum erat, et dictum arcu dignissim sed. Donec consequat purus mi, at viverra sapien ornare a. Nullam suscipit sapien interdum lectus imperdiet, non commodo lacus rhoncus. Sed efficitur finibus aliquam. Nulla facilisi. Proin ac lorem congue quam pulvinar lobortis.
            </p>
            <p>
                <a class="btn btn-default" href="#">Learn more &raquo;</a>
            </p>
        </div>
    </div>



    <div class="row">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner"
              style="height: 200px;max-height: 200px;">

            <div class="item col-md-6 col-md-offset-3"
                style="width:auto; margin:auto;">

                <h2>Mr and Mrs Lee</h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam consectetur ex elit, sed placerat ipsum convallis sit amet. Morbi metus massa, mollis nec auctor sit amet, ullamcorper sit amet ex. Curabitur molestie, odio sed tristique congue, dui tortor iaculis ligula, ac sollicitudin augue enim id libero. Integer quis mollis dui. Sed faucibus vestibulum erat, et dictum arcu dignissim sed. Donec consequat purus mi, at viverra sapien ornare a. Nullam suscipit sapien interdum lectus imperdiet, non commodo lacus rhoncus. Sed efficitur finibus aliquam. Nulla facilisi. Proin ac lorem congue quam pulvinar lobortis.
                </p>  
            </div>

            <div class="item col-md-6 col-md-offset-3" style="width:auto; margin:auto;">
              <h2>Mr and Mrs Ng</h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam consectetur ex elit, sed placerat ipsum convallis sit amet. Morbi metus massa, mollis nec auctor sit amet, ullamcorper sit amet ex. Curabitur molestie, odio sed tristique congue, dui tortor iaculis ligula, ac sollicitudin augue enim id libero. Integer quis mollis dui. Sed faucibus vestibulum erat, et dictum arcu dignissim sed. Donec consequat purus mi, at viverra sapien ornare a. Nullam suscipit sapien interdum lectus imperdiet, non commodo lacus rhoncus. Sed efficitur finibus aliquam. Nulla facilisi. Proin ac lorem congue quam pulvinar lobortis.
                </p>
            </div>
          </div>

          <!-- Left and right controls -->
          <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-5">
            <a href="3" class="btn btn-success">
                View All Testimonials
            </a>
        </div>
    </div>
</asp:Content>
