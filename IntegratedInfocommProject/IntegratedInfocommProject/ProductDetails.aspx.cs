﻿using IntegratedInfocommProject.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;


namespace IntegratedInfocommProject
{
    public partial class ProductDetails : System.Web.UI.Page
    {

        static readonly string scriptErrorLogin = "<script language=\"javascript\">\n" +
       "alert (\"Please login or create account first to facilitate buying\");\n</script>";

        protected void Page_Load(object sender, EventArgs e)
        {

            if ((string)Session["sFlag"] != "T")
            {
                lblShoppingCart.Text = "You have to login first to see your Shopping Cart";
            }

          

        }

        protected void AddToCart_Click(object sender, EventArgs e)
        {

            //declare variables
            string strProductID, strSQLSelect, strSQL, strcartitem, strdatetime, stritemid;
            int intQuantityOnHand, intBuyQuantity, newQty, intOrderNo;
            decimal decUnitPrice;

            SqlConnection con = new SqlConnection();

            con.ConnectionString = (@"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=aspnet-IntegratedInfocommProject;Trusted_Connection=Yes;User ID=Perceptum2018;Password=p12345;Integrated Security=True;");
            con.Open();
            SqlCommand cmd;

            GridViewRow row0 = Product.Rows[0];
            strProductID = row0.Cells[0].Text;
            //extract the QuantityOnHand from the database - based on the product ID

            strSQLSelect = "SELECT QuantityOnHand FROM Products WHERE ProductID = @ProductID";
            cmd = new SqlCommand(strSQLSelect, con);
            cmd.Parameters.Add("@ProductID", SqlDbType.Int).Value = strProductID;
            object oQty = cmd.ExecuteScalar();
            intQuantityOnHand = (int)oQty;

            //extract the Price from the database - based on the product ID

            strSQLSelect = "SELECT PromotionPrice FROM Products WHERE ProductID = @ProductID";
            cmd = new SqlCommand(strSQLSelect, con);
            cmd.Parameters.Add("@ProductID", SqlDbType.Int).Value = strProductID;
            object oUnitPrice = cmd.ExecuteScalar();
            decUnitPrice = (int)oUnitPrice;

            //extract quantity purchased from dropdown list
            intBuyQuantity = int.Parse(ddlQty.Items[ddlQty.SelectedIndex].ToString());

            //calculate the new quantity left 
            newQty = intQuantityOnHand - intBuyQuantity;

            if (intQuantityOnHand < intBuyQuantity)
            {
                Type csType = this.GetType();
                con.Close();
                ClientScript.RegisterStartupScript(csType, "StockOut", scriptStockOut);

                // Add the send email function here
                var sbOutofStockMessage = "The product " + strProductID + " is out of stock!";
                string sendEmail = ConfigurationManager.AppSettings["SendEmail"];
                SendEmail(sbOutofStockMessage.ToString());


                return;
            }

            Session["sProductID"] = strProductID;
            Session["sUnitPrice"] = decUnitPrice.ToString();
            Session["sQuantity"] = newQty.ToString();
            //creation of new order 

            intOrderNo = (int)Session["sOrderNo"];
            strcartitem = (string)Session["CartItemId"];
            stritemid = (string)Session["itemid"];

            DateTime date = DateTime.Now; // will give the date for today
            strdatetime = date.ToLongDateString();

            //insert into orders 
            strSQL = "INSERT INTO Orders(OrderId, DateTime, Quantity, UnitPrice, CartItemId)"
                 + "VALUES (@OrderId, @ProductID, @Qty, @UnitPrice, CartItemId)";
            cmd = new SqlCommand(strSQL, con);
            cmd.Parameters.Add("@OrderId", SqlDbType.Int).Value = intOrderNo; //Check
            cmd.Parameters.Add("@DateTime", SqlDbType.VarChar).Value = strdatetime;
            cmd.Parameters.Add("@Quantity", SqlDbType.Int).Value = intBuyQuantity;
            cmd.Parameters.Add("@UnitPrice", SqlDbType.Money).Value = decUnitPrice;
            cmd.Parameters.Add("@CartItemId", SqlDbType.Money).Value = strcartitem;
            cmd.ExecuteNonQuery();

            //insert into cartitems 
            strSQL = "INSERT INTO CartItems(ItemId, ProductId, CustomerId)"
                 + "VALUES (@ItemId, @ProductId, @CustomerId)";
            cmd = new SqlCommand(strSQL, con);
            cmd.Parameters.Add("@ItemId", SqlDbType.Int).Value = stritemid;
            cmd.Parameters.Add("@ProductId", SqlDbType.VarChar).Value = strProductID;
            cmd.Parameters.Add("@CustomerId", SqlDbType.Money).Value = ();
            cmd.ExecuteNonQuery();


            strSQL = "UPDATE Products SET QuantityOnHand = @NewQty WHERE ProductId = @ProductID";
            cmd = new SqlCommand(strSQL, con);
            cmd.Parameters.Add("@NewQty", SqlDbType.Int).Value = newQty;
            cmd.Parameters.Add("@ProductID", SqlDbType.VarChar).Value = strProductID;
            cmd.ExecuteNonQuery();                     

            con.Close();

            Response.Redirect("ShoppingCart.aspx");


        }
    }
}