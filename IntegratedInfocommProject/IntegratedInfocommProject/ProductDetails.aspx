﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductDetails.aspx.cs" Inherits="IntegratedInfocommProject.ProductDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <h1><asp:Label runat="server" ID="ProductName"></asp:Label></h1><br />
    <h2>Details</h2>
    <asp:Label runat="server" ID="ProductDetail"></asp:Label><br />
    <h2>Normal Price</h2>
    <asp:Label runat="server" ID="NormalPrice"></asp:Label><br />
    <h2>Promotion Price</h2>
    <asp:Label runat="server" ID="PromoPrice"></asp:Label>

    <asp:Label ID="lblShoppingCart" runat="server" Text="Label"></asp:Label>

    <br /> 
    <asp:Button CssClass="btn btn-default" runat="server" Text="Add to Cart" ID="AddToCart" OnClick="AddToCart_Click"/>


    
</asp:Content>
