﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IntegratedInfocommProject.Startup))]
namespace IntegratedInfocommProject
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
