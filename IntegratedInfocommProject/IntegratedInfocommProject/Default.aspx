﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="IntegratedInfocommProject._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

   

        <div id="myCarousel" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner">
            <div class="item active col-md-6 col-md-offset-3 img-responsive"
                style="width:auto;
                          height: 550px;
                          max-height: 550px;
                margin:auto;">
                <img class="center-block" src="Content/images/maxresdefault.jpg" alt="First" />
            </div>

            <div class="item col-md-6 col-md-offset-3 img-responsive"
                style="  width: auto;
                          height: 550px;
                          max-height: 550px;
                margin:auto;">
                <img class="center-block" src="Content/images/SchoolChildren.png" alt="Second" />
            </div>
            <div class="item col-md-6 col-md-offset-3 img-responsive"
                style="  width: auto;
                          height: 550px;
                          max-height: 550px;
                margin:auto;">
                <img class="center-block" src="Content/images/banner_2.png" alt="Third" />
            </div>


          </div>

          <!-- Left and right controls -->
          <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      
        

   <div class="row">
        <div class="col-md-4">
            
            <h2>Programmes</h2>
            <img class="img-responsive" src="Content/images/programmes.JPG" />
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam consectetur ex elit, sed placerat ipsum convallis sit amet. Morbi metus massa, mollis nec auctor sit amet, ullamcorper sit amet ex. Curabitur molestie, odio sed tristique congue, dui tortor iaculis ligula, ac sollicitudin augue enim id libero. Integer quis mollis dui. Sed faucibus vestibulum erat, et dictum arcu dignissim sed. Donec consequat purus mi, at viverra sapien ornare a. Nullam suscipit sapien interdum lectus imperdiet, non commodo lacus rhoncus. Sed efficitur finibus aliquam. Nulla facilisi. Proin ac lorem congue quam pulvinar lobortis.
            </p>
            <p>
                <a class="btn btn-default" href="#">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Experience</h2>
            <img class="img-responsive" src="Content/images/TLL experience.JPG" />

            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam consectetur ex elit, sed placerat ipsum convallis sit amet. Morbi metus massa, mollis nec auctor sit amet, ullamcorper sit amet ex. Curabitur molestie, odio sed tristique congue, dui tortor iaculis ligula, ac sollicitudin augue enim id libero. Integer quis mollis dui. Sed faucibus vestibulum erat, et dictum arcu dignissim sed. Donec consequat purus mi, at viverra sapien ornare a. Nullam suscipit sapien interdum lectus imperdiet, non commodo lacus rhoncus. Sed efficitur finibus aliquam. Nulla facilisi. Proin ac lorem congue quam pulvinar lobortis.            </p>
            <p>
                <a class="btn btn-default" href="#">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Register</h2>
            <img class="img-responsive" src="Content/images/Register.JPG" />

            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam consectetur ex elit, sed placerat ipsum convallis sit amet. Morbi metus massa, mollis nec auctor sit amet, ullamcorper sit amet ex. Curabitur molestie, odio sed tristique congue, dui tortor iaculis ligula, ac sollicitudin augue enim id libero. Integer quis mollis dui. Sed faucibus vestibulum erat, et dictum arcu dignissim sed. Donec consequat purus mi, at viverra sapien ornare a. Nullam suscipit sapien interdum lectus imperdiet, non commodo lacus rhoncus. Sed efficitur finibus aliquam. Nulla facilisi. Proin ac lorem congue quam pulvinar lobortis.
            </p>
            <p>
                <a class="btn btn-default" href="#">Learn more &raquo;</a>
            </p>
        </div>
    </div>

</asp:Content>
