﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ShoppingCart.aspx.cs" Inherits="IntegratedInfocommProject.ShoppingChart" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <style type="text/css">
    .auto-style1 {
        width: 90%;
    }
        .auto-style2 {
            margin-top: 0px;
        }
    </style>
    <h1>Cart Items</h1>
    
    <div id="ShoppingCartdiv"> 
<table id="CartTable" class="auto-style1" style="text-align: center">
    <tr>
        <td>
            <asp:Label ID="lblShoppingCart" runat="server" Text="Your Shopping Cart"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataKeyNames="oOrderNo" DataSourceID="SqlDataSource1" Height="208px" Width="1485px">
                <Fields>
                    <asp:BoundField DataField="oOrderNo" HeaderText="oOrderNo" InsertVisible="False" ReadOnly="True" SortExpression="oOrderNo" />
                    <asp:BoundField DataField="oDate" HeaderText="oDate" SortExpression="oDate" />
                </Fields>
            </asp:DetailsView>
        </td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="DataGrid1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource2" Width="100%" CssClass="auto-style2">
                <Columns>
                    <asp:BoundField DataField="iOrderNo" HeaderText="iOrderNo" SortExpression="iOrderNo" />
                    <asp:BoundField DataField="iProductId" HeaderText="iProductId" SortExpression="iProductId" />
                    <asp:BoundField DataField="ProductName" HeaderText="ProductName" SortExpression="ProductName" />
                    <asp:BoundField DataField="iQty" HeaderText="iQty" SortExpression="iQty" />
                    <asp:BoundField DataField="iUnitPrice" HeaderText="iUnitPrice" SortExpression="iUnitPrice" />
                    <asp:BoundField DataField="iAmt" HeaderText="iAmt" ReadOnly="True" SortExpression="iAmt" />
                </Columns>
            </asp:GridView>
        </td>
    </tr>
    <tr>
        <td>
            <asp:DetailsView ID="DetailsView2" runat="server" AutoGenerateRows="False" DataSourceID="SqlDataSource3" Height="50px" Width="100%">
                <Fields>
                    <asp:BoundField DataField="Amount" HeaderText="Amount" SortExpression="Amount" />
                    <asp:BoundField DataField="GST" HeaderText="GST" ReadOnly="True" SortExpression="GST" />
                    <asp:BoundField DataField="Total" HeaderText="Total" ReadOnly="True" SortExpression="Total" />
                </Fields>
            </asp:DetailsView>
        </td>
    </tr>

    <table>
                <tr>
                    <td rowspan="3" style="padding-left: 20px;">
                        <br />
                        <!--Radio Button List Start-->
                        <h4><b>2. DELIVERY METHODS</b> &nbsp &nbsp
                                                       
                            
                        </h4>
                        &nbsp;<asp:RadioButton ID="Ninja" runat="server" Text="POP Stations" Font-Size="Smaller"/>
                        <br />
                        <p>
                            <asp:Label ID="NinjaLabel" runat="server" Text=" 
                                                                
              Please collect your parcel within 48 hours from arrival at the location."
                                Width="500px"></asp:Label>
                        </p>
                        <p>&nbsp;</p>
                        <p>
                            <asp:RadioButton ID="Standard" runat="server" Text="Standard Courier"/>
                            &nbsp &nbsp
                    <asp:Label ID="StandardCourierLabel" runat="server" Text="Delivery time: 2-5 business days upon dispatch
                        "
                        Width="500px"></asp:Label>


                        </p>
                        <p>
                            &nbsp;
                        </p>
                        <p>
                            <asp:RadioButton ID="Express" runat="server" Text="Express Courier"/>
                            <asp:Label ID="ExpressCourierLabel" runat="server" Text="Delivery time: 1-3 business days upon dispatch
                        "
                                Width="400px"></asp:Label>

                            <!--Radio Button List End-->
                        </p>
                        <p>
                            &nbsp;
                        </p>
                        <p>
                            &nbsp;
                        </p>
                    </td>
                    <td> </td>
                </tr>

                <tr>

                    <td style="padding-left: 15px;">
                        <h4><b>3. PAYMENT METHODS. &nbsp &nbsp
                            </b></h4>
                        &nbsp;</td>
                </tr>

                <tr>

                    <td style="padding-left: 15px;">

                        <asp:RadioButton ID="Cash" runat="server" Width="150px" GroupName="a" Text="Cash On Delivery" />

                        <asp:Image ID="ImageButton1" runat="server"
                            ImageUrl="~/Images/COD.png" Width="200px" />

                        <br />
                        <asp:RadioButton ID="Paypal" GroupName="a" runat="server" Width="100px" Text="Paypal" />
                        <asp:Image ID="PaypalBtn" runat="server" margin-left="50px"
                            ImageUrl="~/Images/paypal.jpg" Width="200px" Height="92px" />
                    </td>
                </tr>

            </table>















    <tr>
        <td>
            <asp:Button ID="btnCheckOut" runat="server" OnClick="btnCheckOut_Click" Text="Check Out" />
              <div id="paypal-button"></div>


            <br />
            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="ddlPayment" ErrorMessage="You have to select a payment mode" ForeColor="Red" Operator="NotEqual" ValueToCompare="Please Choose a Payment Method"></asp:CompareValidator>
            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlDelivery" ErrorMessage="You have to select a delivery mode" ForeColor="Red" Operator="NotEqual" ValueToCompare="Please Choose a Delivery Mode"></asp:CompareValidator>
        </td>
    </tr>
    <tr>
        <td>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OrderNumberConnectionString %>" ProviderName="<%$ ConnectionStrings:OrderNumberConnectionString.ProviderName %>" SelectCommand=" SELECT oOrderNo, oDate FROM Orders WHERE (oOrderNo = ?)      ">
                <SelectParameters>
                    <asp:SessionParameter Name="?" SessionField="sOrderNo" />
                </SelectParameters>
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:orderdetailsconnectionstring %>" ProviderName="<%$ ConnectionStrings:orderdetailsconnectionstring.ProviderName %>" SelectCommand="SELECT iOrderNo, iProductId, ProductName, iQty, iUnitPrice, iAmt FROM itemsView WHERE (iOrderNo = ?)     ">
                <SelectParameters>
                    <asp:SessionParameter Name="?" SessionField="sOrderNo" />
                </SelectParameters>
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ordertotalconnectionstring %>" ProviderName="<%$ ConnectionStrings:ordertotalconnectionstring.ProviderName %>" SelectCommand="SELECT Amount, GST, Total FROM TotalsView WHERE (oOrderNo = ?)">
                <SelectParameters>
                    <asp:SessionParameter Name="?" SessionField="sOrderNo" />
                </SelectParameters>
            </asp:SqlDataSource>
        </td>
    </tr>
</table>
        </div>
</asp:Content>
   
