﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PerceptumIIP.Startup))]
namespace PerceptumIIP
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
