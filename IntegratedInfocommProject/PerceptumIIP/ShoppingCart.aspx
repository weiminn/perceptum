﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ShoppingCart.aspx.cs" Inherits="PerceptumIIP.ShoppingCart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>


        .lblShoppingCart{

            margin-left:50px;

        }

        
.shoppingcartgrid {
    width: 100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color: white;
}


.shoppingcartgrid td {
        padding: 10px;
        border: solid 1px black;
        color: black;
    }

.shoppingcartgrid th {
        background-color: #f3e222;
        text-align: center;
        padding: 10px 2px;
        border: solid 1px black;
        color: black;
    }

.shoppingcartgrid tr:nth-child(even) {
        background-color: #ffffff;
    }

.shoppingcartgrid tr:nth-child(odd) {
        background-color: #dcdcdc;
    }

.shoppingcartgrid .alt {
        background: #dcdcdc;
    }

.shoppingcartgrid .pgr {
        background: #dcdcdc;
    }

.shoppingcartgrid .pgr table {
            margin: 5px 0;
        }

 .shoppingcartgrid .pgr td {
            border-width: 0;
            padding: 0 6px;
            border-left: solid 1px black;
        }

        .shoppingcartgrid .pgr a {
            color: black;
            text-decoration: none;
        }


        #PaymentTable {
    width: 100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color: white;
}


#PaymentTable td {
        padding: 10px;
        border: solid 1px black;
        color: black;
    }

#PaymentTable th {
        background-color: #f3e222;
        text-align: center;
        padding: 10px 2px;
        border: solid 1px black;
        color: black;
    }

#PaymentTable tr:nth-child(even) {
        background-color: #ffffff;
    }

#PaymentTable tr:nth-child(odd) {
        background-color: #dcdcdc;
    }

#PaymentTable .alt {
        background: #dcdcdc;
    }

#PaymentTable .pgr {
        background: #dcdcdc;
    }

#PaymentTable .pgr table {
            margin: 5px 0;
        }

#PaymentTable .pgr td {
            border-width: 0;
            padding: 0 6px;
            border-left: solid 1px black;
        }

#PaymentTable .pgr a {
            color: black;
            text-decoration: none;
        }
#PaymentTable {
    border-collapse: collapse;
    width: 100%;
}




/*table th, td {
    text-align: left;
    padding: 8px;
}

table tr:nth-child(even){background-color: #f2f2f2}

table th {
    background-color: #4CAF50;
    color: white;
}*/
#ShoppingCartdiv{
    margin-bottom: 50px;
}
    </style>


    <br />
    <br />
    <h1>Cart Items</h1>


<div id="ShoppingCartdiv"> 
    <h3>            <asp:Label ID="lblShoppingCart" runat="server" Text="Your Shopping Cart"></asp:Label>
</h3>
<table id="CartTable" style="text-align: center">

   
    <tr>
        <td>
            <asp:GridView ID="DataGrid1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource2" Width="100%" CssClass="shoppingcartgrid" DataKeyNames="OrderId,ProductId">
                <Columns>
                    <asp:BoundField DataField="OrderId" HeaderText="OrderId" SortExpression="OrderId" InsertVisible="False" ReadOnly="True" />
                    <asp:BoundField DataField="ProductId" HeaderText="ProductId" SortExpression="ProductId" InsertVisible="False" ReadOnly="True" />
                    <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                    <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                    <asp:BoundField DataField="PromoPrice" HeaderText="UnitPrice " SortExpression="PromoPrice" />
                </Columns>
            </asp:GridView>
        </td>
    </tr>
    </table>
  
        

    

    <div id="details" runat="server">
                <h4><b>DELIVERY METHODS</b></h4> &nbsp &nbsp

 <asp:RadioButtonList runat="server" ID="del" RepeatDirection="Horizontal" RepeatColumns="4" RepeatLayout="Table" CssClass="RadioButtonWidth" Width="100%">
                    <asp:ListItem runat="server" Value="Ninja">Pick Up at Centre</asp:ListItem> 

                    <asp:ListItem runat="server" Value="Standard">Standard Courier: 2-5 business days upon dispatch (Additional $1.50)</asp:ListItem>

                    <asp:ListItem runat="server" Value="Express"> Express Courier: 1-3 business days upon dispatch(Additional $2.50)</asp:ListItem>
                </asp:RadioButtonList>




     
            <asp:Button ID="View" runat="server" Text="View Payment Details" OnClick="View_Click" />


        <table id="PaymentTable"  class="nav-justified">
        <tr>
            <td style=" text-align: center; " colspan="2">Payment Details</td>
        </tr>
        <tr>
            <td style="width: 685px">Amount</td>
            <td>
                <asp:Label ID="amtwoGST" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 685px">GST </td>
            <td>
                <asp:Label ID="Gst" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            
            <td style="width: 685px">Additional Charges</td>
            <td>
                <asp:Label ID="addcharge" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 685px">Total Amount</td>
            <td>
                <asp:Label ID="Totalamt" runat="server" Text=""></asp:Label>
            </td>
        </tr>
    </table>

   

        <h4><b>PAYMENT METHODS</b> &nbsp &nbsp </h4>

        <div class="row">
            <div class="col-md-5">
                   

                        <asp:RadioButton ID="Cash" runat="server" Width="100px" GroupName="a" Text="Cash On Delivery" />

                        <asp:Image ID="ImageButton1" runat="server"
                            ImageUrl="Content/images/COD.png" Width="200px" />
            </div>

            <div class="col-md-5">

                        
                        <asp:RadioButton ID="Paypal" GroupName="a" runat="server" Width="100px" Text="Paypal" />
                        <asp:Image ID="PaypalBtn" runat="server" margin-left="50px"
                            ImageUrl="Content/images/paypal.jpg" Width="200px" Height="92px" />
            
            </div>

            </div>


         <%--  <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="ddlPayment" ErrorMessage="You have to select a payment mode" ForeColor="Red" Operator="NotEqual" ValueToCompare="Please Choose a Payment Method"></asp:CompareValidator>

            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlDelivery" ErrorMessage="You have to select a delivery mode" ForeColor="Red" Operator="NotEqual" ValueToCompare="Please Choose a Delivery Mode"></asp:CompareValidator>
        --%>
                                            <asp:Button ID="btnCheckOut" runat="server" OnClick="btnCheckOut_Click" Text="Check Out" />
    
    </div>
   
        
            <%--<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:order and date connection string %>" SelectCommand="SELECT [OrderId], [DateTime] FROM [Orders]">
            </asp:SqlDataSource>--%>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand=
                "SELECT Orders.OrderId , Products.ProductId, Products.Name, Orders.Quantity, Products.PromoPrice 
                FROM Orders 
                INNER JOIN Products 
                ON Orders.ProductId = Products.ProductId
                INNER JOIN CustomerOrders
                ON Orders.CustomerOrderId = CustomerOrders.CustomerOrderId
                WHERE CustomerOrders.CustomerOrderId = @CustomerOrderId">
                <SelectParameters>
                    <asp:SessionParameter Name="CustomerOrderId" SessionField="CustomerOrderId" />
                </SelectParameters>
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT [Amount], [GST], [TotalAmt] FROM [Transactions]">
            </asp:SqlDataSource>
        </td>
    

        </div>

    <%--<asp:GridView runat="server" ID="cartGrid" OnRowCommand="cartGrid_RowCommand"
        CssClass="table table-hover table-striped">
        <Columns>
            <asp:ButtonField Text="Delete" />
        </Columns>
    </asp:GridView>

    <asp:Button runat="server" ID="Pay" OnClick="Pay_Click" Text="Pay Now"/>--%>
</asp:Content>
