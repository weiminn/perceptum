﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Parent/Parent.Master" AutoEventWireup="true" CodeBehind="MakeUp.aspx.cs" Inherits="PerceptumIIP.Views.Parent.MakeUp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .availclass {
    width:100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color:white;
}


.availclass td {
    padding:10px; 
    border: solid 1px black;
    color:black;
}

        .availclass th {
        background-color:#f3e222;
        text-align:center;
        padding: 10px 2px;
        border: solid 1px black;
        color: black;
    }

.availclass tr:nth-child(even) {
    background-color: #ffffff;
}

.availclass tr:nth-child(odd) {
    background-color: #dcdcdc;
}

.availclass .alt{
    background:#dcdcdc;
}

.availclass .pgr{

    background: #dcdcdc;

}
    .availclass .pgr table {
        margin: 5px 0;
    }

    .availclass .pgr td {
        border-width:0;
        padding: 0 6px;
        border-left: solid 1px black;
    }
   .availclass .pgr a{
        color:black;
        text-decoration:none;
    }
    </style>
    <h2>Make-Up Lesson Booking</h2>
    <br />

    <%--<div class="row">
        <div class="form-group col-md-2">
            Choose Subject
            <asp:DropDownList runat="server" id="subjectsDDL" CssClass="form-control" AutoPostBack="true">
               <asp:ListItem Value="" Selected="True"></asp:ListItem>

                <asp:ListItem Value="Mathematics">Mathematics</asp:ListItem>
                <asp:ListItem Value="AdditionalMathematics"> Additional Mathematics </asp:ListItem>
                <asp:ListItem Value="Chemistry"> Chemistry </asp:ListItem>
                <asp:ListItem Value="Science">Science</asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="subjectsDDL" CssClass="text-danger" Display="Dynamic" ErrorMessage="Please Select a subject">

           </asp:RequiredFieldValidator>
        </div>
    </div>--%>

    <div class="row">
        <div class="form-group col-md-2">
          <label for="Reason">Reason:</label>
          <textarea class="form-control" rows="5" id="Reason" runat="server"></textarea>
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="Reason" CssClass="text-danger" Display="Dynamic" ErrorMessage="Please indicate a reason">

           </asp:RequiredFieldValidator>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-2">
            Choose Date
            <asp:textbox ID="txtDate" runat="server" cssclass="datepicker-field form-control"/>
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtDate" CssClass="text-danger" Display="Dynamic" ErrorMessage="Please indicate a date">

           </asp:RequiredFieldValidator>
            <br /><asp:Button CssClass="btn btn-default" ID="Search" OnClick="Search_Click" runat="server" Text="Find Classes" />
        </div>
    </div>

<%--    <asp:GridView ID="cGrid" runat="server">
        <Columns>
            <asp:BoundField DataField="ClassId" HeaderText="ClassId" InsertVisible="False" ReadOnly="True" SortExpression="ClassId" />
            <asp:BoundField DataField="ClassDescription" HeaderText="ClassDescription" SortExpression="ClassDescription" />
            <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level" />
            <asp:BoundField DataField="Time" HeaderText="Time" SortExpression="Time" />
        </Columns>
    </asp:GridView>--%>

    <asp:GridView runat="server" ID="classesGrid" OnRowCommand="classesGrid_RowCommand"
    CssClass="availclass">
        <Columns>
            <asp:ButtonField Text="Book Make Up Class" />
        </Columns>
    </asp:GridView>

    <script>
        $(document).ready(function(){
             $('.datepicker-field').datepicker();        
        });
    </script>
</asp:Content>
