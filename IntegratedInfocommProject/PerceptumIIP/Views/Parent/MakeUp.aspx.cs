﻿using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PerceptumIIP.Views.Parent
{
    public partial class MakeUp : System.Web.UI.Page
    {
        private int studentID;
        private int ClassId;

        protected void Page_Load(object sender, EventArgs e)
        {
            studentID = Convert.ToInt32(this.Session["StudentID"]);
            ClassId = Convert.ToInt32(this.Session["ClassId"]);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Confirmation Page.aspx");
        }

        protected void Search_Click(object sender, EventArgs e)
        {

            

            DateTime date = Convert.ToDateTime(txtDate.Text);

            using (var db = new ApplicationDbContext())
            {
                var student = db.Students.SingleOrDefault(x => x.StudentId == studentID);
                var theClass = db.Classes.SingleOrDefault(x => x.ClassId == ClassId).ClassDescription;

                if(student != null)
                {
                    var studentLevel = student.Level;
                    var classes = db.Classes.Where(x =>
                    x.Level == studentLevel &&
                    x.ClassDescription == theClass &&
                    x.Day == date.DayOfWeek
                    ).ToList();

                    if(classes.Count == 0)
                    {
                        Response.Write(@"<script language='javascript'>alert('No classes are available for the selected day!');</script>");
                        return;
                    }

                    DataTable dt = new DataTable();
                    dt.Columns.Add("ID");
                    dt.Columns.Add("Description");
                    dt.Columns.Add("Level");
                    dt.Columns.Add("Day");
                    dt.Columns.Add("Time");
                    dt.Columns.Add("Available Slots");

                    foreach (var classItem in classes)
                    {
                        DataRow dr = dt.NewRow();
                        dr[0] = classItem.ClassId;
                        dr[1] = classItem.ClassDescription;
                        dr[2] = classItem.Level;
                        dr[3] = classItem.Day;
                        dr[4] = classItem.Time.ToString("HH:mm");

                        var slots = db.MakeUpClasses.Where(x => x.ClassId == classItem.ClassId).ToList();
                        int occupiedSlots = 0;
                        foreach (var slot in slots)
                        {
                            DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
                            System.Globalization.Calendar cal = dfi.Calendar;

                            var nextWeek = cal.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstDay, DayOfWeek.Monday) + 1;
                            var slotWeek = cal.GetWeekOfYear(slot.dateTime, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
                            if (nextWeek == slotWeek)
                            {
                                occupiedSlots++;
                            }
                        }

                        dr[5] = 5 - occupiedSlots;
                        dt.Rows.Add(dr);
                    }

                    classesGrid.DataSource = dt;// classes;
                    classesGrid.DataBind();
                }                
            }
        }

        protected void classesGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = classesGrid.Rows[index];
            var classSelected = Convert.ToInt32(row.Cells[1].Text);

            DateTime thisMonth = DateTime.Now.Date;
            DateTime lastMonth = DateTime.Now.AddMonths(-1).Date;

            using (var db = new ApplicationDbContext())
            {
                var theClass = db.Classes.SingleOrDefault(x => x.ClassId == classSelected);

                var attendance = db.Attendances.Where(x =>
                    x.ClassId == ClassId &&
                    x.StudentId == studentID &&
                    x.Date > lastMonth &&
                    x.Status == "Absent"
                ).ToList();

                if(attendance.Count <= 0)
                {
                    Response.Write(@"<script language='javascript'>alert('This student is not eligible for make up classes for this subject!');</script>");
                    return;
                }

                var bookingDay = GetNextWeekday(theClass.Day);

                var slots = db.MakeUpClasses.Where(x => x.ClassId == theClass.ClassId).ToList();
                int occupiedSlots = 0;
                foreach (var slot in slots)
                {
                    DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
                    System.Globalization.Calendar cal = dfi.Calendar;

                    var nextWeek = cal.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstDay, DayOfWeek.Monday) + 1;
                    var slotWeek = cal.GetWeekOfYear(slot.dateTime, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
                    if (nextWeek == slotWeek)
                    {
                        occupiedSlots++;
                    }
                }

                if (occupiedSlots < 5)
                {
                    Session["StudentId"] = studentID;
                    Session["ClassId"] = theClass.ClassId;
                    Session["BookingDay"] = bookingDay;
                    Session["Reason"] = Reason.InnerText;

                    Response.Redirect("MakeUpForm");
                }
            }
        }

        static DateTime GetNextWeekday(DayOfWeek day)
        {
            DateTime result = DateTime.Now.AddDays(1);
            while (result.DayOfWeek != day)
                result = result.AddDays(1);
            return result;
        }

        //protected void test_Click(object sender, EventArgs e)
        //{
        //    DateTime date = Convert.ToDateTime(txtDate.Text);
        //    int day = (int) date.DayOfWeek;

        //    using (SqlConnection con = new SqlConnection(@"Data Source=(LocalDb)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\aspnet-PerceptumIIP.mdf;Initial Catalog=aspnet-PerceptumIIP;Integrated Security=True"))
        //    {
        //        con.Open();
        //        SqlCommand cmd = new SqlCommand(
        //            "SELECT * FROM Classes C " +
        //            "INNER JOIN StudentClasses SC " +
        //            "ON SC.ClassId = C.ClassId " +
        //            "WHERE SC.StudentId = " + studentID + " " +
        //            "AND C.Day = " + day,
        //            con);

        //        SqlDataReader dr = cmd.ExecuteReader();
        //        cGrid.DataSource = dr;
        //        cGrid.DataBind();
        //    }
        //}
    }
}