﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Parent/Parent.Master" AutoEventWireup="true" CodeBehind="AttendanceRecords.aspx.cs" Inherits="PerceptumIIP.Views.Parent.AttendanceRecords" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <style>
                .attendancegrid {
    width: 100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color: white;
}


    .attendancegrid td {
        padding: 10px;
        border: solid 1px black;
        color: black;
    }

    .attendancegrid th {
        background-color: #f3e222;
        text-align: center;
        padding: 10px 2px;
        border: solid 1px black;
        color: black;
    }

    .attendancegrid tr:nth-child(even) {
        background-color: #ffffff;
    }

    .attendancegrid tr:nth-child(odd) {
        background-color: #dcdcdc;
    }

    .attendancegrid .alt {
        background: #dcdcdc;
    }

    .attendancegrid .pgr {
        background: #dcdcdc;
    }

        .attendancegrid .pgr table {
            margin: 5px 0;
        }

        .attendancegrid .pgr td {
            border-width: 0;
            padding: 0 6px;
            border-left: solid 1px black;
        }

        .attendancegrid .pgr a {
            color: black;
            text-decoration: none;
        }
    </style>

    <h2><asp:Label runat="server" ID="stuName"></asp:Label>'s Attendance</h2>
    <h4>Class ID: <asp:Label runat="server" ID="lblClassId"></asp:Label></h4>
    <h4>Class Subject: <asp:Label runat="server" ID="lblClassSubject"></asp:Label></h4>
    <h4>Class Timing: <asp:Label runat="server" ID="lblClassTime"></asp:Label></h4>
    <h4>Teacher: <asp:Label runat="server" ID="lblTeacher"></asp:Label></h4>
    <br />

    <asp:GridView ID="aGrid" runat="server" CssClass="attendancegrid" AutoGenerateColumns="False" DataKeyNames="StudentId,ClassId,Date" DataSourceID="SqlDataSource1" GridLines="None" Font-Size="Medium" Font-Names="Arial" CellPadding="3" CellSpacing="10" AlternatingRowStyle-CssClass="alt" PageSize="7" PagerStyle-CssClass="pgr">
        <Columns>
            <asp:BoundField DataField="Date" HeaderText="Date" ReadOnly="True" SortExpression="Date"  DataFormatString="{0:D}" />
            <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />

        </Columns>
                            <PagerStyle CssClass="pgr"></PagerStyle>

    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT *
FROM Attendances A
WHERE A.StudentId = @StudentId
AND ClassId = @ClassId">
        <SelectParameters>
            <asp:SessionParameter Name="StudentId" SessionField="StudentId" />
            <asp:SessionParameter Name="ClassId" SessionField="ClassId" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:Button ID="GoMakeup" CssClass="btn btn-default" Text="Book Make Up" runat="server" OnClick="GoMakeup_Click"/>
</asp:Content>
