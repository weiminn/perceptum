﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Parent/Parent.Master" AutoEventWireup="true" CodeBehind="RegisterStudents.aspx.cs" Inherits="PerceptumIIP.Views.Parent.RegisterStudents" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <style>
        .col-md-10{
            margin-bottom:20px;
        }
        .form{
            display:grid;
            grid-template-columns:1fr 1fr;
            grid-gap:20px;
        }
        @media (min-width: 768px) and (max-width: 992px) {
            .row {
                font-size: 20px;
            }
            input[type="text"], input[type="password"], input[type="email"], input[type="tel"], input[type="select"] {
                max-width: 100%;
            }
            .form-control {
                height: 40px;
            }
        }
    </style>
       <script>
        $(document).ready(function(){
             $('.datepicker-field').datepicker();        
           });
            function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(300)
                    .height(300);
            };

            reader.readAsDataURL(input.files[0]);
        }
        }
    </script>

    <h1>Register New Student</h1>
    <hr />
        

          <div class="row">
        <div class="col-md-3">
        <asp:Label runat="server" AssociatedControlID="FirstName" CssClass="col-md-5 control-label">First Name</asp:Label>

            <div class="form-group">

            <asp:TextBox runat="server" ID="FirstName" CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="FirstName"
                CssClass="text-danger" ErrorMessage="The first name field is required." />

        </div>
                  </div>

                <div class="col-md-3">

        <asp:Label runat="server" AssociatedControlID="LastName" CssClass="col-md-5 control-label">Last Name</asp:Label>
            <div class="form-group">
            <asp:TextBox runat="server" ID="LastName" CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="LastName"
                CssClass="text-danger" ErrorMessage="The last name field is required." />

        </div>
    </div>

                <div class="col-md-3">
        <asp:Label runat="server" AssociatedControlID="Address" CssClass="col-md-5 control-label">Address</asp:Label>
            <div class="form-group">
            <asp:TextBox runat="server" ID="Address" CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="Address"
                CssClass="text-danger" ErrorMessage="The address field is required." />
        </div>
    </div>

</div>
        <div class="row">

                <div class="col-md-3">
        <asp:Label runat="server" AssociatedControlID="DOB" CssClass="col-md-5 control-label">Date Of Birth</asp:Label>
            <div class="form-group">
            <asp:TextBox runat="server" ID="DOB" CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="DOB" cssclass="form-control"
                Display="Dynamic" ErrorMessage="The date of birth field is required." />
        </div>
    </div>

             

            
                <div class="col-md-3">
        <asp:Label runat="server" AssociatedControlID="Telephone" CssClass="col-md-5 control-label">Telephone</asp:Label>
            <div class="form-group">
            <asp:TextBox runat="server" ID="Telephone"  CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="Telephone"
                CssClass="text-danger" Display="Dynamic" ErrorMessage="The telephone field is required." />

        </div>
</div>
                <div class="col-md-3">
        <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-5 control-label">Email</asp:Label>
            <div class="form-group">
            <asp:TextBox runat="server" ID="Email"  CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                CssClass="text-danger" Display="Dynamic" ErrorMessage="The email field is required." />

        </div>
     </div>  
            </div>
    <div class="row">

       <div class="col-md-3">
        <asp:Label runat="server" AssociatedControlID="School" CssClass="col-md-5 control-label">School</asp:Label>
            <div class="form-group">
            <asp:TextBox runat="server" ID="School" CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="School"
                CssClass="text-danger" Display="Dynamic" ErrorMessage="The school field is required." />

        </div>
    </div>

    <div class="col-md-3" style="width: 170px;">
        <asp:Label runat="server" AssociatedControlID="Level" CssClass="col-md-5 control-label">Level</asp:Label>
        <div class="col-sm-offset-2 col-sm-10">
          <asp:DropDownList
                              ID="Level" runat="server" CssClass="form-control">
                              <asp:ListItem Value="" Selected="True"></asp:ListItem>

                              <asp:ListItem Value="Primary 3">Primary 3</asp:ListItem>
                              <asp:ListItem Value="Primary 4">Primary 4</asp:ListItem>
                              <asp:ListItem Value="Primary 5">Primary 5</asp:ListItem>
                              <asp:ListItem Value="Primary 6">Primary 6</asp:ListItem>
                              <asp:ListItem Value="Secondary 1">Secondary 1</asp:ListItem>
                              <asp:ListItem Value="Secondary 2">Secondary 2</asp:ListItem>
                              <asp:ListItem Value="Secondary 3">Secondary 3</asp:ListItem>
                              <asp:ListItem Value="Secondary 4">Secondary 4</asp:ListItem>

                          </asp:DropDownList>
            </div>
           
           

        
     </div>

       <div class="col-md-3">
        <asp:Label runat="server" AssociatedControlID="Branch" CssClass="col-md-5 control-label">Branch</asp:Label>
            <div class="form-group">
            <asp:RadioButtonList ID="Branch" runat="server" RepeatDirection="Horizontal" Height="16px" Width="362px">

<asp:ListItem Value ="Bishan">Bishan</asp:ListItem>
<asp:ListItem Value ="Sime Darby Centre">Sime Darby Centre</asp:ListItem>
</asp:RadioButtonList>
        

        </div>
     </div>  
        </div>

        <div class="row">
                  <div class="col-md-3">

        <asp:Label runat="server" AssociatedControlID="UserName" CssClass="col-md-5 control-label">UserName</asp:Label>
                       <div class="form-group">

            <asp:TextBox runat="server" ID="UserName" CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="UserName"
                CssClass="text-danger" ErrorMessage="The name field is required." />
          </div>
              </div>


                  <div class="col-md-3">
        <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-5 control-label">Password</asp:Label>
                       <div class="form-group">
            <asp:TextBox runat="server" ID="Password" CssClass="form-control" TextMode="Password" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="Password"
                CssClass="text-danger" ErrorMessage="The password field is required." />
        </div>
    </div>

            </div>

        

       

       <div class="row">
           <div class="col-md-12">
            <img id="blah" src="#" alt="" />
        </div>
                    <asp:FileUpload ID="FileUploadControl" runat="server" onchange="readURL(this)"/>
        </div>

    <asp:Button ID="Submit"  runat="server" Text="Submit" OnClick="Submit_Click" />




</asp:Content>
