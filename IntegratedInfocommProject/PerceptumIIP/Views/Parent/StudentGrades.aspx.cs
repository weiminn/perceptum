﻿using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PerceptumIIP.Views.Parent
{
    public partial class StudentGrades : System.Web.UI.Page
    {
        private int ClassId;
        private int StudentId;

        protected void Page_Load(object sender, EventArgs e)
        {
            ClassId = Convert.ToInt32(Session["ClassId"]);
            StudentId = Convert.ToInt32(Session["StudentId"]);

            using (var db = new ApplicationDbContext())
            {
                var student = db.Students.SingleOrDefault(x => x.StudentId == StudentId);
                var theClass = db.Classes.SingleOrDefault(x => x.ClassId == ClassId);

                var employeeclass = db.EmployeeClasses.FirstOrDefault(x => x.ClassId == ClassId); //return array of records  

                var employee = db.Employees.SingleOrDefault(x => x.EmployeeId == employeeclass.EmployeeId);

                if (student != null && theClass != null)
                {
                    stuName.Text = student.FirstName + " " + student.LastName;
                    lblClassId.Text = theClass.ClassId.ToString();
                    lblClassSubject.Text = theClass.ClassDescription;
                    lblTeacher.Text = employee.FirstName;
                }
            }
        }

        protected void gGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument); ///value give row number that is clicked
            GridViewRow row = gGrid.Rows[index]; // go particular
            var Month = Convert.ToDateTime(row.Cells[0].Text);
            Session["Month"] = Month;

            using (var db = new ApplicationDbContext())
            {
                var feedback = db.FeedBacks.SingleOrDefault(x =>
                    x.StudentId == StudentId &&
                    x.ClassId == ClassId &&
                    x.Month == Month
                );

                if (feedback != null)
                {
                    Response.Write(@"<script language='javascript'>alert('A feedback has already been submitted for the selected month.');</script>");
                }
                else
                {
                    Response.Redirect("GiveFeedback.aspx");
                }
            }
        }
    }
}