﻿using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PerceptumIIP.Views.Parent
{
    public partial class TuitionFeeClasses : System.Web.UI.Page
    {
        private int StudentID;

        private decimal totalFee = 0;
        private int totalUnpaidMonths = 0;

        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            StudentID = Convert.ToInt32(this.Session["StudentID"]);

            //for (int i = 0; i < cfGrid.Rows.Count; i++)
            //{
            //    Label label = (Label)cfGrid.Rows[i].Cells[5].FindControl("lblFee");
            //    totalFee += Convert.ToInt32(label.Text);
            //}

            using (var db = new ApplicationDbContext())
            {
                var student = db.Students.Single(x => x.StudentId == StudentID);
                sfName.Text = student.FirstName;

                var classes = from c in db.Classes
                              join sc in db.StudentClasses on c.ClassId equals sc.ClassId
                              where sc.StudentId == student.StudentId
                              select new
                              {
                                  ClassId = c.ClassId,
                                  ClassDescription = c.ClassDescription,
                                  Level = c.Level,
                                  Day = c.Day.ToString(),
                                  Time = c.Time
                              };

                DataTable dt = new DataTable();
                dt.Columns.Add("ClassId");
                dt.Columns.Add("ClassDescription");
                dt.Columns.Add("Level");
                dt.Columns.Add("Day");
                dt.Columns.Add("Time");

                foreach (var @class in classes.ToList())
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = @class.ClassId;
                    dr[1] = @class.ClassDescription;
                    dr[2] = @class.Level;
                    dr[3] = @class.Day;
                    dr[4] = @class.Time.ToString("hh:mm:tt");

                    dt.Rows.Add(dr);
                }

                cfGrid.DataSource = dt;
                cfGrid.DataBind();

                var registered = db.StudentClasses.Where(x => x.StudentId == StudentID);
                foreach(var reg in registered)
                {
                    var lastFee = db.TuitionFees.Where(x => x.StudentId == student.StudentId && x.ClassId == reg.ClassId).OrderByDescending(x => x.Date).FirstOrDefault().Date;  //obtain the date of the first fee paid 
                    var today = DateTime.Today; // obtain current date for payment 

                    totalUnpaidMonths = (int)(today.Subtract(lastFee).Days / (365.25 / 12)); //subtract to obtain the outstanding months 
                    Session["Months"] = totalUnpaidMonths;
                    totalFee += 70 * totalUnpaidMonths;
                }

                var paidFees = db.TuitionFees.Where(x => x.StudentId == student.StudentId);
                tGrid.DataSource = paidFees.ToList();
                tGrid.DataBind();
            }

            lblTotalFee.Text = totalFee.ToString();

        }


        protected void Pay_Click(object sender, EventArgs e)
        {
            Session["TranscationAmount"] = totalFee;
            Session["FirstMonth"] = false;
            string fee = totalFee.ToString();

            string returnURL = "";
            returnURL += "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_xclick&business=perceptumstore@gmail.com";

            returnURL += "&item_name=Tuition Fee";

            returnURL += "&amount=" + fee;

            returnURL += "&currency=SGD";

            returnURL += "&return=http://perceptumeducation.azurewebsites.netViews/Parent/ParentStudents";

            returnURL += "&cancel_return=http://perceptumeducation.azurewebsites.net/Views/Parent/TuitionFeeClasses";

            Response.Redirect(returnURL);
        }
    }
}