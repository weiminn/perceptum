﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Parent/Parent.Master" AutoEventWireup="true" CodeBehind="DisplayNewClasses.aspx.cs" Inherits="PerceptumIIP.Views.Parent.DisplayNewClasses" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <style>
        .classes {
    width:100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color:white;
}

        
.classes td {
    padding:10px; 
    border: solid 1px black;
    color:black;
}

    .classes th {
        background-color:#f3e222;
        text-align:center;
        padding: 10px 2px;
        border: solid 1px black;
        color: black;
    }

.classes tr:nth-child(even) {
    background-color: #ffffff;
}

.classes tr:nth-child(odd) {
    background-color: #dcdcdc;
}

.classes .alt{
    background:#dcdcdc;
}

.classes .pgr{

    background: #dcdcdc;

}
    .classes .pgr table {
        margin: 5px 0;
    }

    .classes .pgr td {
        border-width:0;
        padding: 0 6px;
        border-left: solid 1px black;
    }
   .classes .pgr a{
        color:black;
        text-decoration:none;
    }
    </style>

    <h3>Level: <asp:Label ID="lbllevel" runat="server" Text=""></asp:Label></h3>
    <br />
    <asp:GridView ID="ncGrid" runat="server" AutoGenerateColumns="False"
        CssClass="classes" OnRowCommand="GridView1_RowCommand" GridLines="None" Font-Size="Medium" Font-Names="Arial" CellPadding="3" CellSpacing="10" AlternatingRowStyle-CssClass="alt" PageSize="7" PagerStyle-CssClass="pgr">
        <Columns>
            <asp:BoundField DataField="ClassId" HeaderText="ClassId" InsertVisible="False" ReadOnly="True" SortExpression="ClassId" />
            <asp:BoundField DataField="ClassDescription" HeaderText="ClassDescription" SortExpression="ClassDescription" />
            <asp:BoundField DataField="Day" HeaderText="Day" SortExpression="Day" DataFormatString ="{0:D}" />
            <asp:BoundField DataField="Time" DataFormatString="{0:t}" HeaderText="Time" SortExpression="Time" />
            <asp:ButtonField Text="Details" />
        </Columns>
                                <PagerStyle CssClass="pgr"></PagerStyle>

    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT Classes.ClassId, Classes.ClassDescription , Classes.Time, Classes.Day FROM Classes WHERE Level=@Level">
        <SelectParameters>
            <asp:SessionParameter Name="Level" SessionField="Level" />
        </SelectParameters>
    </asp:SqlDataSource>







</asp:Content>
