﻿using Microsoft.AspNet.Identity;
using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PerceptumIIP.Views.Parent
{
    public partial class Students : System.Web.UI.Page
    {
        private string userId;
        private int currentParent;

        protected void Page_Load(object sender, EventArgs e)
        {
            string userId = User.Identity.GetUserId();
            //Session["ParentId"] = currentParent;

            if (userId != null)
            {
                using (var db = new ApplicationDbContext())
                {
                    currentParent = db.Parents.SingleOrDefault(x => x.ApplicationUserId == userId).ParentId;
                    Session["ParentId"] = currentParent;
                }
            }
        }

        protected void sGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument); ///value give row number that is clicked
            GridViewRow row = sGrid.Rows[index]; // go particular
            var studentselected = row.Cells[0].Text;
            this.Session["StudentId"] = studentselected;

            if(e.CommandName == "Details")
            {
                Response.Redirect("Classes.aspx");
            }
            else if(e.CommandName == "Materials")
            {
                Response.Redirect("RetrieveFile.aspx");
            }
            
        }

        
    }
}