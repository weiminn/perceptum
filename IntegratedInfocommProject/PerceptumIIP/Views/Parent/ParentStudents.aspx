﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Parent/Parent.Master" AutoEventWireup="true" CodeBehind="ParentStudents.aspx.cs" Inherits="PerceptumIIP.Views.Parent.Students" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
            <link rel="stylesheet" href="/CSS/master.css" type="text/css" />

     <style>
        .parentstudent {
    width:100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color:white;
}


.parentstudent td {
    padding:10px; 
    border: solid 1px black;
    color:black;
}

    .parentstudent th {
        background-color:#f3e222;
        text-align:center;
        padding: 10px 2px;
        border: solid 1px black;
        color: black;
    }

.parentstudent tr:nth-child(even) {
    background-color: #ffffff;
}

.parentstudent tr:nth-child(odd) {
    background-color: #dcdcdc;
}

.parentstudent .alt{
    background:#dcdcdc;
}

.parentstudent .pgr{

    background: #dcdcdc;

}
    .parentstudent .pgr table {
        margin: 5px 0;
    }

    .parentstudent .pgr td {
        border-width:0;
        padding: 0 6px;
        border-left: solid 1px black;
    }
   .parentstudent .pgr a{
        color:black;
        text-decoration:none;
    }
         @media (min-width: 768px) and (max-width: 992px) {
             .parentstudent th {
                 font-size: 20px;
             }

             .parentstudent td {
                 font-size: 20px;
             }
         }
    </style>

    <%--<h1>Students</h1>
    <br /> 

    <asp:GridView ID="studentsGrid" runat="server" OnRowCommand="studentsGrid_RowCommand"
        CssClass="table table-hover table-striped">
        <Columns>
            <asp:ButtonField Text="View" />
        </Columns>
    </asp:GridView>

    <br /><br />--%>

    <h2>Your Children</h2>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
    <asp:GridView ID="sGrid" runat="server" OnRowCommand="sGrid_RowCommand"
        CssClass="parentstudent" AutoGenerateColumns="False" DataKeyNames="StudentId,StudentId1,ParentId" DataSourceID="sDS" GridLines="None" Font-Size="Medium" Font-Names="Arial" CellPadding="3" CellSpacing="10" AlternatingRowStyle-CssClass="alt" PageSize="7" PagerStyle-CssClass="pgr">
        <Columns>
            <asp:BoundField DataField="StudentId" HeaderText="StudentId" InsertVisible="False" ReadOnly="True" SortExpression="StudentId" />
            <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName" />
            <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName" />
            <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
            <asp:BoundField DataField="BirthDate" HeaderText="BirthDate" SortExpression="BirthDate" DataFormatString="{0:dd/MM/yyyy}" />
            <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level" />
            <asp:BoundField DataField="Telephone" HeaderText="Telephone" SortExpression="Telephone" />
            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
            <asp:BoundField DataField="Outlet" HeaderText="Outlet" SortExpression="Outlet" />
            <asp:ButtonField Text="Details" CommandName="Details" />
            <asp:ButtonField Text="Materials" CommandName="Materials" />
        </Columns>
                        <PagerStyle CssClass="pgr"></PagerStyle>

    </asp:GridView>

    <asp:SqlDataSource ID="sDS" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" 
        SelectCommand="
        SELECT * FROM Students S
        INNER JOIN StudentParents SP
        ON S.StudentId = SP.StudentId
        WHERE SP.ParentId = @ParentId"
        >
        <SelectParameters>
            <asp:SessionParameter Name="ParentId" SessionField="ParentId" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>
