﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Parent/Parent.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="PerceptumIIP.Views.Parent.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <style>
        @media (min-width: 768px) and (max-width: 992px) {

            .iframe {
                width:900px;
                height:1000px;
            }
        }
    </style>


     <p align="center"> <iframe width="800" height="600" src="https://app.powerbi.com/view?r=eyJrIjoiMjMxZDk5Y2QtZDhkZC00OWYyLTliODQtZGQxZjBkYWJmMmMzIiwidCI6ImNiYTllMTE1LTMwMTYtNDQ2Mi1hMWFiLWE1NjVjYmEwY2RmMSIsImMiOjEwfQ%3D%3D" frameborder="0" allowFullScreen="true"></iframe></p>

</asp:Content>
