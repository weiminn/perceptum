﻿using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PerceptumIIP.Views.Parent
{
    public partial class Classes : System.Web.UI.Page
    {
        private int StudentID;

        protected void Page_Load(object sender, EventArgs e)
        {
            StudentID = Convert.ToInt32(this.Session["StudentId"]);
            
            using (var db = new ApplicationDbContext())
            {
                var student = db.Students.Single(x => x.StudentId == StudentID);
                stuName.Text = student.FirstName;
                Session["Level"] = student.Level;

                var classes = from c in db.Classes
                              join sc in db.StudentClasses on c.ClassId equals sc.ClassId
                              where sc.StudentId == student.StudentId
                              select new
                              {
                                  ClassId = c.ClassId,
                                  ClassDescription = c.ClassDescription,
                                  Level = c.Level,
                                  Day = c.Day.ToString(),
                                  Time = c.Time
                              };

                DataTable dt = new DataTable();
                dt.Columns.Add("ClassId");
                dt.Columns.Add("ClassDescription");
                dt.Columns.Add("Level");
                dt.Columns.Add("Day");
                dt.Columns.Add("Time");

                foreach (var @class in classes.ToList())
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = @class.ClassId;
                    dr[1] = @class.ClassDescription;
                    dr[2] = @class.Level;
                    dr[3] = @class.Day;
                    dr[4] = @class.Time.ToString("hh:mm:tt");

                    dt.Rows.Add(dr);
                }

                cGrid.DataSource = dt;
                cGrid.DataBind();
            }
        }

       

        protected void classesGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument); ///value give row number that is clicked
            GridViewRow row = cGrid.Rows[index]; // go particular
            var classSelected = row.Cells[0].Text;
            this.Session["ClassId"] = classSelected;

            if (e.CommandName == "Grades")
            {
                Response.Redirect("StudentGrades");
            }
            else if (e.CommandName == "Attendance")
            {
                Response.Redirect("AttendanceRecords");
            }

            else
                    {
                Response.Redirect("ParentFeedbacks");
            }
            
        }

        protected void Register_Click(object sender, EventArgs e)
        {
            
            Response.Redirect("DisplayNewClasses");
            
        }
    }
}