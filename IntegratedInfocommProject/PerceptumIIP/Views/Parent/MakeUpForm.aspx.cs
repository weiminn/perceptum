﻿using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;


namespace PerceptumIIP.Views.Parent
{
    public partial class MakeUpForm : System.Web.UI.Page
    {
        private int studentId;
        private int classId;
        private DateTime bookingDay;
        private string reason;

        private static readonly string scriptSuccessNewAccount =
         "<script language=\"javascript\">\n" +
         "alert (\"Your make-up class has been succesfully requested - Thank You!\");\n" +
         "</script>";

        protected void Page_Load(object sender, EventArgs e)
        {
            studentId = (int) Session["StudentId"];
            classId = (int) Session["ClassId"];
            bookingDay = (DateTime) Session["BookingDay"];
            reason = (string) Session["Reason"];

            using (var db = new ApplicationDbContext())
            {
                var theClass = db.Classes.Single(x => x.ClassId == classId);

                Level.Text = theClass.Level;
                Subject.Text = theClass.ClassDescription;
                Date.Text = bookingDay.ToString("dd MMM yyyy");
                Time.Text = theClass.Time.ToString("HH:mm");
            }

            Session["Level"] = Level.Text;
            Session["Subject"] = Subject.Text;
            Session["Date"] = Date.Text;
            Session["Time"] = Time.Text;
        }

        protected void Confirm_Click(object sender, EventArgs e)
        {
            Type csType = this.GetType();

            using (var db = new ApplicationDbContext())
            {
                MakeUpClass makeUpClass = new MakeUpClass
                {
                    StudentId = studentId,
                    ClassId = classId,
                    dateTime = bookingDay
                };

                db.MakeUpClasses.Add(makeUpClass);
                db.SaveChanges();

                string Level = (string)Session["Level"];
                string Subject = (string)Session["Subject"];
                string Date = (string)Session["Date"];
                string Time = (string)Session["Time"];
                int ParentId = (int)Session["ParentId"];

                MailMessage msg = new MailMessage();
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
                try
                {
                    msg.Subject = "Make Up Booking";
                    msg.Body = "Hello! Your new make up class is confirmed!"  + System.Environment.NewLine + "Level:" + Level + System.Environment.NewLine + "Subject:" + Subject + System.Environment.NewLine + "Date:" + Date + System.Environment.NewLine + "Time:" + Time;
                    msg.From = new MailAddress("perceptumiip@gmail.com");

                    int toMails = 0;
                    var toEmails = from sc in db.StudentParents
                                   join p in db.Parents on sc.StudentId equals p.ParentId
                                   where sc.ParentId == ParentId
                                   select new
                                   {
                                       ParentEmail = p.Email
                                   };
                    foreach (var email in toEmails)
                    {
                        msg.To.Add(new MailAddress(email.ParentEmail));
                        toMails++;
                    }

                    msg.IsBodyHtml = true;
                    client.Host = "smtp.gmail.com";
                    System.Net.NetworkCredential basicauthenticationinfo = new System.Net.NetworkCredential("perceptumiip@gmail.com", "123qweQWE!@#");
                    client.Port = int.Parse("587");
                    client.EnableSsl = true;
                    client.UseDefaultCredentials = false;
                    client.Credentials = basicauthenticationinfo;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;

                    if (toMails > 0)
                    {
                        client.Send(msg);
                    }
                }
                catch (Exception ex)
                {
                }

               // Find your Account Sid and Auth Token at twilio.com/console
                const string accountSid = "AC816a17151f3c002d96ed06d2650d966a";
                const string authToken = "fa3d504add6541a4a9edd3dc6da0278e";
                TwilioClient.Init(accountSid, authToken);

                var to = new PhoneNumber("+6581018260");
                var message = MessageResource.Create(
                   to,
                  from: new PhoneNumber("+18444494470"),
                  body: "Hello! Your new make up class is confirmed!" + "Level:" + Level + "Subject:" + Subject + "Date:" + Date + "Time:" + Time);


                Console.WriteLine(message.Sid);

                ClientScript.RegisterStartupScript(csType, "Success", scriptSuccessNewAccount);

                Response.Redirect("MakeUp");
            }
            ClientScript.RegisterStartupScript(csType, "Success", scriptSuccessNewAccount);

            Response.Redirect("/Views/Parent/MakeUpConfirmation.aspx");
        }
    }
}