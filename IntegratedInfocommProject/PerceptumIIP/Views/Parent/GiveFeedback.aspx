﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Parent/Parent.Master" AutoEventWireup="true" CodeBehind="GiveFeedback.aspx.cs" Inherits="PerceptumIIP.Views.Parent.GiveFeedback" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 298px;
        }
        .auto-style3 {
            width: 136px;
        }
        .RadioButtonWidth label { width: 50px }



        .starRating {
            width:50px;
            height:50px;
            cursor: pointer;
            background-repeat:no-repeat;
            background-color:#ffd777;
            display:block;
        }

       .starempty
        {
            background-image:url(../../../Content/images/starempty.png);
            width: 50px;
            height: 50px;
        }
.starfilled
        {
            background-image: url(../../../Content/images/starfilled.png);
            width: 50px;
            height: 50px;
        }
.starwaiting
        {
            background-image: url(../../../Content/images/starwaiting.png);
            width: 50px;
            height: 50px;
        }
    
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <br />
    <h1>Feedback Form</h1>

  <h3> Name: <asp:Label ID="studName" runat="server" Text=""></asp:Label> </h3>
   <h3> Class: <asp:Label ID="studClass" runat="server" Text=""> </asp:Label> </h3>
   <h3> Subject: <asp:Label ID="subject" runat="server" Text=""> </asp:Label></h3>
    <table cellpadding="3" class="auto-style1">

         
      <h2> Evaluation Criteria</h2>     
           
        <br />
        <tr style="font-size: medium;">
            <td class="auto-style2" style="font-family: Arial;">        Quality Of Teaching Method
                
            </td>
            <td class="auto-style3">
                <asp:RadioButtonList runat="server" ID="tm" RepeatDirection="Horizontal" RepeatColumns="4" RepeatLayout="Table" CssClass="RadioButtonWidth" Width="100%">
                    <asp:ListItem runat="server" Value="1">1</asp:ListItem> 

                    <asp:ListItem runat="server" Value="2">2</asp:ListItem>
                    <asp:ListItem runat="server" Value="3">3</asp:ListItem>
                    <asp:ListItem runat="server" Value="4">4</asp:ListItem>
                </asp:RadioButtonList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tm" Display="Dynamic" ErrorMessage="Fill in required field"></asp:RequiredFieldValidator>
            </td>
        </tr>
       <br />
        <tr style="font-size: medium;">
            <td class="auto-style2">          Quality Of Materials 
</td>
            <td class="auto-style3">
                <asp:RadioButtonList runat="server" ID="qm" RepeatDirection="Horizontal" RepeatColumns="4" RepeatLayout="Table" CssClass="RadioButtonWidth" Width="100%">
                    <asp:ListItem runat="server" Value="1">1</asp:ListItem>
                    <asp:ListItem runat="server" Value="2">2</asp:ListItem>
                    <asp:ListItem runat="server" Value="3">3</asp:ListItem>
                    <asp:ListItem runat="server" Value="4">4</asp:ListItem>
                </asp:RadioButtonList>
       <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="qm" Display="Dynamic" ErrorMessage="Fill in required field"></asp:RequiredFieldValidator>

            </td>
        </tr>
        <br />
        <tr style="font-size: medium;">
            <td class="auto-style2"> Scheduled Orgnanization of Tests and Quizzes
                </td>
            <td class="auto-style3">
                <asp:RadioButtonList runat="server" ID="tq" RepeatDirection="Horizontal" RepeatColumns="4" Display="Dynamic" RepeatLayout="Table" CssClass="RadioButtonWidth" Width="100%">
                    <asp:ListItem runat="server" Value="1">1</asp:ListItem>
                    <asp:ListItem runat="server" Value="2">2</asp:ListItem>
                    <asp:ListItem runat="server" Value="3">3</asp:ListItem>
                    <asp:ListItem runat="server" Value="4">4</asp:ListItem>
                </asp:RadioButtonList>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="tq" Display="Dynamic" ErrorMessage="Fill in required field"></asp:RequiredFieldValidator>

            </td>
        </tr>
        <br />
                <tr style="font-size: medium;">

        <td class="auto-style2">                   Management Student Behavior</td>
                    <td>

            <asp:RadioButtonList runat="server" ID="sb" RepeatDirection="Horizontal" RepeatColumns="4" RepeatLayout="Table" CssClass="RadioButtonWidth" Width="100%">
                <asp:ListItem runat="server" Value="1">1</asp:ListItem>
                <asp:ListItem runat="server" Value="2">2</asp:ListItem>
                <asp:ListItem runat="server" Value="3">3</asp:ListItem>
                <asp:ListItem runat="server" Value="4">4</asp:ListItem>
            </asp:RadioButtonList>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="sb" Display="Dynamic" ErrorMessage="Fill in required field"></asp:RequiredFieldValidator>

                    </td>
        </tr>
    </table>

    <br />

    <h5 style="font-size: medium;">Criteria</h5>
    <h6 style="font-size: medium;"> 1 for Not Satisfied </h6>
          <h6 style="font-size: medium;">  2 for Slightly Satisfied </h6>
         <h6 style="font-size: medium;">   3 for Satisfied </h6>
          <h6 style="font-size: medium;">  4 for Very Satisfied</h6>
    <br />
    
     <div class="form-group">
           <h4> <asp:Label runat="server" AssociatedControlID="Details" CssClass="col-md-2 control-label">Suggestions and Comments</asp:Label> </h4>
            <div class="col-md-4">
                <asp:TextBox runat="server" ID="Details" CssClass="form-control" />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="Details" Display="Dynamic" ErrorMessage="Please fill in suggestions"></asp:RequiredFieldValidator>

            </div>
    </div>

    <ajaxToolkit:Rating ID="selectedRating" runat="server" CurrentRating="1" MaxRating="5" AutoPostBack="true" 
        StarCssClass="starRating" EmptyStarCssClass="starempty" FilledStarCssClass="starfilled" WaitingStarCssClass="starwaiting" ></ajaxToolkit:Rating>
    <asp:Label ID="lblrating" runat="server" Text=""></asp:Label>
           

    <br />
  &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Button ID="btnGiveFeedback" OnClick="GiveFeedback_Click" runat="server" Text="Give Feedback" />

</asp:Content>
