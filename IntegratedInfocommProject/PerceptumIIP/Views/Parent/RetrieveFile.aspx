﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Parent/Parent.Master" AutoEventWireup="true" CodeBehind="RetrieveFile.aspx.cs" Inherits="PerceptumIIP.Views.Parent.RetrieveFile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


    <style>
        .retrievefile {
    width:100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color:white;
}


.retrievefile td {
    padding:10px; 
    border: solid 1px black;
    color:black;
}

        .retrievefile th {
        background-color:#f3e222;
        text-align:center;
        padding: 10px 2px;
        border: solid 1px black;
        color: black;
    }

.retrievefile tr:nth-child(even) {
    background-color: #ffffff;
}

.retrievefile tr:nth-child(odd) {
    background-color: #dcdcdc;
}

.retrievefile .alt{
    background:#dcdcdc;
}

.retrievefile .pgr{

    background: #dcdcdc;

}
    .retrievefile .pgr table {
        margin: 5px 0;
    }

    .retrievefile .pgr td {
        border-width:0;
        padding: 0 6px;
        border-left: solid 1px black;
    }
   .retrievefile .pgr a{
        color:black;
        text-decoration:none;
    }
    </style>

    <asp:GridView ID="FilesGrid" runat="server" CssClass="retrievefile" AutoGenerateColumns="False" DataKeyNames="FileId" onrowcommand="FilesGrid_RowCommand">

        <Columns>
            <asp:BoundField DataField="FileId" HeaderText="File Id"/>
            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
            <asp:ButtonField Text="Download" CommandName="Download" />
        </Columns>
    </asp:GridView>


    <%--<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT FileId, Name FROM Files WHERE Level=@Level">
        <SelectParameters>
            <asp:SessionParameter Name="Level" SessionField="Level" />
        </SelectParameters>
    </asp:SqlDataSource>--%>


</asp:Content>
