﻿using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PerceptumIIP.Views.Parent
{
    public partial class PaymentComplete : System.Web.UI.Page
    {
        private int TranscationAmount = 0;
        private int StudentId;
        private int ClassId;
        private Boolean FirstMonth;
        private int months;

        protected void Page_Load(object sender, EventArgs e)
        {
            TranscationAmount = Convert.ToInt32(Session["TranscationAmount"]);
            StudentId = Convert.ToInt32(Session["StudentId"]);
            ClassId = Convert.ToInt32(Session["ClassId"]);
            FirstMonth = Convert.ToBoolean(Session["FirstMonth"]);
            months = Convert.ToInt32(Session["Months"]);

            if (TranscationAmount > 0)
            {
                if (FirstMonth)
                {
                    using (var db = new ApplicationDbContext())
                    {
                        TuitionFee tuitionFee = new TuitionFee
                        {
                            StudentId = StudentId,
                            ClassId = ClassId,
                            TotalFee = TranscationAmount,
                            Date = DateTime.Now
                        };
                        db.TuitionFees.Add(tuitionFee);
                        db.SaveChanges();

                        StudentClass studentClass = new StudentClass
                        {
                            StudentId = StudentId,
                            ClassId = ClassId,
                            RegistrationDate = DateTime.Today
                        };
                        db.StudentClasses.Add(studentClass);
                        db.SaveChanges();
                    }
                }
                else
                {
                    using (var db = new ApplicationDbContext())
                    {
                        var firstFee = db.TuitionFees.OrderByDescending(x => x.Date).FirstOrDefault().Date;

                        for (DateTime i = firstFee.AddMonths(1); i < DateTime.Today; i.AddMonths(1))
                        {
                            TuitionFee tuitionFee = new TuitionFee
                            {
                                StudentId = StudentId,
                                ClassId = ClassId,
                                TotalFee = 70,
                                Date = i
                            };
                            db.TuitionFees.Add(tuitionFee);
                            db.SaveChanges();
                        }
                    }
                }
            }

            Session["TranscationAmount"] = 0;

            Response.Redirect("ParentStudents");
        }
    }
}