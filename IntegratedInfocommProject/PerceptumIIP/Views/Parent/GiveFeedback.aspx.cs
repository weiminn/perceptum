﻿using Microsoft.AspNet.Identity;
using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;


namespace PerceptumIIP.Views.Parent
{
    public partial class GiveFeedback : System.Web.UI.Page
    {
        
        private int StudentId;
        private int ClassId;
        private int EmployeeId;
        private DateTime Month;
        private decimal theRating;

        protected void Page_Load(object sender, EventArgs e)
        {
            StudentId = Convert.ToInt32(this.Session["StudentId"]);
            ClassId = Convert.ToInt32(Session["ClassId"]);
            Month = Convert.ToDateTime(Session["Month"]);

            using (var db = new ApplicationDbContext())
            {
                var student = db.Students.SingleOrDefault(x => x.StudentId == StudentId);
                var theClass = db.Classes.SingleOrDefault(x => x.ClassId == ClassId);


                if (student != null && theClass != null)
                {
                    studName.Text = student.UserName;
                    studClass.Text = theClass.ClassId.ToString();
                    subject.Text = theClass.ClassDescription;
                }
            }
        }

        private static readonly string scriptSuccessNewAccount =
"<script language=\"javascript\">\n" +
"alert (\"Your Feedback is submitted - Thank You!\");\n" +
"</script>";
        protected void GiveFeedback_Click(object sender, EventArgs e)
        {
            
        Type csType = this.GetType();

            string userId = User.Identity.GetUserId();
            using (var db = new ApplicationDbContext())
            {
                var currentParent = db.Parents.SingleOrDefault(p => p.ApplicationUserId == userId).ParentId;

                Feedback feedback = new Feedback
                {
                    ParentId = currentParent,
                    StudentId = StudentId,
                    Details = Details.Text,
                    ClassId = ClassId,
                    Status = "Approved",
                    Method = Convert.ToInt32(tm.SelectedValue),
                    Material = Convert.ToInt32(qm.SelectedValue),
                    Tests = Convert.ToInt32(tq.SelectedValue),
                    Management = Convert.ToInt32(sb.SelectedValue),
                    Month = Month
                };

                db.FeedBacks.Add(feedback);

                var employeeclass = db.EmployeeClasses.Where(x => x.ClassId == ClassId).ToList();

                foreach(var ec in employeeclass)
                {
                    EmployeeRating Rating = new EmployeeRating
                    {
                        EmployeeId = ec.EmployeeId,
                        Rating = Convert.ToDecimal(selectedRating.CurrentRating)
                    };
                    db.EmployeeRatings.Add(Rating);
                }
                db.SaveChanges();
                lblrating.Text = "You have rated us: " + theRating.ToString();
            }
            ClientScript.RegisterStartupScript(csType, "Success", scriptSuccessNewAccount);

            Response.Redirect("Classes");
        }

        private string getRadioValue(ControlCollection clts, string groupName)
        {
            string ret = "";
            foreach (Control ctl in clts)
            {
                if (ctl.Controls.Count != 0)
                {
                    if (ret == "")
                        ret = getRadioValue(ctl.Controls, groupName);
                }

                if (ctl.ToString() == "System.Web.UI.WebControls.RadioButton")
                {
                    RadioButton rb = (RadioButton)ctl;
                    if (rb.GroupName == groupName && rb.Checked == true)
                        ret = rb.Attributes["Value"];
                }
            }
            return ret;
        }

        protected void Rating_Click(object sender, RatingEventArgs e)
        {
            theRating = Convert.ToDecimal(selectedRating.CurrentRating);
        }
    }
}