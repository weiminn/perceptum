﻿using Microsoft.AspNet.Identity;
using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PerceptumIIP.Views.Parent
{
    public partial class Parent : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var user = Page.User.Identity.GetUserId();
            using (var db = new ApplicationDbContext())
            {
                var parent = db.Parents.SingleOrDefault(x => x.ApplicationUserId == user);
                if(parent != null)
                {
                    name.Text = parent.FirstName;
                }
                
            }

        }

        protected void logout_Click(object sender, EventArgs e)
        {
            Context.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            Response.Redirect("/");
        }
    }
}