﻿using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PerceptumIIP.Views.Parent
{
    public partial class DisplayNewClasses : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lbllevel.Text = (string)Session["Level"];

            using (var db = new ApplicationDbContext())
            {
                var classes = from c in db.Classes
                              where c.Level == lbllevel.Text
                              select new
                              {
                                  ClassId = c.ClassId,
                                  ClassDescription = c.ClassDescription,
                                  Level = c.Level,
                                  Day = c.Day.ToString(),
                                  Time = c.Time
                              };

                DataTable dt = new DataTable();
                dt.Columns.Add("ClassId");
                dt.Columns.Add("ClassDescription");
                dt.Columns.Add("Level");
                dt.Columns.Add("Day");
                dt.Columns.Add("Time");

                foreach (var @class in classes.ToList())
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = @class.ClassId;
                    dr[1] = @class.ClassDescription;
                    dr[2] = @class.Level;
                    dr[3] = @class.Day;
                    dr[4] = @class.Time.ToString("hh:mm:tt");

                    dt.Rows.Add(dr);
                }

                ncGrid.DataSource = dt;
                ncGrid.DataBind();
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument); ///value give row number that is clicked
            GridViewRow row = ncGrid.Rows[index]; // go particular
            var classSelected = row.Cells[0].Text;
            this.Session["ClassId"] = classSelected;
            Response.Redirect("ClassDetails");
        }

        protected void Pay_Click(object sender, EventArgs e)
        {
            Session["TranscationAmount"] = 70;
            Session["FirstMonth"] = true;

            string returnURL = "";
            returnURL += "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_xclick&business=perceptum-store@gmail.com";

            returnURL += "&item_name=" +"Tuition Fee ";

            returnURL += "&amount=1";

            returnURL += "&currency=SGD";

            returnURL += "&return=http://perceptumeducation.azurewebsites.net/Views/Parent/ParentStudents";

            returnURL += "&cancel_return=http://perceptumeducation.azurewebsites.net/Views/Parent/DisplayNewClasses";

            Response.Redirect(returnURL);
        }
    }
}