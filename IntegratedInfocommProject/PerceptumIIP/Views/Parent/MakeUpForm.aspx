﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Parent/Parent.Master" AutoEventWireup="true" CodeBehind="MakeUpForm.aspx.cs" Inherits="PerceptumIIP.Views.Parent.MakeUpForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Booking Confirmation</h1>

    <h2>Level</h2>
    <asp:Label runat="server" ID="Level"></asp:Label>

    <h2>Subject</h2>
    <asp:Label runat="server" ID="Subject"></asp:Label>

    <h2>Date</h2>
    <asp:Label runat="server" ID="Date"></asp:Label>

    <h2>Time</h2>
    <asp:Label runat="server" ID="Time"></asp:Label>


    <br /><br />
    <asp:Button CssClass="btn btn-success" runat="server" ID="Confirm" OnClick="Confirm_Click" Text="Confirm Booking" />
</asp:Content>
