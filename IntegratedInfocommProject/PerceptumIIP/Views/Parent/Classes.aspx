﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Parent/Parent.Master" AutoEventWireup="true" CodeBehind="Classes.aspx.cs" Inherits="PerceptumIIP.Views.Parent.Classes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <style>
        .class {
    width:100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color:white;
}

        
.class td {
    padding:10px; 
    border: solid 1px black;
    color:black;
}

    .class th {
        background-color:#f3e222;
        text-align:center;
        padding: 10px 2px;
        border: solid 1px black;
        color: black;
    }

.class tr:nth-child(even) {
    background-color: #ffffff;
}

.class tr:nth-child(odd) {
    background-color: #dcdcdc;
}

.class .alt{
    background:#dcdcdc;
}

.class pgr{

    background: #dcdcdc;

}
    .class pgr table {
        margin: 5px 0;
    }

    .class pgr td {
        border-width:0;
        padding: 0 6px;
        border-left: solid 1px black;
    }
   .class pgr a{
        color:black;
        text-decoration:none;
    }


       .mu {
    width:100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color:white;
}

        
.mu td {
    padding:10px; 
    border: solid 1px black;
    color:black;
}

    .mu th {
        background-color:#f3e222;
        text-align:center;
        padding: 10px 2px;
        border: solid 1px black;
        color: black;
    }

.mu tr:nth-child(even) {
    background-color: #ffffff;
}

.mu tr:nth-child(odd) {
    background-color: #dcdcdc;
}

.mu alt{
    background:#dcdcdc;
}

.mu pgr{

    background: #dcdcdc;

}
    .mu pgr table {
        margin: 5px 0;
    }

    .mu pgr td {
        border-width:0;
        padding: 0 6px;
        border-left: solid 1px black;
    }
   .mu pgr a{
        color:black;
        text-decoration:none;
    }
    </style>




    <h1><asp:Label ID="stuName" runat="server"></asp:Label> Classes</h1>

    <h2>Enrolled Classes</h2>
    <br />
    <asp:GridView ID="cGrid" runat="server"
        CssClass="class" 
        OnRowCommand="classesGrid_RowCommand" 
        AutoGenerateColumns="False" DataKeyNames="ClassId" GridLines="None" Font-Size="Medium" Font-Names="Arial" CellPadding="3" CellSpacing="10" AlternatingRowStyle-CssClass="alt" PageSize="7" PagerStyle-CssClass="pgr">
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:BoundField DataField="ClassId" HeaderText="ClassId" InsertVisible="False" ReadOnly="True" SortExpression="ClassId" />
            <asp:BoundField DataField="ClassDescription" HeaderText="ClassDescription" SortExpression="ClassDescription" />
            <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level" />
            <asp:BoundField DataField="Day" HeaderText="Day" SortExpression="Day" />
            <asp:BoundField DataField="Time" HeaderText="Time" SortExpression="Time" DataFormatString="{0:t}"/>
            <%--<asp:TemplateField HeaderText="Fee">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblFee">70</asp:Label>
                </ItemTemplate>
            </asp:TemplateField>--%>
            <asp:ButtonField Text="View Grades" CommandName="Grades"/>
            <asp:ButtonField Text="View Attendance" CommandName="Attendance" />
            <asp:ButtonField Text="View Feedback" CommandName="Feedback" />

        </Columns>
                                <PagerStyle CssClass="pgr"></PagerStyle>

    </asp:GridView>
    
    <asp:Button runat="server" Text="Register new classes" CssClass="btn btn-default" ID="Register" OnClick="Register_Click" />
    
    <br />
    <br />

    <%--<asp:SqlDataSource ID="cDS" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="
        SELECT * FROM Classes C
        INNER JOIN StudentClasses SC
        ON C.ClassId = SC.ClassId
        WHERE SC.StudentId = @StudentId">
        <SelectParameters>
            <asp:SessionParameter Name="StudentId" SessionField="StudentId" />
        </SelectParameters>
    </asp:SqlDataSource>--%>

    <br />
    <br />
     <h2>Make Up Classes</h2>
    <asp:GridView ID="muGrid" runat="server"
        CssClass="mu" AutoGenerateColumns="False" DataKeyNames="ClassId,ClassId1,StudentId,dateTime" DataSourceID="muDS" >
        <Columns>
            <asp:BoundField DataField="ClassId" HeaderText="ClassId" InsertVisible="False" ReadOnly="True" SortExpression="ClassId" />
            <asp:BoundField DataField="ClassDescription" HeaderText="ClassDescription" SortExpression="ClassDescription" />
            <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level" />
            <asp:BoundField DataField="Time" HeaderText="Time" SortExpression="Time"  DataFormatString="{0:t}"/>
            <asp:BoundField DataField="dateTime" HeaderText="dateTime" ReadOnly="True" SortExpression="Date"  DataFormatString="{0:D}"/>
        </Columns>

    </asp:GridView>

    <asp:SqlDataSource ID="muDS" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT *
        FROM Classes C
        INNER JOIN MakeUpClasses M
        ON C.ClassId = M.ClassId
        WHERE M.StudentId = @StudentID">
        <SelectParameters>
            <asp:SessionParameter Name="StudentId" SessionField="StudentId" />
        </SelectParameters>
    </asp:SqlDataSource>

    
    <br />
    
    <br />
    
</asp:Content>
