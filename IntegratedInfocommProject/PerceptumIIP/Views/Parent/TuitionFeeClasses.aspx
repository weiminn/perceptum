﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Parent/Parent.Master" AutoEventWireup="true" CodeBehind="TuitionFeeClasses.aspx.cs" Inherits="PerceptumIIP.Views.Parent.TuitionFeeClasses" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .ttclasses {
    width:100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color:white;
}


.ttclasses td {
    padding:10px; 
    border: solid 1px black;
    color:black;
}

        .ttclasses th {
        background-color:#f3e222;
        text-align:center;
        padding: 10px 2px;
        border: solid 1px black;
        color: black;
    }

.ttclasses tr:nth-child(even) {
    background-color: #ffffff;
}

.ttclasses tr:nth-child(odd) {
    background-color: #dcdcdc;
}

.ttclasses .alt{
    background:#dcdcdc;
}

.ttclasses .pgr{

    background: #dcdcdc;

}
    .ttclasses .pgr table {
        margin: 5px 0;
    }

    .ttclasses .pgr td {
        border-width:0;
        padding: 0 6px;
        border-left: solid 1px black;
    }
   .ttclasses .pgr a{
        color:black;
        text-decoration:none;
    }
    </style>
    <h1><asp:Label ID="sfName" runat="server"></asp:Label> Classes</h1>

    <asp:GridView ID="cfGrid" runat="server"
        CssClass="ttclasses"
        AutoGenerateColumns="False" GridLines="None" Font-Size="Medium" Font-Names="Arial" CellPadding="3" CellSpacing="10" AlternatingRowStyle-CssClass="alt" PageSize="7" PagerStyle-CssClass="pgr">
        <Columns>
            <asp:BoundField DataField="ClassId" HeaderText="ClassId" InsertVisible="False" ReadOnly="True" SortExpression="ClassId" />
            <asp:BoundField DataField="ClassDescription" HeaderText="ClassDescription" SortExpression="ClassDescription" />
            <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level" />
            <asp:BoundField DataField="Day" HeaderText="Day" SortExpression="Day"  />
            <asp:BoundField DataField="Time" HeaderText="Time" SortExpression="Time" DataFormatString="{0:t}" />
            <asp:TemplateField HeaderText="Monthly Fee">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblFee">70</asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
                        <PagerStyle CssClass="pgr"></PagerStyle>

    </asp:GridView>

    <asp:SqlDataSource ID="cfDS" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="
        SELECT * FROM Classes C
        INNER JOIN StudentClasses SC
        ON C.ClassId = SC.ClassId
        WHERE SC.StudentId = @StudentId">
        <SelectParameters>
            <asp:SessionParameter Name="StudentId" SessionField="StudentId" />
        </SelectParameters>
    </asp:SqlDataSource>

    <h3>Total Fees: <b><asp:Label runat="server" ID="lblTotalFee"></asp:Label></b></h3>

    <h2>Paid Tuition Fees History</h2>
    <asp:GridView ID="tGrid" runat="server"
        CssClass="ttclasses" Font-Size="Medium" Font-Names="Arial" CellPadding="3" CellSpacing="10" AlternatingRowStyle-CssClass="alt" PageSize="7" PagerStyle-CssClass="pgr">
    </asp:GridView>

 <%--   <asp:GridView ID="classesGrid" runat="server"
        CssClass="table table-hover table-striped" 
        OnRowCommand="classesGrid_RowCommand"
        >
        <Columns>
            <asp:ButtonField Text="View Grades" />
        </Columns>
    </asp:GridView>--%>
    
    <br />
    <asp:Button runat="server" Text="Pay Tuition Fees" CssClass="btn btn-default" ID="Pay" OnClick="Pay_Click" />
</asp:Content>
