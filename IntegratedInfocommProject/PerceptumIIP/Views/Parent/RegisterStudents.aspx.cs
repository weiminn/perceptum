﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PerceptumIIP.Models;

namespace PerceptumIIP.Views.Parent
{
    public partial class RegisterStudents : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }


        protected void Submit_Click(object sender, EventArgs e)
        {
            var userStore = new UserStore<IdentityUser>();
            var manager = new UserManager<IdentityUser>(userStore);

            // Need to create Identity user object
            var user = new IdentityUser()
            {
                UserName = UserName.Text,
                Email = Email.Text
            };
            IdentityResult result = manager.Create(user, Password.Text);

            if (result.Succeeded)
            {
                using (var db = new ApplicationDbContext())
                {
                    Student student = new Student
                    {
                        UserName = UserName.Text,
                        FirstName = FirstName.Text,
                        LastName = LastName.Text,
                        Email = Email.Text,
                        Password = Password.Text,
                        Address = Address.Text,
                        BirthDate = Convert.ToDateTime(DOB.Text),
                        School = School.Text,
                        Level = Level.Text,
                        Outlet = Branch.SelectedValue,
                        Telephone = Convert.ToInt32(Telephone.Text),
                        Image = FileUploadControl.FileBytes,
                        ApplicationUserId = user.Id

                    };


                    db.Students.Add(student);
                    db.SaveChanges();

                    StudentParent studentParent = new StudentParent
                    {

                        StudentId = student.StudentId,
                        ParentId = Convert.ToInt32(Session["ParentId"])
                    };

                    db.StudentParents.Add(studentParent);
                    db.SaveChanges();
                }
            }

            Session["Level"] = Level.Text; //save the level requested in one page 


            Response.Redirect("ParentStudents");

        }
    }

}
