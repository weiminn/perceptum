﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using PerceptumIIP.Models;
using Microsoft.AspNet.Identity;

namespace PerceptumIIP.Views.Parent
{
    public partial class RetrieveFile : System.Web.UI.Page
    {
        private int StudentId;

        protected void Page_Load(object sender, EventArgs e)
        {
            StudentId = Convert.ToInt32(Session["StudentId"]);
            if (!IsPostBack)
            {
                BindGrid();
            }
        }

        private void BindGrid()
        {
            using (var db = new ApplicationDbContext())
            {
                //string userId = User.Identity.GetUserId();

                //var currentParent = db.Parents.SingleOrDefault(x => x.ApplicationUserId == userId).ParentId;

                //var relatedFiles = from sp in db.StudentParents
                //                   join s in db.Students on sp.StudentId equals s.StudentId
                //                   join f in db.File on s.Level equals f.Level

                //                   where sp.ParentId == currentParent

                //                   select new
                //                   {
                //                       FileID = f.FileId,
                //                       Name = f.Level
                //                   };

                var level = db.Students.SingleOrDefault(x => x.StudentId == StudentId).Level;
                var files = db.File.Where(x => x.Level == level).Select(x => new { FileID = x.FileId, Name = x.Name });


                FilesGrid.DataSource = files.ToList();
                FilesGrid.DataBind();
            }
        }


        protected void FilesGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {


            if (e.CommandName == "Download")

            {

                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = FilesGrid.Rows[index];
                var FileSelected = Convert.ToInt32(row.Cells[0].Text);

                using (var db = new ApplicationDbContext())

                {

                    var File = db.File.Where(f => f.FileId.Equals(FileSelected)).FirstOrDefault();

                    if (File != null)

                    {

                        Response.ContentType = File.ContentType;

                        Response.AddHeader("content-disposition", "attachment; filename=" + File.Name);

                        Response.BinaryWrite(File.Data);

                        Response.Flush();

                        Response.End();

                    }

                }

            }





        }
    }
}



