﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Parent/Parent.Master" AutoEventWireup="true" CodeBehind="StudentGrades.aspx.cs" Inherits="PerceptumIIP.Views.Parent.StudentGrades" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .studentgrades {
    width:100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color:white;
}


.studentgrades td {
    padding:10px; 
    border: solid 1px black;
    color:black;
}

.studentgrades th {
    background-color:#f3e222;
    text-align:center;
    padding: 10px 2px;
    border: solid 1px black;
    color: black;
}

.studentgrades tr:nth-child(even) {
    background-color: #ffffff;
}

.studentgrades tr:nth-child(odd) {
    background-color: #dcdcdc;
}

.studentgrades .alt{
    background:#dcdcdc;
}

.studentgrades .pgr{

    background: #dcdcdc;

}
    .studentgrades .pgr table {
        margin: 5px 0;
    }

    .studentgrades .pgr td {
        border-width:0;
        padding: 0 6px;
        border-left: solid 1px black;
    }
   .studentgrades .pgr a{
        color:black;
        text-decoration:none;
    }
    </style>

    <h2><asp:Label runat="server" ID="stuName"></asp:Label>'s Grades</h2>
    <h4>Class ID: <asp:Label runat="server" ID="lblClassId"></asp:Label></h4>
    <h4>Class Subject: <asp:Label runat="server" ID="lblClassSubject"></asp:Label></h4>
    <h4>Teacher: <asp:Label runat="server" ID="lblTeacher"></asp:Label></h4>

    <asp:GridView ID="gGrid" runat="server" AutoGenerateColumns="False" DataKeyNames="ClassId,StudentId,EmployeeId" DataSourceID="gDS" OnRowCommand="gGrid_RowCommand"
        CssClass="studentgrades">
        <Columns>
            <asp:BoundField DataField="Month" HeaderText="Month" DataFormatString="{0:MMM yyyy}" SortExpression="Month" />
            <asp:BoundField DataField="Score" HeaderText="Grades" SortExpression="Grades" />
            <asp:ButtonField Text="Give Feedback" />
        </Columns>

    </asp:GridView>
    <asp:SqlDataSource ID="gDS" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM Grades G
WHERE G.StudentId = @StudentId AND G.ClassId = @ClassId;">
        <SelectParameters>
            <asp:SessionParameter Name="StudentId" SessionField="StudentId" />
            <asp:SessionParameter Name="ClassId" SessionField="ClassId" />
        </SelectParameters>
    </asp:SqlDataSource>

    <%--<asp:Button runat="server" Text="Give Feedback" CssClass="btn btn-info" ID="giveFeedback" OnClick="giveFeedback_Click" />--%>
</asp:Content>
