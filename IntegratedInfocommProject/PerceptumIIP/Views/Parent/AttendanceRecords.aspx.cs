﻿using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PerceptumIIP.Views.Parent
{
    public partial class AttendanceRecords : System.Web.UI.Page
    {
        private int ClassId;
        private int StudentId;

        protected void Page_Load(object sender, EventArgs e)
        {
            ClassId = Convert.ToInt32(Session["ClassId"]);
            StudentId = Convert.ToInt32(Session["StudentId"]);

            using (var db = new ApplicationDbContext())
            {
                var student = db.Students.SingleOrDefault(x => x.StudentId == StudentId);
                var theClass = db.Classes.SingleOrDefault(x => x.ClassId == ClassId);

                var employeeclass = db.EmployeeClasses.FirstOrDefault(x => x.ClassId == ClassId); //return array of records  

                var employee = db.Employees.FirstOrDefault(x => x.EmployeeId == employeeclass.EmployeeId);

                if (student != null && theClass != null)
                {
                    stuName.Text = student.FirstName + " " + student.LastName;
                    lblClassId.Text = theClass.ClassId.ToString();
                    lblClassSubject.Text = theClass.ClassDescription;
                    lblTeacher.Text = employee.FirstName;
                    lblClassTime.Text = theClass.Time.ToString("HH:mm");
                }
            }
        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {

        }

        protected void GoMakeup_Click(object sender, EventArgs e)
        {
            Response.Redirect("MakeUp.aspx");
        }
    }
}