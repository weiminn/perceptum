﻿using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PerceptumIIP.Views.Parent
{
    public partial class ClassDetails : System.Web.UI.Page
    {
        private int StudentId;
        private int ClassId;

        protected void Page_Load(object sender, EventArgs e)
        {
            StudentId = Convert.ToInt32(Session["StudentId"]);
            ClassId = Convert.ToInt32(Session["ClassId"]);

            using (var db = new ApplicationDbContext())
            {
                Class recordClass = db.Classes.SingleOrDefault(x => x.ClassId == ClassId);
                EmployeeClass employeeClass = db.EmployeeClasses.FirstOrDefault(x => x.ClassId == ClassId);
                Employee employee = db.Employees.SingleOrDefault(x => x.EmployeeId == employeeClass.EmployeeId);

                lblDay.Text = recordClass.Day.ToString();
                lblLecturer.Text = employee.FirstName + " " + employee.LastName;
                lblTime.Text = recordClass.Time.ToString("HH:mm");
            }
        }

        protected void register_Click(object sender, EventArgs e)
        {
            using(var db = new ApplicationDbContext())
            {
                StudentClass theClass = db.StudentClasses.SingleOrDefault(x => x.StudentId == StudentId && x.ClassId == ClassId);

                if(theClass != null)
                {
                    Response.Write(@"<script language='javascript'>alert('This student is present for this class!');</script>");
                    return;
                }

                Session["TranscationAmount"] = 70;
                Session["FirstMonth"] = true;

                string returnURL = "";
                returnURL += "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_xclick&business=perceptum-store@gmail.com";

                returnURL += "&item_name=" + "Tuition Fee";

                returnURL += "&amount=" + 70;

                returnURL += "&currency=SGD";

                returnURL += "&return=http://perceptumeducation.azurewebsites.net/Views/Parent/PaymentComplete";

                returnURL += "&cancel_return=http://perceptumeducation.azurewebsites.net/Views/Parent/ClassDetails";

                Response.Redirect(returnURL);
            }
        }
    }
}