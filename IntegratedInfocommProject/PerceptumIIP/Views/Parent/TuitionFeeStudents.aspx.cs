﻿using Microsoft.AspNet.Identity;
using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PerceptumIIP.Views.Parent
{
    public partial class TuitionFees : System.Web.UI.Page
    {
        private int currentParent;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["ParentId"] = currentParent;
            string userId = User.Identity.GetUserId();

            if (userId != null)
            {
                using (var db = new ApplicationDbContext())
                {
                    currentParent = db.Parents.SingleOrDefault(x => x.ApplicationUserId == userId).ParentId;
                    Session["ParentId"] = currentParent;
                }
            }
        }

        protected void sfGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument); ///value give row number that is clicked
            GridViewRow row = sfGrid.Rows[index];
            var studentselected = row.Cells[0].Text;
            this.Session["StudentId"] = studentselected;

            Response.Redirect("TuitionFeeClasses.aspx");
        }
    }
}