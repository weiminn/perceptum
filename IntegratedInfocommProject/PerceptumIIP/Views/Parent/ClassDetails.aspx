﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Parent/Parent.Master" AutoEventWireup="true" CodeBehind="ClassDetails.aspx.cs" Inherits="PerceptumIIP.Views.Parent.ClassDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Description</h1>
    <div class="row">
        <div class="col-md-3">
            <h3>Teacher</h3>
            </div>
        <div class="col-md-3">
           <h3><asp:label runat="server" ID="lblLecturer"></asp:label> </h3>

        </div>
    </div>

     <div class="row">
        <div class="col-md-3">
           <h3> Day </h3>
            </div>
        <div class="col-md-3">
<h3><asp:label runat="server" ID="lblDay"></asp:label> </h3>
        </div>
    </div>
   
    <div class="row">
        <div class="col-md-3">
          <h3> Time </h3>
            </div>
        <div class="col-md-3">
<h3><asp:label runat="server" ID="lblTime"></asp:label>       </h3>

        </div>
    </div>

    <br />

    <asp:Button runat="server" ID="register" OnClick="register_Click" CssClass="btn btn-default" Text="Register"/>
</asp:Content>
