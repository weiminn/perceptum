﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Parent/Parent.Master" AutoEventWireup="true" CodeBehind="TuitionFeeStudents.aspx.cs" Inherits="PerceptumIIP.Views.Parent.TuitionFees" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .tuitionfeestud {
    width:100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color:white;
}


.tuitionfeestud td {
    padding:10px; 
    border: solid 1px black;
    color:black;
}

    .tuitionfeestud th {
        background-color:#f3e222;
        text-align:center;
        padding: 10px 2px;
        border: solid 1px black;
        color: black;
    }

.tuitionfeestud tr:nth-child(even) {
    background-color: #ffffff;
}

.tuitionfeestud tr:nth-child(odd) {
    background-color: #dcdcdc;
}

.tuitionfeestud .alt{
    background:#dcdcdc;
}

.tuitionfeestud .pgr{

    background: #dcdcdc;

}
    .tuitionfeestud .pgr table {
        margin: 5px 0;
    }

    .tuitionfeestud .pgr td {
        border-width:0;
        padding: 0 6px;
        border-left: solid 1px black;
    }
   .tuitionfeestud .pgr a{
        color:black;
        text-decoration:none;
    }
    </style>
    <h2>Your Students</h2>

    <asp:GridView ID="sfGrid" runat="server" OnRowCommand="sfGrid_RowCommand"
        CssClass="tuitionfeestud" AutoGenerateColumns="False"
        DataSourceID="psDS" GridLines="None" Font-Size="Medium" Font-Names="Arial" CellPadding="3" CellSpacing="10" AlternatingRowStyle-CssClass="alt" PageSize="7" PagerStyle-CssClass="pgr">
        <Columns>
            <asp:BoundField DataField="StudentId" HeaderText="StudentId" InsertVisible="False" ReadOnly="True" SortExpression="StudentId" />
            <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName" />
            <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName" />
            <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
            <asp:BoundField DataField="BirthDate" HeaderText="BirthDate" SortExpression="BirthDate" DataFormatString="{0:dd/MM/yyyy}" />
            <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level" />
            <asp:BoundField DataField="Telephone" HeaderText="Telephone" SortExpression="Telephone" />
            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
            <asp:BoundField DataField="Outlet" HeaderText="Outlet" SortExpression="Outlet" />
            
            <asp:ButtonField Text="View Fees" />
        </Columns>
                <PagerStyle CssClass="pgr"></PagerStyle>

    </asp:GridView>

    <asp:SqlDataSource ID="psDS" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" 
        SelectCommand="
        SELECT * FROM Students S
        INNER JOIN StudentParents SP
        ON S.StudentId = SP.StudentId
        WHERE SP.ParentId = @ParentId"
        >
        <SelectParameters>
            <asp:SessionParameter Name="ParentId" SessionField="ParentId" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
