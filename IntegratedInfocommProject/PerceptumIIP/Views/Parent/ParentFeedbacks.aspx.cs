﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PerceptumIIP.Models;

namespace PerceptumIIP
{
    public partial class ParentFeedbacks : System.Web.UI.Page
    {
        private int ClassId;
        private int ParentId;
        //private int ;
        protected void Page_Load(object sender, EventArgs e)
        {
            ParentId = Convert.ToInt32(Session["ParentId"]);
            ClassId = Convert.ToInt32(Session["ClassId"]);
            using (var db = new ApplicationDbContext())
            {


                var feedbacks = db.FeedBacks.Where(x => x.ClassId == ClassId && x.ParentId == ParentId).ToList();

                fdkgrid.DataSource = feedbacks;
                fdkgrid.DataBind();
            }
        }
    }
}