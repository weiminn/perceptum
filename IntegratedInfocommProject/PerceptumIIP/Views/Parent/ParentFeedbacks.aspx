﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Parent/Parent.Master" AutoEventWireup="true" CodeBehind="ParentFeedbacks.aspx.cs" Inherits="PerceptumIIP.ParentFeedbacks" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    

    <style>
        .fdk {
    width:100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color:white;
}

        
.fdk td {
    padding:10px; 
    border: solid 1px black;
    color:black;
}

    .fdk th {
        background-color:#f3e222;
        text-align:center;
        padding: 10px 2px;
        border: solid 1px black;
        color: black;
    }

.fdk tr:nth-child(even) {
    background-color: #ffffff;
}

.fdk tr:nth-child(odd) {
    background-color: #dcdcdc;
}

.fdk .alt{
    background:#dcdcdc;
}

.fdk pgr{

    background: #dcdcdc;

}
    .fdk pgr table {
        margin: 5px 0;
    }

    .fdk pgr td {
        border-width:0;
        padding: 0 6px;
        border-left: solid 1px black;
    }
   .fdk pgr a{
        color:black;
        text-decoration:none;
    }
        </style>



<asp:GridView ID="fdkgrid" AutoGenerateColumns="false"
    CssClass="fdk" runat="server" GridLines="None" Font-Size="Medium" Font-Names="Arial" CellPadding="3" CellSpacing="10" AlternatingRowStyle-CssClass="alt" PageSize="7" PagerStyle-CssClass="pgr">
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>   
    
    <Columns>
        <asp:BoundField DataField="Details" HeaderText="Details" />
        <asp:BoundField DataField="Method" HeaderText="Method" />
        <asp:BoundField DataField="Material" HeaderText="Material" />
        <asp:BoundField DataField="Tests" HeaderText="Tests" />
        <asp:BoundField DataField="Management" HeaderText="Management" />

    </Columns>
  
    <PagerStyle CssClass="pgr"></PagerStyle>

</asp:GridView>
</asp:Content>
