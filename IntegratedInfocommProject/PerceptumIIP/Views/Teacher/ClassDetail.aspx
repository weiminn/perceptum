﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Teacher/Teacher.Master" AutoEventWireup="true" CodeBehind="ClassDetail.aspx.cs" Inherits="PerceptumIIP.Views.Teacher.ClassDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


    <style>

         .classdetail {
    width: 100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color: white;
}


    .classdetail td {
        padding: 10px;
        border: solid 1px black;
        color: black;
    }

    .classdetail th {
        background-color: #f3e222;
        text-align: center;
        padding: 10px 2px;
        border: solid 1px black;
        color: black;
    }

    .classdetail tr:nth-child(even) {
        background-color: #ffffff;
    }

    .classdetail tr:nth-child(odd) {
        background-color: #dcdcdc;
    }

    .classdetail .alt {
        background: #dcdcdc;
    }

    .classdetail .pgr {
        background: #dcdcdc;
    }

        .classdetail .pgr table {
            margin: 5px 0;
        }

        .classdetail .pgr td {
            border-width: 0;
            padding: 0 6px;
            border-left: solid 1px black;
        }

        .classdetail .pgr a {
            color: black;
            text-decoration: none;
        }





    </style>

    <h2>Class</h2>
   <h4> Class ID: <asp:Label ID="ClassID" runat="server"></asp:Label><br /> </h4>
   <h4>Subject: <asp:Label ID="Subject" runat="server"></asp:Label><br /> </h4>
  <h4> Level: <asp:Label ID="Level" runat="server"></asp:Label><br /> </h4>
 <h4> Day: <asp:Label ID="Day" runat="server"></asp:Label><br /> </h4>
  <h4> Time: <asp:Label ID="Time" runat="server"></asp:Label> </h4>

    <h2>Students</h2>
    <asp:GridView runat="server" ID="sGrid"  CssClass="classdetail"
        AutoGenerateColumns="False" DataKeyNames="StudentId,StudentId1,ClassId" DataSourceID="SqlDataSource1"
        onrowcommand="sGrid_RowCommand">
        <Columns>
            <asp:BoundField DataField="StudentId" HeaderText="StudentId" InsertVisible="False" ReadOnly="True" SortExpression="StudentId" />
            <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName" />
            <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName" />
            <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
            <asp:BoundField DataField="BirthDate" HeaderText="BirthDate" SortExpression="BirthDate" DataFormatString="{0:dd/MM/yyyy}" />
            <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level" />
            <asp:BoundField DataField="Telephone" HeaderText="Telephone" SortExpression="Telephone" />
            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
            <asp:ButtonField Text="Give Grade" />
        </Columns>
    </asp:GridView>
            <br />

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM Students S
INNER JOIN StudentClasses SC
ON S.StudentId = SC.StudentId
WHERE SC.ClassId = @ClassId">
        <SelectParameters>
            <asp:SessionParameter Name="ClassId" SessionField="ClassId" />
        </SelectParameters>
    </asp:SqlDataSource>

    <h2>Feedbacks</h2>
    <asp:GridView runat="server" ID="fGrid" AutoGenerateColumns="False" CssClass="classdetail" 
        DataKeyNames="FeedbackId,StudentId1" DataSourceID="fDS">
        <Columns>
            <asp:BoundField DataField="Details" HeaderText="Details" SortExpression="Details" />
            <asp:BoundField DataField="Method" HeaderText="Method" SortExpression="Method" />
            <asp:BoundField DataField="Material" HeaderText="Material" SortExpression="Material" />
            <asp:BoundField DataField="Tests" HeaderText="Tests" SortExpression="Tests" />
            <asp:BoundField DataField="Management" HeaderText="Management" SortExpression="Management" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="fDS" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="        SELECT * FROM Feedbacks F
        INNER JOIN Students S
        ON F.StudentId = S.StudentId
        WHERE F.ClassId = @ClassId
        AND f.Status = 'Approved';">
        <SelectParameters>
            <asp:SessionParameter Name="ClassId" SessionField="ClassId" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
