﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Teacher/Teacher.Master" AutoEventWireup="true" CodeBehind="TeacherClasses.aspx.cs" Inherits="PerceptumIIP.Views.Teacher.TeacherClasses" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>
                .classesgrid {
    width: 100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color: white;
}


    .classesgrid td {
        padding: 10px;
        border: solid 1px black;
        color: black;
    }

    .classesgrid th {
        background-color: #f3e222;
        text-align: center;
        padding: 10px 2px;
        border: solid 1px black;
        color: black;
    }

    .classesgrid tr:nth-child(even) {
        background-color: #ffffff;
    }

    .classesgrid tr:nth-child(odd) {
        background-color: #dcdcdc;
    }

    .classesgrid .alt {
        background: #dcdcdc;
    }

    .classesgrid .pgr {
        background: #dcdcdc;
    }

        .classesgrid .pgr table {
            margin: 5px 0;
        }

        .classesgrid .pgr td {
            border-width: 0;
            padding: 0 6px;
            border-left: solid 1px black;
        }

        .classesgrid .pgr a {
            color: black;
            text-decoration: none;
        }
    </style>



    <h1>Classes</h1>
    <asp:GridView runat="server" ID="cGrid" CssClass="classesgrid" OnRowCommand="cGrid_RowCommand"
        AutoGenerateColumns="False"  GridLines="None" Font-Size="Medium" Font-Names="Arial" CellPadding="3" CellSpacing="10" AlternatingRowStyle-CssClass="alt" PageSize="7" PagerStyle-CssClass="pgr">
        <Columns>
            <asp:BoundField DataField="ClassId" HeaderText="ClassId" InsertVisible="False" ReadOnly="True" SortExpression="ClassId" />
            <asp:BoundField DataField="ClassDescription" HeaderText="ClassDescription" SortExpression="ClassDescription" />
            <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level" />
            <asp:BoundField DataField="Day" HeaderText="Day" SortExpression="Day"/>
            <asp:BoundField DataField="Time" HeaderText="Time" SortExpression="Time" DataFormatString="{0:t}" />
            <asp:ButtonField Text="Detail" CommandName="Detail"/>
            <asp:ButtonField Text="Attendance" CommandName="Attendance"/>
        </Columns>
                <PagerStyle CssClass="pgr"></PagerStyle>

    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand=
        "SELECT * FROM Classes C
        INNER JOIN EmployeeClasses EC
        ON ec.ClassId = c.ClassId
        WHERE EC.EmployeeId = @EmployeeId">
        <SelectParameters>
            <asp:SessionParameter Name="EmployeeId" SessionField="EmployeeId" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
