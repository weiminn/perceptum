﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Teacher/Teacher.Master" AutoEventWireup="true" CodeBehind="GradeRecords.aspx.cs" Inherits="PerceptumIIP.Views.Teacher.GradeRecords" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <style>
        .ui-datepicker-calendar {
            display: none;
        }
        .graderecords {
    width: 100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color: white;
}


    .graderecords td {
        padding: 10px;
        border: solid 1px black;
        color: black;
    }

    .graderecords th {
        background-color: #f3e222;
        text-align: center;
        padding: 10px 2px;
        border: solid 1px black;
        color: black;
    }

    .graderecords tr:nth-child(even) {
        background-color: #ffffff;
    }

    .graderecords tr:nth-child(odd) {
        background-color: #dcdcdc;
    }

    .graderecords .alt {
        background: #dcdcdc;
    }

    .graderecords .pgr {
        background: #dcdcdc;
    }

        .graderecords .pgr table {
            margin: 5px 0;
        }

        .graderecords .pgr td {
            border-width: 0;
            padding: 0 6px;
            border-left: solid 1px black;
        }

        .graderecords .pgr a {
            color: black;
            text-decoration: none;
        }

         @media (min-width: 768px) and (max-width: 992px) {
            .row {
                font-size: 20px;
            }
            .form-control {
                height: 40px;
            }
        }
    </style>

    
    <h2>Grading Records</h2>
    <br />
    <div class="row">
        <div class="form-group col-md-2">
            Choose Month
            <asp:textbox ID="txtMonth" runat="server" cssclass="datepicker-field form-control"/>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtMonth" CssClass="text-danger" Display="Dynamic" ErrorMessage="The month field is required">

           </asp:RequiredFieldValidator>
        </div>
    </div>

    
    <div class="row">
        <div class="form-group col-md-2">
            Choose Subject
            <asp:DropDownList runat="server" id="subjectsDDL" CssClass="form-control" AutoPostBack="true">
                <asp:ListItem Value="" Selected="True"> </asp:ListItem>

                <asp:ListItem Value="Mathematics">Mathematics</asp:ListItem>
                                <asp:ListItem Value="Additional Mathematics"> Additional Mathematics</asp:ListItem>
                <asp:ListItem Value="Chemistry">Chemistry</asp:ListItem>

                <asp:ListItem Value="Science">Science</asp:ListItem>
            </asp:DropDownList>

     <asp:RequiredFieldValidator runat="server" ControlToValidate="subjectsDDL" CssClass="text-danger" Display="Dynamic" ErrorMessage="The subject field is required">

           </asp:RequiredFieldValidator>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-2">
            Choose Day
            <asp:DropDownList runat="server" id="dayDDL" CssClass="form-control" AutoPostBack="true">
                <asp:ListItem Value="" Selected="True"> </asp:ListItem>

                <asp:ListItem Value="Monday">Monday</asp:ListItem>
                <asp:ListItem Value="Tuesday">Tuesday</asp:ListItem>
                <asp:ListItem Value="Wednesday">Wednesday</asp:ListItem>
                <asp:ListItem Value="Thursday">Thursday</asp:ListItem>
                <asp:ListItem Value="Friday">Friday</asp:ListItem>
                <asp:ListItem Value="Saturday">Saturday</asp:ListItem>
                <asp:ListItem Value="Sunday"></asp:ListItem>
            </asp:DropDownList>
             <asp:RequiredFieldValidator runat="server" ControlToValidate="dayDDL" CssClass="text-danger" Display="Dynamic" ErrorMessage="The day field is required">

           </asp:RequiredFieldValidator>
        </div>
    </div>


    <div class="row">
        <div class="form-group col-md-2">
            Choose Time
            <asp:textbox ID="txtTime" runat="server" cssclass="form-control"/>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtTime" CssClass="text-danger" Display="Dynamic" ErrorMessage="The Time field is required">

           </asp:RequiredFieldValidator>
        </div>
    </div>


    <div class="row">
        <div class="form-group col-md-2">
            <asp:Button ID="search" OnClick="search_Click" runat="server" CssClass="btn btn-default" Text="Search" />
        </div>
    </div>


    <asp:GridView CssClass="graderecords"
        runat="server" ID="gGrid">
    </asp:GridView>

    <script>
        $(document).ready(function(){
            $('.datepicker-field').datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'MM yy',
                onClose: function(dateText, inst) { 
                    $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                }
            });   
            
            $('#txtTime').timepicker({});
        });
    </script>
</asp:Content>
