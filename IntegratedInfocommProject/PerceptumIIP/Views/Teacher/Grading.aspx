﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Teacher/Teacher.Master" AutoEventWireup="true" CodeBehind="Grading.aspx.cs" Inherits="PerceptumIIP.Views.Teacher.Grading" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .ui-datepicker-calendar {
            display: none;
        }
             

         @media (min-width: 768px) and (max-width: 992px) {
            .row {
                font-size: 30px;
            }
            input[type="text"], input[type="password"], input[type="email"], input[type="tel"], input[type="select"] {
                max-width: 480px;
            }
            .form-control {
                height: 50px;
            }
        }
    </style>
    

    <h2>Grading Student</h2>
    <div class="row">
        <div class="col-md-6">
            <h2> <asp:Label ID="lblName" runat="server"></asp:Label></h2>
            Student ID: <asp:Label ID="lblStudentId" runat="server"></asp:Label><br />

            Class ID: <asp:Label ID="lblClass" runat="server"></asp:Label><br />
            Subject: <asp:Label ID="lblSubject" runat="server"></asp:Label><br />
            Level: <asp:Label ID="lblLevel" runat="server"></asp:Label><br />
        </div>

    </div>
    
    <br />
            <div class="row">
                <div class="form-group">
            <asp:Label runat="server" CssClass="col-md-2 control-label">Choose Month</asp:Label>
                    <div class="col-md-4">
                    <asp:textbox ID="txtMonth" runat="server" cssclass="datepicker-field form-control"/>
                        </div>
                </div>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtMonth" runat="server" CssClass="text-danger" Display="Dynamic" ErrorMessage="Month field is required"></asp:RequiredFieldValidator>
            </div>
    <br />
            <div class="row">
                <div class="form-group">
                    <asp:Label runat="server" CssClass="col-md-2 control-label">Conduct Grade</asp:Label>
                    <div class="col-md-4">
                        <asp:DropDownList runat="server" ID="thisMonthGrade" CssClass="form-control">
                            <asp:ListItem Value="" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="A+">A+</asp:ListItem>
                            <asp:ListItem Value="A">A</asp:ListItem>
                            <asp:ListItem Value="B">B</asp:ListItem>
                            <asp:ListItem Value="C">C</asp:ListItem>
                            <asp:ListItem Value="D">D</asp:ListItem>
                            <asp:ListItem Value="F">F</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                      <asp:RequiredFieldValidator runat="server" ControlToValidate="thisMonthGrade" CssClass="text-danger" Display="Dynamic" ErrorMessage="The grade field is required">

           </asp:RequiredFieldValidator>
                </div>
            </div>
            

            <asp:Button CssClass="btn btn-default" ID="thisMonthSubmit" OnClick="thisMonthSubmit_Click" runat="server" Text="Submit Grade" />
        
        

        <script>
        $(document).ready(function(){
            $('.datepicker-field').datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'MM yy',
                onClose: function(dateText, inst) { 
                    $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                }
            });   
        });
    </script>
</asp:Content>
