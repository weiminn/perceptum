﻿using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PerceptumIIP.Views.Teacher
{
    public partial class GradeRecords : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void search_Click(object sender, EventArgs e)
        {
            var month = Convert.ToDateTime(txtMonth.Text).Month;
            var hour = Convert.ToDateTime(txtTime.Text);
            var day = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), dayDDL.SelectedValue);//Convert.ToDateTime(dayDDL.SelectedValue).DayOfWeek;

            using (var db = new ApplicationDbContext())
            {
                var grades = from g in db.Grades
                             join s in db.Students on g.StudentId equals s.StudentId
                             join c in db.Classes on g.ClassId equals c.ClassId
                             where 
                             g.Month.Month == month &&
                             c.Day == day &&
                             c.Time.Hour == hour.Hour &&
                             c.ClassDescription == subjectsDDL.SelectedValue

                             select new {
                                 StudentID = s.StudentId,
                                 Name = s.FirstName + " " + s.LastName,
                                 Month = g.Month,
                                 Grade = g.Score
                             };

                if (grades.ToList().Count == 0)
                {
                    Response.Write(@"<script language='javascript'>alert('No grade records are available for the selected options!');</script>");
                    return;
                }

                gGrid.DataSource = grades.ToList().Select(
                    x => 
                        new
                        {
                            StudentID = x.StudentID,
                            Name = x.Name,
                            Month = x.Month.ToString("MMM yyyy"),
                            Grade = x.Grade
                        }
                    );

                gGrid.DataBind();
            }
        }
    }
}