﻿using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PerceptumIIP.Views.Teacher
{
    public partial class AttendanceMarking : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int classId = Convert.ToInt32(Session["ClassId"]);
            using (var db = new ApplicationDbContext())
            {
                var students = from s in db.Students
                               join sc in db.StudentClasses on s.StudentId equals sc.StudentId
                               where sc.ClassId == classId
                               select new
                               {
                                   StudentID = s.StudentId,
                                   Name = s.FirstName + " " + s.LastName,
                                   Email = s.Email
                               };

                DataTable dt = new DataTable();
                dt.Columns.Add("StudentID");
                dt.Columns.Add("Name"); 
                dt.Columns.Add("Email");
                dt.Columns.Add("Status");

                foreach(var student in students)
                {
                    DataRow row = dt.NewRow();
                    row[0] = student.StudentID;
                    row[1] = student.Name;
                    row[2] = student.Email;

                    DateTime today = DateTime.Now.Date;
                    Attendance attendance = db.Attendances.SingleOrDefault(
                        x =>
                        x.StudentId == student.StudentID &&
                        x.Date == today &&
                        x.ClassId == classId
                        );
                    
                    if(attendance != null)
                    {
                        row[3] = attendance.Status;
                    }

                    dt.Rows.Add(row);
                }

                saGrid.DataSource = dt;
                saGrid.DataBind();
            }
        }

        protected void saGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = saGrid.Rows[index];
            int studentId = Convert.ToInt32(row.Cells[0].Text);

            int classId = Convert.ToInt32(Session["ClassId"]);
            String status = e.CommandName.ToString();
            DateTime today = DateTime.Now.Date;

            using (var db = new ApplicationDbContext())
            {
                Attendance studentAttendance = db.Attendances.SingleOrDefault(
                    x =>
                    x.StudentId == studentId &&
                    x.Date == today &&
                    x.ClassId == classId
                    );

                if(studentAttendance == null)
                {
                    Attendance newAttendance = new Attendance
                    {
                        ClassId = classId,
                        StudentId = studentId,
                        Date = today,
                        Status = status
                    };
                    db.Attendances.Add(newAttendance);
                    db.SaveChanges();
                }
                else
                {
                    studentAttendance.Status = status;
                    db.SaveChanges();
                }

                Response.Redirect(Request.RawUrl);
            }
        }
    }
}