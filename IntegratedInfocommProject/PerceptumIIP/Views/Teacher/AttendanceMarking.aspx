﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Teacher/Teacher.Master" AutoEventWireup="true" CodeBehind="AttendanceMarking.aspx.cs" Inherits="PerceptumIIP.Views.Teacher.AttendanceMarking" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <style>
        .attendancegridview {
    width:100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color:white;
}


.attendancegridview td {
    padding:10px; 
    border: solid 1px black;
    color:black;
}

    .attendancegridview th {
        background-color:#f3e222;
        text-align:center;
        padding: 10px 2px;
        border: solid 1px black;
        color: black;
    }
.attendancegridview tr:nth-child(even) {
    background-color: #ffffff;
}

.attendancegridview tr:nth-child(odd) {
    background-color: #dcdcdc;
}

.attendencegridview .alt{
    background:#dcdcdc;
}

.attendancegridview .pgr{

    background: #dcdcdc;

}
.attendancegridview .pgr table {
    margin: 5px 0;
}

.attendancegridview .pgr td {
    border-width:0;
    padding: 0 6px;
    border-left: solid 1px black;
}
.attendancegridview .pgr a{
    color:black;
    text-decoration:none;
}
@media (min-width: 768px) and (max-width: 992px) {
             .attendancegridview th {
                 font-size: 20px;
             }

             .attendancegridview td {
                 font-size: 20px;
             }
         }
</style>


    <h2>Attendance Marking</h2>

    <h3>Students</h3>
    <asp:GridView ID="saGrid" runat="server" CssClass="attendancegridview" OnRowCommand="saGrid_RowCommand" AutoGenerateColumns="false">
        <Columns>
            <asp:BoundField DataField="StudentId" HeaderText="StudentId" ReadOnly="True" SortExpression="StudentId" InsertVisible="False" />
            <asp:BoundField DataField="Name" HeaderText="FirstName" SortExpression="FirstName" />
            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
            <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
            <asp:ButtonField Text="Present" CommandName="Present" />
            <asp:ButtonField Text="Absent" CommandName="Absent" />
        </Columns>

    </asp:GridView>
    <asp:SqlDataSource ID="saDS" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="
        SELECT S.StudentId, S.FirstName, S.LastName, S.Level, S.Telephone, S.Email,
        SC
        FROM Students S
        INNER JOIN StudentClasses SC
        ON SC.StudentId = S.StudentId
        INNER JOIN Attendances A
        ON A.StudentId = S.StudentId
        WHERE SC.ClassId = @ClassId">
        <SelectParameters>
            <asp:SessionParameter Name="ClassId" SessionField="ClassId" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
