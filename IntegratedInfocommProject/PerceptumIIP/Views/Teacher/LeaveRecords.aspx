﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Teacher/Teacher.Master" AutoEventWireup="true" CodeBehind="LeaveRecords.aspx.cs" Inherits="PerceptumIIP.Views.Teacher.LeaveRecords" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>
                .leaverecords {
    width: 100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color: white;
}


    .leaverecords td {
        padding: 10px;
        border: solid 1px black;
        color: black;
    }

    .leaverecords th {
        background-color: #f3e222;
        text-align: center;
        padding: 10px 2px;
        border: solid 1px black;
        color: black;
    }

    .leaverecords tr:nth-child(even) {
        background-color: #ffffff;
    }

    .leaverecords tr:nth-child(odd) {
        background-color: #dcdcdc;
    }

    .leaverecords .alt {
        background: #dcdcdc;
    }

    .leaverecords .pgr {
        background: #dcdcdc;
    }

        .leaverecords .pgr table {
            margin: 5px 0;
        }

        .leaverecords .pgr td {
            border-width: 0;
            padding: 0 6px;
            border-left: solid 1px black;
        }

        .leaverecords .pgr a {
            color: black;
            text-decoration: none;
        }
    </style>



    <h2>Leave Application Records</h2>
    <br />
    <asp:GridView ID="lGrid" runat="server"
        CssClass="leaverecords" AutoGenerateColumns="False" DataKeyNames="LeaveId" DataSourceID="lDS" GridLines="None" Font-Size="Medium" Font-Names="Arial" CellPadding="3" CellSpacing="10" AlternatingRowStyle-CssClass="alt" PageSize="7" PagerStyle-CssClass="pgr">
        <Columns>
            <asp:BoundField DataField="LeaveId" HeaderText="LeaveId" InsertVisible="False" ReadOnly="True" SortExpression="LeaveId" />
            <asp:BoundField DataField="Type" HeaderText="Type" SortExpression="Type" />
            <asp:BoundField DataField="Reason" HeaderText="Reason" SortExpression="Reason" />
            <asp:BoundField DataField="StartDate" HeaderText="StartDate" SortExpression="StartDate" />
            <asp:BoundField DataField="EndDate" HeaderText="EndDate" SortExpression="EndDate" />
            <asp:BoundField DataField="Approval" HeaderText="Approval" SortExpression="Approval" />
            <asp:BoundField DataField="DateApproved" HeaderText="DateApproved" SortExpression="DateApproved" />
        </Columns>
                        <PagerStyle CssClass="pgr"></PagerStyle>

    </asp:GridView>
    <asp:SqlDataSource ID="lDS" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM LEAVES L
WHERE L.EmployeeId = @EmployeeId">
        <SelectParameters>
            <asp:SessionParameter Name="EmployeeId" SessionField="EmployeeId" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
