﻿using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PerceptumIIP.Views.Teacher
{
    public partial class Grading : System.Web.UI.Page
    {
        private int StudentId;
        private int ClassId;

        protected void Page_Load(object sender, EventArgs e)
        {
            StudentId = Convert.ToInt32(Session["StudentId"]); ClassId = Convert.ToInt32(Session["ClassId"]); //Session["StudentId"] = StudentId;
            using (var db = new ApplicationDbContext())
            {
                Student theStudent = db.Students.SingleOrDefault(x => x.StudentId == StudentId);
                Class theClass = db.Classes.SingleOrDefault(x => x.ClassId == ClassId);

                if(theStudent != null && theClass != null)
                {
                    lblStudentId.Text = Convert.ToString(theStudent.StudentId);
                    lblName.Text = theStudent.FirstName + " " + theStudent.LastName;
                    lblSubject.Text = theClass.ClassDescription;
                    lblLevel.Text = theClass.Level;
                    lblClass.Text = theClass.ClassId.ToString();
                }
            }

        }


        static readonly string scriptSuccessNewAccount =
          "<script language=\"javascript\">\n" +
          "alert (\"Grade has been given!\");\n" +
          "</script>";

        protected void thisMonthSubmit_Click(object sender, EventArgs e)
        {

            using (var db = new ApplicationDbContext())
            {
                Grade grade = new Grade
                {
                    ClassId = ClassId,
                    StudentId = StudentId,
                    EmployeeId = Convert.ToInt32(Session["EmployeeId"]),
                    Month = Convert.ToDateTime(txtMonth.Text),
                    Score = thisMonthGrade.SelectedValue//.ToString("MMM yyyy");
                };

                db.Grades.Add(grade);
                db.SaveChanges();

                Type csType = this.GetType();

                ClientScript.RegisterStartupScript(csType, "Error", scriptSuccessNewAccount);
            }


            thisMonthGrade.Text = "";
            txtMonth.Text = "";

            Response.Redirect("TeacherClasses.aspx");

            MailMessage msg = new MailMessage();
            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
            try
            {
                msg.Subject = "Student Grading";
                msg.Body = "Your related Student has been graded. Would you like to arrange for meet-up session to discuss on your child's performance?";
                msg.From = new MailAddress("perceptumiip@gmail.com");

                int toMails = 0;
                using (var db = new ApplicationDbContext())
                {
                    var toEmails = from sc in db.StudentParents
                                   join p in db.Parents on sc.StudentId equals p.ParentId
                                   select new
                                   {
                                       ParentEmail = p.Email
                                   };
                    foreach (var email in toEmails)
                    {
                        msg.To.Add(new MailAddress(email.ParentEmail));
                        toMails++;
                    }

                }
                msg.IsBodyHtml = true;
                client.Host = "smtp.gmail.com";
                System.Net.NetworkCredential basicauthenticationinfo = new System.Net.NetworkCredential("perceptumiip@gmail.com", "123qweQWE!@#");
                client.Port = int.Parse("587");
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = basicauthenticationinfo;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;

                if (toMails > 0)
                {
                    client.Send(msg);
                }
                
            }
            catch (Exception ex)
            {
            }                       
        }
    }
}