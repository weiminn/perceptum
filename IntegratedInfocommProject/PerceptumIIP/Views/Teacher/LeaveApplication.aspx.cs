﻿using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.IO;

namespace PerceptumIIP.Views.Teacher
{
    public partial class LeaveApplication : System.Web.UI.Page
    {


        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            int EmployeeId = Convert.ToInt32(Session["EmployeeId"]);
            using (var db = new ApplicationDbContext())
            {
                DateTime start = Convert.ToDateTime(startDate.Text);
                DateTime end = Convert.ToDateTime(endDate.Text);
                DateTime date = Convert.ToDateTime(DateTime.Now.Date.ToString());


                Type csType = this.GetType();


                if (!FileUpload1.HasFile)
                {
                    ClientScript.RegisterStartupScript(csType, "Error", "<script language=\"javascript\">\n" +
             "alert (\"Please Upload a file!\");\n" +
             "</script>");
                    return;
                }


                if (ddlType.SelectedValue == "Medical")
                {
                    Leave Leave = new Leave
                    {
                        EmployeeId = Convert.ToInt32(Session["EmployeeId"]),
                        Reason = Details.Text,
                        StartDate = start,
                        EndDate = end,
                        Type = ddlType.SelectedValue,
                        Approval = "Approved",
                        DateApproved = DateTime.Now,
                        Image = FileUpload1.FileBytes
                    };

                    db.LeaveApplication.Add(Leave);
                    db.SaveChanges();
                }
                else
                {
                    Leave Leave = new Leave
                    {
                        EmployeeId = Convert.ToInt32(Session["EmployeeId"]),
                        Reason = Details.Text,
                        StartDate = start,
                        EndDate = end,
                        Type = ddlType.SelectedValue,
                        Approval = "Pending",
                        DateApproved = DateTime.Now
                    };

                    db.LeaveApplication.Add(Leave);
                    db.SaveChanges();
                }

            }

            using (var db = new ApplicationDbContext())
            {

                var employeename = db.Employees.SingleOrDefault(x => x.EmployeeId == EmployeeId).FirstName;
                var employeedepartment = db.Employees.SingleOrDefault(x => x.EmployeeId == EmployeeId).Department;
                var employeeemail = db.Employees.SingleOrDefault(x => x.EmployeeId == EmployeeId).Email;
                string empname = employeename;
                string empdept = employeedepartment;
                string empemail = employeeemail;

                Session["empdept"] = employeedepartment;
                Session["Empname"] = empname;
                Session["Empemail"] = empemail;

            }

            string Empname = (string)Session["Empname"];
            string Empdept = (string)Session["Empdept"];
            string startdate = startDate.Text;
            string enddate = endDate.Text;
            string email = (string)Session["empemail"];


            MailMessage msg = new MailMessage();
            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
            try
            {
                msg.Subject = "Leave Application";
                msg.Body = " Details of Medical Leave Application follows:"  +  Empname +  "under" + Empdept +  "has taken medical leave from" +  startdate +  "to" +  enddate;
                //send image via email 
                msg.From = new MailAddress(email);

                if (FileUpload1.HasFile)
                {
                    msg.Attachments.Add(new Attachment(new MemoryStream(FileUpload1.FileBytes), FileUpload1.FileName));
                }
               
                    
                msg.To.Add(new MailAddress("ownagersg@gmail.com"));
                    

                
                msg.IsBodyHtml = true;
                client.Host = "smtp.gmail.com";
                System.Net.NetworkCredential basicauthenticationinfo = new System.Net.NetworkCredential("perceptumiip@gmail.com", "123qweQWE!@#");
                client.Port = int.Parse("587");
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = basicauthenticationinfo;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;

                client.Send(msg);

            }
            catch (Exception ex)
            {
            }

            Response.Redirect("LeaveRecords.aspx");

        }
    }
}