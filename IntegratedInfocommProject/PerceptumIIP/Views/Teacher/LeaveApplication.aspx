﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Teacher/Teacher.Master" AutoEventWireup="true" CodeBehind="LeaveApplication.aspx.cs" Inherits="PerceptumIIP.Views.Teacher.LeaveApplication" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>
            

 @media (min-width: 768px) and (max-width: 992px) {
            .row {
                font-size: 20px;
            }
            .form-control {
                height: 40px;
            }
        }
    
    </style>
    <link class="jsbin" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
<script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.0/jquery-ui.min.js"></script>

    <h1>Leave Application</h1>
    <div class="row">
        <div class="form-group">
            <asp:Label runat="server" CssClass="col-md-2 control-label">Type of Leave</asp:Label>
            <div class="col-md-4">
                <asp:DropDownList runat="server" ID="ddlType" CssClass="form-control">
                    <asp:ListItem Value="" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="Normal">Normal</asp:ListItem>
                    <asp:ListItem Value="Medical">Medical</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlType" ErrorMessage="The leave field is empty"></asp:RequiredFieldValidator>
            </div>
        </div>

    </div>
    <br />
    
    <div class="row">
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Details" CssClass="col-md-2 control-label">Reason</asp:Label>
            <div class="col-md-4">
                <asp:TextBox runat="server" ID="Details" CssClass="form-control" />

   <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Details" ErrorMessage="The reason field is empty"></asp:RequiredFieldValidator>

            </div>
        </div>
    </div>
    <br />
    
    <div class="row">
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="startDate" CssClass="col-md-2 control-label">Start Date</asp:Label>
            <div class="col-md-4">
                <asp:textbox ID="startDate" runat="server" cssclass="datepicker-field form-control"/>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="startDate" ErrorMessage="The start date is empty"></asp:RequiredFieldValidator>

            </div>
        </div>
    </div>
    <br />
    
    <div class="row">
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="endDate" CssClass="col-md-2 control-label">End Date</asp:Label>
            <div class="col-md-4">
                <asp:textbox ID="endDate" runat="server" cssclass=" datepicker-field form-control"/>
     <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="endDate" ErrorMessage="The end date is empty"></asp:RequiredFieldValidator>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <asp:Label runat="server" CssClass="col-md-2 control-label">Medical Documents</asp:Label>


            <div class="col-md-4">
    <asp:FileUpload ID="FileUpload1" runat="server" onchange="readURL(this)" />
</div>
</div>
</div>

                <div class="col-md-12">
    <img id="blah" src="#" alt=""  />
    
        </div>

    <asp:Button CssClass="btn btn-default" ID="Submit" runat="server" Text="Submit" OnClick="Submit_Click" />

    <script>
        $(document).ready(function(){
             $('.datepicker-field').datepicker();        
        });

        function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(300)
                    .height(200);
            };

            reader.readAsDataURL(input.files[0]);
        }
        }
        function showname() {
            var file = document.uploadbox.value;
        }
    </script>
</asp:Content>
