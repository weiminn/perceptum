﻿using Microsoft.AspNet.Identity;
using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PerceptumIIP.Views.Teacher
{
    public partial class TeacherClasses : System.Web.UI.Page
    {
        private int EmployeeId;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            string userId = User.Identity.GetUserId();
            //Session["ParentId"] = currentParent;

            if (userId != null)
            {
                using (var db = new ApplicationDbContext())
                {
                    EmployeeId = db.Employees.SingleOrDefault(x => x.ApplicationUserId == userId).EmployeeId;
                    Session["EmployeeId"] = EmployeeId;

                    var classes = from c in db.Classes
                                  join sc in db.EmployeeClasses on c.ClassId equals sc.ClassId
                                  where sc.EmployeeId == EmployeeId
                                  select new
                                  {
                                      ClassId = c.ClassId,
                                      ClassDescription = c.ClassDescription,
                                      Level = c.Level,
                                      Day = c.Day.ToString(),
                                      Time = c.Time
                                  };

                    DataTable dt = new DataTable();
                    dt.Columns.Add("ClassId");
                    dt.Columns.Add("ClassDescription");
                    dt.Columns.Add("Level");
                    dt.Columns.Add("Day");
                    dt.Columns.Add("Time");

                    foreach (var @class in classes.ToList())
                    {
                        DataRow dr = dt.NewRow();
                        dr[0] = @class.ClassId;
                        dr[1] = @class.ClassDescription;
                        dr[2] = @class.Level;
                        dr[3] = @class.Day;
                        dr[4] = @class.Time.ToString("hh:mm:tt");

                        dt.Rows.Add(dr);
                    }

                    cGrid.DataSource = dt;
                    cGrid.DataBind();
                }
            }
        }

        protected void cGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument); //gives row number that is clicked
            GridViewRow row = cGrid.Rows[index]; 
            var classSelected = row.Cells[0].Text;  //obtain the id
            this.Session["ClassId"] = classSelected;
            if (e.CommandName == "Detail")
            {
                Response.Redirect("ClassDetail.aspx");
            }
            else
            {
                Response.Redirect("AttendanceMarking.aspx");
            }
            
        }

        
    }
}