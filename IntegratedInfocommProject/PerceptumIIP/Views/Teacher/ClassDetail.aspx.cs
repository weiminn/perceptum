﻿using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PerceptumIIP.Views.Teacher
{
    public partial class ClassDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var cID = Convert.ToInt32(Session["ClassId"]);

            using (var db = new ApplicationDbContext())
            {
                var theClass = db.Classes.SingleOrDefault(x => x.ClassId == cID);

                ClassID.Text = Convert.ToString(theClass.ClassId);
                Subject.Text = theClass.ClassDescription;
                Level.Text = theClass.Level;
                Day.Text =  theClass.Day.ToString();
                Time.Text = theClass.Time.ToString("HH:mm");
            }
        }

        protected void sGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = sGrid.Rows[index];
            var studentSelected = row.Cells[0].Text;
            this.Session["StudentId"] = studentSelected;
            Response.Redirect("Grading.aspx");
        }


    }
}