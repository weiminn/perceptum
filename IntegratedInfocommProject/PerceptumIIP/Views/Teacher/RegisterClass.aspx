﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Teacher/Teacher.Master" AutoEventWireup="true" CodeBehind="RegisterClass.aspx.cs" Inherits="PerceptumIIP.Views.Teacher.RegisterClass" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>

.classgridview {
    width:100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color:white;
}


.classgridview td {
    padding:10px; 
    border: solid 1px black;
    color:black;
}

.classgridview th {
    background-color:#f3e222;
    text-align:center;
    padding: 10px 2px;
    border: solid 1px black;
    color: black;
}

classgridview tr:nth-child(even) {
    background-color: #ffffff;
}

.classgridview tr:nth-child(odd) {
    background-color: #dcdcdc;
}

.classgridview .alt{
    background:#dcdcdc;
}

.classgridview .pgr{

    background: #dcdcdc;

}
.classgridview .pgr table {
    margin: 5px 0;
}

.classgridview .pgr td {
    border-width:0;
    padding: 0 6px;
    border-left: solid 1px black;
}
.classgridview .pgr a{
    color:black;
    text-decoration:none;
}
</style>


    <h2>Available Classes</h2>

    <asp:GridView ID="cGrid" runat="server"  CssClass="classgridview" OnRowCommand="cGrid_RowCommand" AutoGenerateColumns="false">
        <Columns>
            <asp:BoundField DataField="ClassId" HeaderText="Class ID" SortExpression="ClassId"/>
            <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description"/>
            <asp:BoundField DataField="Day" HeaderText="Day" SortExpression="Day"/>
            <asp:BoundField DataField="Time" HeaderText="Time" SortExpression="Time"/>
            <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status"/>
            <asp:ButtonField Text="Register" CommandName="Register" />
            <asp:ButtonField Text="Deregister" CommandName="Deregister" />
        </Columns>
    </asp:GridView>
</asp:Content>
