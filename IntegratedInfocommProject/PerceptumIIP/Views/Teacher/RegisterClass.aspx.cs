﻿using Microsoft.AspNet.Identity;
using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PerceptumIIP.Views.Teacher
{
    public partial class RegisterClass : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var user = User.Identity.GetUserId();

            using (var db = new ApplicationDbContext())
            {
                var classes = db.Classes.ToList();
                var employee = db.Employees.SingleOrDefault(x => x.ApplicationUserId == user);

                DataTable dt = new DataTable();
                dt.Columns.Add("ClassId");
                dt.Columns.Add("Description");
                dt.Columns.Add("Day");
                dt.Columns.Add("Time");
                dt.Columns.Add("Status");

                foreach (var c in classes)
                {
                    DataRow dr = dt.NewRow();

                    dr[0] = c.ClassId;
                    dr[1] = c.ClassDescription;
                    dr[2] = c.Day.ToString();
                    dr[3] = c.Time.ToString("hh:mm:tt");

                    var reg = db.EmployeeClasses.SingleOrDefault(x => x.EmployeeId == employee.EmployeeId && x.ClassId == c.ClassId);
                    if(reg != null)
                    {
                        dr[4] = "Teaching";
                    }
                    else
                    {
                        dr[4] = "Not Teaching";
                    }

                    dt.Rows.Add(dr);
                }

                cGrid.DataSource = dt;
                cGrid.DataBind();
            }
        }

        protected void cGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = cGrid.Rows[index];
            int cId = Convert.ToInt32(row.Cells[0].Text);

            using (var db = new ApplicationDbContext())
            {
                var user = User.Identity.GetUserId();
                var employee = db.Employees.SingleOrDefault(x => x.ApplicationUserId == user);

                if (e.CommandName == "Register")
                {
                    var existing = db.EmployeeClasses.SingleOrDefault(x => x.ClassId == cId && x.EmployeeId == employee.EmployeeId);

                    if(existing == null)
                    {
                        EmployeeClass employeeClass = new EmployeeClass
                        {
                            EmployeeId = employee.EmployeeId,
                            ClassId = cId
                        };
                        db.EmployeeClasses.Add(employeeClass);
                        db.SaveChanges();
                    }
                }
                else if(e.CommandName == "Deregister")
                {
                    var ec = db.EmployeeClasses.SingleOrDefault(x => x.ClassId == cId && x.EmployeeId == employee.EmployeeId);

                    if (ec != null)
                    {
                        db.EmployeeClasses.Remove(ec);
                        db.SaveChanges();
                    }
                }

                Response.Redirect(Request.RawUrl);
            }
        }
    }
}