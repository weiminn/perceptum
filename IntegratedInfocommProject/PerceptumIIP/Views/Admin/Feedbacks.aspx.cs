﻿using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PerceptumIIP.Views.Admin
{
    public partial class Feedbacks : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void fbGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = fbGrid.Rows[index];
            int fId = Convert.ToInt32(row.Cells[0].Text);

            using (var db = new ApplicationDbContext())
            {
                Feedback feedback = db.FeedBacks.SingleOrDefault(x => x.FeedbackId == fId);

                if (e.CommandName == "Approve")
                {
                    feedback.Status = "Approved";
                }
                else if (e.CommandName == "Reject")
                {
                    feedback.Status = "Rejected";
                }

                db.SaveChanges();
            }

            Response.Redirect(Request.RawUrl);
        }
    }
    }
