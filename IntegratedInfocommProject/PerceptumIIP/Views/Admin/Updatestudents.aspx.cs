﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Microsoft.AspNet.Identity;
using PerceptumIIP.Models;
using System.Net.Mail;
using System.Text;



namespace PerceptumIIP.Views.Admin

{
    public partial class Updatestudents : System.Web.UI.Page
    {

        public int StudentId;
        public string UFlag = "F";


        static readonly string scriptSuccessUpdate = "<script language=\"javascript\">\n" +
        "alert (\"Update Successful!\");\n </script>";

        static readonly string scriptSuccessSent = "<script language=\"javascript\">\n" +
       "alert (\"Your Password has been successfully sent to your email!\");\n </script>";

        

        protected void Page_Load(object sender, EventArgs e)
        {


            StudentId = Convert.ToInt32(Session["StudentId"]);

            lblFirstName.Text = (string)Session["FirstName"];
            lblLastName.Text = (string)Session["LastName"];
            lblBirthDate.Text = (string)Session["BirthDate"];
            lblAdd.Text = (string)Session["Address"];
            lblSchool.Text = (string)Session["School"];
            lblLevel.Text = (string)Session["Level"];
            lblTel.Text = (string)Session["Telephone"];
            lblemail.Text = (string)Session["Email"];
            lbloutlet.Text = (string)Session["Outlet"];

            


        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {


            if (txtFirstName.Text != "")
            {
                String strFName = "FirstName";
                String strFValue = txtFirstName.Text;
                UpdateStudents(strFName, strFValue);
                Session["FirstName"] = txtFirstName.Text;
            }
            if (txtLastName.Text != "")
            {
                String strFName = "LastName";
                String strFValue = txtLastName.Text;
                UpdateStudents(strFName, strFValue);
                Session["LastName"] = txtLastName.Text;
            }
            if (txtBirthDate.Text != "")
            {
                String strFName = "BirthDate";
                String strFValue = txtBirthDate.Text;
                UpdateStudents(strFName, strFValue);
                Session["BirthDate"] = txtBirthDate.Text;
            }
            if (txtAdd.Text != "")
            {
                String strFName = "Address ";
                String strFValue = txtAdd.Text;
                UpdateStudents(strFName, strFValue);
                Session["Address"] = txtAdd.Text;
            }
            if (txtSchool.Text != "")
            {
                String strFName = "School";
                String strFValue = txtSchool.Text;
                UpdateStudents(strFName, strFValue);
                Session["School"] = txtSchool.Text;
            }

            if (textLevel.Text != "")
            {
                String strFName = "Level";
                String strFValue = textLevel.Text;
                UpdateStudents(strFName, strFValue);
                Session["Level"] = textLevel.Text;
            }

           
            if (txtemail.Text != "")
            {
                String strFName = "SubLevel";
                String strFValue = txtemail.Text;
                UpdateStudents(strFName, strFValue);
                Session["Email"] = txtemail.Text;
            }
            if (txtTel.Text != "")
            {
                String strFName = "Telephone";
                String strFValue = txtTel.Text;
                UpdateStudents(strFName, strFValue);
                Session["Telephone"] = txtTel.Text;
            }
            if (txtoutlet.Text != "")
            {
                String strFName = "Outlet";
                String strFValue = txtoutlet.Text;
                UpdateStudents(strFName, strFValue);
                Session["Outlet"] = txtoutlet.Text;
            }

            GridViewRow row1 = GridView1.Rows[0];
            string oldimage = row1.Cells[0].Text;
            Session["soldimage"] = oldimage;

            if (f1.HasFile)
            {
                String strFName = "Image";
                Byte[] Image = f1.FileBytes;
                int strStudentId = Convert.ToInt32(Session["StudentId"]);
                using (var db = new ApplicationDbContext())
                {
                    var product = db.Students.SingleOrDefault(x => x.StudentId == strStudentId);
                    product.Image = Image;
                    db.SaveChanges();
                }

                Session["oldimage"] = f1.HasFile;

            }

            if (UFlag == "T")
            {
                Type strType = this.GetType();
                ClientScript.RegisterStartupScript(strType, "Success", scriptSuccessUpdate);

            }

            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtBirthDate.Text = "";
            txtAdd.Text = "";
            textLevel.Text = "";
            txtemail.Text = "";
            txtTel.Text = "";
            txtoutlet.Text = "";

        }

        public void UpdateStudents(string strFName, string strFValue)

        {
            string strStudentId = (string)Session["StudentId"];


            SqlConnection con = new SqlConnection(@"Data Source=perceptum.database.windows.net;Initial Catalog=Perceptum V1;Connect Timeout=15;Encrypt=true;MultipleActiveResultSets=True;Persist Security Info=True;Trusted_Connection=False;User ID=Perceptum2018;Password=Pm123456;");

            con.Open();
            SqlCommand cmd;
            Type csType = this.GetType();
            String strSQL = "UPDATE Students  SET " + strFName +
                " = @newValue WHERE StudentId = @StudentId";
            cmd = new SqlCommand(strSQL, con);
            cmd.Parameters.Add("@newValue", SqlDbType.Char).Value = strFValue;
            cmd.Parameters.Add("@StudentId", SqlDbType.NVarChar).Value = strStudentId;
            cmd.ExecuteNonQuery();
            con.Close();
            Type strType = this.GetType();
            ClientScript.RegisterStartupScript(strType, "Success", scriptSuccessUpdate);
        }

        protected void btnResetPassword_Click(object sender, EventArgs e)
        {
            string password;
            using (var db = new ApplicationDbContext())
            {
                password = db.Students.SingleOrDefault(x => x.StudentId == StudentId).Password;
            }

            MailMessage msg = new MailMessage();
            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
            try
            {
                msg.Subject = "Your Password";
                msg.Body = "Your password is " + password;
                msg.From = new MailAddress("perceptumiip@gmail.com");
                msg.To.Add(new MailAddress((string)Session["Email"]));
                msg.IsBodyHtml = true;
                client.Host = "smtp.gmail.com";
                System.Net.NetworkCredential basicauthenticationinfo = new System.Net.NetworkCredential("perceptumiip@gmail.com", "123qweQWE!@#");
                client.Port = int.Parse("587");
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = basicauthenticationinfo;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;

                
                client.Send(msg);
                Type strType = this.GetType();
                ClientScript.RegisterStartupScript(strType, "Success", scriptSuccessSent);
            }
            catch (Exception ex)
            {

            }

         
        }

       
    }
}
