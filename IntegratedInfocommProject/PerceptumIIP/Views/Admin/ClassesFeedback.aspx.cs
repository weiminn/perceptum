﻿using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PerceptumIIP
{
    public partial class ClassesFeedback : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int cid = (int)Session["cId"];

            using (var db = new ApplicationDbContext())
            {

                var mathclasses = from c in db.Classes
                                  join f in db.FeedBacks on c.ClassId equals f.ClassId
                                  where f.ClassId == cid
                                  select new
                                  {
                                      Details = f.Details,
                                      Method = f.Method,
                                      Material = f.Material,
                                      Tests = f.Tests,
                                      Management = f.Management,
                                      Date = f.Month

                                  };
                DataTable sdt = new DataTable();
                sdt.Columns.Add("Details");
                sdt.Columns.Add("Method");
                sdt.Columns.Add("Material");
                sdt.Columns.Add("Tests");
                sdt.Columns.Add("Management");
                sdt.Columns.Add("Date");

                foreach (var @class in mathclasses.ToList())
                {
                    DataRow dr = sdt.NewRow();
                    dr[0] = @class.Details;
                    dr[1] = @class.Method;
                    dr[2] = @class.Material;
                    dr[3] = @class.Tests;
                    dr[4] = @class.Management;
                    dr[5] = @class.Date.ToString("d/M/yyyy");


                    sdt.Rows.Add(dr);
                }

                mathdetails.DataSource = sdt;
                mathdetails.DataBind();


            }



           
        }
    }
}