﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="StudentRegister.aspx.cs" Inherits="PerceptumIIP.Views.Admin.StudentRegister" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <style>
        @media (min-width: 768px) and (max-width: 992px) {
            .row {
                font-size: 20px;
            }
            input[type="text"], input[type="password"], input[type="email"], input[type="tel"], input[type="select"] {
                max-width: 480px;
            }
            .form-control {
                height: 50px;
            }
        }
    </style>



    <h1>Register New Student</h1>
    <hr />
    
    <div class="row">
        <div class="col-md-3">
            <asp:Label runat="server" AssociatedControlID="FirstName" CssClass="col-md-5 control-label">First Name</asp:Label>
            <div class="form-group">
                <asp:TextBox runat="server" ID="FirstName" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="FirstName"
                    CssClass="text-danger" ErrorMessage="The first name field is required." />
                       <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ValidationExpression="^[a-zA-Z]+$" ControlToValidate="FirstName" ErrorMessage="Please enter text only"></asp:RegularExpressionValidator>

            </div>
        </div> 

        <div class="col-md-3">
            <asp:Label runat="server" AssociatedControlID="LastName" CssClass="col-md-5 control-label">Last Name</asp:Label>
            <div class="form-group">
                <asp:TextBox runat="server" ID="LastName" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="LastName"
                    CssClass="text-danger" ErrorMessage="The last name field is required." />
                 <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ValidationExpression="^[a-zA-Z]+$" ControlToValidate="LastName" ErrorMessage="Please enter text only"></asp:RegularExpressionValidator>

            </div>
        </div>

        <div class="col-md-3">
            <asp:Label runat="server" AssociatedControlID="Address" CssClass="col-md-5 control-label">Address</asp:Label>
            <div class="form-group">
                <asp:TextBox runat="server" ID="Address" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Address"
                CssClass="text-danger" ErrorMessage="The address field is required." />
        </div>
    </div>
        
    </div>
    <div class="row">

    <div class="col-md-3">
        <asp:Label runat="server" AssociatedControlID="DOB" CssClass="col-md-5 control-label">Date Of Birth</asp:Label>
        <div class="form-group">
            <asp:TextBox runat="server" ID="DOB" CssClass="datepicker-field form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="DOB"
                CssClass="text-danger" Display="Dynamic" ErrorMessage="The date of birth field is required." />
        </div>
   
    </div>

            <div class="col-md-3">
        <asp:Label runat="server" AssociatedControlID="txtEmail" CssClass="col-md-5 control-label">Email</asp:Label>
        <div class="form-group">
            <asp:TextBox runat="server" ID="txtEmail"  CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtEmail"
                CssClass="text-danger" Display="Dynamic" ErrorMessage="The email field is required." />
 <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" validationexpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"  ControlToValidate="txtEmail" ErrorMessage="Please enter in email format"></asp:RegularExpressionValidator>

        </div>
     </div>
    
         <div class="col-md-3">
        <asp:Label runat="server" AssociatedControlID="Telephone" CssClass="col-md-5 control-label">Telephone</asp:Label>
        <div class="form-group">
            <asp:TextBox runat="server" ID="Telephone"  CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="Telephone"
                CssClass="text-danger" Display="Dynamic" ErrorMessage="The telephone field is required." />
                   <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationExpression="\d+" ControlToValidate="Telephone" ErrorMessage="Please enter numbers only"></asp:RegularExpressionValidator>

        </div>
     </div> 
    </div>

    
    <div class="row">
     <div class="col-md-3">
        <asp:Label runat="server" AssociatedControlID="School" CssClass="col-md-5 control-label">School</asp:Label>
        <div class="form-group">
            <asp:TextBox runat="server" ID="School" CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="School"
                CssClass="text-danger" Display="Dynamic" ErrorMessage="The school field is required." />
        </div>
    </div>

           <div class="col-md-3">
        <asp:Label runat="server" AssociatedControlID="DDLevel" CssClass="col-md-5 control-label">Level</asp:Label>
        <div class="col-sm-offset-2 col-sm-10">
          <asp:DropDownList
                              ID="DDLevel" runat="server" CssClass="form-control">
                              <asp:ListItem Value="" Selected="True"></asp:ListItem>
                              <asp:ListItem Value="Primary 3">Primary 3</asp:ListItem>
                              <asp:ListItem Value="Primary 4">Primary 4</asp:ListItem>
                              <asp:ListItem Value="Primary 5">Primary 5</asp:ListItem>
                              <asp:ListItem Value="Primary 6">Primary 6</asp:ListItem>
                              <asp:ListItem Value="Secondary 1">Secondary 1</asp:ListItem>
                              <asp:ListItem Value="Secondary 2">Secondary 2</asp:ListItem>
                              <asp:ListItem Value="Secondary 3">Secondary 3</asp:ListItem>
                              <asp:ListItem Value="Secondary 4">Secondary 4</asp:ListItem>

                          </asp:DropDownList>
            </div>
           
           <asp:RequiredFieldValidator runat="server" ControlToValidate="DDLevel" CssClass="text-danger" Display="Dynamic" ErrorMessage="The level field is required">

           </asp:RequiredFieldValidator>

        
     </div>

        <div class="col-md-3">
    <asp:Label runat="server" AssociatedControlID="Branch" CssClass="col-md-2 control-label">Branch</asp:Label>
        <div class="col-md-12">
            <asp:RadioButtonList ID="Branch" runat="server" CssClass="col-md-2 control-label" RepeatDirection="Horizontal" style="left: -7px; top: 2px; width: 308px; height: 12px">
<asp:ListItem Value ="Bishan">Bishan</asp:ListItem>
<asp:ListItem Value ="Sime Darby Centre">Sime Darby Centre</asp:ListItem>
</asp:RadioButtonList>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="Branch"
                CssClass="text-danger" Display="Dynamic" ErrorMessage="Please Select a Branch" />

        </div>
            </div>

    </div>
        
    <div class="row">
     
     
    <div class="col-md-3">
        <asp:Label runat="server" AssociatedControlID="txtUserName" CssClass="col-md-5 control-label">UserName</asp:Label>
        <div class="form-group">
            <asp:TextBox runat="server" ID="txtUserName" CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtUserName"
                CssClass="text-danger" ErrorMessage="The name field is required." />
             <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" validationexpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"  ControlToValidate="txtUserName" ErrorMessage="Please enter in email format"></asp:RegularExpressionValidator>

        </div>
    </div>
    
    <div class="col-md-3">
        <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-5 control-label">Password</asp:Label>
        <div class="form-group">
            <asp:TextBox runat="server" ID="Password" CssClass="form-control" TextMode="Password" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="Password"
                CssClass="text-danger" ErrorMessage="The password field is required." />
        </div>
    </div>
</div>     
    
    <div class="row">

            
    <div class="col-md-3">
        <asp:Label runat="server" AssociatedControlID="ParentID" CssClass="col-md-5 control-label">ParentID</asp:Label>
        <div class="form-group">
            <asp:TextBox runat="server" ID="ParentID"  CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="ParentID"
                CssClass="text-danger" Display="Dynamic" ErrorMessage="The ParentID field is required." />
                               <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="\d+" ControlToValidate="ParentID" ErrorMessage="Please enter numbers only"></asp:RegularExpressionValidator>

        </div>
     </div>

    <div class="col-md-3">
        <asp:Label runat="server" AssociatedControlID="txtClassID" CssClass="col-md-5 control-label">Class ID</asp:Label>
        <div class="form-group">
            <asp:TextBox runat="server" ID="txtClassID"  CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtClassID"
                CssClass="text-danger" Display="Dynamic" ErrorMessage="The Class ID field is required." />
                               <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationExpression="\d+" ControlToValidate="txtClassID" ErrorMessage="Please enter numbers only"></asp:RegularExpressionValidator>

        </div>
     </div>   
    </div>
        
           
        
    <div class="row">
    <asp:FileUpload ID="FileUploadControl" CssClass="btn btn-default" runat="server" onchange="readURL(this)"/>
        <div class="col-md-12">
            <img id="blah" src="#" alt="" />
        </div>
        
    <div class="col-md-offset-8 col-md-10">
        <div class="form-group">
            <asp:Button ID="CreateUser" CssClass="btn btn-default" runat="server" OnClick="CreateUser_Click1" Text="Register" />
        </div>
    </div>
    </div>
       
        <script>
        $(document).ready(function(){
             $("#DOB").datepicker();        
        });

        function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(300)
                    .height(300);
            };

            reader.readAsDataURL(input.files[0]);
        }
        }

    </script>
    <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/ui/jquery.ui.core.js"></script>  
<script type="text/javascript" src="js/ui/jquery.ui.datepicker.js"></script>



</asp:Content>
