﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="StudentProfile.aspx.cs" Inherits="PerceptumIIP.Views.Admin.StudentProfile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>

        .StudentProfileGrid {
    width: 100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color: white;
}


    .StudentProfileGrid td {
        padding: 10px;
        border: solid 1px black;
        color: black;
    }

    .StudentProfileGrid th {
        background-color: #f3e222;
        text-align: center;
        padding: 10px 2px;
        border: solid 1px black;
        color: black;
    }

    .StudentProfileGrid tr:nth-child(even) {
        background-color: #ffffff;
    }

    .StudentProfileGrid tr:nth-child(odd) {
        background-color: #dcdcdc;
    }

    .StudentProfileGrid .alt {
        background: #dcdcdc;
    }

    .StudentProfileGrid .pgr {
        background: #dcdcdc;
    }

        .StudentProfileGrid .pgr table {
            margin: 5px 0;
        }

        .StudentProfileGrid .pgr td {
            border-width: 0;
            padding: 0 6px;
            border-left: solid 1px black;
        }

        .StudentProfileGrid .pgr a {
            color: black;
            text-decoration: none;
        }
    </style>
            <link rel="stylesheet" href="/CSS/master.css" type="text/css" />


    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" BorderColor="#B4EAE9" CssClass="table-responsive col-md-4">
        <Columns>
            <asp:TemplateField>
                <HeaderTemplate></HeaderTemplate>
                <ItemTemplate>
                    <img src='data:image/jpg;base64,<%# Eval("Image") != System.DBNull.Value ? Convert.ToBase64String((byte[])Eval("Image")) : string.Empty %>' alt="image" height="200" width="200"/>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

    <br />
    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataKeyNames="StudentId" DataSourceID="SqlDataSource2" CssClass="StudentProfileGrid" GridLines="None" Font-Size="Medium" Font-Names="Arial" CellPadding="3" CellSpacing="10" AlternatingRowStyle-CssClass="alt" PageSize="7" PagerStyle-CssClass="pgr">
        <Columns>
            <asp:BoundField DataField="StudentId" HeaderText="StudentId" InsertVisible="False" ReadOnly="True" SortExpression="StudentId" />
            <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName" />
            <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName" />
            <asp:BoundField DataField="Password" HeaderText="Password" SortExpression="Password" />
            <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
            <asp:BoundField DataField="BirthDate" HeaderText="BirthDate" SortExpression="BirthDate" />
            <asp:BoundField DataField="School" HeaderText="School" SortExpression="School" />
            <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level" />
            <asp:BoundField DataField="Telephone" HeaderText="Telephone" SortExpression="Telephone" />
            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
            <asp:BoundField DataField="Outlet" HeaderText="Outlet" SortExpression="Outlet" />
        </Columns>
        <PagerStyle CssClass="pgr"></PagerStyle>

    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT StudentId, FirstName, LastName, Password, Address, BirthDate , School , Level, Telephone, Email, Outlet, Image FROM Students WHERE (StudentId=@StudentId)">
        <SelectParameters>
            <asp:SessionParameter Name="StudentID" SessionField="StudentId" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:Button ID="Update" runat="server" Text="Update" OnClick="Update_Click" />
    <asp:Button ID="Delete" runat="server" Text="Delete" OnClick="Delete_Click" />
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT Image FROM Students WHERE (StudentId = @StudentId)">
        <SelectParameters>
            <asp:SessionParameter Name="StudentID" SessionField="StudentId" />
        </SelectParameters>
    </asp:SqlDataSource>


    






















</asp:Content>
