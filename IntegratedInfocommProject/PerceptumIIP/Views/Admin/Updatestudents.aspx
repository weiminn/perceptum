﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Updatestudents.aspx.cs" Inherits="PerceptumIIP.Views.Admin.Updatestudents" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        /*.auto-style1 {
            width: 2513px;
        }
        .auto-style2 {
            width: 2015px;
        }
        .auto-style3 {
            width: 445px;
        }
        .auto-style4 {
            width: 1314px;
        }
        table {
            border-style: none;
            margin: 1px;
            text-align: center;
            height:100px;
        }
        .auto-style5 {
            width: 1500px;
            height: 33px;
        }
        .auto-style6 {
            width: 1314px;
            height: 33px;
        }
        .auto-style7 {
            width: 1500px;
            height: 33px;
        }*/
        .btn-block{
            margin-right:20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div class="row">
        <div class="col-md-3">
<h2>Data Item </h2>

        </div>
        <div class="col-md-3">
<h2>Existing Data</h2>
        </div>
        <div class="col-md-3">
<h2>New Data </h2>       </div>
    </div>
    
    <div class="row">
        <div class="col-md-3">
           <h4> First Name</h4>
        </div>
        <div class="col-md-3">
    <h4><asp:Label ID="lblFirstName" runat="server"></asp:Label></h4>

        </div>
        <div class="col-md-3">
       <asp:TextBox ID="txtFirstName" runat="server" Width="250px"></asp:TextBox>

        </div>
              <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" CssClass="text-danger" Display="Dynamic" ValidationExpression="^[a-zA-Z]+$" ControlToValidate="txtFirstName" ErrorMessage="Please enter text only"></asp:RegularExpressionValidator>

    </div>
    
    <div class="row">
        <div class="col-md-3">
            <h4>Last Name</h4>
        </div>
        <div class="col-md-3">
                <h4><asp:Label ID="lblLastName" runat="server"></asp:Label></h4>

        </div>
        <div class="col-md-3">
                   <asp:TextBox ID="txtLastName" runat="server" Width="250px"></asp:TextBox>

        </div>
                      <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" CssClass="text-danger" Display="Dynamic" ValidationExpression="^[a-zA-Z]+$" ControlToValidate="txtLastName" ErrorMessage="Please enter text only"></asp:RegularExpressionValidator>

    </div>
    
    <div class="row">
        <div class="col-md-3">
           <h4> Birth Date </h4>
        </div>
        <div class="col-md-3">
                           <h4> <asp:Label ID="lblBirthDate" runat="server"></asp:Label> </h4>

        </div>
        <div class="col-md-3">
                            <asp:TextBox ID="txtBirthDate" runat="server" Width="250px"></asp:TextBox>

        </div>
                              <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" CssClass="text-danger" Display="Dynamic" ValidationExpression="^([0]{0,1}[1-9]|1[012])/([1-9]|([012][0-9])|(3[01]))/[0-9]{4}$" ControlToValidate="txtBirthDate" ErrorMessage="Date format not in mm/dd/yyyy only"></asp:RegularExpressionValidator>

    </div>
    
    <div class="row">
        <div class="col-md-3">
           <h4> Address</h4>
        </div>
        <div class="col-md-3">
                           <h4> <asp:Label ID="lblAdd" runat="server"></asp:Label> </h4>

        </div>
        <div class="col-md-3">
                            <asp:TextBox ID="txtAdd" runat="server" Width="250px"></asp:TextBox>

        </div>

    </div>

        
    <div class="row">
        <div class="col-md-3">
            <h4>School</h4>
        </div>
        <div class="col-md-3">
                           <h4> <asp:Label ID="lblSchool" runat="server"></asp:Label></h4>

        </div>
        <div class="col-md-3">
                            <asp:TextBox ID="txtSchool" runat="server" Width="250px"></asp:TextBox>

        </div>

    </div>

     <div class="row">
        <div class="col-md-3">
          <h4>  Level</h4>
        </div>
        <div class="col-md-3">
                        <h4><asp:Label ID="lblLevel" runat="server"></asp:Label> </h4>

        </div>
        <div class="col-md-3">
                            <asp:TextBox ID="textLevel" runat="server" Width="250px"></asp:TextBox>

        </div>
    </div>

       <div class="row">
        <div class="col-md-3">
           <h4> Telephone</h4>
        </div>
        <div class="col-md-3">
                        <h4><asp:Label ID="lblTel" runat="server"></asp:Label></h4>    

        </div>
        <div class="col-md-3">
                            <asp:TextBox ID="txtTel" runat="server" Width="250px"></asp:TextBox>

        </div>
             <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationExpression="\d+" CssClass="text-danger" Display="Dynamic" ControlToValidate="txtTel" ErrorMessage="Please enter numbers"></asp:RegularExpressionValidator>
               <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ValidationExpression=".{8,}$" CssClass="text-danger" Display="Dynamic" ControlToValidate="txtTel" ErrorMessage="Please enter 8 characters"></asp:RegularExpressionValidator>

    </div>


     <div class="row">
        <div class="col-md-3">
           <h4>Email</h4>
        </div>
        <div class="col-md-3">
                         <h4>  <asp:Label ID="lblemail" runat="server"></asp:Label> </h4>

        </div>
        <div class="col-md-3">
                            <asp:TextBox ID="txtemail" runat="server" Width="250px"></asp:TextBox>

        </div>
   <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" validationexpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" CssClass="text-danger" Display="Dynamic"  ControlToValidate="txtemail" ErrorMessage="Email in wrong format!"></asp:RegularExpressionValidator>

    </div>

     <div class="row">
        <div class="col-md-3">
            <h4>Branch</h4>
        </div>
        <div class="col-md-3">
                           <h4> <asp:Label ID="lbloutlet" runat="server"></asp:Label></h4>

        </div>
        <div class="col-md-3">
                            <asp:TextBox ID="txtoutlet" runat="server" Width="250px"></asp:TextBox>

        </div>
                                   <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" CssClass="text-danger" Display="Dynamic" ValidationExpression="^[a-zA-Z]+$" ControlToValidate="txtoutlet" ErrorMessage="Please enter text only"></asp:RegularExpressionValidator>


    </div>

     <div class="row">
        <div class="col-md-3">
            <h4>Image</h4>
        </div>

        <div class="col-md-3">
              <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" BorderColor="#B4EAE9" CssClass="table-responsive col-md-4">
        <Columns>
            <asp:TemplateField>
                <HeaderTemplate></HeaderTemplate>
                <ItemTemplate>
                    <img src='data:image/jpg;base64,<%# Eval("Image") != System.DBNull.Value ? Convert.ToBase64String((byte[])Eval("Image")) : string.Empty %>' alt="image" height="200" width="200"/>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
            </div>

        <div class="col-md-3">
            <asp:FileUpload ID="f1" runat="server" />
        </div>
    </div>
       
                       <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT Image FROM Students WHERE StudentId=@StudentId">
                           <SelectParameters>
                               <asp:SessionParameter Name="StudentId" SessionField="StudentId" />
                           </SelectParameters>
    </asp:SqlDataSource>

     
                    <div class="col-md-9 col-md-offset-7">

                <asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" CssClass="btn btn-default" Text="Update" />
                <asp:Button runat="server" ID="ResetPassword" OnClick="btnResetPassword_Click" CssClass="btn btn-default" Text="Reset Password" />
                        </div>
</asp:Content>
