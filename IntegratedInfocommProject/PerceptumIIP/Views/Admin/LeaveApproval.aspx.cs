﻿using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PerceptumIIP.Views.Admin
{
    public partial class LeaveApproval : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (var db = new ApplicationDbContext())
            {
                var leaves = db.LeaveApplication.ToList();
                DataTable dt = new DataTable();
                dt.Columns.Add("Leave ID");
                dt.Columns.Add("Employee ID");
                dt.Columns.Add("Employee Name");
                dt.Columns.Add("Start Date");
                dt.Columns.Add("End Date");
                dt.Columns.Add("Status");



                foreach (var leave in leaves)
                {

                    if(leave.Type != "Medical")
                    {
                        var theEmployee = db.Employees.Single(x => x.EmployeeId == leave.EmployeeId);
                        DataRow dr = dt.NewRow();
                        dr[0] = leave.LeaveId;
                        dr[1] = theEmployee.EmployeeId;
                        dr[2] = theEmployee.FirstName + " " + theEmployee.LastName;
                        dr[3] = leave.StartDate.ToString("dd MMM yyyy");
                        dr[4] = leave.EndDate.ToString("dd MMM yyyy");
                        dr[5] = leave.Approval;
                        dt.Rows.Add(dr);
                    }
                  
                }

                leavegrid.DataSource = dt;
                leavegrid.DataBind();
            }
        }
        protected void LeavePageChange(object sender, GridViewPageEventArgs e)
        {

            leavegrid.PageIndex = e.NewPageIndex;
            leavegrid.DataBind();
        }
        private static readonly string scriptUpdated = "<script language=\"javascript\">\n" +
"alert (\"Leave Approved! \");\n" +
"</script>";
        private static readonly string scriptRejected = "<script language=\"javascript\">\n" +
"alert (\"Leave Rejected! \");\n" +
"</script>";

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = leavegrid.Rows[index];
            var lId = Convert.ToInt32(row.Cells[2].Text);
            Type csType = this.GetType();

            using (var db = new ApplicationDbContext())
            {
                var theLeave = db.LeaveApplication.Single(x => x.LeaveId == lId);

                if(e.CommandName == "Approve" || e.CommandName == "Reject")
                {
                    if (e.CommandName == "Approve")
                    {
                        theLeave.Approval = "Approved";
                        ClientScript.RegisterStartupScript(csType, "Updated", scriptUpdated);
                    }
                    else if(e.CommandName == "Reject")
                    {
                        theLeave.Approval = "Rejected";
                        ClientScript.RegisterStartupScript(csType, "Rejected", scriptRejected);
                    }
                    db.SaveChanges();
                    Response.Redirect(Request.RawUrl);
                }
            }
        }
    }
}