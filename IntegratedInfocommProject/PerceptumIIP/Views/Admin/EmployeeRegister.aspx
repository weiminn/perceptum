﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="EmployeeRegister.aspx.cs" Inherits="PerceptumIIP.Views.Admin.EmployeeRegister" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .col-md-10{
            margin-bottom:20px;
        }

         @media (min-width: 768px) and (max-width: 992px) {
            .row {
                font-size: 20px;
            }
            input[type="text"], input[type="password"], input[type="email"], input[type="tel"], input[type="select"] {
                max-width: 480px;
            }
            .form-control {
                height: 50px;
            }
        }
    </style>

    <h1>Register New Employee</h1>
    <hr />


   
    
    <div class="row">
        <div class="col-md-3">
            <asp:Label runat="server" AssociatedControlID="FirstName" CssClass="col-md-5 control-label">First Name</asp:Label>
            <div class="form-group">
                <asp:TextBox runat="server" ID="FirstName" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="FirstName"
                    CssClass="text-danger" ErrorMessage="The first name field is required." />
         <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" CssClass="text-danger" Display="Dynamic" ValidationExpression="^[a-zA-Z]+$" ControlToValidate="FirstName" ErrorMessage="Please enter text only"></asp:RegularExpressionValidator>

            </div>
        </div>

        <div class="col-md-3">
            <asp:Label runat="server" AssociatedControlID="LastName" CssClass="col-md-5 control-label">Last Name</asp:Label>
            <div class="form-group">
                <asp:TextBox runat="server" ID="LastName" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="LastName"
                    CssClass="text-danger" ErrorMessage="The last name field is required." />
  <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" CssClass="text-danger" Display="Dynamic" ValidationExpression="^[a-zA-Z]+$" ControlToValidate="LastName" ErrorMessage="Please enter text only"></asp:RegularExpressionValidator>

            </div>
        </div>

        <div class="col-md-3">
            
        <asp:Label runat="server" AssociatedControlID="Address" CssClass="col-md-5 control-label">Address</asp:Label>
        <div class="form-group">
            <asp:TextBox runat="server" ID="Address" CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="Address"
                CssClass="text-danger" Display="Dynamic" ErrorMessage="The address field is required." />
        </div>
    </div>

    </div>


     <div class="row">

         <div class="col-md-3">
        <asp:Label runat="server" AssociatedControlID="DOB" CssClass="col-md-5 control-label">Date of Birth</asp:Label>
        <div class="form-group">
            <asp:textbox ID="DOB" runat="server" cssclass="form-control" onfocus="myFunction(this)"/>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="DOB"
                CssClass="text-danger" Display="Dynamic" ErrorMessage="The date of birth field is required." />
        </div>
    </div>


           <div class="col-md-3">
        <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-5 control-label">Email</asp:Label>
        <div class="form-group">
            <asp:TextBox runat="server" ID="Email" CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                CssClass="text-danger" Display="Dynamic" ErrorMessage="The email field is required." />
             <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" CssClass="text-danger" Display="Dynamic" validationexpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"  ControlToValidate="Email" ErrorMessage="Please enter in email format"></asp:RegularExpressionValidator>

        </div>
    </div>
             <div class="col-md-3">
        <asp:Label runat="server" AssociatedControlID="Telephone" CssClass="col-md-5 control-label">Telephone</asp:Label>
        <div class="form-group">
            <asp:TextBox runat="server" ID="Telephone" CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="Telephone"
                CssClass="text-danger" Display="Dynamic" ErrorMessage="The telephone field is required." />
                               <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" CssClass="text-danger" Display="Dynamic" ValidationExpression="\d+" ControlToValidate="Telephone" ErrorMessage="Please enter numbers only"></asp:RegularExpressionValidator>

        </div>
    </div>

    </div>


    <div class="row">
            <div class="col-md-3">
       <asp:Label runat="server" AssociatedControlID="DDDepartment" CssClass="col-md-5 control-label">Department</asp:Label>
        <div class="col-sm-offset-2 col-sm-10">
          <asp:DropDownList
                              ID="DDDepartment" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Value="Mathematics">Mathematics</asp:ListItem>
                                                            <asp:ListItem Value="Science">Science</asp:ListItem>


                          </asp:DropDownList>
            </div>
           
           <asp:RequiredFieldValidator runat="server" ControlToValidate="DDDepartment" CssClass="text-danger" Display="Dynamic" ErrorMessage="Department field is required">

           </asp:RequiredFieldValidator>
</div>
    <div class="col-md-3">
        <asp:Label runat="server" AssociatedControlID="Position" CssClass="col-md-5 control-label">Position</asp:Label>
        <div class="form-group">
            <asp:TextBox runat="server" ID="Position" CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="Position"
                CssClass="text-danger" ErrorMessage="The position field is required." />
        </div>
    </div>

        <div class="col-md-3">
        <asp:Label runat="server" AssociatedControlID="Salary" CssClass="col-md-5 control-label">Salary</asp:Label>
        <div class="form-group">
            <asp:TextBox runat="server" ID="Salary" CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="Salary"
                CssClass="text-danger" Display="Dynamic" ErrorMessage="The salary field is required." />
                               <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="\d+" CssClass="text-danger" Display="Dynamic" ControlToValidate="Salary" ErrorMessage="Please enter numbers only"></asp:RegularExpressionValidator>

        </div>
    </div>

    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="UserName" CssClass="col-md-5 control-label">UserName</asp:Label>
                <asp:TextBox runat="server" ID="UserName" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="UserName"
                    CssClass="text-danger" ErrorMessage="The name field is required." />
                             <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" CssClass="text-danger" Display="Dynamic" validationexpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"  ControlToValidate="UserName" ErrorMessage="Please enter in email format"></asp:RegularExpressionValidator>

            </div>
        </div>
   
    <div class="col-md-3">
        <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-5 control-label">Password</asp:Label>
        <div class="form-group">
            <asp:TextBox runat="server" ID="Password" CssClass="form-control" TextMode="Password" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="Password"
                CssClass="text-danger" ErrorMessage="The password field is required." />

        </div>
    </div>
         
    </div>
    
    <div class="row">
    <div class="col-md-offset-8 col-md-10">
        <div class="form-group">
            <asp:Button ID="CreateUser" CssClass="btn btn-default" runat="server" OnClick="CreateUser_Click" Text="Register" CausesValidation="false"/>
        </div>
    </div>
    </div>

    <script>
        function myFunction(obj) {
            $(obj).datepicker();
        }

        $(document).ready(function(){
             $('.datepicker-field').datepicker();        
        });
    </script>

</asp:Content>
