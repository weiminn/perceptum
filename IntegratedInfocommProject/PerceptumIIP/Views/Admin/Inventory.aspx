﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Inventory.aspx.cs" Inherits="PerceptumIIP.Views.Admin.Inventory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <style>
        .inventorygridview {
    width:100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color:white;
}


.inventorygridview td {
    padding:10px; 
    border: solid 1px black;
    color:black;
}

    .inventorygridview th {
        background-color:#f3e222;
        text-align:center;
        padding: 10px 2px;
        border: solid 1px black;
        color: black;
    }

.inventorygridview tr:nth-child(even) {
    background-color: #ffffff;
}

.inventorygridview tr:nth-child(odd) {
    background-color: #dcdcdc;
}

.inventorygridview .alt{
    background:#dcdcdc;
}

.inventorygridview .pgr{

    background: #dcdcdc;

}
    .inventorygridview .pgr table {
        margin: 5px 0;
    }

    .inventorygridview .pgr td {
        border-width:0;
        padding: 0 6px;
        border-left: solid 1px black;
    }
   .inventorygridview .pgr a{
        color:black;
        text-decoration:none;
    }
   .inventorygridview .pgr a:hover{
       color: #000;
       text-decoration:underline;
   }
    </style>
            <link rel="stylesheet" href="/CSS/master.css" type="text/css" />



    <asp:GridView ID="InventoryGrid" runat="server" AutoGenerateColumns="False" DataKeyNames="ProductId" OnPageIndexChanging="InventoryPageChange"  CssClass="inventorygridview" AllowPaging="true" OnRowCommand="InventoryGrid_RowCommand" GridLines="None" Font-Size="Medium" Font-Names="Arial" CellPadding="3" CellSpacing="10" AlternatingRowStyle-CssClass="alt" PageSize="7" PagerStyle-CssClass="pgr">
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:BoundField DataField="ProductId" HeaderText="ProductId" InsertVisible="False" ReadOnly="True" SortExpression="ProductId" />
            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
            <asp:BoundField DataField="Details" HeaderText="Details" SortExpression="Details" />
            <asp:BoundField DataField="Salesitem" HeaderText="Salesitem" SortExpression="Salesitem" />
            <asp:BoundField DataField="ProductCategory" HeaderText="ProductCategory" SortExpression="ProductCategory" />
            <asp:BoundField DataField="NormalPrice" HeaderText="NormalPrice" SortExpression="NormalPrice" />
            <asp:BoundField DataField="PromoPrice" HeaderText="PromoPrice" SortExpression="PromoPrice" />
            <asp:BoundField DataField="QuantityOnHand" HeaderText="QuantityOnHand" SortExpression="QuantityOnHand" />
            <asp:ButtonField Text="Details" CommandName="Details" />


        </Columns>
                <PagerStyle CssClass="pgr"></PagerStyle>

    </asp:GridView>
<%--    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Products]"></asp:SqlDataSource>--%>




</asp:Content>
