﻿using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using PerceptumIIP.Models;

namespace PerceptumIIP.Views.Admin
{
    public partial class Attendance : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            


        }

            protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
            {

                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = GridView1.Rows[index];
                var StudentId = Convert.ToInt32(row.Cells[0].Text);

                using (var db = new ApplicationDbContext())
                {
                    var Attendance = db.Attendances.Single(x => x.StudentId == StudentId);

                    if (e.CommandName == "Present")
                    {
                        Attendance.Status = "Present";
                    }
                    else
                    {
                        Attendance.Status = "Absent";
                    }
                }

            }

           




        }
    }
