﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Microsoft.AspNet.Identity;
using PerceptumIIP.Models;
using System.Net.Mail;
using System.Text;

namespace PerceptumIIP.Views.Admin
{
    public partial class Edit_Inventory : System.Web.UI.Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {

            string ProductId = Convert.ToString(Session["ProductId"]);

            lblName.Text = (string)Session["Name"];
            lbldetails.Text = (string)Session["Details"];
            lblsalesitem.Text = (string)Session["SalesItem"];
            lblprodcat.Text = (string)Session["ProductCategory"];
            lblnormprice.Text = (string)Session["NormalPrice"];
            lblpromoprice.Text = (string)Session["PromoPrice"];
            lblqty.Text = (string)Session["QuantityOnHand"];
        }
        private static readonly string scriptSuccessUpdate = "<script language=\"javascript\">\n" +
"alert (\"Update Successful!\");\n </script>";

        protected void btnUpdate_Click(object sender, EventArgs e)
        {


            if (txtName.Text != "")
            {
                String strFName = "Name";
                String strFValue = txtName.Text;
                UpdateProducts(strFName, strFValue);
                Session["Name"] = txtName.Text;
            }
            if (txtdetails.Text != "")
            {
                String strFName = "Details";
                String strFValue = txtdetails.Text;
                UpdateProducts(strFName, strFValue);
                Session["Details"] = txtdetails.Text;
            }
            if (txtsalesitem.Text != "")
            {
                String strFName = "SalesItem";
                String strFValue = txtsalesitem.Text;
                UpdateProducts(strFName, strFValue);
                Session["SalesItem"] = txtsalesitem.Text;
            }
            if (txtprodcat.Text != "")
            {
                String strFName = "ProductCategory";
                String strFValue = txtprodcat.Text;
                UpdateProducts(strFName, strFValue);
                Session["ProductCategory"] = txtprodcat.Text;
            }
            if (txtnormprice.Text != "")
            {
                String strFName = "NormalPrice";
                String strFValue = txtnormprice.Text;
                UpdateProducts(strFName, strFValue);
                Session["NormalPrice"] = txtnormprice.Text;
            }

            if (txtpromoprice.Text != "")
            {
                String strFName = "PromoPrice";
                String strFValue = txtpromoprice.Text;
                UpdateProducts(strFName, strFValue);
                Session["PromoPrice"] = txtpromoprice.Text;
            }


            if (txtqty.Text != "")
            {
                String strFName = "QuantityOnHand";
                String strFValue = txtqty.Text;
                UpdateProducts(strFName, strFValue);
                Session["QuantityOnHand"] = txtqty.Text;
            }

            GridViewRow row1 = GridView1.Rows[0];
            string oldimage = row1.Cells[0].Text;
            Session["soldimage"] = oldimage;

            if (f1.HasFile)
            {
                String strFName = "Image";
                Byte[] Image = f1.FileBytes;//f1.SaveAs(Request.PhysicalApplicationPath + "/Content/images/" + f1.FileName.ToString());
                //String strFValue = "~/Content/images/" + f1.FileName.ToString();
                //UpdateProducts(strFName, strFValue);

                int strProductId = Convert.ToInt32(Session["ProductId"]);
                using (var db = new ApplicationDbContext())
                {
                    var product = db.Products.SingleOrDefault(x => x.ProductId == strProductId);
                    product.Image = Image;
                    db.SaveChanges();
                }

                Session["oldimage"] = f1.HasFile;
            }



            Type strType = this.GetType();
            ClientScript.RegisterStartupScript(strType, "Success", scriptSuccessUpdate);
            Response.Redirect("Inventory.aspx");
        }


        public void UpdateProducts(string strFName, string strFValue)

        {
            string strProductId = (string)Session["ProductId"];


            SqlConnection con = new SqlConnection(@"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=aspnet-PerceptumIIP2;Trusted_Connection=Yes;User ID=Perceptum2018;Password=p12345;Integrated Security=True;");
            con.Open();
            SqlCommand cmd;
            Type csType = this.GetType();
            String strSQL = "UPDATE Products  SET " + strFName +
                " = @newValue WHERE ProductId = @ProductId";
            cmd = new SqlCommand(strSQL, con);
            cmd.Parameters.Add("@newValue", SqlDbType.Char).Value = strFValue;
            cmd.Parameters.Add("@ProductId", SqlDbType.NVarChar).Value = strProductId;
            cmd.ExecuteNonQuery();
            con.Close();

        }
    }
}
