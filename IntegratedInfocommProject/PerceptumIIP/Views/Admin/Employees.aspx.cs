﻿using System;
using System.Configuration;
using System.Data;
using PerceptumIIP.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PerceptumIIP.Views.Admin
{
    public partial class Employees : System.Web.UI.Page
    {

       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EmployeeGrid.DataBind();
            }
        }

        protected void EmployeePageChange(object sender , GridViewPageEventArgs e)
        {

            EmployeeGrid.PageIndex = e.NewPageIndex;
            EmployeeGrid.DataBind();
        }
        private static readonly string scriptStockOut = "<script language=\"javascript\">\n" +
"alert (\"Item successfully deleted! \");\n" +
"</script>";

        protected void EmployeeGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if(e.CommandName == "Edit")
            {
           
                int index = Convert.ToInt32(e.CommandArgument); //gives row number that is clicked
                GridViewRow row = EmployeeGrid.Rows[index];
                var employeeid = row.Cells[0].Text;
                var name = row.Cells[1].Text;
                var address = row.Cells[2].Text;
                var birthdate = row.Cells[3].Text;
                var department = row.Cells[4].Text;
                var position = row.Cells[5].Text;
                var email = row.Cells[6].Text;
                var telephone = row.Cells[7].Text;
                var salary = row.Cells[8].Text;

                Session["EmployeeId"] = employeeid;
                Session["FirstName"] = name;
                Session["Add"] = address;
                Session["BirthDate"] = birthdate;
                Session["Department"] = department;
                Session["Salary"] = salary;
                Session["Telephone"] = telephone;
                Session["Email"] = email;
            
                Session["EmployeeId"] = employeeid;
                
                Response.Redirect("UpdateEmployees.aspx");

            }



            if (e.CommandName == "DeleteEm")
            {

                int index = Convert.ToInt32(e.CommandArgument); //gives row number that is clicked
                GridViewRow row = EmployeeGrid.Rows[index];
                int employeeid = Convert.ToInt32(row.Cells[0].Text);

                using (var db = new ApplicationDbContext())
                { 
                    var theEmployee = db.Employees.SingleOrDefault(x => x.EmployeeId == employeeid);
                    db.Employees.Remove(theEmployee);
                    db.SaveChanges();

                    Type csType = this.GetType();
                    ClientScript.RegisterStartupScript(csType, "Delete", scriptStockOut);
                    Response.Redirect("Employees.aspx");

                }



            }
        }

    }
}