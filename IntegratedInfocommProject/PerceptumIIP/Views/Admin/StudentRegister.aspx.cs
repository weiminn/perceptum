﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PerceptumIIP.Views.Admin
{
    public partial class StudentRegister : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        private static readonly string scriptSuccessNewAccount =
          "<script language=\"javascript\">\n" +
          "alert (\"Your account has been succesfully created - Thank You!\");\n" +
          "</script>";

        protected void CreateUser_Click1(object sender, EventArgs e)
        {
            Type csType = this.GetType();
            var userStore = new UserStore<IdentityUser>();
            var manager = new UserManager<IdentityUser>(userStore);

            // Need to create Identity user object
            var user = new IdentityUser()
            {
                UserName = txtUserName.Text,
                Email = txtEmail.Text
            };
            IdentityResult result = manager.Create(user, Password.Text);

            if (result.Succeeded)
            {
                using (var db = new ApplicationDbContext())
                {
                    Student student = new Student
                    {
                        UserName = txtUserName.Text,
                        FirstName = FirstName.Text,
                        LastName = LastName.Text,
                        Email = txtEmail.Text,
                        Password = Password.Text,
                        Address = Address.Text,
                        BirthDate = Convert.ToDateTime(DOB.Text),
                        Level = DDLevel.Text,
                        Outlet = Branch.SelectedValue,
                        Telephone = Convert.ToInt32(Telephone.Text),
                        Image = FileUploadControl.FileBytes,                        School = School.Text,
                        ApplicationUserId = user.Id

                    };

                    db.Students.Add(student);
                    db.SaveChanges();

                    if (txtClassID.Text == string.Empty)
                    {
                        StudentClass studentClass = new StudentClass
                        {

                            StudentId = student.StudentId,
                            ClassId = Convert.ToInt32(txtClassID.Text)
                        };

                        db.StudentClasses.Add(studentClass);
                        db.SaveChanges();
                    }



                    if (ParentID.Text == string.Empty)
                    {
                        StudentParent studentParent = new StudentParent
                        {

                            StudentId = student.StudentId,
                            ParentId = Convert.ToInt32(ParentID.Text)
                        };

                        db.StudentParents.Add(studentParent);
                        db.SaveChanges();
                    }
                }
                ClientScript.RegisterStartupScript(csType, "Success", scriptSuccessNewAccount);

                Response.Redirect("EmployeeStudents.aspx");

            }
        }
    }
}