﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using PerceptumIIP.Models;

namespace PerceptumIIP.Views.Admin
{
    public partial class StudentProfile : System.Web.UI.Page
    {

        static readonly string scriptStockOut = "<script language=\"javascript\">\n" +
"alert (\"Item successfully deleted! \");\n" +
"</script>";
        protected void Page_Load(object sender, EventArgs e)
        {
            string studentid = Convert.ToString(Session["StudentId"]);
        }

        protected void Update_Click(object sender, EventArgs e)
        {
            Session["StudentId"] = GridView2.Rows[0].Cells[0].Text;
            Session["FirstName"] = GridView2.Rows[0].Cells[1].Text;
            Session["LastName"] = GridView2.Rows[0].Cells[2].Text;
            Session["Password"] = GridView2.Rows[0].Cells[3].Text;
            Session["Address"] = GridView2.Rows[0].Cells[4].Text;
            Session["BirthDate"] = GridView2.Rows[0].Cells[5].Text;
            Session["School"] = GridView2.Rows[0].Cells[6].Text;
            Session["Level"] = GridView2.Rows[0].Cells[7].Text;
            Session["Telephone"] = GridView2.Rows[0].Cells[8].Text;
            Session["Email"] = GridView2.Rows[0].Cells[9].Text;
            Session["Outlet"] = GridView2.Rows[0].Cells[10].Text;

            Response.Redirect("Updatestudents");
        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            int studentid = Convert.ToInt32(Session["StudentId"]);

            using (var db = new ApplicationDbContext())
            {
                var theStudent = db.Students.SingleOrDefault(x => x.StudentId == studentid);
                db.Students.Remove(theStudent);
                db.SaveChanges();

                Type csType = this.GetType();
                ClientScript.RegisterStartupScript(csType, "Delete", scriptStockOut);

                Response.Redirect("EmployeeStudents.aspx");
            }
        }
    }
}