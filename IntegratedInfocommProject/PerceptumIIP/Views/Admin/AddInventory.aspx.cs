﻿using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace PerceptumIIP.Views.Admin
{
    public partial class AddInventory : System.Web.UI.Page
    {

        private static readonly string scriptSuccessNewAccount =
         "<script language=\"javascript\">\n" +
         "alert (\"Your item has been succesfully added - Thank You!\");\n" +
         "</script>";
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void Sumbit_Click(object sender, EventArgs e)
        {
            Type csType = this.GetType();

            if (!FileUploadControl.HasFile)
            {
                ClientScript.RegisterStartupScript(csType, "Success", "<script language=\"javascript\">\n" +
         "alert (\"Please Upload a file!\");\n" +
         "</script>");
                return;
            }
            

            if(SalesItem.Text == "N" && NormalPrice.Text != PromoPrice.Text)
            {
                ClientScript.RegisterStartupScript(csType, "Success", "<script language=\"javascript\">\n" +
                        "alert (\"Normal Price and Promotion Price not the same!\");\n" +
                        "</script>");
            }
            
            using (var db = new ApplicationDbContext())
            {
                Product Product = new Product
                {
                    Name = Name.Text,
                    Details = Details.Text,
                    ProductCategory = ProductCategory.Text,
                    Salesitem = SalesItem.Text,
                    NormalPrice = Convert.ToDecimal(NormalPrice.Text),
                    PromoPrice = Convert.ToDecimal(PromoPrice.Text),
                    QuantityOnHand = Convert.ToInt32(QuantityOnHand.Text),
                    Image = FileUploadControl.FileBytes
                };

                Session["ProductName"] = (string)Name.Text;


                db.Products.Add(Product);
                db.SaveChanges();
                db.Dispose();
            }
            Response.Redirect("/Views/Admin/Inventory.aspx");

            ClientScript.RegisterStartupScript(csType, "Success", scriptSuccessNewAccount);

        }
    }
}