﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Edit Inventory.aspx.cs" Inherits="PerceptumIIP.Views.Admin.Edit_Inventory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

        <div class="row">

        <div class="col-md-3">
<h2>Data Item</h2>


            </div>
        <div class="col-md-3">
<h2>Existing Data</h2>
        </div>
        <div class="col-md-3">
<h2>New Data</h2>   
    </div>
    </div>

    
    <div class="row">
        <div class="col-md-3">
            Name
        </div>
        <div class="col-md-3">
    <h4> <asp:Label ID="lblName" runat="server"></asp:Label> </h4>

        </div>
        <div class="col-md-3">
       <asp:TextBox ID="txtName" runat="server" Width="250px"></asp:TextBox>

        </div>
                          <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" CssClass="text-danger" Display="Dynamic" ValidationExpression="^[a-zA-Z]+$" ControlToValidate="txtName" ErrorMessage="Please enter text only"></asp:RegularExpressionValidator>

    </div>
    
    <div class="row">
        <div class="col-md-3">
            Details
        </div>
        <div class="col-md-3">
           <h4>    <asp:Label ID="lbldetails" runat="server"></asp:Label></h4> 

        </div>
        <div class="col-md-3">
                   <asp:TextBox ID="txtdetails" runat="server" Width="250px"></asp:TextBox>

        </div>
                                      <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" CssClass="text-danger" Display="Dynamic" ValidationExpression="^[a-zA-Z]+$" ControlToValidate="txtdetails" ErrorMessage="Please enter text only"></asp:RegularExpressionValidator>

    </div>
    
    <div class="row">
        <div class="col-md-3">
            Sales Item
        </div>
        <div class="col-md-3">
                       <h4>  <asp:Label ID="lblsalesitem" runat="server"></asp:Label> </h4>

        </div>
        <div class="col-md-3">
                            <asp:TextBox ID="txtsalesitem" runat="server" Width="250px"></asp:TextBox>

        </div>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" CssClass="text-danger" Display="Dynamic" ValidationExpression="^[a-zA-Z]+$" ControlToValidate="txtsalesitem" ErrorMessage="Please enter text only"></asp:RegularExpressionValidator>

    </div>
    
    <div class="row">
        <div class="col-md-3">
            Product Category 
        </div>
        <div class="col-md-3">
                         <h4>  <asp:Label ID="lblprodcat" runat="server"></asp:Label> </h4> 

        </div>
        <div class="col-md-3">
                            <asp:TextBox ID="txtprodcat" runat="server" Width="250px"></asp:TextBox>

        </div>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" CssClass="text-danger" Display="Dynamic" ValidationExpression="^[a-zA-Z]+$" ControlToValidate="txtprodcat" ErrorMessage="Please enter text only"></asp:RegularExpressionValidator>

    </div>

        
    <div class="row">
        <div class="col-md-3">
            Normal Price
        </div>
        <div class="col-md-3">
                         <h4>  <asp:Label ID="lblnormprice" runat="server"></asp:Label> </h4>

        </div>
        <div class="col-md-3">
                            <asp:TextBox ID="txtnormprice" runat="server" Width="250px"></asp:TextBox>

        </div>
                           <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="\d+" ControlToValidate="txtnormprice" ErrorMessage="Please enter numbers only"></asp:RegularExpressionValidator>

    </div>

     <div class="row">
        <div class="col-md-3">
            Promotion Price
        </div>
        <div class="col-md-3">
                        <h4>   <asp:Label ID="lblpromoprice" runat="server"></asp:Label> </h4>

        </div>
        <div class="col-md-3">
                            <asp:TextBox ID="txtpromoprice" runat="server" Width="250px"></asp:TextBox>

        </div>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationExpression="\d+" ControlToValidate="txtpromoprice" ErrorMessage="Please enter numbers only"></asp:RegularExpressionValidator>

    </div>

       <div class="row">
        <div class="col-md-3">
            Quantity On Hand
        </div>
        <div class="col-md-3">
                        <h4>   <asp:Label ID="lblqty" runat="server"></asp:Label> </h4>

        </div>
        <div class="col-md-3">
                            <asp:TextBox ID="txtqty" runat="server" Width="250px"></asp:TextBox>

        </div>
                                                      <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ValidationExpression="\d+" ControlToValidate="txtqty" ErrorMessage="Please enter numbers only"></asp:RegularExpressionValidator>

    </div>

       
           
       
        <div class="row">
            <div class="col-md-3">
                Image
            </div>
            <div class="col-md-3">
              <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource2" BorderColor="#B4EAE9" CssClass="table-responsive col-md-4">
        <Columns>
            <asp:TemplateField>
                <HeaderTemplate></HeaderTemplate>
                <ItemTemplate>
                    <img src='data:image/jpg;base64,<%# Eval("Image") != System.DBNull.Value ? Convert.ToBase64String((byte[])Eval("Image")) : string.Empty %>' alt="image" height="200" width="200"/>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
            </div>

                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT Image FROM [Products] WHERE ([ProductId] = @ProductId)">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="ProductId" SessionField="ProductId" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
                
            
           
        
            <div class="col-md-3">
                <asp:FileUpload ID="f1" runat="server" CssClass="auto-style3" />
            </div>     
            </div>
                                <div class="col-md-9 col-md-offset-8">

                <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn btn-default" OnClick="btnUpdate_Click" />

        </div>









</asp:Content>
