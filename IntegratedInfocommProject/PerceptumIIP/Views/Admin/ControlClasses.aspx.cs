﻿using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PerceptumIIP.Views.Admin
{
    public partial class ControlClasses : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private static readonly string scriptSuccessNewAccount =
          "<script language=\"javascript\">\n" +
          "alert (\"Your class has been succesfully created - Thank You!\");\n" +
          "</script>";

        protected void submit_Click(object sender, EventArgs e)
        {
            Type csType = this.GetType();

            using (var db = new ApplicationDbContext())
            {

                Class newClass = new Class
                {
                    ClassDescription = ClassDescription.Text,
                    Day = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), Day.Text),
                    Time = DateTime.ParseExact(Time.Text, "HH:mm", System.Globalization.CultureInfo.CurrentCulture),
                    Level = DDLevel.Text,
                    MakeUpSlots = 5
                };

                db.Classes.Add(newClass);
                db.SaveChanges();
                db.Dispose();

            }

            ClientScript.RegisterStartupScript(csType, "Success", scriptSuccessNewAccount);

            ClassDescription.Text = "";
            Day.Text = "";
            Time.Text = "";
            DDLevel.Text = "";


        }
    }
}