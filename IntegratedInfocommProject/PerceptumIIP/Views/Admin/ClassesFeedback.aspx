﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="ClassesFeedback.aspx.cs" Inherits="PerceptumIIP.ClassesFeedback" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
      <style>
        .classesgrid {
    width:100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color:white;
}


.classesgrid td {
    padding:10px; 
    border: solid 1px black;
    color:black;
}

    .classesgrid th {
        background-color:#f3e222;
        text-align:center;
        padding: 10px 2px;
        border: solid 1px black;
        color: black;
    }

.classesgrid tr:nth-child(even) {
    background-color: #ffffff;
}

.classesgrid tr:nth-child(odd) {
    background-color: #dcdcdc;
}

.classesgrid .alt{
    background:#dcdcdc;
}

.classesgrid .pgr{

    background: #dcdcdc;

}
    .classesgrid .pgr table {
        margin: 5px 0;
    }

    .classesgrid .pgr td {
        border-width:0;
        padding: 0 6px;
        border-left: solid 1px black;
    }
    .classesgrid .pgr a{
        color:black;
        text-decoration:none;
    }
          </style>

        <asp:GridView ID="mathdetails" runat="server" CssClass="classesgrid" GridLines="None" Font-Size="Medium" Font-Names="Arial" CellPadding="3"  CellSpacing="10"  AlternatingRowStyle-CssClass="alt"  PageSize="7"  PagerStyle-CssClass="pgr"></asp:GridView>

</asp:Content>
