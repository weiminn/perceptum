﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="InventoryProfile.aspx.cs" Inherits="PerceptumIIP.Views.Admin.InventoryProfile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

      <style>

        .InventoryProfileGrid {
    width: 100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color: white;
}


    .InventoryProfileGrid td {
        padding: 10px;
        border: solid 1px black;
        color: black;
    }

    .InventoryProfileGrid th {
        background-color: #f3e222;
        text-align: center;
        padding: 10px 2px;
        border: solid 1px black;
        color: black;
    }

    .InventoryProfileGrid tr:nth-child(even) {
        background-color: #ffffff;
    }

    .InventoryProfileGrid tr:nth-child(odd) {
        background-color: #dcdcdc;
    }

    .InventoryProfileGrid .alt {
        background: #dcdcdc;
    }

    .InventoryProfileGrid .pgr {
        background: #dcdcdc;
    }

        .InventoryProfileGrid .pgr table {
            margin: 5px 0;
        }

        .InventoryProfileGrid .pgr td {
            border-width: 0;
            padding: 0 6px;
            border-left: solid 1px black;
        }

        .InventoryProfileGrid .pgr a {
            color: black;
            text-decoration: none;
        }
    </style>
            <link rel="stylesheet" href="/CSS/master.css" type="text/css" />


    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" BorderColor="#B4EAE9" CssClass="table-responsive col-md-4">
        <Columns>
            <asp:TemplateField>
                <HeaderTemplate></HeaderTemplate>
                <ItemTemplate>
                    <img src='data:image/jpg;base64,<%# Eval("Image") != System.DBNull.Value ? Convert.ToBase64String((byte[])Eval("Image")) : string.Empty %>' alt="image" height="200" width="200"/>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataKeyNames="ProductId" DataSourceID="SqlDataSource2" CssClass="InventoryProfileGrid" GridLines="None" Font-Size="Medium" Font-Names="Arial" CellPadding="3" CellSpacing="10" AlternatingRowStyle-CssClass="alt" PageSize="7" PagerStyle-CssClass="pgr">
       
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>

        <Columns>
            <asp:BoundField DataField="ProductId" HeaderText="ProductId" InsertVisible="False" ReadOnly="True" SortExpression="ProductId" />
            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
            <asp:BoundField DataField="Details" HeaderText="Details" SortExpression="Details" />
            <asp:BoundField DataField="Salesitem" HeaderText="Salesitem" SortExpression="Salesitem" />
            <asp:BoundField DataField="ProductCategory" HeaderText="ProductCategory" SortExpression="ProductCategory" />
            <asp:BoundField DataField="NormalPrice" HeaderText="NormalPrice" SortExpression="NormalPrice" />
            <asp:BoundField DataField="PromoPrice" HeaderText="PromoPrice" SortExpression="PromoPrice" />
            <asp:BoundField DataField="QuantityOnHand" HeaderText="QuantityOnHand" SortExpression="QuantityOnHand" />
        </Columns>
       
        <PagerStyle CssClass="pgr"></PagerStyle>

    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Products] WHERE ([ProductId] = @ProductId)">
        <SelectParameters>
            <asp:SessionParameter Name="ProductId" SessionField="ProductId"/>
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:Button ID="Update" runat="server" Text="Update" OnClick="Update_Click" />
    <asp:Button ID="Delete" runat="server" Text="Delete" OnClick="Delete_Click" />
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT Image FROM Products WHERE ProductId=@ProductId">
        <SelectParameters>
            <asp:SessionParameter Name="ProductId" SessionField="ProductId" />
        </SelectParameters>
    </asp:SqlDataSource>




</asp:Content>
