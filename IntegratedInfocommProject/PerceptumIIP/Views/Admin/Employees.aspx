﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Employees.aspx.cs" Inherits="PerceptumIIP.Views.Admin.Employees" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
            <link rel="stylesheet" href="/CSS/master.css" type="text/css" />

    <h1>Employees</h1>
    <asp:GridView ID="EmployeeGrid" runat="server" AutoGenerateColumns="False" OnPageIndexChanging="EmployeePageChange" DataKeyNames="EmployeeId" DataSourceID="SqlDataSource1" AllowPaging="True" CssClass="EmployeeGrid" GridLines="None" Font-Size="Medium" Font-Names="Arial" CellPadding="3"  CellSpacing="10"  AlternatingRowStyle-CssClass="alt"  PageSize="7"  PagerStyle-CssClass="pgr" OnRowCommand="EmployeeGrid_RowCommand"> 
        <Columns>
            <asp:BoundField DataField="EmployeeId" HeaderText="EmployeeId" InsertVisible="False" ReadOnly="True" SortExpression="EmployeeId" />
            <asp:BoundField DataField="FirstName" HeaderText="Name" SortExpression="FirstName" />
            <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
            <asp:BoundField DataField="DOB" HeaderText="BirthDate" SortExpression="DOB" DataFormatString="{0:D}"/>
            <asp:BoundField DataField="Department" HeaderText="Department" SortExpression="Department" />
            <asp:BoundField DataField="Position" HeaderText="Position" SortExpression="Position" />
            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
            <asp:BoundField DataField="Telephone" HeaderText="Telephone" SortExpression="Telephone" />
            <asp:BoundField DataField="Salary" HeaderText="Salary" SortExpression="Salary" />
            <asp:ButtonField Text="Edit" CommandName="Edit" />
            <asp:ButtonField Text="Delete" CommandName="DeleteEm" />


        </Columns>
        <PagerStyle CssClass="pgr"></PagerStyle>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Employees]"></asp:SqlDataSource>
    



</asp:Content>
