﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Classes.aspx.cs" Inherits="PerceptumIIP.Views.Admin.Classes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .classesgrid {
    width:100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color:white;
}


.classesgrid td {
    padding:10px; 
    border: solid 1px black;
    color:black;
}

    .classesgrid th {
        background-color:#f3e222;
        text-align:center;
        padding: 10px 2px;
        border: solid 1px black;
        color: black;
    }

.classesgrid tr:nth-child(even) {
    background-color: #ffffff;
}

.classesgrid tr:nth-child(odd) {
    background-color: #dcdcdc;
}

.classesgrid .alt{
    background:#dcdcdc;
}

.classesgrid .pgr{

    background: #dcdcdc;

}
    .classesgrid .pgr table {
        margin: 5px 0;
    }

    .classesgrid .pgr td {
        border-width:0;
        padding: 0 6px;
        border-left: solid 1px black;
    }
    .classesgrid .pgr a{
        color:black;
        text-decoration:none;
    }
    </style>

    <h1>Mathematics Classes</h1>
   
    <asp:GridView ID="mathgrid" runat="server"  OnRowCommand="mathGrid_RowCommand" AutoGenerateColumns="False" DataKeyNames="ClassId" CssClass="classesgrid" GridLines="None" Font-Size="Medium" Font-Names="Arial" CellPadding="3"  CellSpacing="10"  AlternatingRowStyle-CssClass="alt"  PageSize="7"  PagerStyle-CssClass="pgr">
        <Columns>
            <asp:BoundField DataField="ClassId" HeaderText="ClassId" InsertVisible="False" ReadOnly="True" SortExpression="ClassId" />
            <asp:BoundField DataField="FirstName" HeaderText="Name" InsertVisible="False" ReadOnly="True" SortExpression="FirstName" />
            <asp:BoundField DataField="ClassDescription" HeaderText="ClassDescription" SortExpression="ClassDescription" />
            <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level" />
            <asp:BoundField DataField="Day" HeaderText="Day" SortExpression="Day" DataFormatString="{0:D}" ReadOnly="true" />
            <asp:BoundField DataField="Time" HeaderText="Time" SortExpression="Time" DataFormatString="{0:t}" />
            <asp:ButtonField Text="Feedback Details" CommandName="Details" />
        </Columns>
                <PagerStyle CssClass="pgr"></PagerStyle>

    </asp:GridView>


    <%--<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT ClassId, ClassDescription, Level, Day, Time  FROM Classes WHERE ClassDescription='Mathematics'"></asp:SqlDataSource>--%>

    <br />

    <h1>Science Classes</h1>

    <asp:GridView ID="sciencegrid" runat="server" OnRowCommand="sciencegrid_RowCommand" AutoGenerateColumns="False" DataKeyNames="ClassId" CssClass="classesgrid" GridLines="None" Font-Size="Medium" Font-Names="Arial" CellPadding="3"  CellSpacing="10"  AlternatingRowStyle-CssClass="alt"  PageSize="7"  PagerStyle-CssClass="pgr">
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:BoundField DataField="ClassId" HeaderText="ClassId" InsertVisible="False" ReadOnly="True" SortExpression="ClassId" />
            <asp:BoundField DataField="FirstName" HeaderText="Name" InsertVisible="False" ReadOnly="True" SortExpression="FirstName" />

            <asp:BoundField DataField="ClassDescription" HeaderText="ClassDescription" SortExpression="ClassDescription" />
            <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level" />
            <asp:BoundField DataField="Day" HeaderText="Day" SortExpression="Day" DataFormatString="{0:D}" />
            <asp:BoundField DataField="Time" HeaderText="Time" SortExpression="Time" DataFormatString="{0:t}" />
            <asp:ButtonField Text="Feedback Details" CommandName="Details" />

        </Columns>
                        <PagerStyle CssClass="pgr"></PagerStyle>

    </asp:GridView>

    <%--<asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT ClassId, ClassDescription, Level, Day, Time FROM Classes WHERE ClassDescription='Science'"></asp:SqlDataSource>--%>

</asp:Content>
