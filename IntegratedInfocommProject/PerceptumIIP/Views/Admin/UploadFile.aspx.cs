﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using PerceptumIIP.Models;
using Microsoft.AspNet.Identity;

namespace PerceptumIIP.Views.Admin
{
    public partial class UploadFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

           
        }

        private static readonly string scriptSuccessNewAccount =
       "<script language=\"javascript\">\n" +
       "alert (\"You have successfullu upload the materials!\");\n" +
       "</script>";


        protected void Save_Click(object sender, EventArgs e)
        {
            Type csType = this.GetType();

            string filename = Path.GetFileName(FileUpload1.PostedFile.FileName);
            string contenttype = FileUpload1.PostedFile.ContentType;
            string level = lblevel.Text.ToString();

            using (Stream fs = FileUpload1.PostedFile.InputStream)
            {
                using (BinaryReader br = new BinaryReader(fs))
                {


                    byte[] bytes = br.ReadBytes((Int32)fs.Length);
                    using (var db = new ApplicationDbContext())
                    {

                        Files newfiles = new Files
                        {
                            Name = Convert.ToString(filename),
                            ContentType = contenttype,
                            Data = bytes,
                            Level = level
                        };

                        
                        db.File.Add(newfiles);
                        db.SaveChanges();

                    }




                }
                ClientScript.RegisterStartupScript(csType, "Success", scriptSuccessNewAccount);

            }

            lblevel.Text = "";
        }
        
      
    }
}
