﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="UploadFile.aspx.cs" Inherits="PerceptumIIP.Views.Admin.UploadFile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <style>

         @media (min-width: 768px) and (max-width: 992px) {
            .row {
                font-size: 30px;
            }
            input[type="text"], input[type="password"], input[type="email"], input[type="tel"], input[type="select"] {
                max-width: 480px;
            }
            .form-control {
                height: 50px;
            }
        }
    </style>


    <h1>Upload Class Materials</h1>

    <div class="row">
   <div class="col-md-3">

            <asp:Label runat="server" CssClass="col-md-5 control-label">Specify Level</asp:Label>
                   <div class="form-group">
 <asp:TextBox runat="server" ID="lblevel" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="lblevel"
                                        CssClass="text-danger" ErrorMessage="The first name field is required." />


        </div>
       </div>

        </div>

                <asp:FileUpload ID="FileUpload1" runat="server" CssClass="btn btn-default" />
    <br />
    <asp:Button ID="Save" runat="server" Text="Save" CssClass="btn btn-default" OnClick="Save_Click" />










</asp:Content>
