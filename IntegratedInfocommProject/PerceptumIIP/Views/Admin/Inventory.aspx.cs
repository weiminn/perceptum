﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PerceptumIIP.Models;


namespace PerceptumIIP.Views.Admin
{
    public partial class Inventory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InventoryGrid.DataBind();
            }

            using (var db = new ApplicationDbContext())
            {
                var products = db.Products.ToList();
                InventoryGrid.DataSource = products;
                InventoryGrid.DataBind();
            }
        }

        protected void InventoryPageChange(object sender, GridViewPageEventArgs e)
        {

            InventoryGrid.PageIndex = e.NewPageIndex;
            InventoryGrid.DataBind();
        }
        protected void InventoryGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Details")
            {
                int index = Convert.ToInt32(e.CommandArgument); //gives row number that is clicked
                GridViewRow row = InventoryGrid.Rows[index];

                var Inventoryid = row.Cells[0].Text;
                Session["ProductId"] = Inventoryid;
                Response.Redirect("InventoryProfile.aspx");

            }

        }
    }
}