﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Parents.aspx.cs" Inherits="PerceptumIIP.Views.Admin.Parents" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


    <style>
        .parentsgridview {
    width:100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color:white;
}


.parentsgridview td {
    padding:10px; 
    border: solid 1px black;
    color:black;
}

    .parentsgridview th {
        background-color:#f3e222;
        text-align:center;
        padding: 10px 2px;
        border: solid 1px black;
        color: black;
    }

.parentsgridview tr:nth-child(even) {
    background-color: #ffffff;
}

.parentsgridview tr:nth-child(odd) {
    background-color: #dcdcdc;
}

.parentsgridview .alt{
    background:#dcdcdc;
}

.parentsgridview .pgr{

    background: #dcdcdc;

}
    .parentsgridview .pgr table {
        margin: 5px 0;
    }

    .parentsgridview .pgr td {
        border-width:0;
        padding: 0 6px;
        border-left: solid 1px black;
    }
    .parentsgridview .pgr a{
        color:black;
        text-decoration:none;
    }
    </style>

    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="ParentId" DataSourceID="SqlDataSource1" Cssclass="parentsgridview" GridLines="None" Font-Size="Medium" Font-Names="Arial" CellPadding="3" CellSpacing="10" AlternatingRowStyle-CssClass="alt" PageSize="7" PagerStyle-CssClass="pgr">
        <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:BoundField DataField="ParentId" HeaderText="ParentId" InsertVisible="False" ReadOnly="True" SortExpression="ParentId" />
            <asp:BoundField DataField="UserName" HeaderText="UserName" SortExpression="UserName" />
            <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName" />
            <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName" />
            <asp:BoundField DataField="Password" HeaderText="Password" SortExpression="Password" />
            <asp:BoundField DataField="Telephone" HeaderText="Telephone" SortExpression="Telephone" />
            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
        </Columns>
        <PagerStyle CssClass="pgr"></PagerStyle>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Parents]"></asp:SqlDataSource>



</asp:Content>



