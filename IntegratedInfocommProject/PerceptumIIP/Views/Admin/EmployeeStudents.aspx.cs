﻿using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PerceptumIIP.Views.Admin
{
    public partial class Studnts : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                StudentGrid.DataBind();
            }

            using (var db = new ApplicationDbContext())
            {
                var students = db.Students.ToList();
                StudentGrid.DataSource = students;
                StudentGrid.DataBind();
            }
        }

        protected void StudentPageChange(object sender, GridViewPageEventArgs e)
        {

            StudentGrid.PageIndex = e.NewPageIndex;
            StudentGrid.DataBind();
        }

        protected void StudentGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if(e.CommandName == "Details")
            {
                int index = Convert.ToInt32(e.CommandArgument); //gives row number that is clicked
                GridViewRow row = StudentGrid.Rows[index];
                var studentid = row.Cells[0].Text;
                Session["StudentId"] = studentid;
                Response.Redirect("StudentProfile.aspx");
            }
        }
    }
}