﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AddInventory.aspx.cs" Inherits="PerceptumIIP.Views.Admin.AddInventory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>

        @media (min-width: 768px) and (max-width: 992px) {
            .row {
                font-size: 20px;
            }
            input[type="text"], input[type="password"], input[type="email"], input[type="tel"], input[type="select"] {
                max-width: 480px;
            }
            .form-control {
                height: 50px;
            }
        }
    </style>
    <h1>Add Inventory</h1>
    <div class="form-group">
        <asp:Label runat="server" AssociatedControlID="Name" CssClass="col-md-2 control-label">Name</asp:Label>
        <div class="col-md-10">
            <asp:TextBox runat="server" ID="Name" CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="Name"
                CssClass="text-danger" ErrorMessage="The name field is required." />
        </div>
    </div>
   <br />
     <div class="form-group">
        <asp:Label runat="server" AssociatedControlID="Details" CssClass="col-md-2 control-label">Details</asp:Label>
        <div class="col-md-10">
            <asp:TextBox runat="server" ID="Details" CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="Details"
                CssClass="text-danger" ErrorMessage="The details field is required." />
        </div>
    </div>
    <br />
    <div class="form-group">
        <asp:Label runat="server" AssociatedControlID="ProductCategory" CssClass="col-md-2 control-label">Product Category</asp:Label>
        <div class="col-md-10">
            <asp:TextBox runat="server" ID="ProductCategory" CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="ProductCategory"
                CssClass="text-danger" ErrorMessage="The product category field is required." />
                   <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^[a-zA-Z]+$" ControlToValidate="ProductCategory" ErrorMessage="Please enter text only"></asp:RegularExpressionValidator>

        </div>
    </div>
    <br />
     <div class="form-group">
        <asp:Label runat="server" AssociatedControlID="SalesItem" CssClass="col-md-2 control-label">Sales Item </asp:Label>
        <div class="col-md-10">
            <asp:TextBox runat="server" ID="SalesItem" CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="SalesItem"
                CssClass="text-danger" ErrorMessage="The salesitem field is required." />
       <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationExpression="^[a-zA-Z]+$" ControlToValidate="SalesItem" ErrorMessage="Please enter text only"></asp:RegularExpressionValidator>

        </div>
    </div>
    <br />
    <div class="form-group">
        <asp:Label runat="server" AssociatedControlID="NormalPrice" CssClass="col-md-2 control-label">Normal Price </asp:Label>
        <div class="col-md-10">
            <asp:TextBox runat="server" ID="NormalPrice" CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="NormalPrice"
                CssClass="text-danger" ErrorMessage="The normal price field is required." />
       <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="\d+" ControlToValidate="NormalPrice" ErrorMessage="Please enter numbers only"></asp:RegularExpressionValidator>

        </div>
    </div> 
    <br />
    <div class="form-group">
        <asp:Label runat="server" AssociatedControlID="PromoPrice" CssClass="col-md-2 control-label">Promotion Price </asp:Label>
        <div class="col-md-10">
            <asp:TextBox runat="server" ID="PromoPrice" CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="PromoPrice"
                CssClass="text-danger" ErrorMessage="The promo price field is required." />
                   <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationExpression="\d+" ControlToValidate="PromoPrice" ErrorMessage="Please enter numbers only"></asp:RegularExpressionValidator>

        </div>
    </div>
    <br />
    <div class="form-group">
        <asp:Label runat="server" AssociatedControlID="QuantityonHand" CssClass="col-md-2 control-label">Quantity On Hand</asp:Label>
        <div class="col-md-10">
            <asp:TextBox runat="server" ID="QuantityOnHand" CssClass="form-control" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="QuantityonHand"
                CssClass="text-danger" ErrorMessage="The quantity field is required." />
            <asp:RangeValidator ID="RangeValidator1" ControlToValidate="QuantityOnHand" Type="Integer" MinimumValue="1" MaximumValue="60" runat="server" ErrorMessage="Only Numeric Allowed, value must be from 1 to 60!"></asp:RangeValidator>
        </div>
    </div>


    <asp:FileUpload ID="FileUploadControl" runat="server" />

    <br />
    <br />

    <asp:Button ID="Sumbit" runat="server" Text="Sumbit" OnClick="Sumbit_Click"/>
</asp:Content>
