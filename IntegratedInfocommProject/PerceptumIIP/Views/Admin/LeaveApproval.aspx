﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="LeaveApproval.aspx.cs" Inherits="PerceptumIIP.Views.Admin.LeaveApproval" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


    <style>
        .leavegridview {
    width:100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color:white;
}


.leavegridview td {
    padding:10px; 
    border: solid 1px black;
    color:black;
}

    .leavegridview th {
        background-color:#f3e222;
        text-align:center;
        padding: 10px 2px;
        border: solid 1px black;
        color: black;
    }

.leavegridview tr:nth-child(even) {
    background-color: #ffffff;
}

.leavegridview tr:nth-child(odd) {
    background-color: #dcdcdc;
}

.leavegridview .alt{
    background:#dcdcdc;
}

.leavegridview .pgr{

    background: #dcdcdc;

}
    .leavegridview .pgr table {
        margin: 5px 0;
    }

    .leavegridview .pgr td {
        border-width:0;
        padding: 0 6px;
        border-left: solid 1px black;
    }
    .leavegridview .pgr a{
        color:black;
        text-decoration:none;
    }
    </style>


    <h2>Requested Leaves</h2>
    <br />
    <asp:GridView ID="leavegrid" runat="server" OnRowCommand="GridView1_RowCommand" OnPageIndexChanging="LeavePageChange" AllowPaging="True" GridLines="None" Font-Size="Medium" Font-Names="Arial" CellPadding="3"  CellSpacing="10"  AlternatingRowStyle-CssClass="alt"  PageSize="7" PagerStyle-CssClass="pgr" 
        CssClass="leavegridview">
        <Columns>
            <asp:ButtonField Text="Approve" CommandName="Approve" />
            <asp:ButtonField Text="Reject" CommandName="Reject" />
        </Columns>
        <PagerStyle CssClass="pgr"></PagerStyle>

    </asp:GridView>
</asp:Content>
