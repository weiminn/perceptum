﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Microsoft.AspNet.Identity;
using PerceptumIIP.Models;
using System.Net.Mail;
using System.Text;

namespace PerceptumIIP.Views.Admin
{
    public partial class UpdateEmployees : System.Web.UI.Page
    {
        public int EmployeeId;
        public string UFlag = "F";


        private static readonly string scriptSuccessUpdate = "<script language=\"javascript\">\n" +
        "alert (\"Update Successful!\");\n </script>";

        private static readonly string scriptPasswordReset = "<script language=\"javascript\">\n" +
        "alert (\"Your Password has been sent to your email!\");\n </script>";

        protected void Page_Load(object sender, EventArgs e)
        {

            

            EmployeeId = Convert.ToInt32(Session["EmployeeId"]);

            lblFirstName.Text = (string)Session["FirstName"];

            lblAdd.Text = (string)Session["Add"];

            lbldepartment.Text = (string)Session["Department"];
            lblsalary.Text = (string)Session["Salary"];
            lbltelephone.Text = (string)Session["Telephone"];
            lblemail.Text = (string)Session["Email"];

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            if (txtFirstName.Text != "")
            {
                String strFName = "FirstName";
                String strFValue = txtFirstName.Text;
                UpdateEmployee(strFName, strFValue);
                Session["FirstName"] = txtFirstName.Text;
            }

            if (txtAdd.Text != "")
            {
                String strFName = "Address";
                String strFValue = txtAdd.Text;
                UpdateEmployee(strFName, strFValue);
                Session["Add"] = txtAdd.Text;
            }

            if (txtdepartment.Text != "")
            {
                String strFName = "Department";
                String strFValue = txtdepartment.Text;
                UpdateEmployee(strFName, strFValue);
                Session["Department"] = txtdepartment.Text;
            }
            if (textsalary.Text != "")
            {
                String strFName = "Salary";
                String strFValue = textsalary.Text;
                UpdateEmployee(strFName, strFValue);
                Session["Salary"] = textsalary.Text;
            }

            if (txttelephone.Text != "")
            {
                String strFName = "Telephone";
                String strFValue = txttelephone.Text;
                UpdateEmployee(strFName, strFValue);
                Session["Telephone"] = txttelephone.Text;
            }

            if (txtemail.Text != "")
            {
                String strFName = "Email";
                String strFValue = txtemail.Text;
                UpdateEmployee(strFName, strFValue);
                Session["Email"] = txtemail.Text;
            }
            if (UFlag == "T")
            {
                Type strType = this.GetType();
                ClientScript.RegisterStartupScript(strType, "Success", scriptSuccessUpdate);

            }

            txtFirstName.Text = "";
            txtAdd.Text = "";
            txtdepartment.Text = "";
            textsalary.Text= "";
            txttelephone.Text = "";
            txtemail.Text = "";


        }


        public void UpdateEmployee(string strFName, string strFValue)

        {
            string strEmployeeId = (string)Session["EmployeeId"];


            SqlConnection con = new SqlConnection(@"Data Source=perceptum.database.windows.net;Initial Catalog=Perceptum V1;Connect Timeout=15;Encrypt=true;MultipleActiveResultSets=True;Persist Security Info=True;Trusted_Connection=False;User ID=Perceptum2018;Password=Pm123456;");

              con.Open();
            SqlCommand cmd;
            Type csType = this.GetType();
            String strSQL = "UPDATE Employees  SET " + strFName +
                " = @newValue WHERE EmployeeId = @EmployeeId";
            cmd = new SqlCommand(strSQL, con);
            cmd.Parameters.Add("@newValue", SqlDbType.Char).Value = strFValue;
            cmd.Parameters.Add("@EmployeeId", SqlDbType.NVarChar).Value = strEmployeeId;
            cmd.ExecuteNonQuery();
            con.Close();

            Type strType = this.GetType();
            ClientScript.RegisterStartupScript(strType, "Success", scriptSuccessUpdate);


        }

        protected void btnResetPassword_Click(object sender, EventArgs e)
        {
            Type strType = this.GetType();

            string password;
            using (var db = new ApplicationDbContext())
            {
                password = db.Employees.SingleOrDefault(x => x.EmployeeId == EmployeeId).Password;
            }

            MailMessage msg = new MailMessage();
            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
            try
            {
                msg.Subject = "Your Password";
                msg.Body = "Your password is " + password;
                msg.From = new MailAddress("perceptumiip@gmail.com");
                msg.To.Add(new MailAddress((string)Session["Email"]));
                msg.IsBodyHtml = true;
                client.Host = "smtp.gmail.com";
                System.Net.NetworkCredential basicauthenticationinfo = new System.Net.NetworkCredential("perceptumiip@gmail.com", "123qweQWE!@#");
                client.Port = int.Parse("587");
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = basicauthenticationinfo;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;


                client.Send(msg);
                ClientScript.RegisterStartupScript(strType, "ResetPassword", scriptPasswordReset);


            }
            catch (Exception ex)
            {

            }
        }
    }
}