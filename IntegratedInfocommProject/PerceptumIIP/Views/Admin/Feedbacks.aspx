﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Feedbacks.aspx.cs" Inherits="PerceptumIIP.Views.Admin.Feedbacks" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <style>
        .feedbackgrid {
    width:100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color:white;
}

        
.feedbackgrid td {
    padding:10px; 
    border: solid 1px black;
    color:black;
}

    .classes th {
        background-color:#f3e222;
        text-align:center;
        padding: 10px 2px;
        border: solid 1px black;
        color: black;
    }

.classes tr:nth-child(even) {
    background-color: #ffffff;
}

.classes tr:nth-child(odd) {
    background-color: #dcdcdc;
}

.classes .alt{
    background:#dcdcdc;
}

.classes .pgr{

    background: #dcdcdc;

}
    .classes .pgr table {
        margin: 5px 0;
    }

    .classes .pgr td {
        border-width:0;
        padding: 0 6px;
        border-left: solid 1px black;
    }
   .classes .pgr a{
        color:black;
        text-decoration:none;
    }
    </style>
    <h2>Feedbacks</h2>

    <asp:GridView ID="fbGrid" runat="server" AutoGenerateColumns="False" DataKeyNames="FeedbackId" DataSourceID="fbDS"
        CssClass="feedbackgrid" OnRowCommand="fbGrid_RowCommand" GridLines="None" Font-Size="Medium" Font-Names="Arial" CellPadding="3" CellSpacing="10" AlternatingRowStyle-CssClass="alt" PageSize="7" PagerStyle-CssClass="pgr">
        
        <Columns>
            <asp:BoundField DataField="FeedbackId" HeaderText="FeedbackId" InsertVisible="False" ReadOnly="True" SortExpression="FeedbackId" />
            <asp:BoundField DataField="ParentId" HeaderText="ParentId" SortExpression="ParentId" />
            <asp:BoundField DataField="StudentId" HeaderText="StudentId" SortExpression="StudentId" />
            <asp:BoundField DataField="Details" HeaderText="Details" SortExpression="Details" />
            <asp:ButtonField CommandName="Approve" Text="Approve"/>
            <asp:ButtonField CommandName="Reject" Text="Reject"/>
        </Columns>
                                    <PagerStyle CssClass="pgr"></PagerStyle>

    </asp:GridView>
    <asp:SqlDataSource ID="fbDS" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT *
FROM Feedbacks
WHERE
Status = 'Pending'"></asp:SqlDataSource>
</asp:Content>
