﻿using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace PerceptumIIP.Views.Admin
{
    public partial class Classes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (var db = new ApplicationDbContext())
            {

                var scienceemployeeclasses = from c in db.Classes
                                             join ec in db.EmployeeClasses on c.ClassId equals ec.ClassId
                                             join em in db.Employees on ec.EmployeeId equals em.EmployeeId
                                             where c.ClassDescription == "Science"
                                             select new
                                             {
                                                 ClassId = c.ClassId,
                                                 ClassDescription = c.ClassDescription,
                                                 Level = c.Level,
                                                 Day = c.Day.ToString(),
                                                 Time = c.Time,
                                                 FirstName = em.FirstName
                                             };

                DataTable sdt = new DataTable();
                sdt.Columns.Add("ClassId");
                sdt.Columns.Add("ClassDescription");
                sdt.Columns.Add("Level");
                sdt.Columns.Add("Day");
                sdt.Columns.Add("Time");
                sdt.Columns.Add("FirstName");

                foreach (var @class in scienceemployeeclasses.ToList())
                {
                    DataRow dr = sdt.NewRow();
                    dr[0] = @class.ClassId;
                    dr[1] = @class.ClassDescription;
                    dr[2] = @class.Level;
                    dr[3] = @class.Day;
                    dr[4] = @class.Time.ToString("hh:mm:tt");
                    dr[5] = @class.FirstName;


                    sdt.Rows.Add(dr);
                }

                sciencegrid.DataSource = sdt;
                sciencegrid.DataBind();

                var mathemployeeclasses = from c in db.Classes
                                          join ec in db.EmployeeClasses on c.ClassId equals ec.ClassId
                                          join em in db.Employees on ec.EmployeeId equals em.EmployeeId

                                          where c.ClassDescription == "Mathematics"
                                          select new
                                          {
                                              ClassId = c.ClassId,
                                              ClassDescription = c.ClassDescription,
                                              Level = c.Level,
                                              Day = c.Day.ToString(),
                                              Time = c.Time,
                                              FirstName = em.FirstName
                                          };

                DataTable mdt = new DataTable();
                mdt.Columns.Add("ClassId");
                mdt.Columns.Add("ClassDescription");
                mdt.Columns.Add("Level");
                mdt.Columns.Add("Day");
                mdt.Columns.Add("Time");
                mdt.Columns.Add("FirstName");

                foreach (var @class in mathemployeeclasses.ToList())
                {
                    DataRow dr = mdt.NewRow();
                    dr[0] = @class.ClassId;
                    dr[1] = @class.ClassDescription;
                    dr[2] = @class.Level;
                    dr[3] = @class.Day;
                    dr[4] = @class.Time.ToString("hh:mm:tt");
                    dr[5] = @class.FirstName;

                    mdt.Rows.Add(dr);
                }

                mathgrid.DataSource = mdt;
                mathgrid.DataBind();
            }

        }
        protected void mathGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = mathgrid.Rows[index];
            int cId = Convert.ToInt32(row.Cells[0].Text);

            Session["cId"] = Convert.ToInt32(cId);
            Response.Redirect("ClassesFeedback.aspx");
        }
        protected void sciencegrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = sciencegrid.Rows[index];
            int cId = Convert.ToInt32(row.Cells[0].Text);

            Session["cId"] = Convert.ToInt32(cId);
            Response.Redirect("ClassesFeedback.aspx");
        }
    }
}