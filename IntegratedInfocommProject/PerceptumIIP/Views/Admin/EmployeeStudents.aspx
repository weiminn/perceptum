﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="EmployeeStudents.aspx.cs" Inherits="PerceptumIIP.Views.Admin.Studnts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="stylesheet" href="/CSS/master.css" type="text/css" />

    <h1>Student Particulars</h1>

    <tr>
    <td style="padding: 10px">
    <asp:GridView ID="StudentGrid" runat="server" AutoGenerateColumns="False" DataKeyNames="StudentId" OnPageIndexChanging="StudentPageChange" AllowPaging="true" Height="237px" Width="100%" Cssclass="studentgridview" GridLines="None" Font-Size="Medium" Font-Names="Arial" CellPadding="3" CellSpacing="10" AlternatingRowStyle-CssClass="alt" PageSize="7" PagerStyle-CssClass="pgr"  OnRowCommand="StudentGrid_RowCommand">
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:BoundField DataField="StudentId" HeaderText="StudentId" SortExpression="StudentId" InsertVisible="False" ReadOnly="True" />
            <asp:BoundField DataField="FirstName" HeaderText="Name" SortExpression="FirstName" />
            <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
            <asp:BoundField DataField="BirthDate" HeaderText="BirthDate" SortExpression="BirthDate" DataFormatString="{0:D}" />
            <asp:BoundField DataField="School" HeaderText="School" SortExpression="School" />
            <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level" />
            <asp:BoundField DataField="Telephone" HeaderText="Telephone" SortExpression="Telephone" />
            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
            <asp:BoundField DataField="Outlet" HeaderText="Outlet" SortExpression="Outlet" />
            <asp:ButtonField Text="Details" CommandName="Details"/>
        </Columns>

<PagerStyle CssClass="pgr"></PagerStyle>
    </asp:GridView>
        </td>

        </tr>
    <%--<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Students]"></asp:SqlDataSource>--%>




</asp:Content>
