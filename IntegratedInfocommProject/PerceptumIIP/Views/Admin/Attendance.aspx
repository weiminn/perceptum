﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Attendance.aspx.cs" Inherits="PerceptumIIP.Views.Admin.Attendance" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">







   Class: <asp:Label ID="lblClass" runat="server" Text=""></asp:Label>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="StudentId,StudentId1,ClassId" DataSourceID="SqlDataSource1" OnRowCommand="GridView1_RowCommand">
        <Columns>
            <asp:BoundField DataField="StudentId" HeaderText="StudentId" InsertVisible="False" ReadOnly="True" SortExpression="StudentId" />
            <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName" />
            <asp:ButtonField Text="Present" CommandName="Present" />
            <asp:ButtonField Text="Absent" CommandName="Absent" />

        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand=" SELECT * FROM Students S
        INNER JOIN StudentClasses SC
        ON S.StudentId = SC.StudentId
        WHERE SC.ClassId = @ClassId">
        <SelectParameters>
            <asp:SessionParameter Name="ClassId" SessionField="ClassId" />
        </SelectParameters>
    </asp:SqlDataSource>


<asp:SqlDataSource ID="SqlDataSource2" runat="server"></asp:SqlDataSource>







</asp:Content>
