﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using PerceptumIIP.Models;



namespace PerceptumIIP.Views.Admin
{
    public partial class InventoryProfile : System.Web.UI.Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {
            int productid = Convert.ToInt32(Session["ProductId"]);

        }
        private static readonly string scriptSuccessDelete = "<script language=\"javascript\">\n" +
"alert (\"Delete Successful!\");\n </script>";

        protected void Update_Click(object sender, EventArgs e)
        {
            Session["ProductId"] = GridView2.Rows[0].Cells[0].Text;
            Session["Name"] = GridView2.Rows[0].Cells[1].Text;
            Session["Details"] = GridView2.Rows[0].Cells[2].Text;
            Session["SalesItem"] = GridView2.Rows[0].Cells[3].Text;
            Session["ProductCategory"] = GridView2.Rows[0].Cells[4].Text;
            Session["NormalPrice"] = GridView2.Rows[0].Cells[5].Text;
            Session["PromoPrice"] = GridView2.Rows[0].Cells[6].Text;
            Session["QuantityOnHand"] = GridView2.Rows[0].Cells[7].Text;

            Response.Redirect("Edit Inventory.aspx");
        }
    
        protected void Delete_Click(object sender, EventArgs e)
        {
            int productid = Convert.ToInt32(Session["ProductId"]);
            Type csType = this.GetType();

            using (var db = new ApplicationDbContext())
            {
                var theProduct = db.Products.SingleOrDefault(x => x.ProductId == productid);
                db.Products.Remove(theProduct);
                db.SaveChanges();


            }
            ClientScript.RegisterStartupScript(csType, "Delete", scriptSuccessDelete);

            Response.Redirect("Inventory.aspx");
            }
        }
    }

