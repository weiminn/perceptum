﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="ControlClasses.aspx.cs" Inherits="PerceptumIIP.Views.Admin.ControlClasses" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>

         @media (min-width: 768px) and (max-width: 992px) {
            .row {
                font-size: 20px;
            }
            input[type="text"], input[type="password"], input[type="email"], input[type="tel"], input[type="select"] {
                max-width: 480px;
            }
            .form-control {
                height: 50px;
            }
        }
    </style>
    <h1>New Class</h1>
    <br />
    <br />
    <div class="form-group">
        <asp:Label CssClass="col-md-2 col-form-label" style="font:Arial" runat="server" AssociatedControlID="ClassDescription">Class Description</asp:Label>
        <div class="col-md-10">
            <asp:TextBox runat="server" ID="ClassDescription" CssClass="form-control"/>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ClassDescription" CssClass="text-danger" Display="Dynamic" ErrorMessage="Class Description field is empty"></asp:RequiredFieldValidator>
        </div>
    </div>
    <br />
    <br />


     <div class="form-group">
        <asp:Label runat="server" AssociatedControlID="Day" CssClass="col-md-2 control-label">Day</asp:Label>
        <div class="col-md-10">
            <asp:TextBox runat="server" ID="Day" CssClass="form-control" />
   <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Day" ErrorMessage="Day field is empty"></asp:RequiredFieldValidator>
         <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="\d+" CssClass="text-danger" Display="Dynamic" ControlToValidate ="Day" ErrorMessage="Please enter numbers only"></asp:RegularExpressionValidator>

        </div>
    </div>
    <br />
    <br />


    <div class="form-group">
        <asp:Label runat="server" AssociatedControlID="Time" CssClass="col-md-2 control-label">Time</asp:Label>
        <div class="col-md-10">
            <asp:TextBox runat="server" ID="Time" CssClass="form-control" />
               <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="Time" CssClass="text-danger" Display="Dynamic" ValidationExpression="^(20|21|22|23|[01]\d|\d)(([:][0-5]\d){1,2})$" ErrorMessage="Please enter time format in 24H"></asp:RequiredFieldValidator>

        </div>
    </div>
    <br />
    <br />

    <div class="form-group">
        <asp:Label runat="server" AssociatedControlID="DDLevel" CssClass="col-md-2 control-label">Level</asp:Label>
        <div class="col-md-3">
            <asp:DropDownList
                ID="DDLevel" runat="server" CssClass="form-control">
                <asp:ListItem Value="" Selected="True"></asp:ListItem>

                <asp:ListItem Value="Primary 3">Primary 3</asp:ListItem>
                <asp:ListItem Value="Primary 4">Primary 4</asp:ListItem>
                <asp:ListItem Value="Primary 5">Primary 5</asp:ListItem>
                <asp:ListItem Value="Primary 6">Primary 6</asp:ListItem>
                <asp:ListItem Value="Secondary 1">Secondary 1</asp:ListItem>
                <asp:ListItem Value="Secondary 2">Secondary 2</asp:ListItem>
                <asp:ListItem Value="Secondary 3">Secondary 3</asp:ListItem>
                <asp:ListItem Value="Secondary 4">Secondary 4</asp:ListItem>

            </asp:DropDownList>
           
            <asp:RequiredFieldValidator runat="server" ControlToValidate="DDLevel" CssClass="text-danger" Display="Dynamic" ErrorMessage="The level field is required">
            </asp:RequiredFieldValidator>
        </div>
        
        
     </div>

    <br />
    <br />


    <br />
    <br />

    

    <div class="form-group">
        <asp:Button ID="submit" runat="server" Text="Submit" CssClass="btn btn-default" OnClick="submit_Click" />
    </div>

</asp:Content>
