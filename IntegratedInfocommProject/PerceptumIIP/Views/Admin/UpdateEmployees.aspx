﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="UpdateEmployees.aspx.cs" Inherits="PerceptumIIP.Views.Admin.UpdateEmployees" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
      <div class="row">
        <div class="col-md-3">
<h2>Data Item</h2>

        </div>
        <div class="col-md-3">
<h2>Existing Data</h2>
        </div>
        <div class="col-md-3">
<h2>New Data  </h2>      

        </div>
    </div>
    
    <div class="form-horziontal row">
        <div class="col-md-3">
            <h4>First Name</h4>
        </div>
        <div class="col-md-3">
  <h4><asp:Label ID="lblFirstName" runat="server"></asp:Label> </h4>

        </div>
        <div class="col-md-3">
       <asp:TextBox ID="txtFirstName" runat="server" Width="250px"></asp:TextBox>

        </div>
      <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" CssClass="text-danger" Display="Dynamic" ValidationExpression="^[a-zA-Z]+$" ControlToValidate="txtFirstName" ErrorMessage="Please enter text only"></asp:RegularExpressionValidator>

    </div>

    
    
    <div class="form-horziontal row">
        <div class="col-md-3">
            <h4>Department</h4>
        </div>
        <div class="col-md-3">
                         <h4>   <asp:Label ID="lbldepartment" runat="server"></asp:Label></h4>


        </div>
        <div class="col-md-3">
                            <asp:TextBox ID="txtdepartment" runat="server" Width="250px"></asp:TextBox>

        </div>
              <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" CssClass="text-danger" Display="Dynamic" ValidationExpression="^[a-zA-Z]+$" ControlToValidate="txtDepartment" ErrorMessage="Please enter text only"></asp:RegularExpressionValidator>

    </div>

    <div class="form-horziontal row">
        <div class="col-md-3">
           <h4>Email</h4>
        </div>
        <div class="col-md-3">
                          <h4>  <asp:Label ID="lblemail" runat="server"></asp:Label></h4>

        </div>
        <div class="col-md-3">
                            <asp:TextBox ID="txtemail" runat="server" Width="250px"></asp:TextBox>

        </div>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" validationexpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"  ControlToValidate="txtemail" ErrorMessage="Email in wrong format!"></asp:RegularExpressionValidator>

    </div>
    
    
    <div class="form-horziontal row">
        <div class="col-md-3">
           <h4> Address </h4>
        </div>
        <div class="col-md-3">
                         <h4>    <asp:Label ID="lblAdd" runat="server"></asp:Label></h4>

        </div>
        <div class="col-md-3">
                            <asp:TextBox ID="txtAdd" runat="server" Width="250px"></asp:TextBox>

        </div>


    </div>

        
    <div class="form-horziontal row">
        <div class="col-md-3">
          <h4> Telephone </h4>
        </div>
        <div class="col-md-3">
                            <h4>  <asp:Label ID="lbltelephone" runat="server"></asp:Label></h4>

        </div>
        <div class="col-md-3">
                            <asp:TextBox ID="txttelephone" runat="server" Width="250px"></asp:TextBox>

        </div>
               <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="\d+" CssClass="text-danger" Display="Dynamic" ControlToValidate="txttelephone" ErrorMessage="Please enter numbers"></asp:RegularExpressionValidator>
               <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationExpression=".{8,}$" CssClass="text-danger" Display="Dynamic" ControlToValidate="txttelephone" ErrorMessage="Please enter 8 characters"></asp:RegularExpressionValidator>

    </div>

     <div class="form-horziontal row">
        <div class="col-md-3">
           <h4> Salary</h4>
        </div>
        <div class="col-md-3">
                            <h4><asp:Label ID="lblsalary" runat="server"></asp:Label></h4>

        </div>
        <div class="col-md-3">
                            <asp:TextBox ID="textsalary" runat="server" Width="250px"></asp:TextBox>

        </div>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ValidationExpression="\d+" CssClass="text-danger" Display="Dynamic" ControlToValidate="txttelephone" ErrorMessage="Please enter numbers"></asp:RegularExpressionValidator>

    </div>

   
                <div class="col-md-9 col-md-offset-7">
                <asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" CssClass="btn btn-default" Text="Update" />
                <asp:Button runat="server" ID="ResetPassword" OnClick="btnResetPassword_Click" CssClass="btn btn-default" Text="Reset Password" />
                    </div>
</asp:Content>
