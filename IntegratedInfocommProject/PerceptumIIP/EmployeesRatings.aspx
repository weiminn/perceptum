﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmployeesRatings.aspx.cs" Inherits="PerceptumIIP.EmployeesRatings" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>


        .starRating{
            width:100%;
            margin-right:20px;
        }
    </style>
    
    <div class="container-fluid" style="margin: 0; padding: 0;">
      <div class="row">
        <div class="col-sm-12" style="padding: 0;">
          <div id="my-slider" class="carousel slide" data-ride="carousel">

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">

              <div class="item active">
               <img src="Content/images/slider2.png" />
               <div class="carousel-caption"><h1>Employees</h1></div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="pagegrid">

      <%--<div class="topgrid">

          <div class="profile1">

          <div class="profilespacer"></div>
  
          <div class="profileimg"><img class="roundimg" src="Content/images/10.jpg" /></div>
  
          <div class="profiledetails">
            <div style="font-size: 18px;"></div>
            <div style="font-size: 15px;">- Customer</div>
            <div>Star Rating</div>
          </div>
  
          <div class="testtitle">
          Kind-hearted teacher...!
          </div>
  
          <div class="testcontent">
              <img style="width: 30px; margin: -20px 10px 0 0;" src="Content/images/quote-left.png" />
              Valerie really enjoys her lessons at Perceptum Education! She always shares with me that her teachers are cheerful and kind-hearted, and shower her with lots of encouragement. In addition to the nurturing environment, I am also extremely satisfied with the curriculum here.
              <img style="width: 30px; margin: 0 0 -10px 10px;" src="Content/images/right-quotation-mark.png" />
          </div>
        </div>

          <div class="profile1">

          <div class="profilespacer" style="background: #ff0042;"></div>
  
          <div class="profileimg"><img class="roundimg" src="Content/images/10.jpg" /></div>
  
          <div class="profiledetails">
            <div style="font-size: 18px;">Jane Koh</div>
            <div style="font-size: 15px;">- Customer</div>
                          <div>Star Rating</div>

          </div>

          

          <div class="testtitle">
          Kind-hearted teacher...!
          </div>
  
          <div class="testcontent">
              <img style="width: 30px; margin: -20px 10px 0 0;" src="Content/images/quote-leftp.png" />
              Valerie really enjoys her lessons at Perceptum Education! She always shares with me that her teachers are cheerful and kind-hearted, and shower her with lots of encouragement. In addition to the nurturing environment, I am also extremely satisfied with the curriculum here.
              <<img style="width: 30px; margin: 0 0 -10px 10px;" src="Content/images/right-quotation-markp.png" />
          </div>
        </div>

        <div class="profile2">

          <div class="profilespacer" style="background: #ec3466;"></div>
  
          <div class="profileimg"><img class="roundimg" src="Content/images/11.jpg" /></div>
  
          <div class="profiledetails">
            <div style="font-size: 18px;">Rebecca Tan</div>
            <div style="font-size: 15px;">- Customer</div>
                          <div>Star Rating</div>

          </div>
  
          <div class="testtitle">
          Impressive curriculum...!
          </div>
  
          <div class="testcontent">
              <img style="width: 30px; margin: -20px 10px 0 0;" src="Content/images/quote-leftp.png" />
              We have chosen Perceptum as we find that you have brilliantly managed to design an impressive curriculum and hire a fantastic team of teachers to deliver it. Your curriculum goes beyond the normal school curriculum, and many of your teachers have experience beyond the education sector, which adds an interesting flavour to the classroom.
              <<img style="width: 30px; margin: 0 0 -10px 10px;" src="Content/images/right-quotation-markp.png" />
          </div>
        </div>

      </div>

          <div class="profile3">

          <div class="profilespacer"></div>
  
          <div class="profileimg"><img class="roundimg" src="Content/images/y_u_no_photo_Square.png" /></div>
  
          <div class="profiledetails">
            <div style="font-size: 18px;">Mrs Chan</div>
            <div style="font-size: 15px;">- Customer</div>
            <div>Star Rating</div>
          </div>
  
          <div class="testtitle">
          Tremendous improvements...!
          </div>
  
          <div class="testcontent">
              <img style="width: 30px; margin: -20px 10px 0 0;" src="Content/images/quote-left.png" />
              Dear Mr Lee, words cannot express our appreciation of your efforts in tutoring our son, Peng Yu, in his PSLE Maths. Under your tutelage, he has shown tremendous improvements and has grown in confidence in tracking Math problems. He has gone on to achieve an A* in his PSLE Maths and has gained entry to Hwa Chong.
              <img style="width: 30px; margin: 0 0 -10px 10px;" src="Content/images/right-quotation-mark.png" />
          </div>
        </div>--%>
  
      <div class="bottomgrid">

        <div class="profile3">

          <div class="profilespacer" style="background: #ff0042;"></div>
  
          <div class="profileimg"><img class="roundimg" src="Content/images/14.jpg" /></div>
  
          <div class="profiledetails">
            <div style="font-size: 18px;">Andrew Hoang</div>
            <div>
                <ajaxToolkit:Rating ID="Rating_1" runat="server" ReadOnly="true"
                StarCssClass="starRating" EmptyStarCssClass="starempty" FilledStarCssClass="starfilled" WaitingStarCssClass="starwaiting" ></ajaxToolkit:Rating>

            </div>

          </div>
  
          <div class="testtitle">
          Quirky
          </div>
  
          <div class="testcontent">
              <img style="width: 30px; margin: -20px 10px 0 0;" src="Content/images/quote-leftp.png" />
              Andrew has been teaching at Perceptum Education for over a year. He loves mathematics and wants to pass on his passion to all of his students. Having graduated from National University of Singapore with a Bachelor's in Science, he is well-equipped with the knowledge and skills to share his passion with his students.
              <img style="width: 30px; margin: 0 0 -10px 10px;" src="Content/images/right-quotation-markp.png" />
          </div>
        </div>

        <div class="profile4">

          <div class="profilespacer" style="background: #ec3466;"></div>
  
          <div class="profileimg"><img class="roundimg" src="Content/images/13.jpg" /></div>
  
          <div class="profiledetails">
            <div style="font-size: 18px;">Adeline Lim</div>
              <div>
                <ajaxToolkit:Rating ID="Rating1" runat="server" ReadOnly="true"
                StarCssClass="starRating" EmptyStarCssClass="starempty" FilledStarCssClass="starfilled" WaitingStarCssClass="starwaiting" ></ajaxToolkit:Rating>

            </div>
          </div>
  
          <div class="testtitle">
          Scientific 
          </div>
  
          <div class="testcontent">
              <img style="width: 30px; margin: -20px 10px 0 0;" src="Content/images/quote-leftp.png" />
              Adeline is passionate about teaching. She is patient with her students and would go the extra mile to help her students out of her lesson hours. Her belief is that she needs to learn about her students in order to teach effectively. With a Bachelor's degree in Science, she is well versed in learning about the different concepts and applying them to solve problems.
              <img style="width: 30px; margin: 0 0 -10px 10px;" src="Content/images/right-quotation-markp.png" />
          </div>
        </div>

          <div class="profile5">

          <div class="profilespacer" style="background: #ed4d01;"></div>
  
          <div class="profileimg"><img class="roundimg" src="Content/images/14.jpg" /></div>
  
          <div class="profiledetails">
            <div style="font-size: 18px;">Jammy Tan</div>
              <div>
                <ajaxToolkit:Rating ID="Rating2" runat="server" ReadOnly="true"
                StarCssClass="starRating" EmptyStarCssClass="starempty" FilledStarCssClass="starfilled" WaitingStarCssClass="starwaiting" ></ajaxToolkit:Rating>

            </div>
          </div>
  
          <div class="testtitle">
          Adventurous
          </div>
  
          <div class="testcontent">
              <img style="width: 30px; margin: -20px 10px 0 0;" src="Content/images/quote-lefto.png" />
              Jammy is not your everyday school teacher. An anaylst by day and educator by night, Jammy appearecitated that a good teacher must alos be a universal learner. In engaging his students, he believes that within every individual lies the potential for greatness, and that it is his mission to help them realise that. Having a Bachelor's in Mechanical Engineering, Jammy has been facilitating student learning for the past 3 years. He specalises in Mathematics at Primary School.
              <img style="width: 30px; margin: 0 0 -10px 10px;" src="Content/images/right-quotation-marko.png" />
          </div>
        </div>


        <div class="profile6">

          <div class="profilespacer" style="background: #00aac4;"></div>
  
          <div class="profileimg"><img class="roundimg" src="Content/images/15.jpg" /></div>
  
          <div class="profiledetails">
            <div style="font-size: 18px;">Leong Wen Ting</div>
              <div>
                <ajaxToolkit:Rating ID="Rating3" runat="server" ReadOnly="true"
                StarCssClass="starRating" EmptyStarCssClass="starempty" FilledStarCssClass="starfilled" WaitingStarCssClass="starwaiting" ></ajaxToolkit:Rating>

            </div>
          </div>
  
          <div class="testtitle">
          Caring
          </div>
  
          <div class="testcontent">
              <img style="width: 30px; margin: -20px 10px 0 0;" src="Content/images/quote-leftb.png" />
              Wen Ting is caring and approachable. It has always been her wish to shape the minds of children and to help realise their full potential. Nurturing them and watching them succeed in life fills her with a great sense of satisfaction. She believes that Science will open her students' minds to new ideas and allow them to reach beyond the stars, beyond tomorrow.
              <img style="width: 30px; margin: 0 0 -10px 10px;" src="Content/images/right-quotation-markb.png" />
          </div>
        </div>

        <div class="profile7">

          <div class="profilespacer" style="background: #ec3466;"></div>
  
          <div class="profileimg"><img class="roundimg" src="Content/images/16.jpg" /></div>
  
          <div class="profiledetails">
            <div style="font-size: 18px;">Jasline Ho</div>
              <div>
                <ajaxToolkit:Rating ID="Rating4" runat="server" ReadOnly="true"
                StarCssClass="starRating" EmptyStarCssClass="starempty" FilledStarCssClass="starfilled" WaitingStarCssClass="starwaiting" ></ajaxToolkit:Rating>

            </div>
          </div>
  
          <div class="testtitle">
          Firm
          </div>
  
          <div class="testcontent">
              <img style="width: 30px; margin: -20px 10px 0 0;" src="Content/images/quote-leftp.png" />
              Jasline has been teaching Mathematics at Perceptum for over 2 years. Throughout her teaching experience, she has become a firm believer that all students have the potential to perform well in Maths. The key is confidence, and she seeks to build that confidence in her students by showing the casual relation of efforts and results.
              <img style="width: 30px; margin: 0 0 -10px 10px;" src="Content/images/right-quotation-markp.png" />
          </div>
        </div>

        <div class="profile8">

          <div class="profilespacer"></div>
  
          <div class="profileimg"><img class="roundimg" src="Content/images/17.jpg" /></div>
  
          <div class="profiledetails">
            <div style="font-size: 18px;">Daniel Soh</div>
              <div>
                <ajaxToolkit:Rating ID="Rating5" runat="server" ReadOnly="true"
                StarCssClass="starRating" EmptyStarCssClass="starempty" FilledStarCssClass="starfilled" WaitingStarCssClass="starwaiting" ></ajaxToolkit:Rating>

            </div>
          </div>
  
          <div class="testtitle">
          Advocator 
          </div>
  
          <div class="testcontent">
              <img style="width: 30px; margin: -20px 10px 0 0;" src="Content/images/quote-left.png" />
              Daniel has been teaching at Perceptum for more than 5 years. He is a huge advocator of suffering builds character. He enjoys challenge and seeks out new ways to solve the same questions. He guides his students to see that there are many different ways to tackle a problem, and ignites them to think of the possibilities of new discoveries of their own.
              <img style="width: 30px; margin: 0 0 -10px 10px;" src="Content/images/right-quotation-mark.png" />
          </div>
        </div>

      </div>
  
    </div>

<style>

.starRating {
            width:50px;
            height:50px;
            cursor: pointer;
            background-repeat:no-repeat;
            display:block;
        }

.starempty
        {
            background-image:url(../../../Content/images/starempty.png);
            width: 50px;
            height: 50px;
        }
.starfilled
        {
            background-image: url(../../../Content/images/starfilled.png);
            width: 50px;
            height: 50px;
        }
.starwaiting
        {
            background-image: url(../../../Content/images/starwaiting.png);
            width: 50px;
            height: 50px;
        }

.pagegrid{
  display: grid;
  grid-template-columns: 1fr;
  /*background: pink;*/
  margin: 5% 10%;
  padding: 5px;
}
.topgrid{
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: 50px;
  padding-bottom: 50px;
  /*grid-column-gap: 5px;
  background: green;*/
}
.bottomgrid{
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-column-gap: 50px;
  grid-row-gap: 50px;
  /*background: purple;*/
}
.profile1{
  grid-column: 1/2;
  display: grid;
  grid-template-columns: 1fr;
  /*justify-items: center;*/
  text-align: center;
  background: #fff;
  font-family: Arial;
  margin: 0;
  padding: 0;
  border: 5px solid #eaeaea;
}
.profile2{
  grid-column: 2/3;
  display: grid;
  grid-template-columns: 1fr;
  /*justify-items: center;*/
  text-align: center;
  background: #fff;
  font-family: Arial;
  margin: 0;
  padding: 0;
  border: 5px solid #eaeaea;
}
.profile3{
  grid-column: 1/2;
  display: grid;
  grid-template-columns: 1fr;
  /*justify-items: center;*/
  text-align: center;
  background: #fff;
  font-family: Arial;
  margin: 0;
  padding: 0;
  border: 5px solid #eaeaea;
}
.profile4{
  grid-column: 2/3;
  display: grid;
  grid-template-columns: 1fr;
  /*justify-items: center;*/
  text-align: center;
  background: #fff;
  font-family: Arial;
  margin: 0;
  padding: 0;
  border: 5px solid #eaeaea;
}
.profile5{
  grid-column: 3/4;
  display: grid;
  grid-template-columns: 1fr;
  /*justify-items: center;*/
  text-align: center;
  background: #fff;
  font-family: Arial;
  margin: 0;
  padding: 0;
  border: 5px solid #eaeaea;
}
.profile6{
  grid-column: 1/2;
  display: grid;
  grid-template-columns: 1fr;
  /*justify-items: center;*/
  text-align: center;
  background: #fff;
  font-family: Arial;
  margin: 0;
  padding: 0;
  border: 5px solid #eaeaea;
}
.profile7{
  grid-column: 2/3;
  display: grid;
  grid-template-columns: 1fr;
  /*justify-items: center;*/
  text-align: center;
  background: #fff;
  font-family: Arial;
  margin: 0;
  padding: 0;
  border: 5px solid #eaeaea;
}
.profile8{
  grid-column: 3/4;
  display: grid;
  grid-template-columns: 1fr;
  /*justify-items: center;*/
  text-align: center;
  background: #fff;
  font-family: Arial;
  margin: 0;
  padding: 0;
  border: 5px solid #eaeaea;
}
.profilespacer{
  height: 80px;
  background: #ff0042;
}
.profileimg{
  background: #fff;
  height: 50px;
  /*justify-self: start;*/
}
.roundimg{
    width: 100px;
    position: relative;
    bottom: 50px;
    border-radius: 50%;
    border-style: solid;
    border-color: #fff;
    border-width: 5px;
}
.profiledetails{
  display: grid;
  grid-template-columns: 1fr;
  background: #fff;
  padding: 5px 0 5px 0;
}
.testtitle{
  background: #fff;
  font-weight: bold;
  font-size: 17px;
  padding: 5px 0 5px 0;
  border-top-style: solid;
  border-color: #eaeaea;
}
.testcontent{
  background: #fff;
  padding: 5px 15px 15px 15px;
}
</style>
</asp:Content>
