﻿using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace PerceptumIIP
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Database.SetInitializer(new NullDatabaseInitializer<ApplicationDbContext>());
        }

        void Session_Start(object sender, EventArgs e)
        {

            HttpRequest _httprequest = HttpContext.Current.Request;
            if (_httprequest.Browser.IsMobileDevice)
            {

                string path = _httprequest.Url.PathAndQuery;
                bool _isMobileDevice = path.StartsWith("/Mobile", StringComparison.OrdinalIgnoreCase);
                if (!_isMobileDevice)
                {

                    string _redirectTo = "/Mobile";
                    HttpContext.Current.Response.Redirect(_redirectTo);

                }
            }

        }
    }
}