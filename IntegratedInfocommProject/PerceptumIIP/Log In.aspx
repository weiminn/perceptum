﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Log In.aspx.cs" Inherits="PerceptumIIP.Log_In" %>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
  
    <style>

        .login{
            justify-content:center;

        }
      
        .col-md-2{
            height:25px;
        }
      .col-md-6{
          width:0%;
      }
        .form-horziontal row{
            height:30px;
        }
        .col-md-offset-5{
            margin-left:25%;
        }
        .btn-sm{
            max-width:200%;
            padding: 10px 20px;
        }
    </style>

        <section id="loginForm">
        
                    <asp:PlaceHolder runat="server" ID="ErrorMessage" Visible="false">
                        <p class="text-danger">
                            <asp:Literal runat="server" ID="FailureText" />
                        </p>
                    </asp:PlaceHolder>

               
            <div class="form-horziontal row">
                    <div class="form-group">
                        <div class="col-md-2 col-md-offset-3">
                        <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Email</asp:Label>
                            </div>
                        <div class="col-md-3">
                            <asp:TextBox runat="server" ID="Email" CssClass="form-control" TextMode="Email" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="Email" CssClass="text-danger"
                                 ErrorMessage="The email field is required." />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" validationexpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"  ControlToValidate="Email" ErrorMessage="Email in wrong format!"></asp:RegularExpressionValidator>
                                       </div>
                        </div>
                </div>
                <div class="form-horziontal row">
                    <div class="form-group">
                        <div class="col-md-2 col-md-offset-3">
                        <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-2 control-label">Password</asp:Label>
                            </div>
                        <div class="col-md-3">
                            <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="Password" CssClass="text-danger" ErrorMessage="The password field is required." />

                        </div>

                    </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-offset-7 col-md-4">
                            <asp:Button runat="server" OnClick="LogIn" Text="Log in" CssClass="btn btn-default btn-block" Width="52px" />
                        </div>
                    </div>
               
                                              </section>
    
          

    <%--<div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <div class="checkbox">
                                <asp:CheckBox runat="server" ID="RememberMe" />
                                <asp:Label runat="server" AssociatedControlID="RememberMe">Remember me?</asp:Label>
                            </div>
                        </div>
                    </div>--%>
</asp:Content>