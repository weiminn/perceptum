﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Classes.aspx.cs" Inherits="PerceptumIIP.Classes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <asp:DataList ID="DataList1" runat="server" DataKeyField="ClassId" Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" RepeatColumns="3" CellSpacing="10" style="padding-bottom:10px; margin:auto; clear:both">
                <ItemTemplate>
                    <asp:HyperLink runat="server" NavigateUrl='<%# Eval("pProdID", "~/ProductDetails.aspx?pProdID={0}") %>' Text="">
                    <asp:Image ID="ProductIMG" runat="server" ImageUrl='<%# Eval("pPic1") %>' style="height:300px;width:300px;" CssClass="img-responsive"></asp:Image>  
                        </asp:HyperLink>              
                    <br />
                    Name:
                    <asp:Label ID="pProdNameLabel" runat="server" Text='<%# Eval("pProdName") %>' />
                    <br />

                    Type:
                    <asp:Label ID="pProdTypeLabel" runat="server" Text='<%# Eval("pProdType") %>' />
                    <br />
                    Price:
                    <asp:Label ID="pProdPriceLabel" runat="server" Text='<%# Eval("pProdPrice","{0:C}") %>' />
                    <br />
                </ItemTemplate>
            </asp:DataList>





    <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>





</asp:Content>
