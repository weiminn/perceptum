﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductDetails.aspx.cs" Inherits="PerceptumIIP.ProductDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <br />
    <br />
    <br />

    <table id="DetailsTable" class="auto-style3" style="border-style: none; padding: 2px; margin: 1px; text-align: center">
        <tr>
            <td class="auto-style2" style="text-align: center; border-style: none; padding: 5px; margin: 1px; height: 50px;">Product Details
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Products] WHERE ProductId=@ProductId">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="ProductId" QueryStringField="ProductId" />
                    </SelectParameters>

                </asp:SqlDataSource>
                
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" BorderColor="#B4EAE9" CssClass="table-responsive col-md-4">
        <Columns>
            <asp:TemplateField>
                <HeaderTemplate>Image</HeaderTemplate>
                <ItemTemplate>
                    <img src='data:image/jpg;base64,<%# Eval("Image") != System.DBNull.Value ? Convert.ToBase64String((byte[])Eval("Image")) : string.Empty %>' alt="image" height="200" width="200"/>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
                <asp:GridView ID="ProductsGridView" runat="server" AutoGenerateColumns="False" DataKeyNames="ProductId" DataSourceID="SqlDataSource1">
                    <Columns>
                        <asp:BoundField DataField="ProductId" HeaderText="ProductId" InsertVisible="False" ReadOnly="True" SortExpression="ProductId" />
                        <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                        <asp:BoundField DataField="Details" HeaderText="Details" SortExpression="Details" />
                        <asp:BoundField DataField="Salesitem" HeaderText="Salesitem" SortExpression="Salesitem" />
                        <asp:BoundField DataField="ProductCategory" HeaderText="ProductCategory" SortExpression="ProductCategory" />
                        <asp:BoundField DataField="NormalPrice" HeaderText="NormalPrice" SortExpression="NormalPrice" />
                        <asp:BoundField DataField="PromoPrice" HeaderText="PromoPrice" SortExpression="PromoPrice" />
                        <asp:BoundField DataField="QuantityOnHand" HeaderText="QuantityOnHand" SortExpression="QuantityOnHand" />
                    </Columns>
                </asp:GridView>

                <asp:Label ID="qtylabel" runat="server" Text="Please Select Quantity"></asp:Label>
                <asp:DropDownList ID="ddlQty" runat="server">
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                            <asp:ListItem>5</asp:ListItem>
                            <asp:ListItem>6</asp:ListItem>
                            <asp:ListItem>7</asp:ListItem>
                            <asp:ListItem>8</asp:ListItem>
                            <asp:ListItem>9</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                    </asp:DropDownList>
    <asp:Label ID="lblShoppingCart" runat="server"></asp:Label>

            </td>
        </tr>
    </table>

    <br />

    <br /> 
    <asp:Button CssClass="btn btn-default" runat="server" Text="Buy" ID="AddToCart" OnClick="AddToCart_Click"/>
</asp:Content>
