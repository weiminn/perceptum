﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.AspNet.Identity;
using PerceptumIIP.Models;

namespace PerceptumIIP
{
    public class DataRoutines : System.Web.UI.Page

    {


        public DataRoutines()
        {

        }

        public void CreateRecs(string strCustID)
        {


            string strOFlag = "F"; //status flag to indicate if there is a need to generate new order
            DateTime today = DateTime.Today;

            var userId = (string)Session["UserId"];
            /// strCustID = userId;

            var currentCustomer = new Customer();
            using (var db = new ApplicationDbContext())
            {
                currentCustomer = db.Customers.SingleOrDefault(x => x.ApplicationUserId == userId);
                Session["CustomerId"] = currentCustomer.customerId;
            }

            strCustID = currentCustomer.customerId.ToString();

            // strSql = "INSERT INTO CustomerOrders (customerId) VALUES(@customerId)";
            //cmd = new SqlCommand(strSql, con);
            //cmd.Parameters.Add("@customerId", SqlDbType.VarChar).Value = strCustID;
            //con.Open();
            //cmd.ExecuteScalar();
            //con.Close();


            // insert a new order record of status “ordering”
            SqlConnection con = new SqlConnection();
            con.ConnectionString = (@"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=aspnet-PerceptumIIP;Trusted_Connection=Yes;User ID=Perceptum2018;Password=p12345;Integrated Security=True;");

            String strStatus = "Ordering";


            // check to see if there is an active order record

            using (var db = new ApplicationDbContext())
            {
                var id = Convert.ToInt16(strCustID);
                var customerOrder = db.CustomerOrders.Where(x => x.customerId == id).OrderByDescending(x => x.CustomerOrderId).FirstOrDefault();

                if(customerOrder != null)
                {
                    if(customerOrder.Status != "Ordering")
                    {
                        strOFlag = "T";
                    }
                }
                else
                {
                    strOFlag = "T";
                }

                if (strOFlag == "T")
                {
                    // create new customer order 
                    CustomerOrder custorders = new CustomerOrder
                    {

                        customerId = Convert.ToInt32(strCustID),
                        Status = strStatus,


                    };
                    db.CustomerOrders.Add(custorders);
                    db.SaveChanges();
                    Session["CustomerOrderId"] = custorders.CustomerOrderId;
                }
                else
                {
                    Session["CustomerOrderId"] = customerOrder.CustomerOrderId;  // store the active order no in sOrderNo
                    con.Close();
                    return;
                }

            }

            //strSql = "SELECT Status FROM CustomerOrders WHERE customerId = @CustID "
            //+ "ORDER BY CustomerOrderId DESC;";

            //cmd = new SqlCommand(strSql, con);
            //cmd.Parameters.Add("@CustID", SqlDbType.VarChar).Value = strCustID;
            //con.Open();
            //rdr = cmd.ExecuteReader();
            //Boolean booRows = rdr.HasRows; //does this data object has any rows?
            //if (booRows)  // when booRows is true, there are order records for the user 
            //{
            //    rdr.Read();
            //    if ((string)rdr["Status"] != "Ordering") //status of an active order is NOT "Ordering"
            //    {
            //        strOFlag = "T"; // "T" means there is a need to create a new Orders record
            //    }
            //}
            //else { strOFlag = "T"; } //when boorows is false means there is no order records for the user
            //            con.Close();
            //        if (strOFlag == "T")
            //        {
            //            // create new customer order 
            //            using (var db = new ApplicationDbContext())
            //            {
            //                CustomerOrder custorders = new CustomerOrder
            //                {

            //                    customerId = Convert.ToInt32(strCustID),
            //                    Status = strStatus,


            //                };
            //                db.CustomerOrders.Add(custorders);
            //                db.SaveChanges();

            //                int orderid = 0;
            //                //var orderno = db.CustomerOrders.SingleOrDefault(p => p.OrderId == orderid);


            //                //Session["OrderId"] = orderno;
            //                // strSql = "INSERT INTO CustomerOrders (customerId, Status) VALUES (@CustID, @Status)";
            //                //cmd = new SqlCommand(strSql, con);
            //                //cmd.Parameters.Add("@CustID", SqlDbType.VarChar).Value = strCustID;
            //                //cmd.Parameters.Add("@Status", SqlDbType.VarChar).Value = strStatus;
            //                //  cmd.Parameters.Add("@Date", today.ToString("yyyy-MM-dd"));
            //                //con.Open();
            //                //cmd.ExecuteNonQuery();
            //                // con.Close();

            //                /*strdate = "INSERT INTO CustOrders(oDate)" + "VALUES (@oDate)";
            //            cmd = new SqlCommand(strdate, Con);
            //            cmd.Parameters.Add("@oDate", today.ToString("yyyy-MM-dd"));
            //            cmd.ExecuteNonQuery();*/
            //            }

            //            // retrieve order No - this order No is needed when the user buys an item
            //        }
            //        else
            //        {
            //            strSql = "SELECT OrderId FROM CustomerOrders WHERE customerId = @CustID "
            //+ "ORDER BY CustomerOrderId DESC;";
            //            cmd = new SqlCommand(strSql, con);
            //            cmd.Parameters.Add("@CustID", SqlDbType.VarChar).Value = strCustID;
            //            con.Open();
            //            rdr = cmd.ExecuteReader();
            //            rdr.Read();

            //            var oId = rdr["OrderId"];

            //            Session["OrderId"] = rdr["OrderId"];  // store the active order no in sOrderNo
            //            con.Close();
            //            return;
            //        }




        }


    }
}