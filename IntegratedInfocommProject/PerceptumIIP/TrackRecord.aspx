﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TrackRecord.aspx.cs" Inherits="PerceptumIIP.TrackRecord" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <style>
.grid{
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  justify-items: center;
  /*background: pink;*/
  grid-column-gap: 5px;
  grid-row-gap: 20px;
  /*padding: 50px 5px 50px 5px;*/
  padding: 5% 10%;
  margin: 0;
}
.why{
  grid-column: 1 / 4;
  display: grid;
  grid-template-columns: 1fr;
  text-align: left;
  /*background: blue;*/
  /*padding: 5px 100px 5px 100px;*/
}
.why1{
    /*background: yellow;*/
    grid-column: 1 / 2;
}
.track{
  grid-column: 1 / 4;
  display: grid;
  grid-template-columns: 1fr;
  text-align: left;
  /*background: blue;*/
  /*padding: 5px 100px 5px 100px;*/
  grid-row-gap: 20px;
}
.track1{
    grid-column: 1 / 2;
    /*background: yellow;*/
    font-size: 24px;
    font-weight: bold;
}
.track2{
    grid-column: 1 / 2;
    /*background: yellow;*/
}
.info{
  grid-column: 1 / 4;
  text-align: center;
  display: grid;
  grid-template-columns: 1fr 1fr;
  /*background: blue;*/
  justify-items: center;
  grid-column-gap: 5px;
  grid-row-gap: 5px;
  padding: 5px;
  margin: 0;
}
.info1{
  grid-column: 1 / 3;
  text-align: center;
  font-size: 24px;
  font-weight: bold;
  /*background: yellow;*/
}
.info2{
  grid-column: 1 / 2;
  text-align: center;
  /*background: yellow;*/
}
.info3{
  grid-column: 2 / 3;
  text-align: center;
  /*background: yellow;*/
}
.info4{
  grid-column: 1 / 2;
  text-align: center;
  /*background: yellow;*/
}
.info5{
  grid-column: 2 / 3;
  text-align: center;
  /*background: yellow;*/
}
</style>
<div class="container-fluid" style="margin: 0; padding: 0;">
  <div class="row">
    <div class="col-sm-12" style="padding: 0;">
      <div id="my-slider" class="carousel slide" data-ride="carousel">

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">

          <div class="item active">
           <img src="Content/images/slider2.png" />
           <div class="carousel-caption"><h1>Track Record</h1></div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<div class="grid">

  <div class="why">
    <div class="why1">Close and constant attention to our teaching and learning quality has helped us make key improvements to our teaching methodologies and curriculum design. This means that our students are primed to achieve stellar results with 9 out of 10 students achieving an A or A* at the PSLE, and a majority of our students scoring distinctions in English and Math at the ‘O’ Levels.</div>
  </div>

  <div class="info">
  
    <div class="info1">Percentage of A*s & As per subject at the 2017 PSLE</div>
    <div class="info2"><img style="height: 150px;" src="Content/images/Picture1.png" /></div>
    <div class="info3"><img style="height: 150px;" src="Content/images/Picture2.png" /></div>
    <div class="info4">Mathematics – 90%</div>
    <div class="info5">Science – 90%</div>
    
  </div>
  
  <div class="track">
      <div class="track1">Continuing To Love Learning At The Tertiary Level</div>
      <div class="track2">Our alumni have gone on to expand their intellectual horizons at various universities both within Singapore and abroad. Whether at NUS, NTU or SMU in Singapore, Oxford or Cambridge in the UK, or Stanford, Harvard or Yale in the US, The Learning Lab graduates have embraced the love of learning and gone on to be world-ready students and upstanding members of society.</div>
  </div>
  
</div>








</asp:Content>
