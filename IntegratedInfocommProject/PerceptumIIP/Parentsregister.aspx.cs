﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using PerceptumIIP.Models;



namespace PerceptumIIP
{
    public partial class Parentsregister : System.Web.UI.Page
    {
        static readonly string scriptSuccessNewAccount =
       "<script language=\"javascript\">\n" +
       "alert (\"Your account has been succesfully created - Thank You!\");\n" +
       "</script>";

        static readonly string scriptErrorEmail =
 "<script language=\"javascript\">\n" +
 "alert (\"Error - Email you keyed in is taken up, please key in another Email\");\n" +
 "</script>";
        protected void Page_Load(object sender, EventArgs e)
        {
            Submit.Enabled = false;
            Password.Attributes["value"] = Password.Text;
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            Type csType = this.GetType();

            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var signInManager = Context.GetOwinContext().Get<ApplicationSignInManager>();
            var user = new ApplicationUser() {
                UserName = Email.Text,
                Email = Email.Text,
                Role = "Parent"
            };
            IdentityResult result = manager.Create(user, Password.Text);

            if (result.Succeeded)
            {
                using (var db = new ApplicationDbContext())
                {
                    Parent newParent = new Parent
                    {

                        UserName = UserName.Text,
                        FirstName = FirstName.Text,
                        LastName = LastName.Text,

                        Email = Email.Text,
                        Telephone = Convert.ToInt32(Telephone.Text),
                        Password = Password.Text,
                        ApplicationUserId = user.Id,

                    };

                    db.Parents.Add(newParent);
                    db.SaveChanges();
                }
                using (var db = new ApplicationDbContext())
                {
                    {
                        Customer newCustomer = new Customer
                        {

                            UserName = UserName.Text,
                            FirstName = FirstName.Text,
                            LastName = LastName.Text,

                            Email = Email.Text,
                            Telephone = Convert.ToInt32(Telephone.Text),
                            Password = Password.Text,
                            ApplicationUserId = user.Id,
                        };

                        db.Customers.Add(newCustomer);
                        db.SaveChanges();

                    }
                  
                }
                signInManager.SignIn(user, isPersistent: false, rememberBrowser: false); // sign in user 

                Session["UserId"] = user.Id;
                String strUserId = (string)Session["UserId"];
                DataRoutines DRObject = new DataRoutines();
                DRObject.CreateRecs(strUserId);

                Session["PName"] = (string)UserName.Text;
                Session["PFirstName"] = (string)FirstName.Text;
                Session["PLastName"] = (string)LastName.Text;
                Session["PEmail"] = (string)Email.Text;
                Session["PTelephone"] = (string)Telephone.Text;
                Session["PPassword"] = (string)Password.Text;

                ClientScript.RegisterStartupScript(csType, "Success", scriptSuccessNewAccount);

                IdentityHelper.RedirectToReturnUrl("/Views/Parent/ParentStudents", Response);
                //IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                //return url 

            }



            //else
            //{
            //    ErrorMessage.Text = result.Errors.FirstOrDefault();

            //    ClientScript.RegisterStartupScript(csType, "Error", scriptErrorEmail);

            //}



        }

        protected void agreeCheck_CheckedChanged(object sender, EventArgs e)
        {
            Submit.Enabled = agreeCheck.Checked;
        }
    }
}