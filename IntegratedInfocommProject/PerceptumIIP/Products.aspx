﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Products.aspx.cs" Inherits="PerceptumIIP.Products" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        #Content1_DataList1 tbody tr td{
            padding:5px;
        }
        .proditems{
            margin: 20px 0 0 20px;
        }
    </style>
  <br />
    <br />

    <h3>Perceptum Resources</h3>

    <%--<asp:GridView ID="ProductsGrid" runat="server" OnRowCommand="ProductsGrid_RowCommand"
        CssClass="table table-hover table-striped">
        <Columns>
            <asp:ButtonField Text="View" />
            <asp:TemplateField>
                <HeaderTemplate>Image</HeaderTemplate>
                <ItemTemplate>
                    <img src='data:image/jpg;base64,<%# Eval("Image") != System.DBNull.Value ? Convert.ToBase64String((byte[])Eval("Image")) : string.Empty %>' alt="image" height="200" width="200"/>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>--%>

   <link href="CSS/master.css" rel="stylesheet" />

    <div class="container-fluid">
        <asp:DropDownList runat="server" AutoPostBack="true" ID="catddl" OnSelectedIndexChanged="catddl_SelectedIndexChanged">
            <asp:ListItem Value="" Selected="True"></asp:ListItem>
            <asp:ListItem Value="Science">Science</asp:ListItem>
            <asp:ListItem Value="Mathematics">Mathematics</asp:ListItem>
        </asp:DropDownList>
    <asp:DataList ID="DataList1" runat="server" DataKeyField="ProductId"  Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" RepeatColumns="3" CellSpacing="10" style="padding-bottom:10px; margin:auto; clear:both" ><%--DataSourceID="SqlDataSource1"--%>
        <ItemTemplate>
            
            <asp:HyperLink runat="server" NavigateUrl='<%# Eval("ProductId", "ProductDetails.aspx?ProductId={0}") %>' Text="">
                    <img src='data:image/jpg;base64,<%# Eval("Image") != System.DBNull.Value ? Convert.ToBase64String((byte[])Eval("Image")) : string.Empty %>' alt="image" height="200" width="200"/>
            </asp:HyperLink>
            <br />
            Name:
            <asp:Label ID="NameLabel" runat="server" Text='<%# Eval("Name") %>' />
            <br />
           
         
            Type:
            <asp:Label ID="ProductCategoryLabel" runat="server" Text='<%# Eval("ProductCategory") %>' />
            <br />
       
            Price:
            <asp:Label ID="PromoPriceLabel" runat="server" Text='<%# Eval("PromoPrice") %>' />
            <br />
            <%--QuantityOnHand:
            <asp:Label ID="QuantityOnHandLabel" runat="server" Text='<%# Eval("QuantityOnHand") %>' />--%>
            
        </ItemTemplate>
        </asp:DataList>
    <%--<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Products]"></asp:SqlDataSource>--%>
</div>

</asp:Content>
