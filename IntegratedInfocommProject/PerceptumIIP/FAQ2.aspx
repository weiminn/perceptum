﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FAQ2.aspx.cs" Inherits="PerceptumIIP.FAQ2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

        <style>
.faq{
  grid-column: 2 / 5;
  text-align: left;
  /*background: red;*/
  margin: 45px 0 45px 0;
  padding: 0;
}
.q, .a{
  display: block;
}
.q{
  background: #eaeaea;
  color: #000;
  margin: 5px;
  padding: 5px;
}
.a{
  /*background: #eaeaea;*/
  margin: 0 5px 0 5px;
  color: #828282;
}
.qalist{
  /*background: pink;*/
  list-style-type: none;
  margin: 0 100px 0 0;
  padding: 0;
}
.faqgrid{
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    /*background: blue;*/
    margin: 0;
    padding: 0;
}
.faqnav{
    grid-column: 1 / 2;
    text-align: left;
    /*background: yellow;*/
    padding: 0 0 0 75px;
}
.generalbutton{
  background-color: rgba(255, 255, 255, 0);
  color: #828282;
  border: none;
  border-radius: 8px;
  padding: 9px 0 9px 25px;
  margin: 50px 9px 7.75px 9px;
  min-width: 133px;
  display: block;
  text-align: left;
  /*border: 1px solid #ffd966;*/
  font-family: Moon;
  font-weight: bold;
  font-size: 11px;
}
.curriculumbutton{
  background-color: rgba(255, 255, 255, 0);
  color: #828282;
  border: none;
  border-radius: 8px;
  padding: 9px 0 9px 25px;
  margin: 7.75px 9px 7.75px 9px;
  min-width: 133px;
  display: block;
  text-align: left;
  /*border: 1px solid #ffd966;*/
  font-family: Moon;
  font-weight: bold;
  font-size: 11px;
}
.generalbutton:hover, .curriculumbutton:hover{
    background-color: #eaeaea;
    /*border: 1px solid #ffd966;*/
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
function faqControl() {
    $(".a").hide();
    $(".q").click(function(){
        $(this).next().slideToggle();
    });
}

$(document).ready(function() {
  faqControl();
    });
</script>

<div class="container-fluid" style="margin: 0; padding: 0;">
  <div class="row">
    <div class="col-sm-12" style="padding: 0;">
      <div id="my-slider" class="carousel slide" data-ride="carousel">

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">

          <div class="item active">
           <img src="Content/images/slider2.png" />
           <div class="carousel-caption"><h1>FAQ</h1></div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>

<div class="faqgrid">

    <div class="faqnav">
        <a runat="server" href="FAQ.aspx"><button type="button" class="generalbutton">General</button></a>
        <a runat="server" href="FAQ2.aspx"><button type="button" class="curriculumbutton">Curriculum</button></a>
    </div>

    <div class="faq">
      <ul class="qalist">
        <li class="q">How do I register with Perceptum?</li>
        <li class="a">
          <p>
Registration is typically done at the beginning of a term during late August to September, however places are limited and given on a first-come-first-serve basis.
              </p>
            <p>
              Even so, students can register any time, but slots need to be available, and Perceptum typically do not take in students during dates very close to examinations.</p>
          
        </li>
        <li class="q">How are the classes structured in a week?</li>
        <li class="a">
          <p>
Currently we have weekday classes from Wednesday to Friday as well as weekend classes. We have about 33 classes in the weekdays and 42 classes for weekends to provide convince for students to come to tuition after their school during the weekdays and weekends for those who are free.</p>
          
        </li>
        <li class="q">How often will I get feedback from teachers?</li>
        <li class="a">Our teachers communicate regularly with parents regarding their child’s progress. We also have Student Progress Reports, which are communicated to Parents via the Parent Portal monthly. If you have questions you may request to speak with your child’s teacher(s).</li>
        <li class="q">What is your average class size?</li>
        <li class="a">Perceptum Education classes keep strictly to a class size of 12 students with an extra 3 seats available for students with make up lessons. This allows the child to have more time to clarify doubts within the lesson and will allow the tutor to better identify students that may not fully understand difficult topics during the class.</li>
        <li class="q">What are the rates of Perceptum?</li>
        <li class="a">Perceptum Education tuition rates are $65 per class and classes are once a week. This makes a monthly tuition fee of $260 per subject and since Perceptum Education teaches both math and science and specializes in primary school education, most parents would spend $520 per month per child.</li>
      </ul>
    </div>
</div>




</asp:Content>
