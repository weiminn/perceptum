﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Testimonial.aspx.cs" Inherits="PerceptumIIP.Testimonial" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <style>

        .starRating {
            width:50px;
            height:50px;
            cursor: pointer;
            background-repeat:no-repeat;
            display:block;
        }

       .starempty
        {
            background-image:url(../../../Content/images/starempty.png);
            width: 50px;
            height: 50px;
        }
.starfilled
        {
            background-image: url(../../../Content/images/starfilled.png);
            width: 50px;
            height: 50px;
        }
.starwaiting
        {
            background-image: url(../../../Content/images/starwaiting.png);
            width: 50px;
            height: 50px;
        }
    
.rating {


    padding: 10px 0 0 20px;
}






    </style>
    <div class="container-fluid" style="margin: 0; padding: 0;">
      <div class="row">
        <div class="col-sm-12" style="padding: 0;">
          <div id="my-slider" class="carousel slide" data-ride="carousel">

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">

              <div class="item active">
               <img src="Content/images/slider2.png" />
               <div class="carousel-caption"><h1>Testimonials</h1></div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>

    


    <asp:GridView ID="tGrid" runat="server"
    CssClass="table table-hover table-striped"></asp:GridView>

    <div class="pagegrid">

      <div class="topgrid">

          <div class="profile1">

          <div class="profilespacer" style="background: #ff0042;"></div>
  
          <div class="profileimg"><img class="roundimg" src="Content/images/10.jpg" /></div>
  
          <div class="profiledetails">
            <div style="font-size: 18px;">Jane Koh</div>
            <div style="font-size: 15px;">Parent</div>
          </div>

          <div class="testtitle">
          Best Teachers
          </div>
  
          <div class="testcontent">
              <img style="width: 30px; margin: -20px 10px 0 0;" src="Content/images/quote-leftp.png" />
              Valerie really enjoys her lessons at Perceptum Education! She always shares with me that her teachers are cheerful and kind-hearted, and shower her with lots of encouragement. In addition to the nurturing environment, I am also extremely satisfied with the curriculum here.
              <img style="width: 30px; margin: 0 0 -10px 10px;" src="Content/images/right-quotation-markp.png" />
          </div>
        </div>

        <div class="profile2">

          <div class="profilespacer" style="background: #ec3466;"></div>
  
          <div class="profileimg"><img class="roundimg" src="Content/images/11.jpg" /></div>
  
          <div class="profiledetails">
            <div style="font-size: 18px;">Rebecca Tan</div>
            <div style="font-size: 15px;">Parent</div>
          </div>
  
          <div class="testtitle">
          Impressive curriculum...!
          </div>
  
          <div class="testcontent">
              <img style="width: 30px; margin: -20px 10px 0 0;" src="Content/images/quote-leftp.png" />
              We have chosen Perceptum as we find that you have brilliantly managed to design an impressive curriculum and hire a fantastic team of teachers to deliver it. Your curriculum goes beyond the normal school curriculum, and many of your teachers have experience beyond the education sector, which adds an interesting flavour to the classroom.
              <img style="width: 30px; margin: 0 0 -10px 10px;" src="Content/images/right-quotation-markp.png" />
          </div>
        </div>

      </div>

  
      <div class="bottomgrid">


        <div class="profile3">

          <div class="profilespacer" style="background: #ff0042;"></div>
  
          <div class="profileimg"><img class="roundimg" src="Content/images/12.jpg" /></div>
  
          <div class="profiledetails">
            <div style="font-size: 18px;">Sarah Lee</div>
            <div style="font-size: 15px;">Parent</div>
          </div>
  
          <div class="testtitle">
          Dedicated teacher...!
          </div>
  
          <div class="testcontent">
              <img style="width: 30px; margin: -20px 10px 0 0;" src="Content/images/quote-leftp.png" />
              Teachers were very dedicated and hardworking. Throughout the preparation period, they helped him to work on areas of weakness and taught him effective exam techniques that really helped him to better understand the topics that were tested. When we received his PSLE results on 24 November, we were overjoyed — our son achieved a PSLE score above 270 and he got an A* for all of his subjects!
              <img style="width: 30px; margin: 0 0 -10px 10px;" src="Content/images/right-quotation-markp.png" />
          </div>
        </div>

        <div class="profile4">

          <div class="profilespacer" style="background: #ec3466;"></div>
  
          <div class="profileimg"><img class="roundimg" src="Content/images/13.jpg" /></div>
  
          <div class="profiledetails">
            <div style="font-size: 18px;">April Neo</div>
            <div style="font-size: 15px;">Parent</div>
          </div>
  
          <div class="testtitle">
          Gained confidence...!
          </div>
  
          <div class="testcontent">
              <img style="width: 30px; margin: -20px 10px 0 0;" src="Content/images/quote-leftp.png" />
              Ever since Jessica started taking Science and English lessons at Perceptum, she has gained considerable confidence in these subjects. She is given very comprehensive Science notes weekly, which effectively condense difficult topics and illustrate critical concepts. Morbi a convallis urna. Vestibulum ut hendrerit turpis. Donec blandit porta metus, 
              <img style="width: 30px; margin: 0 0 -10px 10px;" src="Content/images/right-quotation-markp.png" />
          </div>
        </div>

          <div class="profile5">

          <div class="profilespacer" style="background: #ed4d01;"></div>
  
          <div class="profileimg"><img class="roundimg" src="Content/images/14.jpg" /></div>
  
          <div class="profiledetails">
            <div style="font-size: 18px;">John Smith</div>
            <div style="font-size: 15px;">Parent</div>
          </div>
  
          <div class="testtitle">
          Awesome mathematics teacher...!
          </div>
  
          <div class="testcontent">
              <img style="width: 30px; margin: -20px 10px 0 0;" src="Content/images/quote-lefto.png" />
              Dear Mr. Lee, thank you for helping me in my Maths and for bringing up my mark to an A* from C. I will always remember you as an awesome Mathematics teacher. Morbi a convallis urna. Vestibulum ut hendrerit turpis. Donec blandit porta metus, 
              non tristique justo. Aenean ac molestie dui. Sed vel blandit odio. Donec suscipit metus at fermentum ullamcorper.
              <img style="width: 30px; margin: 0 0 -10px 10px;" src="Content/images/right-quotation-marko.png" />
          </div>
        </div>


        <div class="profile6">

          <div class="profilespacer" style="background: #00aac4;"></div>
  
          <div class="profileimg"><img class="roundimg" src="Content/images/15.jpg" /></div>
  
          <div class="profiledetails">
            <div style="font-size: 18px;">Jenny Doe</div>
            <div style="font-size: 15px;">Parent</div>
          </div>
  
          <div class="testtitle">
          I am so touched...!
          </div>
  
          <div class="testcontent">
              <img style="width: 30px; margin: -20px 10px 0 0;" src="Content/images/quote-leftb.png" />
              Mr Soh is such a good teacher. I texted him some questions from James’ school and he took time to reply till 12 am. Just texted me to ask whether James can understand or not… I am so touched!!! 
              <img style="width: 30px; margin: 0 0 -10px 10px;" src="Content/images/right-quotation-markb.png" />
          </div>
        </div>

        <div class="profile7">

          <div class="profilespacer" style="background: #ec3466;"></div>
  
          <div class="profileimg"><img class="roundimg" src="Content/images/16.jpg" /></div>
  
          <div class="profiledetails">
            <div style="font-size: 18px;">Kelley Chalamet</div>
            <div style="font-size: 15px;">Parent</div>
          </div>
  
          <div class="testtitle">
          From B to A*...!
          </div>
  
          <div class="testcontent">
              <img style="width: 30px; margin: -20px 10px 0 0;" src="Content/images/quote-leftp.png" />
              I can’t thank you enough for all you’ve done to help Faye in her Math. You have been so patient and encouraging. Under your teaching, her results has improved from B to A*. Thank you so much. She has grown and learnt ever since we enrolled her in the centre in 2015.
              <img style="width: 30px; margin: 0 0 -10px 10px;" src="Content/images/right-quotation-markp.png" />
          </div>
        </div>

        <div class="profile8">

          <div class="profilespacer"></div>
  
          <div class="profileimg"><img class="roundimg" src="Content/images/17.jpg" /></div>
  
          <div class="profiledetails">
            <div style="font-size: 18px;">James Chan</div>
            <div style="font-size: 15px;">Parent</div>
          </div>
  
          <div class="testtitle">
          Tremendous improvements...!
          </div>
  
          <div class="testcontent">
              <img style="width: 30px; margin: -20px 10px 0 0;" src="Content/images/quote-left.png" />
              Dear Mr Lee, words cannot express our appreciation of your efforts in tutoring our son, Peng Yu, in his PSLE Maths. Under your tutelage, he has shown tremendous improvements and has grown in confidence in tracking Math problems. He has gone on to achieve an A* in his PSLE Maths and has gained entry to Hwa Chong.
              <img style="width: 30px; margin: 0 0 -10px 10px;" src="Content/images/right-quotation-mark.png" />
          </div>
        </div>

      </div>
                  </div>


<style>
.pagegrid{
  display: grid;
  grid-template-columns: 1fr;
  /*background: pink;*/
  margin: 5% 10%;
  padding: 5px;
}
.topgrid{
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: 50px;
  padding-bottom: 50px;
  /*grid-column-gap: 5px;
  background: green;*/
}
.bottomgrid{
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-column-gap: 50px;
  grid-row-gap: 50px;
  /*background: purple;*/
}
.profile1{
  grid-column: 1/2;
  display: grid;
  grid-template-columns: 1fr;
  /*justify-items: center;*/
  text-align: center;
  background: #fff;
  font-family: Arial;
  margin: 0;
  padding: 0;
  border: 5px solid #eaeaea;
}
.profile2{
  grid-column: 2/3;
  display: grid;
  grid-template-columns: 1fr;
  /*justify-items: center;*/
  text-align: center;
  background: #fff;
  font-family: Arial;
  margin: 0;
  padding: 0;
  border: 5px solid #eaeaea;
}
.profile3{
  grid-column: 1/2;
  display: grid;
  grid-template-columns: 1fr;
  /*justify-items: center;*/
  text-align: center;
  background: #fff;
  font-family: Arial;
  margin: 0;
  padding: 0;
  border: 5px solid #eaeaea;
}
.profile4{
  grid-column: 2/3;
  display: grid;
  grid-template-columns: 1fr;
  /*justify-items: center;*/
  text-align: center;
  background: #fff;
  font-family: Arial;
  margin: 0;
  padding: 0;
  border: 5px solid #eaeaea;
}
.profile5{
  grid-column: 3/4;
  display: grid;
  grid-template-columns: 1fr;
  /*justify-items: center;*/
  text-align: center;
  background: #fff;
  font-family: Arial;
  margin: 0;
  padding: 0;
  border: 5px solid #eaeaea;
}
.profile6{
  grid-column: 1/2;
  display: grid;
  grid-template-columns: 1fr;
  /*justify-items: center;*/
  text-align: center;
  background: #fff;
  font-family: Arial;
  margin: 0;
  padding: 0;
  border: 5px solid #eaeaea;
}
.profile7{
  grid-column: 2/3;
  display: grid;
  grid-template-columns: 1fr;
  /*justify-items: center;*/
  text-align: center;
  background: #fff;
  font-family: Arial;
  margin: 0;
  padding: 0;
  border: 5px solid #eaeaea;
}
.profile8{
  grid-column: 3/4;
  display: grid;
  grid-template-columns: 1fr;
  /*justify-items: center;*/
  text-align: center;
  background: #fff;
  font-family: Arial;
  margin: 0;
  padding: 0;
  border: 5px solid #eaeaea;
}
.profilespacer{
  height: 80px;
  background: #ff0042;
}
.profileimg{
  background: #fff;
  height: 50px;
  /*justify-self: start;*/
}
.roundimg{
    width: 100px;
    position: relative;
    bottom: 50px;
    border-radius: 50%;
    border-style: solid;
    border-color: #fff;
    border-width: 5px;
}
.profiledetails{
  display: grid;
  grid-template-columns: 1fr;
  background: #fff;
  padding: 5px 0 5px 0;
}
.testtitle{
  background: #fff;
  font-weight: bold;
  font-size: 17px;
  padding: 5px 0 5px 0;
  border-top-style: solid;
  border-color: #eaeaea;
}
.testcontent{
  background: #fff;
  padding: 5px 15px 15px 15px;
}
</style>
</asp:Content>
