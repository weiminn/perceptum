﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="PerceptumIIP.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
     <br />
    <br />
    <br />
   <%--Map--%>
    <div id="LocationMap" style="width:100%; height:400px;"></div>

    <script type="text/javascript">
        var mapInstance;var markers=[];var locations=[["Sime Darby Centre","1.336693","103.783779","896 Dunearn Rd,\r\nSingapore 589472"],["Bishan","1.358985","103.843406","261 Bishan Street 22,\r\Singapore 797653"],["Tampines Mall","1.352614","103.944693","Tampines Mall\r\n4 Tampines Central 5, #05-07\r\nSingapore 529510\r\n"]];function myMap(){var myLatLng={lat:1.317547,lng:103.843753};var mapProp={center:new google.maps.LatLng(1.317547,103.843753),zoom:12,scrollwheel:false,};var map=new google.maps.Map(document.getElementById("LocationMap"),mapProp);var infowindow=new google.maps.InfoWindow();var marker,i;var iconMarker='https://www.thelearninglab.com.sg/wp-content/themes/thelearninglab/img/ic-marker.png';for(i=0;i<locations.length;i++){marker=new google.maps.Marker({position:new google.maps.LatLng(locations[i][1],locations[i][2]),map:map,title:locations[i][0],icon:iconMarker});google.maps.event.addListener(marker,'click',(function(marker,i){var htmlInfo='';htmlInfo+='<h5><strong>'+locations[i][0]+'</strong></h5>';htmlInfo+='<p style="font-size:12px; color:#777;">'+locations[i][3]+'</p>';return function(){infowindow.setContent(htmlInfo);infowindow.open(map,marker);};})(marker,i));markers.push(marker);}
        google.maps.event.addDomListener(window,"resize",function(){var center=map.getCenter();google.maps.event.trigger(map,"resize");map.setCenter(center);});mapInstance=map;}
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBYFNTRvM0TOJdh5hJgzFburyo4KvWLob4&amp;callback=myMap"></script>

    <%--End of Map--%>

<div class="grid">
  <div class="Centre1">
    <div><p style="margin-bottom: 20px; color: #828282;">Sime Darby Centre</p></div>
    <img style="height:100px; width:100px" src="Content/images/256-256-35be70cfc8168446fdd9c9803ea9f6a1.png" />
    <p>896 Dunearn Rd, Singapore 589472</p>
    <p>Jesline Ho, +65 9389 6788</p>
    <p>Jesline56@gmail.com</p>
  </div>

  <div class="Centre2">
      <div><p style="margin-bottom: 20px; color: #828282;">Bishan</p></div>
      <img style="height:100px; width:100px" src="Content/images/256-256-35be70cfc8168446fdd9c9803ea9f6a1.png" />
      <p>261 Bishan Street 22,Singapore 797653</p>
      <p>Tay Khai Boon, +65 9337 4335</p>
      <p>tkb@gmail.com</p>
  </div>

  <div class="Centre3">
      <div><p style="margin-bottom: 20px; color: #828282;">Tampines Mall</p></div>
      <img style="height:100px; width:100px" src="Content/images/256-256-35be70cfc8168446fdd9c9803ea9f6a1.png" />
      <p>Tampines Central 5, #05-07 Singapore 529510</p>
      <p>Caroline Tan, +65 9250 3369</p>
      <p>misscaroline@gmail.com</p>
  </div>
</div>


<div class="Forms" style="margin-bottom: 60px;">
    <h5>Drop us a Message!</h5>
    

  <div class="form-group">
      <div class="row">
          <div class="col-md-6 col-md-offset-5 text-center">
    <input type="text" class="form-control" id="inputName" aria-describedby="emailHelp" placeholder="Let us know your name!">
  </div>
</div>

</div>

  <div class="form-group">
      <div class="row">
                <div class="col-md-6 col-md-offset-5 text-center">

    <input type="text" class="form-control" id="inputNumber" aria-describedby="emailHelp" placeholder="Enter your phone number">
                    </div>
  </div>
      </div>



  <div class="form-group">
            <div class="row">
                      <div class="col-md-6 col-md-offset-5 text-center">

    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">

  </div>
                </div>
      </div>
      
      <div class="form-group">
    <textarea class="form-control" id="exampleTextarea" rows="2"></textarea>
          </div>

          <div class="form-group">

  <button type="submit" class="btn btn-primary" onclick="Submit">Submit</button>
                    </div>


    </div>

<style>

    .col-md-6{
        margin-bottom:5px;
    }

.Map{
    width: 600px
}

.grid {
  grid-column: 1 / 4;
  text-align: center;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  justify-content: center;
  grid-column-gap: 20px;
  grid-row-gap: 5px;
  padding: 5px;
  margin: 0;

}


.Centre1{
  grid-column: 1 / 2;
  text-align: center;
  font-family: Moon, Arial, Helvetica, sans-serif;
  font-weight: bold;
  border:solid;
  border-color:darkgrey;
}

.Centre2{
  grid-column: 2 / 3;
  text-align: center;
  font-family: Moon, Arial, Helvetica, sans-serif;
  font-weight: bold;
  border:solid;
  border-color:darkgrey;
}

.Centre3{
  grid-column: 3 / 4;
  text-align: center;
  font-family: Moon, Arial, Helvetica, sans-serif;
  font-weight: bold;
  border:solid;
  border-color:darkgrey;
}


.title{
  grid-area: title;
}
.sidebar{
  grid-area: sidebar;
}
.content{
  grid-area: content;
  display: grid; 
}

.GridForms {
    display:grid;
    grid-template-columns: 1fr 1fr 1fr
}

.Forms{
    grid-column: 2 / 3;
    justify-content: center;
    text-align:center;
}



</style>
</asp:Content>
