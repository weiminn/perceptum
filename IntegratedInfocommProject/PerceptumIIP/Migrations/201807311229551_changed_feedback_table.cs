namespace PerceptumIIP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changed_feedback_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Feedbacks", "DateTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Feedbacks", "DateTime");
        }
    }
}
