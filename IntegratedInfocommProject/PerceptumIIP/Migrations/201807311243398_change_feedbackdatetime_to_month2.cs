namespace PerceptumIIP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_feedbackdatetime_to_month2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Feedbacks", "Month", c => c.DateTime(nullable: false));
            DropColumn("dbo.Feedbacks", "DateTime");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Feedbacks", "DateTime", c => c.DateTime(nullable: false));
            DropColumn("dbo.Feedbacks", "Month");
        }
    }
}
