﻿using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using AjaxControlToolkit;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PerceptumIIP.Models;

namespace PerceptumIIP
{
    public partial class EmployeesRatings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (var db = new ApplicationDbContext())
            {
                //var ratings = from r in db.EmployeeRatings
                //                group r by new
                //                {
                //                    r.EmployeeId
                //                } into er

                //                select new
                //                {
                //                    Rating = er.Average(a => a.Rating),
                //                    er.Key.EmployeeId
                //                };

                //                var ratingDetails = from r in ratings
                //                    join em in db.Employees on r.EmployeeId equals em.EmployeeId

                //                select new
                //                {
                //                    Name = em.FirstName + " " + em.LastName,
                //                    Rating = r.Rating
                //                };

                var rating1 = from r in db.EmployeeRatings
                             where r.EmployeeId == 12
                             group r by new
                             {
                                 r.EmployeeId
                             } into er
                             select new
                             {
                                 er.Key.EmployeeId,
                                 Rating = er.Average(a => a.Rating)
                             };
                if(rating1 != null)
                {
                    Rating_1.CurrentRating = Convert.ToInt32(rating1.FirstOrDefault().Rating);
                }

                

                var rating2 = from r in db.EmployeeRatings
                              where r.EmployeeId == 5
                              group r by new
                              {
                                  r.EmployeeId
                              } into er
                              select new
                              {
                                  er.Key.EmployeeId,
                                  Rating = er.Average(a => a.Rating)
                              };
                if (rating2 != null)
                {
                    Rating4.CurrentRating = Convert.ToInt32(rating2.FirstOrDefault().Rating);
                }

                
                var rating3 = from r in db.EmployeeRatings
                              where r.EmployeeId == 6
                              group r by new
                              {
                                  r.EmployeeId
                              } into er
                              select new
                              {
                                  er.Key.EmployeeId,
                                  Rating = er.Average(a => a.Rating)
                              };
                if (rating3 != null)
                {
                    Rating3.CurrentRating = Convert.ToInt32(rating3.FirstOrDefault().Rating);
                }

                var rating7 = from r in db.EmployeeRatings
                              where r.EmployeeId == 7
                              group r by new
                              {
                                  r.EmployeeId
                              } into er
                              select new
                              {
                                  er.Key.EmployeeId,
                                  Rating = er.Average(a => a.Rating)
                              };
                if (rating7 != null)
                {
                    Rating1.CurrentRating = Convert.ToInt32(rating7.FirstOrDefault().Rating);
                }


                var rating5 = from r in db.EmployeeRatings
                              where r.EmployeeId == 4
                              group r by new
                              {
                                  r.EmployeeId
                              } into er
                              select new
                              {
                                  er.Key.EmployeeId,
                                  Rating = er.Average(a => a.Rating)
                              };
                if (rating5 != null)
                {
                    Rating5.CurrentRating = Convert.ToInt32(rating5.FirstOrDefault().Rating);
                }

                var rating8 = from r in db.EmployeeRatings
                              where r.EmployeeId == 11
                              group r by new
                              {
                                  r.EmployeeId
                              } into er
                              select new
                              {
                                  er.Key.EmployeeId,
                                  Rating = er.Average(a => a.Rating)
                              };
                if (rating8 != null)
                {
                    Rating2.CurrentRating = Convert.ToInt32(rating8.FirstOrDefault().Rating);
                }
            }
        }

        private DataTable GetData(string query)
        {
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection();
            con.ConnectionString = (@"Data Source=tcp:perceptum.database.windows.net;Initial Catalog=Perceptum V1;Connect Timeout=15;Encrypt=true;MultipleActiveResultSets=True;Trusted_Connection=False;User ID=Perceptum2018;Password=Pm123456;");

            con.Open();
            SqlCommand cmd;
            string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            using (con = new SqlConnection(constr))
            {
                using (cmd = new SqlCommand(query))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        sda.Fill(dt);
                    }
                }
                return dt;
            }
        }
    }
}