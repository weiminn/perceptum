﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using PerceptumIIP.Models;
using System.Linq;
using System.Web.Providers.Entities;
using Microsoft.AspNet.Identity.Owin;

namespace PerceptumIIP
{
    public partial class SiteMaster : MasterPage
    {
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;

        protected void Page_Init(object sender, EventArgs e)
        {
            Page p = this.Context.Handler as Page;
            if (p != null)
            {
                // set master page
                if (Request.Browser.IsMobileDevice)
                {
                    p.MasterPageFile = "~/Site.mobile.master";
                }
            }

            // The code below helps to protect against XSRF attacks
            var requestCookie = Request.Cookies[AntiXsrfTokenKey];
            Guid requestCookieGuidValue;
            if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
            {
                // Use the Anti-XSRF token from the cookie
                _antiXsrfTokenValue = requestCookie.Value;
                Page.ViewStateUserKey = _antiXsrfTokenValue;
            }
            else
            {
                // Generate a new Anti-XSRF token and save to the cookie
                _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
                Page.ViewStateUserKey = _antiXsrfTokenValue;

                var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                {
                    HttpOnly = true,
                    Value = _antiXsrfTokenValue
                };
                if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                {
                    responseCookie.Secure = true;
                }
                Response.Cookies.Set(responseCookie);
            }

            Page.PreLoad += master_Page_PreLoad;
        }

        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
                ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
            }
            else
            {
                // Validate the Anti-XSRF token
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                    || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                {
                    throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            //if (Page.User.Identity.IsAuthenticated)
            {
             //   string userId = Page.User.Identity.GetUserId();

               // using (var db = new ApplicationDbContext())
                {
                 //   Customer currentuser = db.Customers.SingleOrDefault(x => x.ApplicationUserId == userId);
                   // string customernname = currentuser.FirstName;
                    //Session["FirstName"] = currentuser.FirstName;
                }
                //lblwelcome.Text = "Welcome" + Session["FirstName"] + "!";
            }
            //else
            {

              //  lblwelcome.Text = "";
            }
        }

        protected void Unnamed_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            Context.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }

        protected void logout(object sender, EventArgs e)
        {
            Session["CName"] = "";
            Session["CFirstName"] = "";
            Session["CLastName"] = "";
            Session["CEmail"] = "";
            Session["CTelephone"] = "";

            Session["CPassword"] = "";
            Session["CEmail"] = "";
        }

        protected void switch_Click(object sender, EventArgs e)
        {
            var luser = Page.User.Identity.GetUserId();
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = manager.FindById(luser);

            if(user.Role == "Admin")
            {
                Response.Redirect("/Views/Admin/Default");
            }
            else if (user.Role == "Parent")
            {
                Response.Redirect("/Views/Parent/ParentStudents.aspx");
            }
            else if (user.Role == "Teacher")
            {
                Response.Redirect("/Views/Teacher/TeacherClasses.aspx");
            }
        }
    }

}