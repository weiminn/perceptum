﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ordersdetailtracking.aspx.cs" Inherits="PerceptumIIP.ordersdetailtracking" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <br />
    <style>
        .ordersdetail   {
    width:100%;
    margin: 5px 0 10px 0;
    border: solid 1px black;
    background-color:white;
}


.ordersdetail td {
    padding:10px; 
    border: solid 1px black;
    color:black;
}

    .ordersdetail th {
        background-color:#f3e222;
        text-align:center;
        padding: 10px 2px;
        border: solid 1px black;
        color: black;
    }
.ordersdetail tr:nth-child(even) {
    background-color: #ffffff;
}

.ordersdetail tr:nth-child(odd) {
    background-color: #dcdcdc;
}

.ordersdetail .alt{
    background:#dcdcdc;
}

.ordersdetail .pgr{

    background: #dcdcdc;

}
    .ordersdetail .pgr table {
        margin: 5px 0;
    }

    .ordersdetail .pgr td {
        border-width:0;
        padding: 0 6px;
        border-left: solid 1px black;
    }
    .ordersdetail .pgr a{
        color:black;
        text-decoration:none;
    }
    </style>
    <h1>Your Order History</h1>
    
    <asp:GridView ID="GridView1" runat="server" CssClass="ordersdetail" AutoGenerateColumns="False" GridLines="None" Font-Size="Medium" Font-Names="Arial" CellPadding="3" CellSpacing="10" AlternatingRowStyle-CssClass="alt" PageSize="7" PagerStyle-CssClass="pgr">
        
        <Columns>
            <asp:BoundField DataField="TransactionId" HeaderText="Transaction ID" InsertVisible="False" ReadOnly="True" SortExpression="TransactionId" />
            <asp:BoundField DataField="CustomerOrderId" HeaderText="Customer Order ID" SortExpression="CustomerOrderId" />
            <asp:BoundField DataField="PaymentMode" HeaderText="PaymentMode" SortExpression="PaymentMode" />
            <asp:BoundField DataField="DeliveryMode" HeaderText="DeliveryMode" SortExpression="DeliveryMode" />
            <asp:BoundField DataField="Amount" HeaderText="Amount" SortExpression="Amount" />
            <asp:BoundField DataField="GST" HeaderText="GST" SortExpression="GST" />
            <asp:BoundField DataField="TotalAmt" HeaderText="TotalAmt" SortExpression="Total Amountt" />
            <asp:BoundField DataField="DateTime" HeaderText="DateTime" SortExpression="Date Time"/>
        </Columns>
    <PagerStyle CssClass="pgr"></PagerStyle>

    </asp:GridView>


<%--    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT *
FROM Transactions T
INNER JOIN CustomerOrders CO
ON T.CustomerOrderId = CO.CustomerOrderId
WHERE CO.customerId = @CustomerId">
        <SelectParameters>
            <asp:SessionParameter Name="CustomerId" SessionField="CustomerId" />
        </SelectParameters>
    </asp:SqlDataSource>--%>


</asp:Content>
