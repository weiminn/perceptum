﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using PerceptumIIP.Models;


namespace PerceptumIIP
{
    public partial class ordersdetailtracking : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var loggedInUser = User.Identity.GetUserId();
            if(User.Identity.IsAuthenticated)
            {
                using (var db = new ApplicationDbContext())
                {
                    var customerId = db.Customers.SingleOrDefault(x => x.ApplicationUserId == loggedInUser).customerId;

                    var orders = from t in db.Transactions
                                 join co in db.CustomerOrders on t.CustomerOrderId equals co.CustomerOrderId
                                 where co.customerId == customerId
                                 select new
                                 {
                                     TransactionId = t.TransactionId,
                                     CustomerOrderId = co.CustomerOrderId,
                                     PaymentMode = t.PaymentMode,
                                     DeliveryMode = t.DeliveryMode,
                                     Amount = t.Amount,
                                     GST = t.GST,
                                     TotalAmt = t.TotalAmt,
                                     DateTime = t.DateTime
                                 };

                    GridView1.DataSource = orders.ToList();
                    GridView1.DataBind();
                }
            }
        }
    }
}