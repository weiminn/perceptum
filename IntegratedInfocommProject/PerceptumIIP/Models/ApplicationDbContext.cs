﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PerceptumIIP.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        //tables
        public DbSet<Student> Students { get; set; }
        public DbSet<Parent> Parents { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Customer> Customers { get; set; }

        public DbSet<StudentParent> StudentParents { get; set; }
        public DbSet<StudentClass> StudentClasses { get; set; }
        public DbSet<EmployeeClass> EmployeeClasses { get; set; }

        public DbSet<Class> Classes { get; set; }
        public DbSet<MakeUpClass> MakeUpClasses { get; set; }

        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        public DbSet<Grade> Grades { get; set; }
        public DbSet<Feedback> FeedBacks { get; set; }
        public DbSet<TuitionFee> TuitionFees { get; set; }
        public DbSet<Leave> LeaveApplication { get; set; }
        public DbSet<CustomerOrder> CustomerOrders { get; set; }
        public DbSet<Attendance> Attendances { get; set; }
        public DbSet<EmployeeRating> EmployeeRatings { get; set; }
        public DbSet<Files> File { get; set; }

    }
}