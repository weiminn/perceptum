﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Programmes.aspx.cs" Inherits="PerceptumIIP.Programmes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <%--Header Photo--%>
    <div class="Header-Photo">
         <div class="Header-Inner" role:"listbox">
             <img style="width:100%"; src="Content/images/programmes-header.jpg" />
         </div>
    </div>

    <%--Fees--%>
        <div class="col-md-12 text-center">
              <h1>Course Fees For 2018</h1>
        </div>
    <table class="table table-theme" border="0" cellspacing="0" cellpadding="5">
<thead>
<tr>
<td><b>Subject</b></td>
<td><b>Level</b></td>
<td><b>Monthly Fee (excluding GST)</b></td>
</tr>
</thead>
<tbody>
<tr>
<td rowspan="2">Mathematics</td>
<td>P3 - P6</td>
<td>$70.00</td>
</tr>
<tr>
<td>S1 - S2</td>
<td>$75.00</td>
</tr>
<tr>
<td rowspan="2">Science</td>
<td>P3 - P6</td>
<td>$70.00</td>
</tr>
<tr>
<td>S1 - S2</td>
<td>$75.00</td>
</tr>
<tr>
<td rowspan="1">Additional Mathematics</td>
<td>S3 - S4</td>
<td>$80.00</td>
</tr>
<tr>
<td rowspan="1">Chemistry</td>
<td>S3 - S4</td>
<td>$85.00</td>
</tr>
</tbody>
</table>

    <div class="grid">
        
          <div class="Mathematics">
            <h2>Mathematics</h2>
            <h5>(Upper Primary - Lower Secondary)</h5>
              <p>
                 <img style="width:100%; height:100%"src="Content/images/PriSec%20Maths.jpg" />
              </p>
            <p>
                Perceptum Education Mathematics Programmes are well-regarded by students and parents. Our Primary and Secondary courses are crafted (and continuously refined) in accordance to the latest syllabus and examination format that is prescribed by Singapore Ministry of Education (MOE) and the Singapore Examination & Assessment Board (SEAB). Backed by more than a decade of Maths experience, our students can be assured that our curricula are of a premium quality that is not only examination specific but also intellectually provoking to stimulate an advanced thought process. 
            </p>
            <p>
                <a class="btn btn-default" data-toggle="modal" data-target=".math-modal-lg" href="#">Find out More! &raquo;</a>
            </p>
          </div>
        
          <div class="Science">
            <h2>Science</h2>
            <h5>(Upper Primary - Lower Secondary)</h5>
              <p>
                  <img style="width:100%; height:100%" src="Content/images/Science.jpg" />
              </p>
            <p>
                Our Science enrichment classes are unlike the usual science tuition. Incorporating 21st Century learning processes and the Socratic questioning approach, Perceptum's Science programme incorporates engaging videos and interactive activities that focus on the students’ development of critical reasoning skills and answering techniques. Students will also be given specially designed games and quizzes to challenge their inferential skills and answering techniques. Enrichment exercises will be given to reinforce the learning points from each lesson
            </p>
            <p>
                <a class="btn btn-default" data-toggle="modal" data-target=".science-modal-lg" href="#">Find out More! &raquo;</a>
            </p>
          </div>

          <div class="AMaths">
            <h2>A Maths</h2>
            <h5>(Upper Secondary)</h5>
              <p>
                  <img style="width:100%; height:100%" src="Content/images/A%20Maths.jpg" />
              </p>
            <p>
                Our Additional Maths Specialists are graduates from reputable institutes of higher learning such as National University of Singapore, Nanyang Technological University, Imperial College London, University of Cambridge, and the University of Queensland. Many of whom are also alumni of top Singapore schools such as Hwa Chong Institution, RI/RJC, CHIJ St Nicholas Girls' School, Dunman High School, Victoria JC, and ACS. Your child can be assured of the quality of instruction and will benefit from the insider examination and study tips that our Maths Specialists provide.
            </p>
            <p>
                <a class="btn btn-default" href="#" data-toggle="modal" data-target=".amath-modal-lg">Find out More! &raquo;</a>
            </p>
          </div>

            <div class="Chemistry">
            <h2>Chemistry</h2>
            <h5>(Upper Secondary)</h5>
                <p>
                    <img style="width:100%; height:100%"src="Content/images/Chemistry.jpg" />
                </p>
            <p>
                At Perceptum Education, we breakdown challenging Chemistry concepts into simple, easy-to-understand, bite-sized pieces that increases our student’s ability to understand and retain content! This provides the foundation from which they can build their repertoire of skills and gives them the confidence to approach an exam hall. The result is a child who has a competitive edge over their peers in Chemistry. We ensure that students walk out of every single lesson with their questions and doubts cleared, and equipped with new skills to answer the more challenging application questions.
            </p>
            <p>
                <a class="btn btn-default" href="#" data-toggle="modal" data-target=".chemistry-modal-lg">Find out More! &raquo;</a>
            </p>
            </div>

    </div> <%--End of 1st Row--%>

    <%--PopOutButton1--%>
    <div class="modal fade math-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Mathematics</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Perceptum Education Mathematics Programmes are well-regarded by students and parents. Our Primary and Secondary courses are crafted (and continuously refined) in accordance to the latest syllabus and examination format that is prescribed by Singapore Ministry of Education (MOE) and the Singapore Examination & Assessment Board (SEAB).</p>
                <table class="table table-theme" border="0" cellspacing="0" cellpadding="5">
<thead>
<tr>
<td><b>Location</b></td>
<td><b>Level</b></td>
<td><b>Days</b></td>
<td><b>Time</b></td>
</tr>
</thead>
<tbody>
<%--SimeDarbyCentre --%>
<tr>
<td rowspan="23">Sime Darby Centre</td>
<td rowspan="2">Primary 3</td>
<td>Wednesday</td>
<td>04.00pm - 06.00pm</td>
</tr>
<tr>
<td>Saturday</td>
<td>11.15am - 01.15pm</td>
</tr>
<tr>


<td rowspan="5">Primary 4</td>
<td>Wednesday</td>
<td>04.00pm - 06.00pm</td>
</tr>
<tr>
<td>Friday</td>
<td>04.00pm - 06.00pm</td>
</tr>
<tr>
<td rowspan="2">Saturday</td>
<td>09.00am - 11.00am</td>
</tr>
<tr>
<td>02.00pm - 04.00pm</td>
</tr>
<tr>
<td>Sunday</td>
<td>04.15pm - 06.15pm</td>
</tr>
<tr>


<td rowspan="5">Primary 5</td>
<td>Wednesday</td>
<td>06.15pm - 08.15pm</td>
</tr>
<tr>
<td>Friday</td>
<td>06.15pm - 08.15pm</td>
</tr>
<tr>
<td rowspan="2">Saturday</td>
<td>11.15am - 01.15pm</td>
</tr>
<tr>
<td>04.15pm - 06.15pm</td>
</tr>
<tr>
<td>Sunday</td>
<td>11.15pm - 01.15pm</td>
</tr>

<tr>
<td rowspan="9">Primary 6</td>
<td>Wednesday</td>
<td>06.15pm - 08.15pm</td>
</tr>
<tr>
<td rowspan="2">Wednesday</td>
<td>04.00pm - 06.00pm</td>
</tr>
<tr>
<td>06.15pm - 08.15pm</td>
</tr>
<tr>
<td rowspan="2">Friday</td>
<td>04.00pm - 06.00pm</td>
</tr>
<tr>
<td>06.15pm - 08.15pm</td>
</tr>
<tr>
<td rowspan="3">Saturday</td>
<td>09.00am - 11.00am</td>
</tr>
<tr>
<td>11.15am - 1.15pm</td>
</tr>
<tr>
<td>02.00pm - 04.00pm</td>
</tr>
<tr>
<td>Sunday</td>
<td>02.00pm - 04.00pm</td>
</tr>

<tr>
<td rowspan="2">Secondary 1</td>
<td rowspan="2">Saturday</td>
<td>02.00pm - 04.00pm</td>
</tr>
<tr>
<td>04.15pm - 06.15pm</td>
</tr>

<%--Bishan--%>
<tr>
<td rowspan="25">261 Bishan St.22</td>
<td>Primary 3</td>
<td>Saturday</td>
<td>09.00am - 11.00am</td>
</tr>

<tr>
<td rowspan="2">Primary 4</td>
<td>Friday</td>
<td>04.00pm - 06.00pm</td>
</tr>
<tr>
<td>Saturday</td>
<td>09.00am - 11.00am</td>
</tr>

<tr>
<td rowspan="2">Primary 5</td>
<td rowspan="2">Friday</td>
<td>04.00pm - 06.00pm</td>
</tr>
<tr>
<td>06.15pm - 08.15pm</td>
</tr>

</tbody>
</table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
      </div>
    </div>

    <%--PopOutButton2--%>
    <div class="modal fade science-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Science</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Our Science enrichment classes are unlike the usual science tuition. Incorporating 21st Century learning processes and the Socratic questioning approach, Perceptum's Science programme incorporates engaging videos and interactive activities that focus on the students’ development of critical reasoning skills and answering techniques. </p>
                                <table class="table table-theme" border="0" cellspacing="0" cellpadding="5">
<thead>
<tr>
<td><b>Location</b></td>
<td><b>Level</b></td>
<td><b>Days</b></td>
<td><b>Time</b></td>
</tr>
</thead>
<tbody>
<%--SimeDarbyCentre --%>
<tr>
<td rowspan="23">Sime Darby Centre</td>
<td rowspan="2">Primary 3</td>
<td>Wednesday</td>
<td>04.00pm - 06.00pm</td>
</tr>
<tr>
<td>Saturday</td>
<td>11.15am - 01.15pm</td>
</tr>
<tr>


<td rowspan="5">Primary 4</td>
<td>Wednesday</td>
<td>04.00pm - 06.00pm</td>
</tr>
<tr>
<td>Friday</td>
<td>04.00pm - 06.00pm</td>
</tr>
<tr>
<td rowspan="2">Saturday</td>
<td>09.00am - 11.00am</td>
</tr>
<tr>
<td>02.00pm - 04.00pm</td>
</tr>
<tr>
<td>Sunday</td>
<td>04.15pm - 06.15pm</td>
</tr>
<tr>


<td rowspan="5">Primary 5</td>
<td>Wednesday</td>
<td>06.15pm - 08.15pm</td>
</tr>
<tr>
<td>Friday</td>
<td>06.15pm - 08.15pm</td>
</tr>
<tr>
<td rowspan="2">Saturday</td>
<td>11.15am - 01.15pm</td>
</tr>
<tr>
<td>04.15pm - 06.15pm</td>
</tr>
<tr>
<td>Sunday</td>
<td>11.15pm - 01.15pm</td>
</tr>

<tr>
<td rowspan="9">Primary 6</td>
<td>Wednesday</td>
<td>06.15pm - 08.15pm</td>
</tr>
<tr>
<td rowspan="2">Wednesday</td>
<td>04.00pm - 06.00pm</td>
</tr>
<tr>
<td>06.15pm - 08.15pm</td>
</tr>
<tr>
<td rowspan="2">Friday</td>
<td>04.00pm - 06.00pm</td>
</tr>
<tr>
<td>06.15pm - 08.15pm</td>
</tr>
<tr>
<td rowspan="3">Saturday</td>
<td>09.00am - 11.00am</td>
</tr>
<tr>
<td>11.15am - 1.15pm</td>
</tr>
<tr>
<td>02.00pm - 04.00pm</td>
</tr>
<tr>
<td>Sunday</td>
<td>02.00pm - 04.00pm</td>
</tr>

<tr>
<td rowspan="2">Secondary 1</td>
<td rowspan="2">Saturday</td>
<td>02.00pm - 04.00pm</td>
</tr>
<tr>
<td>04.15pm - 06.15pm</td>
</tr>

<%--Bishan--%>
<tr>
<td rowspan="25">261 Bishan St.22</td>
<td>Primary 3</td>
<td>Saturday</td>
<td>09.00am - 11.00am</td>
</tr>

<tr>
<td rowspan="2">Primary 4</td>
<td>Friday</td>
<td>04.00pm - 06.00pm</td>
</tr>
<tr>
<td>Saturday</td>
<td>09.00am - 11.00am</td>
</tr>

<tr>
<td rowspan="2">Primary 5</td>
<td rowspan="2">Friday</td>
<td>04.00pm - 06.00pm</td>
</tr>
<tr>
<td>06.15pm - 08.15pm</td>
</tr>

</tbody>
</table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
      </div>
    </div>

    <%--PopOutButton3--%>
    <div class="modal fade amath-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Additional Mathematics</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Our Additional Maths Specialists are graduates from reputable institutes of higher learning such as National University of Singapore, Nanyang Technological University, Imperial College London, University of Cambridge, and the University of Queensland.</p>
                                <table class="table table-theme" border="0" cellspacing="0" cellpadding="5">
<thead>
<tr>
<td><b>Location</b></td>
<td><b>Level</b></td>
<td><b>Days</b></td>
<td><b>Time</b></td>
</tr>
</thead>
<tbody>
<%--SimeDarbyCentre --%>
<tr>
<td rowspan="2">Sime Darby Centre</td>
<td>Secondary 3</td>
<td>Sunday</td>
<td>11.15am - 01.15pm</td>
</tr>
<tr>
<td>Secondary 4</td>
<td>Sunday</td>
<td>04.15pm - 06.15pm</td>
</tr>
<tr>

<%--Bishan--%>
<tr>
<td rowspan="2">261 Bishan St.22</td>
<td>Secondary 3</td>
<td>Saturday</td>
<td>11.15am - 01.15pm</td>
</tr>
<tr>
<td>Secondary 4</td>
<td>Saturday</td>
<td>09.00am - 11.00am</td>
</tr>
<tr>

<%--Tampiness Mall--%>
<tr>
<td rowspan="15">Tampiness Mall</td>
<td rowspan="3">Secondary 3</td>
<td>Wednesday</td>
<td>04.00pm - 06.00pm</td>
</tr>
<tr>
<td>Thursday</td>
<td>06.15pm - 08.15pm</td>
</tr>
<tr>
<td>Saturday</td>
<td>04.15pm - 06.15pm</td>
</tr>

<tr>
<td rowspan="5">Secondary 4</td>
<td>Wednesday</td>
<td>06.15pm - 08.15pm</td>
</tr>
<tr>
<td>Thursday</td>
<td>04.00pm - 06.00pm</td>
</tr>
<tr>
<td>Friday</td>
<td>06.15pm - 08.15pm</td>
</tr>
<tr>
<td rowspan="2">Saturday</td>
<td>11.15am - 01.15pm</td>
</tr>
<tr>
<td>02.00pm - 04.00pm</td>
</tr>


</tbody>
</table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
      </div>
    </div>

    <%--PopOutButton4--%>
    <div class="modal fade chemistry-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Chemistry</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>At Perceptum Education, we breakdown challenging Chemistry concepts into simple, easy-to-understand, bite-sized pieces that increases our student’s ability to understand and retain content!</p>
                                <table class="table table-theme" border="0" cellspacing="0" cellpadding="5">
<thead>
<tr>
<td><b>Location</b></td>
<td><b>Level</b></td>
<td><b>Days</b></td>
<td><b>Time</b></td>
</tr>
</thead>
<tbody>
<%--SimeDarbyCentre --%>
<tr>
<td rowspan="2">Sime Darby Centre</td>
<td>Secondary 3</td>
<td>Sunday</td>
<td>11.15am - 01.15pm</td>
</tr>
<tr>
<td>Secondary 4</td>
<td>Sunday</td>
<td>04.15pm - 06.15pm</td>
</tr>
<tr>

<%--Bishan--%>
<tr>
<td rowspan="2">261 Bishan St.22</td>
<td>Secondary 3</td>
<td>Saturday</td>
<td>11.15am - 01.15pm</td>
</tr>
<tr>
<td>Secondary 4</td>
<td>Saturday</td>
<td>09.00am - 11.00am</td>
</tr>
<tr>

<%--Tampiness Mall--%>
<tr>
<td rowspan="15">Tampiness Mall</td>
<td rowspan="3">Secondary 3</td>
<td>Wednesday</td>
<td>04.00pm - 06.00pm</td>
</tr>
<tr>
<td>Thursday</td>
<td>06.15pm - 08.15pm</td>
</tr>
<tr>
<td>Saturday</td>
<td>04.15pm - 06.15pm</td>
</tr>

<tr>
<td rowspan="5">Secondary 4</td>
<td>Wednesday</td>
<td>06.15pm - 08.15pm</td>
</tr>
<tr>
<td>Thursday</td>
<td>04.00pm - 06.00pm</td>
</tr>
<tr>
<td>Friday</td>
<td>06.15pm - 08.15pm</td>
</tr>
<tr>
<td rowspan="2">Saturday</td>
<td>11.15am - 01.15pm</td>
</tr>
<tr>
<td>02.00pm - 04.00pm</td>
</tr>
</tbody>
</table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
      </div>
    </div>



<style>
.Header-Inner{
    width:100%;
}
.grid {
    display:grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    justify-items:center;
    grid-column-gap: 5px;
    margin: 0 0 60px 0;
}



.Mathematics{
    grid-column: 1 / 2;
  font-family: Moon, Arial, Helvetica, sans-serif;
}

.Science{
    grid-column: 2 / 3;
  font-family: Moon, Arial, Helvetica, sans-serif;
}

.AMaths{
    grid-column: 3 / 4;
  font-family: Moon, Arial, Helvetica, sans-serif;
}

.Chemistry{
    grid-column: 4 / 5;
  font-family: Moon, Arial, Helvetica, sans-serif;
}




</style>




</asp:Content>
