﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="PerceptumIIP.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
        <meta name="viewport" content="width=device-width" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="HandheldFriendly" content="True">
  <style>
.body{
  font-family: Arial, Helvetica, sans-serif;
  font-size: 16px;
    }
.grid{
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr 1fr;
  /*.grid-template-areas:
  "content1 content1"
  "content2 content3";*/
  justify-items: center;
  grid-column-gap: 0px;
  grid-row-gap: 20px;
  padding: 50px 150px 50px 150px;
  margin: 0;
}
.content1/*photo*/{
    grid-column: 1 / 3;
    text-align: center;
    float: left;
    margin-right: 47px;
}
.content2{
    grid-column: 3 / 8;
    text-align: left;
}
.content3{
    grid-column: 1 / 6;
    text-align: left;
}
.content4/*photo*/{
    grid-column: 6 / 8;
    text-align: center;
    margin-left: 47px;
}
.content5/*photo*/{
    grid-column: 1 / 3;
    text-align: center;
    margin-right: 47px;
}
.content6{
    grid-column: 3 / 8;
    text-align: left;
}
.grid>div {
  /*text-align: center;*/
  /*width: 500px;*/
}
.who{
    height: 300px;
}
.vertical-align-who{
    position: relative;
    top: 50%;
    transform: translateY(-50%);
}
</style>
<div class="container-fluid" style="margin: 0; padding: 0;">
  <div class="row">
    <div class="col-sm-12" style="padding: 0;">
      <div id="my-slider" class="carousel slide" data-ride="carousel">

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">

          <div class="item active">
           <img src="Content/images/slider2.png" />
           <div class="carousel-caption"><h1>About</h1></div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<div class="grid">
  <div class="content1">
    <div>
        <img style="height: 300px" src="Content/images/About-Us-1.jpg" />
    </div>
  </div>
  <div class="content2">
    <div>
        <div class="who">
            <div class="vertical-align-who">
            <p style="font-size: 30px; font-weight: bold; padding-bottom: 30px;">Who We Are.</p>
            <p>More than just being former Department Heads, ex-school teachers, authors, 
               we would rather think of ourselves as passionate and effective teachers who 
               know how to design lessons to help our pupils achieve mastery.
            </p>
            </div>
        </div>
    </div>
  </div>
  <div class="content3">
    <div>
        <div class="who">
            <div class="vertical-align-who">
            <p style="font-size: 30px; font-weight: bold; padding-bottom: 30px;">Our Vision.</p>
            <p>At Perceptum Education, we will provide high quality instructional programmes to 
               help every child to have a strong foundation in Mathematics, to be problem-solver 
               and critical thinker.
            </p>
            </div>
        </div>
    </div>
  </div>
  <div class="content4">
    <div>
        <img style="height: 300px" src="Content/images/About-Us-2.jpg" />
    </div>
  </div>
  <div class="content5">
    <div>
        <img style="height: 300px" src="Content/images/Detailed-Lesson-Planning-and-Delivery-1.jpg" />
    </div>
  </div>
  <div class="content6">
    <div>
        <div class="who">
            <div class="vertical-align-who">
            <p style="font-size: 30px; font-weight: bold; padding-bottom: 10px;">Our Values.</p>
            <p><b>We</b> believe that a quality teacher is a caring teacher.</p>
            <p><b>We</b> believe that a rigorous curriculum will help our pupils to understand Maths & Science concepts, 
               to master the best problem-solving skills techniques, and then, to excel in examinations.</p>
            <p><b>We</b> believe that our creative programmes help pupils to have fun learning and to think differently from others.</p>
            <p><b>We</b> believe that working closely with parents will help our pupils to have a right attitude towards learning.</p>
            </div>
        </div>
    </div>
  </div>
</div>

</asp:Content>
