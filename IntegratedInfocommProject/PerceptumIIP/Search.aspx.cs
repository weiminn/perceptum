﻿using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PerceptumIIP
{
    public partial class Search : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
           // Search.aspx? Search = "+txtsearchpg.Text

            txtsearchpg.Text = (string)Session["Search"];
            string Search = txtsearchpg.Text;
            SqlConnection con = new SqlConnection();
            con.ConnectionString = (@"Data Source=perceptum.database.windows.net;Initial Catalog=Perceptum V1;Connect Timeout=15;Encrypt=true;MultipleActiveResultSets=True;Persist Security Info=True;Trusted_Connection=False;User ID=Perceptum2018;Password=Pm123456;");
            con.Open();
            SqlCommand cmd;
            SqlDataReader rdr;

            string sqlselect = "SELECT * FROM Products";
            cmd = new SqlCommand(sqlselect, con);
            rdr = cmd.ExecuteReader();


            string sqldisplay = "SELECT * FROM Products WHERE Name LIKE '%' + @Name + '%'";
            cmd = new SqlCommand(sqldisplay, con);
            cmd.Parameters.AddWithValue("@Name", Search);



            rdr.Close();
            cmd.ExecuteNonQuery();

            SqlDataReader rdrrow;
            rdrrow = cmd.ExecuteReader();
            Boolean booRows = rdrrow.HasRows;
            if (booRows)
            {
                lblnodata.Text = "Search Results";
            }
            else
            {
                lblnodata.Text = "No Results Displayed";
            }

            rdrrow.Close();
            con.Close();
        }

    }
}