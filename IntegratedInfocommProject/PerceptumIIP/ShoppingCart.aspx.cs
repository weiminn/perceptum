﻿using Microsoft.AspNet.Identity;
using PerceptumIIP.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace PerceptumIIP
{
    public partial class ShoppingCart : System.Web.UI.Page
    {

        static readonly string scriptErrorPayment =
"<script language=\"javascript\">\n" +
"alert (\"Please Select a payment method\");\n" +
"</script>";

        static readonly string scriptErrorDelivery =
"<script language=\"javascript\">\n" +
"alert (\"Please Select a delivery method\");\n" +
"</script>";


        static readonly string scriptErrorLogin = "<script language=\"javascript\">\n" +
"alert (\"Please login or create account first to facilitate buying\");\n</script>";


        string userId;


       // private Customer currentCustomer = new Customer();

        protected void Page_Load(object sender, EventArgs e)
        { 

            string userId = User.Identity.GetUserId();
            
            if(userId == null)
            {

                Type csType = this.GetType();

                ClientScript.RegisterStartupScript(csType, "LogIn", scriptErrorLogin);
                btnCheckOut.Visible = false;
                amtwoGST.Visible = false;
                Gst.Visible = false;
                Totalamt.Visible = false;
                details.Visible = false;
            }

            else
            {
                // check to see if items have been ordered
                SqlConnection con = new SqlConnection();
                con.ConnectionString = (@"Data Source=tcp:perceptum.database.windows.net;Initial Catalog=Perceptum V1;Connect Timeout=15;Encrypt=true;MultipleActiveResultSets=True;Trusted_Connection=False;User ID=Perceptum2018;Password=Pm123456;");

         SqlCommand cmd;
                SqlDataReader rdr;
                int intCustOrderNo = (int)Session["CustomerOrderId"];
                String strSql = "SELECT * FROM Orders WHERE CustomerOrderId = " + intCustOrderNo;
                cmd = new SqlCommand(strSql, con);
                con.Open();
                rdr = cmd.ExecuteReader();
                Boolean booRows = rdr.HasRows;
                if (booRows)
                {
                    lblShoppingCart.Text = "Your Shopping Cart";

                    btnCheckOut.Visible = true;
                }
                else
                {
                    lblShoppingCart.Text = "Your Shopping Cart is empty";

                    btnCheckOut.Visible = false;
                }
                con.Close();

            }


      

        }

        protected void View_Click(object sender, EventArgs e)
        {
            decimal AmtwoGST, TotalAmt, gst, FinalAmt;
            AmtwoGST = Convert.ToInt32(Session["AmtwoGST"]);
            decimal GSTAmt = 0.07M;
            gst = AmtwoGST * GSTAmt;
            TotalAmt = gst + AmtwoGST;

            

            amtwoGST.Text = AmtwoGST.ToString();
            Gst.Text = gst.ToString();

            Type csType = this.GetType();

           
            if (del.SelectedValue == null)
            {
                ClientScript.RegisterStartupScript(csType, "Error", scriptErrorDelivery);
                return;

            }
            //Delivery Radio Buttons

            string delivery = string.Empty;

            if (del.SelectedValue == "Ninja")
            {
                delivery = "Centre";
                
                Paypal.Visible = false;
                decimal centre = 0M;
                addcharge.Text = centre.ToString();
                FinalAmt = centre + TotalAmt;
                Session["FinalAmt"] = FinalAmt;

                Totalamt.Text = FinalAmt.ToString();

            }

            else if (del.SelectedValue == "Standard")
            {


                delivery = "Standard Courier $1.50";
                Cash.Visible = false;
                decimal standard = 1.50M;
                addcharge.Text = standard.ToString();
                FinalAmt = standard + TotalAmt;
                Session["FinalAmt"] = FinalAmt;

                Totalamt.Text = FinalAmt.ToString();

            }
            else if (del.SelectedValue == "Express")
            {
                
                delivery = "Express Courier $2.50";
                Cash.Visible = false;
                decimal express = 2.50M;
                addcharge.Text = express.ToString();
                FinalAmt = express + TotalAmt;
                Session["FinalAmt"] = FinalAmt;

                Totalamt.Text = FinalAmt.ToString();

            }

            string Del = delivery.ToString();
            Session["Del"] = Del;
        }



        protected void btnCheckOut_Click(object sender, EventArgs e)
        {
            string payment = string.Empty;
            string strDel = Convert.ToString(Session["Del"]);

            if (Paypal.Checked)
            {
                payment = "Paypal";
            }
            else if (Cash.Checked)
            {
                payment = "Cash";
            }
            Type csType = this.GetType();

            if (Paypal.Checked == false && Cash.Checked == false)
            {
                ClientScript.RegisterStartupScript(csType, "Error", scriptErrorPayment);
                return;
            }
            //Extracting Values from Grid & Details View

            int intOrderNo = (int)Session["CustomerorderId"];
            string strPay = payment.ToString();
            
            SqlConnection con = new SqlConnection();
            con.ConnectionString = (@"Data Source=tcp:perceptum.database.windows.net;Initial Catalog=Perceptum V1;Connect Timeout=15;Encrypt=true;MultipleActiveResultSets=True;Trusted_Connection=False;User ID=Perceptum2018;Password=Pm123456;");

            using (var db = new ApplicationDbContext())
            {
                CustomerOrder customerOrder = db.CustomerOrders.SingleOrDefault(x => x.CustomerOrderId == intOrderNo);
                customerOrder.Status = "Ordered";
                db.SaveChanges();

                Transaction transaction = new Transaction
                {
                    PaymentMode = strPay,
                    DeliveryMode = strDel,
                    Amount = Convert.ToDecimal(amtwoGST.Text),
                    GST = Convert.ToDecimal(Gst.Text),
                    TotalAmt = Convert.ToDecimal(Totalamt.Text),
                    CustomerOrderId = intOrderNo,
                    DateTime = DateTime.Now
                };
                db.Transactions.Add(transaction);
                db.SaveChanges();

                String strUserId = (string)Session["UserId"];
                DataRoutines DRObject = new DataRoutines();
                DRObject.CreateRecs(strUserId);

             
            }

            if (Paypal.Checked == true)
            {

                //declare an int to get ordernumber 
                int PaypalOrderNumber = (int)Session["CustomerorderId"];

                //declare a string to contain the total amount from PaymentDetails 

                decimal PaypalTotalAmount = Convert.ToDecimal(Totalamt.Text);

                string returnURL = "";
                returnURL += "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_xclick&business=perceptum-store@gmail.com";

                returnURL += "&item_name=" + (string)Session["FirstName"] + "'s Purchase from Perceptum (Trans ID: "
                    //order number
                    + PaypalOrderNumber.ToString() + ")";

                returnURL += "&amount=" + PaypalTotalAmount;

                returnURL += "&currency=SGD";

                returnURL += "&return=http://perceptumeducation.azurewebsites.net/ordersdetailtracking.aspx";

                returnURL += "&cancel_return=http://perceptumeducation.azurewebsites.net/Products.aspx";

                Response.Redirect(returnURL);

            }

            Response.Redirect("ordersdetailtracking.aspx");
        }

       
    }
}