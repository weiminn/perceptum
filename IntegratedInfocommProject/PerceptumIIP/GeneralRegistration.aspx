﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GeneralRegistration.aspx.cs" Inherits="PerceptumIIP.GeneralRegistration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

 <%--   <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" /> 
    </p>--%>
    <br />
    <br />
    <br />
    <div class="form-horizontal">
        <hr />
<%--        <asp:ValidationSummary runat="server" CssClass="text-danger" />--%>
        

        <h1>Customer Registration</h1>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="UserName" CssClass="col-md-2 control-label">UserName</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="UserName" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="UserName"
                     ErrorMessage="The name field is required." />
               <asp:RegularExpressionValidator runat="server" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="UserName" ForeColor="Red" ErrorMessage="Invalid email address.">

               </asp:RegularExpressionValidator>


            </div>
              </div>


         <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="FirstName" CssClass="col-md-2 control-label">First Name</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="FirstName" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="FirstName"
                  ErrorMessage="The first name field is required." />
     <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="^[a-zA-Z]+$" ControlToValidate="FirstName" ErrorMessage="Please enter text only"></asp:RegularExpressionValidator>

            </div>
              </div>

         <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="LastName" CssClass="col-md-2 control-label">Last Name</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="LastName" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="LastName"
                    ErrorMessage="The last name field is required." />
          <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationExpression="^[a-zA-Z]+$" ControlToValidate="LastName" ErrorMessage="Please enter text only"></asp:RegularExpressionValidator>

            </div>
              </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Email</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Email" CssClass="form-control" TextMode="Email" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                     ErrorMessage="The email field is required." />
                               <asp:RegularExpressionValidator runat="server" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="Email" ForeColor="Red" ErrorMessage="Invalid email address.">

               </asp:RegularExpressionValidator>
            </div>
        </div>

         <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Telephone" CssClass="col-md-2 control-label">Telephone</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Telephone" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Telephone"
                    ErrorMessage="The telephone field is required." />
            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="\d+" ControlToValidate="Telephone" ErrorMessage="Please enter numbers only"></asp:RegularExpressionValidator>

            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-2 control-label">Password</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Password"
                   ErrorMessage="The password field is required." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="ConfirmPassword" CssClass="col-md-2 control-label">Confirm password</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="ConfirmPassword" TextMode="Password" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmPassword"
                     Display="Dynamic" ErrorMessage="The confirm password field is required." />
                <asp:CompareValidator runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword"
                    Display="Dynamic" ErrorMessage="The password and confirmation password do not match." />
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-1">
                <asp:CheckBox ID="agreeCheck" runat="server" Checked="false" AutoPostBack="true" OnCheckedChanged="agreeCheck_CheckedChanged" />
            </div>
            <div class="col-md-5">
                By selecting this check box, you have aggreed to the our following terms and conditions and privacy policies.
            </div>
        </div>

        <div class="form-group">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal1">
                View Terms and Conditions
            </button>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2">
                View Privacy Policies
            </button>

            <!-- Modal -->
            <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Terms and Conditions</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                    1. Introduction
These Website Standard Terms and Conditions (“Terms”) written on this website shall manage your usage of SmileTutor’s website. The Website is SmileTutor Pte Ltd’s ownership (hereinafter referred to “SmileTutor”, “we”). By using this Website, you agreed to accept all terms and conditions written in here. You should not use this Website if you disagree with any of these Website Standard Terms and Conditions.

Please read these Terms and Conditions attentively. Your access to and usage of the tuition service is conditioned on your acceptance of and compliance with these Terms and Conditions. These Terms are applied to all users, visitors and others who access or use this Website. You agree that using the Site as the visitor, viewer or registered member, you must be at least eighteen (18) years of age or older. If you are between thirteen (13) and eighteen (18), you have parental permission to enter into an agreement to accept these Terms and to use the tuition services.

2. Description of website services offered
Through our Services, we enable students or their parents to connect with tutors for arrangements including but not limited to, private tuition at home, tuition in public places, group tuition, Skype tutoring lessons, or homework help.

All visitors of our site, whether they are registered or not, shall be considered as “users” of the contained Services provided for this Terms. Once individual registers for these Services, through the process of creating an account or requesting a tutor, the user shall then be considered a “member.” There are two types of members; “Client” – a customer, who hire tutor from SmileTutor; “Tutor” – a person, registered in SmileTutor’s Website and providing tuition services.

The user and the member acknowledges and agrees that Services provided and made available through our website are the sole property of SmileTutor. At its discretion, we may offer additional website Services or renew, change or verify any actual content and Services, and this Agreement shall apply to any and all additional, updated, altered or revised Services unless otherwise specified. SmileTutor does hereby reserve the right to cancel and cease offering any of the Services mentioned above. You acknowledge, accept and agree that SmileTutor shall not be liable for any such renewals, changes, revisions or terminations of any of Our Services and products. Should you not agree to the, updated, overviewed or changed terms, you must stop using the provided Services immediately.

The user and member, as well as the client and the tutor, understands, acknowledges and agrees that Services offered shall be provided “AS IS,” and such SmileTutor shall not assume any responsibility or obligation for the timeliness, missed lessons, etc., any other wrong actions, communication or personalization settings.

3. Registration, member account, security, username, and password.
To register and become a “member” of the Website, you have to be in good standing and not a person who has been previously barred from receiving our services.

When you register, SmileTutor may collect information such as your name, birth date, gender, race, occupation and personal interests. It is due to client’s preferences. There is no violation of Article 12 of the Constitution of the Republic of Singapore.

Users who wish to register with SmileTutor are required to provide accurate, valid, complete and current information about themselves as prompted by registration forms provided. You agree to update your information should there be any changes, in order to keep registered information actual, complete, true and accurate. If you provide information that is not accurate or updated, we may terminate your access to the Website and our Services. We are not responsible for any failure in providing the Services, which result from information that is not true, accurate, current and complete.

SmileTutor reserves the right to use your registration information for it’s intended purpose, for instance posting a tutor’s profile in public for attracting clients or sending it to prospective clients directly. Or, for instance, providing the contact number of a client to a tutor who has arrived at their doorstep. Nevertheless, your personal information would not be public. Such information as your name, password, telephone number, date of birth, etc. should not be public, as it is private one. For full information, look at our privacy policy.

When you create an account, you are the sole authorized user of your account. You shall be liable for the preservation of the secrecy of your password/login and for all activities that transpire on/within your account. You are liable for any actions or omission of any user(s) that access your member’s profile information that, if undertaken by you, would be deemed a breach of these Terms and Conditions. It shall be a responsibility to notify SmileTutor immediately any unauthorized access or use of your password and account or any other breach of security. If you ever find out or suspect that someone accesses your account without authorization, you are advised to inform us immediately. SmileTutor shall not be held liable for any loss and damage arising from any failure to comply with this term of use.

4. Rates, Commission, and Payment
All currency references are in Singapore dollars (SGD). You shall be responsible for payment of all levies, duties or taxes that are imposed by taxing rules for any payment or fees you may collect through using our services.

Tuition rates listed on our website are merely guidelines. The exact price for tuition services may differ depending on client’s requirements, market conditions, availability of tutors, etc. The tutors can establish rates by themselves. But the final decision concerning price shall be accepted by confirmed agreement of both parties via an official confirmation invoice sent by SmileTutor.

Clients shall pay the invoice and inform SmileTutor within three days of the due date. It is the client’s responsibility to ensure that the bank receipt is kept as proof of transaction made through cash deposit transfer.

If the Client or Tutor has any outstanding amount owed to SmileTutor, SmileTutor will try its best to ask for payment via phone calls and sms messages. However, after 14 (fourteen) days, if the payment is not made, payment reminder letters shall be sent by mail. The account may also be referred to our lawyers for further recovery actions if settlement is still delayed despite payment letters sent. The debtor shall be liable to pay all costs (including administrative, solicitor and client costs incurred by SmileTutor in demanding and enforcing payment of money due on your above account(s).

Tuition rates agreed upon should not be changed unless clients and tutors mutually agree. SmileTutor reserves the right to claim its commission based on the original agreed upon tuition rate.

In most cases, we will collect the conducted tuition fees directly from the client (sometimes, we also collect payment from tutor) and will then subsequently resolve any payment issues owing to the tutor. Thus, for cancelled tuition arrangements, clients are required to deal directly with the agency. If clients decide to pay the tutor directly and bypass the agency, clients shall be made liable to SmileTutor for the commission amount if we fail to collect our commission from the tutor, in addition to any administrative fees incurred by the agency in collecting the fees.

In general, tutors shall not collect the agency commission from the client directly. If they do so, they shall inform the agency immediately. At notice by the agency, they shall make payment to the agency in full within 3 days. Tutors, who fail to follow these instructions, shall be liable to pay the full amount of payment and additionally, administrative fees, incurred by the agency in collecting the payment from the tutor.

SmileTutor reserves the right to change commission due to non-fulfillment of tutor’s obligations or other reasons. Clients are liable to pay tuition fees for all attended lessons, regardless of how satisfactory the lessons were. For more information – read the website.

Refunds may be made to the client’s bank account in case of violations of terms because of the tutor’s fault, or other reasons. Client can be additionally charged for improper or unfulfilled payment obligation. SmileTutor warns about this with messages.

If there are disputes or concerns regarding tuition fees between tutors and clients, we will provide assistance, to our reasonable means, to help tutors to claim fees from clients and vice versa. However, SmileTutor shall not be legally responsible for any payment disputes between tutors and clients. Clients shall inform the agency and the tutor (if the client has the tutor’s contact) if a postponement or cancellation of any lesson is needed. If the tutor is only informed less than two hours away to the start time of the lesson, clients shall be liable to pay for the loss time and transport cost to the tutor. The amount payable will be 50% of 1 lesson. The client shall pay this amount in cash immediately on the tutor’s next visit or via bank transfer to the tutor if there is no more next visit. Cancellations or postponement of lessons shall not affect our commission amount.

5. Essential provisions relating to the relationship between the client and the tutor
•The client may request a replacement tutor or cancel at any time after the first
lesson, but will still have to pay the attended tuition fees to SmileTutor.
•The tutoring period is usually stated clearly as we arrange the tuition assignment.
In the event that the tutoring period is not specified, all parties should be prepared to commit for at least 3 months.
•There is an unspoken agreement to complete minimum 1 lesson upon tutor confirmation. If clients decide to cancel after 1 or more lessons for any reason, only conducted lessons shall need to be paid for and SmileTutor will refund any the remaining fees paid in advance. However, for cancellations before first lesson has even commenced, clients will be liable for administrative fees no less than the equivalent of the first lesson fees which must be paid by the due date specified in the invoice.
•Clients shall inform the agency and the tutor (if the client has the tutor’s contact) if a postponement or cancellation of any lesson is needed. If the tutor is only informed less than two hours away to the start time of the lesson, clients shall be liable to pay for the loss time and transport cost to the tutor. The amount payable will be 50% of 1 lesson. The client shall pay this amount in cash immediately on the tutor’s next visit or via bank transfer to the tutor if there is no more next visit. Cancellations or postponement of lessons shall not affect our commission amount.
•The tuition schedule and the date and time of the first lesson shall be fixed and hold to.
Clients are allowed to renegotiate the schedule with tutors. Any changes in the frequency of lessons in the first 4 weeks of lessons must be made known to SmileTutor. SmileTutor may adjust commission under such conditions.

6. Termination
SmileTutor expects all tutors and members to conduct themselves appropriately in a professional and ethical manner.

General rules include, but are not limited to:
-Do not be late and always deliver a full tuition session.
-At least two days’ notice if a change of timing is needed, unless due to sudden sickness. MC must be provided.
-No physical and verbal abuse will be tolerated. (e.g discouragement, scolding etc.)
-Regular communication with parents to keep them updated of progress

As a member, you agree that SmileTutor may, without any prior written notice, immediately suspend, terminate, discontinue and/or limit your account, any email associated with your account, and access to any of our Services. The cause for such termination, discontinuance, suspension and/or limitation of access shall include, but is not limited to:
1. any breach or violation of our Terms and Conditions or any other incorporated agreement, regulation and/or guideline;
2. by way of request from law enforcement or any other governmental agencies;
3. unexpected technical or security issues and/or problems;
4. any extended periods of inactivity;
5. any engagement by you in any fraudulent or illegal activities
6. the nonpayment of any associated fees that may be owed by you
7. unethical conduct
8. poor performance

In addition, you agree that any and all terminations, suspensions, discontinuances, and/or limitations of access for cause is made at our sole discretion and that we are not responsible to you or any other third parties with regards to the termination of your profile, associated email address and/or access to any of our Website. Furthermore, you agree that SmileTutor may cease operations at any time.

The termination of your account may contain any and/or all of the following:
a) the removal of any access to all or part of the Services offered within SmileTutor;
b) the password deletion and any and all related information, files, and any such content that can be associated with or inside your account, or any part of thereof;
c) the barring of any further use of all or part of our Website.

As a member, you may also terminate or cancel your profile, associated email address and/or access to our Website by submitting a cancellation or termination request to us via email 7 days prior to termination date.

7. Limitation of liability and indemnification
SmileTutor does its best to ensure that all information on the Website is accurate. If you find any inaccurate information on the Website, please let us know by sending an email and we will correct it, where we agree, as soon as practicable.

As we mostly rely on information submitted by users online, we cannot guarantee the qualifications and other information about our tutors. SmileTutor does its best to provide accurate tuition services. However, no one is secured from human error, for example, in case of error in tuition rates/address via SMS confirmation or invoice, etc.

SmileTutor gives no warranty or assurance about the content of the Website. As the Website is under constant development, its contents may be incorrect or out-of-date and are subject to change without notice. While we make every effort to ensure that the content of the Website is accurate, we cannot accept liability for the accuracy of all the content at any given point in time.

SmileTutor makes every effort to ensure that its computer infrastructure is a virus- and error-free but does not warrant that any content that is available for downloading from the Website does not contain viruses, infection or other code that has destructive or contaminating properties. You are responsible for carrying out sufficient procedures and virus checks (including anti-virus and other security verifications) to gratify your particular requirements for the accuracy of information output and input.

Neither SmileTutor nor any of its employees, sub-contractors and agents shall be liable to you or any other party for any loss, demand, damages or claim whatsoever (whether such loss, demand, damages or claims were known, foreseeable, or otherwise) arising in connection with or out of the usage of the Website or information, materials or content included on the Website.

SmileTutor will try to provide clients and tutors with the best match possible, but due to varying factors, we cannot guarantee good results. While SmileTutor will try to resolve conflicts between tutors and clients, we hold no legal liability for problems and disputes that arise. Neither SmileTutor nor any of its employees, sub-contractors and agents shall be responsible for any quarrels, clashes, crime, which may happen between the clients and the tutors when they realize the tuition services.

In no event shall SmileTutor or any of its employees, sub-contractors or agents be liable for any indirect or consequential damage or loss including, without limitation, any;
•loss of actual or anticipated profits (including loss of profits on contracts);
•loss of revenue;
•loss of business;
•loss of opportunity;
•loss of anticipated savings;
•loss of goodwill;
•loss of use of money or otherwise
•loss of reputation;
•loss or damage to or corruption of data,
and whether or not advised of the possibility of such demand, claim, loss, damages or loss and arising in tort (including negligence), contract or otherwise, to the fullest extent that is allowed by law.

The Website provides hypertext links to other sites operated by other services. The usage of such a links means you are leaving the Website. SmileTutor takes no responsibility for and gives no warranties, endorsements, guarantees or representations in respect of, linked sites. We are not liable for the privacy practices, nor do we accept any responsibility in connection with the information of, such websites, including those of our group entities, which will in some cases have their privacy policies tailored to the particular business practices and educational sectors in which they operate.

We are not a publisher of content supplied by third parties and users of the internet. Any offers, services, statements, advice, opinions, or other content that is available by third parties, including information of providers, or users, are those of the authors or distributors and not of us. We do not necessarily endorse nor are we liable for the accuracy or reliability of any content made on the Website.

The information on the Website is not directed to address your particular demands. Such information does not constitute any form of recommendation or advice by our service and is not intended to be relied upon by you in creation (or refraining from creation) any specific educational, or other, decisions. You should take your advice and make specific inquiries and independently verify any information before relying upon it.

At the request of clients or tutors, we may need to specify gender or race preferences in the jobs or marketing messages we send out via various communication channels This is purely due to clients’ or tutors’ specified preferences and not the agency’s initiative or intention. We are fully supportive and cooperative with TADM guidelines. We do not encourage or condone any form of bigotry or racism – clients that request for specific gender or race are expected to provide valid reasons (for instance, if they are not able to be home to supervise their daughter thus preferring a female teacher for safety reasons)

You agree to defend, hold and indemnify harmless SmileTutor, its directors, agents, officers, employees, and third parties, for any costs, losses, liabilities and expenses (including reasonable fees) relating to or arising out of users use of or inability to use the Site or services, any user postings made by you, your violation of any terms of this contract or your breach of any third parties rights, or your violation of any applicable laws, rules or regulations. SmileTutor reserves the right of assuming the exclusive defense and control of any matter otherwise subject to indemnification by a user, in which event the user will fully cooperate with SmileTutor in asserting any available defenses.

8. Intellectual Property Rights
The Website’s owner and its licensors own all the intellectual property rights and materials involved in this service. You are admitted to the site only for using the material displayed on this Website.

As a condition of your use of the Website, you warrant to SmileTutor that you will not use the Website for any reasons that are illegal or prohibited by these Terms. You may not use the Website in any manner, which could disable, overburden, impair, or damage the Website. You may not attempt to obtain any materials or information through any methods that are not available or provided by our service.

All content that is a part of the Site, such as images, logos, graphics, text as well as any software used on the Site, is the property of SmileTutor and is protected by copyright and other applicable laws. You agree to follow all copyright and other proprietary notices or other limitations that are in any content and will not make any changes thereto.

You will not publish, modify, transmit, reverse engineer, participate in the transfer or sale, or in other way use any of the content, in whole or in part, found on the Website. You are not entitled to make any unauthorized use of any protected content, and in particular, you will not delete or change any proprietary rights or attribution notices in any content. You have to use protected content solely for your personal use and do not let any other persons use the described content without the prior written permission of SmileTutor or the copyright owner. You agree that you do not acquire any ownership rights in any protected content. We do not grant you any licenses, implied or expressed, to the intellectual property of SmileTutor or its licensors except as expressly authorized by these Terms.

9. Restrictions and obligations
Some areas of this service are restricted from being access by you and may further restrict access by you to any areas of this service at absolute discretion. Any user ID and password you may have for this Website are confidential, and you must respect confidentiality as well.
You warrant that you will follow, without limitation, all applicable international, national and local laws and regulations concerning your usage of the service and not inhibit the usage and enjoyment of the service by other users or with the operation and management of the Website.

You shall provide accurate, authorized, complete, and true information when providing materials or information on the service, including, without limitation, information required to be provided through SmileTutor Website’s registration form. This includes any certificates, documents, or photos that you upload to our website or send to us or any of our coordinators via various channels. If you submit any inaccurate, false, unauthorized, incomplete or untrue information or you do not meet our criteria as a member, SmileTutor reserves the right to terminate your use and access to the Website temporarily or permanently. You warrant that you will not impersonate any others, whether actual or fictitious, when using the service, or defame or otherwise harm any party through your use of our service.

SmileTutor also reserves the right not to provide services for clients or tutors without reason. Accessing the Website is forbidden from territories where the Service or any Content is illegal; you are responsible for compliance with local laws.

•fraud or fraudulent misrepresentation; or
•the tort of deceit; or
•any other liability which may not be limited or excluded by law.

10. Website Rules and Conduct
The Service is available only for your own personal, non-commercial use. You shall not use the Service for any purpose that is prohibited by these Terms of Use, including:

You shall not (and shall not permit any third party to) either (a) take any action or (b) post, submit, link, upload, download, to or otherwise distribute or facilitate distribution of any content on or through the Service, that:
•infringes any patent, trademark, trade secret, copyright, a right of publicity or another right of any other person or entity;
•is unlawful, abusive, threatening, harassing, defamatory, libelous, deceptive, fraudulent, invasive of another’s privacy, tortious, obscene, offensive, or profane;
•involves commercial activities and sales without SmileTutor’s prior written consent such as contests, sweepstakes, barter, advertising, or pyramid schemes;
•impersonates any person or entity, including any employee or representative of SmileTutor.

You also shall not (directly or indirectly):
•take any action that may impose or imposes (as determined by SmileTutor in its sole discretion) an unreasonable or disproportionately large load on SmileTutor’s (or its third party providers’) infrastructure;
•interfere or attempt to interfere with the proper working of the Service or any activities conducted on the Service;
• bypass any measures SmileTutor may use to restrict or prevent access to the Service (or other accounts, networks or computer systems connected to the Service);
•Reverse engineering the Service
•decompile, reverse, decipher, disassemble engineer or otherwise attempt to derive any software code or underlying algorithms of any part of the Website, except to the limited extent Singapore or other applicable laws specifically prohibit such restriction;
•translate, modify or otherwise create derivative works of any part of the Service; or
•distribute, lease, copy, rent or otherwise transfer any or all of the rights that you receive hereunder.

SmileTutor reserves the right to remove any content from the Website or another feature of the Website at any time, for any reasons or no reason at all.

You shall abide by all copyright notices, information, and restrictions contained in any Content accessed through the Service. You shall not license, rent, publicly display, adapt modify, sell, create, distribute, copy, reproduce, transmit, publicly perform, publish, edit derivative works from, or otherwise exploit any content or third party submissions or other proprietary rights not owned by you:
(i) without the consent of the respective owners or other valid right and
(ii) in any way that violates any third party right.

You can, to the extent the Website expressly authorizes you to do so, copy or download Content, and other items displayed on the service for download, for personal use only, provided that you maintain all copyright and other similar notices contained in such Content.

Deleting your Account affects all data associated with that account and any services you use. You understand that all your notes and information will be deleted.

Please be aware that the service or other features of the Website may contain, or direct you to services with content that some people may find offensive or inappropriate.

11. Use of communication services
The Site may contain chat areas (blog), news groups, communities, forums, calendars, personal web pages, and/or other message or communication facilities created to enable you to communicate with the public at large or with a group (communication services). You adhere to use these communication services only to send, receive and post material and messages and that are proper and related to the particular communication service.

By way of example, and not as a limitation, you agree that when using a communication service, you will not: harass, abuse, stalk, threaten, defame or otherwise violate the legal rights (such as rights of privacy and publicity) of others; post, distribute, upload, publish any inappropriate, defamatory, infringing, obscene, indecent or unlawful material, information, name or topic; upload files that contain software or other material protected by intellectual property laws (or by rights of privacy of publicity) unless you own or control the rights thereto or have received all necessary consents; advertise or offer to sell or buy any goods or services for any business purpose, unless communication service specifically allows such messages.

SmileTutor has no obligation to monitor the communication services. We do not control or endorse the content, messages or information found in any communication service and, therefore, SmileTutor specifically disclaims any liability in the matter of the communication services and any actions resulting from your joining in in any communication service. Managers and hosts have not authorized spokespersons of SmileTutor, and their views do not necessarily reflect those of SmileTutor.

SmileTutor reserves the right to review materials posted to a communication service and to remove any materials in its sole discretion. We have the right to terminate your access to any of the communication services at any time without notice for any reason whatsoever.

12. Arbitration
If the parties are not able to overcome any dispute between them arising out of or concerning service’s Terms, or any provisions hereof, whether in contract, tort, or otherwise at law or in equity for damages or any other relief, then such conflict shall be overcame only by final and binding arbitration or a similar arbitration service selected by the parties, in Singapore. The arbitrator’s award is final, and judgment is to be entered upon it in any court having jurisdiction. If any legal or equitable action, proceeding or arbitration arises out of or concerns these Terms and Conditions, the prevailing party shall is entitled to recover its costs and reasonable fees. User agrees to arbitrate all disputes and claims in regards to these Terms or any conflicts arising as a result of these Terms, whether directly or indirectly, including Tort claims that are a result of these Terms and Conditions.

13. Modification of Terms
SmileTutor reserves the right, at its sole discretion, to change or substitute the Terms and Conditions. It is your responsibility to scan the Terms periodically for modifications. Your continued usage of the Services following the posting of any modifications to the Terms will be subject to the new Terms.

Unless otherwise specified herein, this contract constitutes the entire agreement between you and SmileTutor concerning the Website, and it supersedes all prior or contemporaneous communications and proposals, whether electronic, oral or written, between the user and SmileTutor with respect to the Website. A printed version of this agreement and any notice given in electronic form shall be admissible in judicial or administrative proceedings based upon or be relating to this agreement to the same extent and subject to the same conditions as other documents and records originally generated and maintained in printed form. It is the express wish to the parties that this agreement and all related documents be written in English.
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
                </div>
            </div>

            <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel2">Privacy Policies</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                    Privacy Policy
Tutelage Centre Pte Ltd understands you care how information about you is used and shared, and we appreciate your trust that we will do so carefully and sensibly.

 

Information Sharing

This privacy policy sets out how Tutelage Centre Pte Ltd uses and protects any information that you give us when you use this website. Tutelage Centre Pte Ltd is committed to ensuring that your privacy is protected. Should we ask you to provide certain information by which you can be identified when using this website, then you can be assured that it will only be used in accordance with this privacy statement.

 

Information Security

 We take appropriate security measures to protect against unauthorized access to or unauthorized alteration, disclosure or destruction of data. These include internal reviews of our data collection, storage and security measures, including physical security measures to guard against unauthorized access to systems where we store personal data.

 

We restrict access to personal information to Tutelage Centre Private Limited employees, contractors and agents who need to know that information in order to process it on our behalf. These individuals are bound by confidentiality obligations and may be subject to discipline, including termination and criminal prosecution, if they fail to meet these obligations.

 

We are committed to ensure that your information is secure. In order to prevent unauthorised access or disclosure, we have put in place suitable physical and electronic procedures to safeguard and secure the information we collect online.

 

Changes to this Privacy Policy

Please note that this Privacy Policy may change from time to time. We will not reduce your rights under this Privacy Policy without your explicit consent. We will post any Privacy Policy changes on this page and if the changes are significant, we will provide a more prominent notice such as email notification.
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button ID="Register" runat="server" OnClick="CreateUser_Click" Text="Register" CssClass="btn btn-default" />
            </div>
        </div>
    </div>
</asp:Content>


