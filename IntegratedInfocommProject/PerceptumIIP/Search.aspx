﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="PerceptumIIP.Search" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <br />
    <br />
    <br />
        <asp:TextBox ID="txtsearchpg" runat="server" Visible="True"></asp:TextBox>

    <asp:GridView ID="GridView1" runat="server" DataSourceID="SqlDataSource1" AutoGenerateColumns="False" DataKeyNames="ProductId">
        <Columns>
            <asp:BoundField DataField="ProductId" HeaderText="ProductId" InsertVisible="False" ReadOnly="True" SortExpression="ProductId" />
            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
            <asp:BoundField DataField="Details" HeaderText="Details" SortExpression="Details" />
            <asp:BoundField DataField="Salesitem" HeaderText="Salesitem" SortExpression="Salesitem" />
            <asp:BoundField DataField="ProductCategory" HeaderText="ProductCategory" SortExpression="ProductCategory" />
            <asp:BoundField DataField="NormalPrice" HeaderText="NormalPrice" SortExpression="NormalPrice" />
            <asp:BoundField DataField="PromoPrice" HeaderText="PromoPrice" SortExpression="PromoPrice" />
            <asp:BoundField DataField="QuantityOnHand" HeaderText="QuantityOnHand" SortExpression="QuantityOnHand" />
        </Columns>
        </asp:GridView>
   
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />
        <asp:Label ID="lblnodata" runat="server"></asp:Label>

<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM Products WHERE @Name LIKE '%' +@Name+ '%'">
    <SelectParameters>
        <asp:QueryStringParameter Name="Name" QueryStringField="Search" />

    </SelectParameters>
</asp:SqlDataSource>



</asp:Content>
