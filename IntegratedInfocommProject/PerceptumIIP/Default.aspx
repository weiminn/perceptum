﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="PerceptumIIP._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="content/bootstrapJS/jquery-2.1.1.min.js" type="text/javascript"></script>

   <%-- <script type="text/javascript">
        $(window).on('load',function(){
            $('#exampleModal').modal('show');
        });
</script>
    
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Disclaimer!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
This is a school project. The website was created and hosted for the sole purpose of testing and presenting our website prototype for Perceptum Education. We make no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the website or the information, products, services, or related graphics contained on the website for any purpose. Any reliance you place on such information is therefore strictly at your own risk. In no event will we be liable for any loss or damage including without limitation, indirect or consequential loss or damage, or any loss or damage whatsoever arising from loss of data or profits arising out of, or in connection with, the use of this website.

<p>Ngee Ann Polytechnic - BIT  </p>    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">PROCEED TO WEBSITE</button>
      </div>
    </div>
  </div>
</div>--%>

    <div class="container-fluid">
  <div class="row">
    <div class="col-sm-12" style="padding: 0;">
      <div id="my-slider" class="carousel slide" data-ride="carousel">

          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#my-slider" data-slide-to="0" class="active"></li>
            <li data-target="#my-slider" data-slide-to="1"></li>
            <li data-target="#my-slider" data-slide-to="2"></li>
          </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">

          <div class="item active">
              <img src="Content/images/slider1.png" />
           <div class="carousel-caption"><h1>Investment In Knowledge</h1></div>
          </div>

          <div class="item">
              <img src="Content/images/slider2.png" />
           <div class="carousel-caption"><h1>Nurturing Teachers</h1></div>
          </div>

          <div class="item">
              <img src="Content/images/slider3.png" />
           <div class="carousel-caption"><h1>Exponential Growth</h1></div>
          </div>

        </div>

          <!-- Left and right controls -->
          <a class="left carousel-control" href="#my-slider" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#my-slider" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
      </div>
    </div>
  </div>
</div>

<style>

    @media (min-width: 768px) and (max-width: 992px) {

        .container-fluid {
            width:100%;
            padding-right:15px;
            padding-left:15px;
            margin-right:auto;
            margin-left:auto;
        }

        .grid{
            display:block;
           background:red;
        }

        .links{
            background:red;
            display:block;
            grid-template-columns:0;
            max-width: 300px;


        }
    }
    .container-fluid{
        width:100%;
    }
.carousel-control.left, .carousel-control.right {
   background-image:none !important;
   filter:none !important;
}
.grid{
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  justify-items: center;
  /*background: pink;*/
  grid-column-gap: 5px;
  grid-row-gap: 20px;
  padding: 50px 0 50px 0;
  margin: 0;
}
.links{
  grid-column: 1 / 4;
  text-align: center;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  /*background: blue;*/
  justify-items: center;
  grid-column-gap: 20px;
  grid-row-gap: 5px;
  padding: 5px;
  margin: 0;
}
.link1{
  grid-column: 1 / 2;
  text-align: center;
  font-family: Moon, Arial, Helvetica, sans-serif;
  font-weight: bold;
  /*background: yellow;*/
}
.link2{
  grid-column: 2 / 3;
  text-align: center;
  font-family: Moon, Arial, Helvetica, sans-serif;
  font-weight: bold;
  /*background: yellow;*/
}
.link3{
  grid-column: 3 / 4;
  text-align: center;
  font-family: Moon, Arial, Helvetica, sans-serif;
  font-weight: bold;
  /*background: yellow;*/
}
.why{
  grid-column: 1 / 4;
  display: grid;
  grid-template-columns: 1fr;
  text-align: center;
  justify-items: center;
  /*background: blue;*/
  width: 100%;
  grid-row-gap: 20px;
  padding: 0 100px 0 100px;
  background-color: #eaeaea;
}
.why div{
    /*background: yellow;*/
}
.test{
  grid-column: 1 / 4;
  text-align: center;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  /*background: blue;*/
  justify-items: center;
  grid-column-gap: 20px;
  grid-row-gap: 20px;
  margin: 0;
  padding: 0 100px 0 100px;
}
.title{
  grid-column: 1 / 4;
  text-align: center;
  /*background: yellow;*/
  font-size: 24px;
  font-weight: bold;
  /*padding-top: 20px;*/
}
.test1{
  grid-column: 1 / 2;
  text-align: center;
  /*background: yellow;*/
  background-color: #eaeaea;
  padding: 10px;
}
.test2{
  grid-column: 2 / 3;
  text-align: center;
  /*background: yellow;*/
  background-color: #eaeaea;
  padding: 10px;
}
.test3{
  grid-column: 3 / 4;
  text-align: center;
  /*background: yellow;*/
  background-color: #eaeaea;
  padding: 10px;
}
.test1name{
  grid-column: 1 / 2;
  text-align: left;
  font-weight: bold;
  /*background: yellow;*/
}
.test2name{
  grid-column: 2 / 3;
  text-align: left;
  font-weight: bold;
  /*background: yellow;*/
}
.test3name{
  grid-column: 3 / 4;
  text-align: left;
  font-weight: bold;
  /*background: yellow;*/
}
.learnmorebutton{
  background-color: #ffd966;
  color: #828282;
  border: none;
  border-radius: 20px;
  padding: 9px 0 9px 0;
  margin: 7.75px 9px 27.75px 9px;
  width: 133px;
  display: inline-block;
  border: 1px solid #ffd966;
  font-size: 11px;
  font-family: Moon;
  font-weight: bold;
}
.viewallbutton{
  grid-column: 2 / 3;
  text-align: center;
  background-color: #ffd966;
  color: #828282;
  border: none;
  border-radius: 20px;
  padding: 9px 0 9px 0;
  margin: 7.75px 9px 7.75px 9px;
  min-width: 107px;
  display: inline-block;
  border: 1px solid #ffd966;
  font-size: 11px;
  font-family: Moon;
  font-weight: bold;
}
.learnmorebutton:hover, .viewallbutton:hover{
    color: #ffd966;
    background-color: #fff;
    border: 1px solid #ffd966;
}
.viewall{
    grid-column: 2 / 3;
}
</style>
<div class="grid">

  <div class="links">
      <div class="link1">
        <a runat="server" href="Programmes.aspx">
            <div><p style="margin-bottom: 20px; color: #828282;">OUR PROGRAMMES</p></div>
            <img  style="height: 250px;" src="Content/images/MPC-Experience-Day_Event-Highlight-1.jpg" />
        </a>
      </div>
  
      <div class="link2">
        <a runat="server" href="Testimonial.aspx">
            <div><p style="margin-bottom: 20px; color: #828282;">STUDENT EXPERIENCE
</p></div>
            <img  style="height: 250px;" src="Content/images/Century-Square-OH-banner-head-370x265.jpg" />
        </a>
      </div>
  
      <div class="link3">
        <a runat="server" href="Parentsregister.aspx">
            <div><p style="margin-bottom: 20px; color: #828282;">REGISTER WITH US</p></div>
            <img  style="height: 250px;" src="Content/images/Math-Parent-Workshop_LP-Banner-370x265.jpg" />
        </a>
      </div>
   </div>
  
  <div class="why">
      <div style="font-size: 36px; font-weight: bold; padding-top: 20px;">Why Perceptum Education?</div>
      <div>High quality instructional programmes to help every child build a strong foundation, to be problem-solver and critical thinker.</div>
      <div>Our curriculum is based on a unique teaching and learning model that gives your child the right balance in effective learning and academic performance – by equipping them with the right knowledge, skills and dispositions.</div>
      <div>Since 2005, we have established a reputation among parents, students and educators for consistently helping students achieve excellent results at the PSLE and O Level examinations.</div>
      <a runat="server" href="About.aspx"> <button type="button" class="learnmorebutton">Learn more</button></a>
  </div>
  
  <div class="test">
  
    <div class="title">Testimonials</div>

   <div class="profile1">

          <div class="profilespacer" style="background: #ff0042;"></div>
  
          <div class="profileimg"><img class="roundimg" src="Content/images/10.jpg" /></div>
  
          <div class="profiledetails">
            <div style="font-size: 18px;">Jane Koh</div>
            <div style="font-size: 15px;">Parent</div>
          </div>

          <div class="testtitle">
          Kind-hearted teacher...!
          </div>
  
          <div class="testcontent">
              <img style="width: 30px; margin: -20px 10px 0 0;" src="Content/images/quote-leftp.png" />
              Valerie really enjoys her lessons at Perceptum Education! She always shares with me that her teachers are cheerful and kind-hearted, and shower her with lots of encouragement. In addition to the nurturing environment, I am also extremely satisfied with the curriculum here.
              <<img style="width: 30px; margin: 0 0 -10px 10px;" src="Content/images/right-quotation-markp.png" />
          </div>
        </div>

     <div class="profile7">

      <div class="profilespacer" style="background: #ec3466;"></div>
  
      <div class="profileimg"><img class="roundimg" src="Content/images/16.jpg" /></div>
  
      <div class="profiledetails">
        <div style="font-size: 18px;">Kelley Chalamet</div>
        <div style="font-size: 15px;">Parent</div>
        
      </div>
  
      <div class="testtitle">
      From B to A*...!
      </div>
  
      <div class="testcontent">
          <img style="width: 30px; margin: -20px 10px 0 0;" src="Content/images/quote-leftp.png" />
          I can’t thank you enough for all you’ve done to help Faye in her Math. You have been so patient and encouraging. Under your teaching, her results has improved from B to A*. She has grown and learnt ever since we enrolled her in the centre in 2015. Thank you so much.
          <img style="width: 30px; margin: 0 0 -10px 10px;" src="Content/images/right-quotation-markp.png" />
      </div>
    </div>

       <div class="profile6">

      <div class="profilespacer" style="background: #00aac4;"></div>
  
      <div class="profileimg"><img class="roundimg" src="Content/images/15.jpg" /></div>
  
      <div class="profiledetails">
        <div style="font-size: 18px;">Jenny Doe</div>
        <div style="font-size: 15px;">Parent</div>
      </div>
  
      <div class="testtitle">
      I am so touched...!
      </div>
  
      <div class="testcontent">
          <img style="width: 30px; margin: -20px 10px 0 0;" src="Content/images/quote-leftb.png" />
          Mr Soh is such a good teacher. I texted him some questions from James’ school and he took time to reply till 12 am. Just texted me to ask whether James can understand or not… I am so touched!!! 
          <img style="width: 30px; margin: 0 0 -10px 10px;" src="Content/images/right-quotation-markb.png" />
      </div>
    </div>

      <div class="viewall"><a runat="server" href="~/Testimonial"><button type="button" class="viewallbutton">View All</button></a></div>
    
  </div>
  
</div>

<style>
    .pagegrid{
  display: grid;
  grid-template-columns: 1fr;
  /*background: pink;*/
  margin: 5% 10%;
  padding: 5px;
}
.topgrid{
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: 50px;
  padding-bottom: 50px;
  /*grid-column-gap: 5px;
  background: green;*/
}
.bottomgrid{
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-column-gap: 50px;
  grid-row-gap: 50px;
  /*background: purple;*/
}
.profile1{
  grid-column: 1/2;
  display: grid;
  grid-template-columns: 1fr;
  /*justify-items: center;*/
  text-align: center;
  background: #fff;
  font-family: Arial;
  margin: 0;
  padding: 0;
  border: 5px solid #eaeaea;
}
.profile2{
  grid-column: 2/3;
  display: grid;
  grid-template-columns: 1fr;
  /*justify-items: center;*/
  text-align: center;
  background: #fff;
  font-family: Arial;
  margin: 0;
  padding: 0;
  border: 5px solid #eaeaea;
}
.profile3{
  grid-column: 1/2;
  display: grid;
  grid-template-columns: 1fr;
  /*justify-items: center;*/
  text-align: center;
  background: #fff;
  font-family: Arial;
  margin: 0;
  padding: 0;
  border: 5px solid #eaeaea;
}
.profile4{
  grid-column: 2/3;
  display: grid;
  grid-template-columns: 1fr;
  /*justify-items: center;*/
  text-align: center;
  background: #fff;
  font-family: Arial;
  margin: 0;
  padding: 0;
  border: 5px solid #eaeaea;
}
.profile5{
  grid-column: 3/4;
  display: grid;
  grid-template-columns: 1fr;
  /*justify-items: center;*/
  text-align: center;
  background: #fff;
  font-family: Arial;
  margin: 0;
  padding: 0;
  border: 5px solid #eaeaea;
}
.profile6{
  grid-column: 3/4;
  display: grid;
  grid-template-columns: 1fr;
  /*justify-items: center;*/
  text-align: center;
  background: #fff;
  font-family: Arial;
  margin: 0;
  padding: 0;
  border: 5px solid #eaeaea;
}
.profile7{
  grid-column: 2/3;
  display: grid;
  grid-template-columns: 1fr;
  /*justify-items: center;*/
  text-align: center;
  background: #fff;
  font-family: Arial;
  margin: 0;
  padding: 0;
  border: 5px solid #eaeaea;
}
.profile8{
  grid-column: 3/4;
  display: grid;
  grid-template-columns: 1fr;
  /*justify-items: center;*/
  text-align: center;
  background: #fff;
  font-family: Arial;
  margin: 0;
  padding: 0;
  border: 5px solid #eaeaea;
}
.profilespacer{
  height: 80px;
  background: #ff0042;
}
.profileimg{
  background: #fff;
  height: 50px;
  /*justify-self: start;*/
}
.roundimg{
    width: 100px;
    position: relative;
    bottom: 50px;
    border-radius: 50%;
    border-style: solid;
    border-color: #fff;
    border-width: 5px;
}
.profiledetails{
  display: grid;
  grid-template-columns: 1fr;
  background: #fff;
  padding: 5px 0 5px 0;
}
.testtitle{
  background: #fff;
  font-weight: bold;
  font-size: 17px;
  padding: 5px 0 5px 0;
  border-top-style: solid;
  border-color: #eaeaea;
}
.testcontent{
  background: #fff;
  padding: 5px 15px 15px 15px;
}
</style>
</asp:Content>
