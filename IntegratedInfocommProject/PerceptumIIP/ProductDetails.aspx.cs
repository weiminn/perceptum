﻿using Microsoft.AspNet.Identity;
using PerceptumIIP.Models;
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;


namespace PerceptumIIP
{
    public partial class ProductDetails : System.Web.UI.Page
    {
        private int ProductID;
        private int customerID;

        static readonly string scriptStockOut = "<script language=\"javascript\">\n" +
"alert (\"Sorry Stock Out!  Please choose a smaller quantity or another product \");\n" +
"</script>";

        static readonly string scriptErrorLogin = "<script language=\"javascript\">\n" +
"alert (\"Please login or create account first to facilitate buying\");\n</script>";

        protected void Page_Load(object sender, EventArgs e)
        {

        string userId = User.Identity.GetUserId();


            if (!User.Identity.IsAuthenticated)
            {

                Type csType = this.GetType();
                ClientScript.RegisterStartupScript(csType, "LogIn", scriptErrorLogin);
                qtylabel.Visible = false;
                ProductsGridView.Visible = false;
                GridView1.Visible = false;
                ddlQty.Visible = false;
                AddToCart.Visible = false;

            }
            else
            {
                using (var db = new ApplicationDbContext())
                {
                    customerID = db.Customers.SingleOrDefault(x => x.ApplicationUserId == userId).customerId;
                }
                qtylabel.Visible = true;
                ProductsGridView.Visible = true;
                ddlQty.Visible = true;
                AddToCart.Visible = true;

            }

        }

        protected void AddToCart_Click(object sender, EventArgs e)
        {
            //declare variables
            string strProductID, strSQLSelect, strSQL, strdatetime;
            int intQuantityOnHand, intBuyQuantity, newQty, intCustomerOrderNo;
            decimal decUnitPrice;

            SqlConnection con = new SqlConnection();
            con.ConnectionString = (@"Data Source=tcp:perceptum.database.windows.net;Initial Catalog=Perceptum V1;Connect Timeout=15;Encrypt=true;MultipleActiveResultSets=True;Trusted_Connection=False;User ID=Perceptum2018;Password=Pm123456;");

            con.Open();
            SqlCommand cmd;

            GridViewRow row0 = ProductsGridView.Rows[0];
            strProductID = row0.Cells[0].Text;
            //extract the QuantityOnHand from the database - based on the product ID

            strSQLSelect = "SELECT QuantityOnHand FROM Products WHERE ProductID = @ProductID";
            cmd = new SqlCommand(strSQLSelect, con);
            cmd.Parameters.Add("@ProductID", SqlDbType.Int).Value = strProductID;
            object QuantityOnHand = cmd.ExecuteScalar();
            intQuantityOnHand = (int)QuantityOnHand;

            //extract the Price from the database - based on the product ID

            strSQLSelect = "SELECT PromoPrice FROM Products WHERE ProductID = @ProductID";
            cmd = new SqlCommand(strSQLSelect, con);
            cmd.Parameters.Add("@ProductID", SqlDbType.Int).Value = strProductID;
            object PromoPrice = cmd.ExecuteScalar();
            decUnitPrice = (decimal)PromoPrice;

            //extract quantity purchased from dropdown list
            intBuyQuantity = int.Parse(ddlQty.Items[ddlQty.SelectedIndex].ToString());

            //calculate the new quantity left 
            newQty = intQuantityOnHand - intBuyQuantity;

            if (intQuantityOnHand < intBuyQuantity)
            {
                Type csType = this.GetType();
                con.Close();
                ClientScript.RegisterStartupScript(csType, "StockOut", scriptStockOut);

                // Add the send email function here
                //var sbOutofStockMessage = "The product " + strProductID + " is out of stock!";
                //string sendEmail = ConfigurationManager.AppSettings["SendEmail"];
                //SendEmail(sbOutofStockMessage.ToString());


                return;
            }

            Session["sProductID"] = strProductID;
            Session["sUnitPrice"] = decUnitPrice.ToString();
            Session["sQuantity"] = newQty.ToString();
            

            intCustomerOrderNo = (int)Session["CustomerOrderId"];
            DateTime date = DateTime.Now; // will give the date for today
            strdatetime = date.ToLongDateString();

            int TotalPrice = Convert.ToInt32(decUnitPrice) * intBuyQuantity;

            Session["TotalPrice"] = (int)TotalPrice;

            using (var db = new ApplicationDbContext())
            {
                Order order = new Order
                {
                    ProductId = Convert.ToInt32(strProductID),
                    CustomerOrderId = intCustomerOrderNo,
                    DateTime = DateTime.Now,
                    Quantity = intBuyQuantity,
                    UnitPrice = Convert.ToInt32(decUnitPrice),
                    TotalPrice = TotalPrice,
                };
                db.Orders.Add(order);
                db.SaveChanges();
            }


            strSQL = "SELECT SUM(TotalPrice) FROM Orders WHERE CustomerOrderId = @CustomerOrderId";
            cmd = new SqlCommand(strSQL, con);
            cmd.Parameters.Add("@CustomerOrderId", SqlDbType.Int).Value = intCustomerOrderNo; //Check

           object AmtwoGST = cmd.ExecuteScalar();
           int TotalPricee = Convert.ToInt32(AmtwoGST);

            Session["AmtwoGST"] = TotalPricee;



            strSQL = "UPDATE Products SET QuantityOnHand = @NewQty WHERE ProductId = @ProductID";
            cmd = new SqlCommand(strSQL, con);
            cmd.Parameters.Add("@NewQty", SqlDbType.Int).Value = newQty;
            cmd.Parameters.Add("@ProductID", SqlDbType.VarChar).Value = strProductID;
            cmd.ExecuteNonQuery();

            con.Close();






            Response.Redirect("ShoppingCart.aspx");
        }
    }
}
    
