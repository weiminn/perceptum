﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using PerceptumIIP.Models;

namespace PerceptumIIP.Account
{
    public partial class Register : Page
    {
        protected void CreateUser_Click(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var signInManager = Context.GetOwinContext().Get<ApplicationSignInManager>();
            var user = new ApplicationUser() {
                UserName = Email.Text,
                Email = Email.Text,
                Role = "Customer"
            };
            IdentityResult result = manager.Create(user, Password.Text);
            if (result.Succeeded)
            {
                using (var db = new ApplicationDbContext())
                {
                    Customer newCustomer = new Customer
                    {
                        UserName = UserName.Text,
                        FirstName = FirstName.Text,
                        LastName = LastName.Text,

                        Email = Email.Text,
                        Telephone = Convert.ToInt32(Telephone.Text),
                        Password = Password.Text,
                        ApplicationUserId = user.Id
                    };

                    db.Customers.Add(newCustomer);
                    db.SaveChanges();
                }
            

                Session["CName"] = (string)UserName.Text;
                Session["CFirstName"] = (string)FirstName.Text;
                Session["CLastName"] = (string)LastName.Text;
                Session["CEmail"] = (string)Email.Text;
                Session["CTelephone"] = (string)Telephone.Text;

                Session["CPassword"] = (string)Password.Text;
                Session["CEmail"] = (string)Email.Text;


                //var autheticationmanager = HttpContext.Current.GetOwinContext().Authentication;
                //var userIdentity = manager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
                //autheticationmanager.SignIn(new Microsoft.Owin.Security.AuthenticationProperties() { }, userIdentity);

                signInManager.SignIn(user, isPersistent: false, rememberBrowser: false);

                Session["UserId"] = user.Id;
                String strUserId = (string)Session["UserId"];
                DataRoutines DRObject = new DataRoutines();
                DRObject.CreateRecs(strUserId);

                IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
        }
            else 
            {
                ErrorMessage.Text = result.Errors.FirstOrDefault(); ////object where return data is stored - not successful , obtaining error from first line and displaying as error
            }
            Response.Redirect("Account/Login");

        }

    }
}