﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using PerceptumIIP.Models;

namespace PerceptumIIP.Account
{
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          
        }

        protected void LogIn(object sender, EventArgs e)
        {
            if (IsValid)
            {
                // Validate the user password
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

                // This doen't count login failures towards account lockout
                // To enable password failures to trigger lockout, change to shouldLockout: true
                var result = signinManager.PasswordSignIn(Email.Text, Password.Text, RememberMe.Checked, shouldLockout: false);
                    
                PerceptumIIP.Models.ApplicationUser user = manager.FindByEmail(Email.Text);
                switch (result)
                {
                    case SignInStatus.Success:

                        if (user.Role == "Admin")
                        {
                           
                                IdentityHelper.RedirectToReturnUrl("/Views/Admin/Default.aspx", Response);
                        }
                        else if (user.Role == "Parent")
                        {
                            Session["UserId"] = user.Id;

                            String strUserId = (string)Session["UserId"];
                            DataRoutines DRObject = new DataRoutines();
                            DRObject.CreateRecs(strUserId);



                            IdentityHelper.RedirectToReturnUrl("/Views/Parent/ParentStudents.aspx", Response);
                        }
                        else if (user.Role == "Teacher")
                        {
                            IdentityHelper.RedirectToReturnUrl("/Views/Teacher/TeacherClasses.aspx", Response);
                        }
                        else if (user.Role == "Customer")
                        {
                            Session["UserId"] = user.Id;
                            String strUserId = (string)Session["UserId"];
                            DataRoutines DRObject = new DataRoutines();
                            DRObject.CreateRecs(strUserId);

                            IdentityHelper.RedirectToReturnUrl("Default.aspx", Response);
                        }

                        else
                        {
                            IdentityHelper.RedirectToReturnUrl("/", Response);
                        }

                        //IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                        break;
                    case SignInStatus.LockedOut:
                        Response.Redirect("/Account/Lockout");
                        break;
                    case SignInStatus.RequiresVerification:
                        Response.Redirect(String.Format("/Account/TwoFactorAuthenticationSignIn?ReturnUrl={0}&RememberMe={1}",
                                                        Request.QueryString["ReturnUrl"],
                                                        RememberMe.Checked), 
                                                        true);
                        break;
                    case SignInStatus.Failure:
                    default:
                        FailureText.Text = "Invalid login attempt";
                        ErrorMessage.Visible = true;
                        break;
                }
            }
        }
    }
}